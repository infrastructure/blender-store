# Upgrading the store

Here we try to document all the hacks & fixes we did to get WooCommerce and Crux to play nice.

All patches are stored in the `patches` directory for easy application with `git apply`.
After applying a patch, run the appropriate `git diff` command and update the patch
for the latest version of the patched software.

**NOTE:** after updating any plugin, reload the "installed plugins " page to see if there are
any post-update actions to perform.

The license keys for WooCommerce plugins can be set in Dashboard → WooCommerce Helper.
Connecting to Ton's WooCommerce acccount may also enable updates & out-of-date warnings
in the plugins overview.

## Payment gateways

The payment gateways that were used to create currently active recurring automatic
payments should always stay enabled in the [WooCommerce Checkout
settings](https://store.blender.org/wp-admin/admin.php?page=wc-settings&tab=checkout).

Which payment gateways are shown on the store's checkout page can be controlled
through the [Currency Switcher options](https://store.blender.org/wp-admin/admin.php?page=aelia_cs_options_page).

## Commercial plugins

The following WordPress/WooCommerce plugins/theme are paid for. Usernames and
passwords can be found in the 1Password vault.

NOTE: before buying anything, visit https://themeforest.net/downloads to see
which versions can already be downloaded. For example, Tax Display by Country
and Crux only require payment to extend their support, not to download a new
version.

- Sequential Order Numbers Pro (https://woocommerce.com/my-account/downloads/)
- WooCommerce Subscriptions (https://woocommerce.com/my-account/downloads/)
- Recaptcha for WooCommerce (https://woocommerce.com/my-account/downloads/)
- Aelia Currency Switcher (https://aelia.co/my-account/downloads/)
- Crux theme (https://themeforest.net/item/crux-a-modern-and-lightweight-woocommerce-theme/6503655)
- Aelia Tax Display by Country for WooCommerce (https://codecanyon.net/item/tax-display-by-country-for-woocommerce/8184759)

## Upgrading WooCommerce and/or Crux

- You have to manually check for updates to the following plugins:
    - Aelia Currency Switcher plugin.
    - Aelia Tax Display by Country for WooCommerce (https://themeforest.net/downloads).

- Apply `patches/pdf-invoices-infinite-loop.diff` to prevent an infinite loop when running on a
  new installation.

- Apply `patches/woocommerce-pdf-invoices.diff`.

- Apply `patches/paypal-gateway-ipn-handler.diff` to work around a bug where a function that no
  longer exists is called anyway. If the patch doesn't apply, hopefully the bug has been fixed.

- Crux can be obtained from [ThemeForest](https://themeforest.net/downloads).

- The Crux Assistant plugin is bundled with Crux, and can be installed from
  store.blender.org/wp-admin/themes.php?page=tgmpa-install-plugins

- We have a Crux child theme Blender Crux, which contains all our overrides/changes to the Crux
  theme. Just make sure it's the active theme. If there are `.dist` and `.diff` files, it means
  that a file from another source was modified and bundled with our theme as override. The
  `.dist` is the original file, and the `.diff` contains our changes to it. After taking a new
  copy of the now-updated original file, reapply the diff and re-diff it so that the diff is
  accurate w.r.t. the new original.

## Plugins NOT to upgrade

- The LayerSlider plugin is bundled with Crux, in `wp-content/themes/crux/config-layerslider/layerslider.zip`.
  This version should be used (i.e. unzipped into `wp-content/plugins`), and **NOT** the latest
  version of LayerSlider.
- StagTools 2.3.4 and 2.3.5 were missing some files, f.e. `stagtools/includes/widgets/lib/TwitterWP/lib/TwitterWP.php`,
  that are required by other plugins. Because of this, it was downgraded to
  version 2.3.3. When a new release contains the missing files and is not
  causing any issues with the Twitter widget on the front page, the plugin can
  be upgraded.

## WooCommerce PDF Invoices & Packing Slips

WooCommerce PDF Invoices & Packing Slips uses our own invoice & packing slip templates. They are
copies of the built-in "Simple" template, but with the added header image, some restyling of the
packing slip, and `'excl'` argument of the `get_woocommerce_totals('excl')` call.

After upgrading WooCommerce PDF Invoices & Packing Slips, reapply patch
`patches/woocommerce-pdf-invoices.diff` to allow that `'excl'` argument.

Do **NOT** select a header image in the plugin options. The header image is included in the
template, as it should be full-width (and our markup takes care of that).

Make sure that the plugin options are set to use the "blender" style.


## MySQL hacks

The following database trigger was installed to prevent empty user_login fields:

```
DELIMITER //
CREATE TRIGGER prevent_empty_user_login BEFORE INSERT ON wp_users FOR EACH ROW BEGIN
    IF NEW.user_login = '' THEN
        SET NEW.user_login = NEW.user_email;
    END IF;
END; //
DELIMITER ;
```

The following database trigger was installed to always set the login to the email address:

```
DELIMITER //
CREATE TRIGGER user_login_to_email_on_update BEFORE UPDATE ON wp_users FOR EACH ROW BEGIN
    SET NEW.user_login = NEW.user_email;
END; //
DELIMITER ;
```

## Checklist after upgrading

This checklist is based on things that got messed up in the past. It is **not** a complete
list; only use this list after you've checked the store for proper functioning yourself
using your brain.

* Slideshow on front page should work.
* Layout on https://store.blender.org/product-category/training/ should be ok.
* Log in with the wrong password and click the "Forgot Password" link. It should redirect to Blender
  ID's password reset page.
* Go to https://store.blender.org/my-account/edit-account/ and check that you cannot edit your email
  address there. A link to Blender ID should be shown.
