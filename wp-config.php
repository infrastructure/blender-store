<?php
ini_set('display_errors',1);

// Import local definitions first; later define() calls won't change them.
if (file_exists(dirname(__FILE__) . '/wp-config-local.php')) {
	require_once(dirname(__FILE__) . '/wp-config-local.php');
}

define('DISALLOW_FILE_EDIT', true);


// ** Braintree Payment Gateway settings ** //
define( 'BO_BRAINTREE_MERCHANT_ID_EUR', 'set-in-config-local');
define( 'BO_BRAINTREE_MERCHANT_ID_USD', 'set-in-config-local');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'set-in-config-local');
define('SECURE_AUTH_KEY',  'set-in-config-local');
define('LOGGED_IN_KEY',    'set-in-config-local');
define('NONCE_KEY',        'set-in-config-local');
define('AUTH_SALT',        'set-in-config-local');
define('SECURE_AUTH_SALT', 'set-in-config-local');
define('LOGGED_IN_SALT',   'set-in-config-local');
define('NONCE_SALT',       'set-in-config-local');


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_store');

/** MySQL database username */
define('DB_USER', 'wp_store');

/** MySQL database password */
define('DB_PASSWORD', 'set-in-config-local');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** https://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP */
define( 'WP_MEMORY_LIMIT', '128M' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

// Local
define('CLOUDAPI_BADGER_ENDPOINT', 'http://set-in-config-local@localhost:5000/service/badger');

define('BLENDERID_BADGER_ENDPOINT', 'http://blender-id:8000/api/badger');
define('BLENDERID_CREATE_USER_ENDPOINT', 'http://blender-id:8000/api/create-user/');
define('BLENDERID_CHECK_USER_ENDPOINT', 'http://blender-id:8000/api/check-user/');
define('BLENDERID_AUTHENTICATE_ENDPOINT', 'http://blender-id:8000/api/authenticate/');
define('BLENDERID_AUTH_TOKEN', '-set-in-config-local-');
define('BLENDERID_WEBHOOK_USER_CHANGED_SECRET', '-set-in-config-local-');

define('BID_CRON_INTERVAL_SECS', 300);  // MUST be more than 10 seconds, see _bidq_flush_bid_queue() in blender-id-queue.php
define('BID_QUEUE_KEEP_422_SECS', 0);  // max age of messages that get a 422 response from Blender ID. 0 to ignore and queue forever.


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
define('FS_METHOD','direct');

