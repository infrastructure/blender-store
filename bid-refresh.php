<?php
/**
 * Refreshes subscription status of a specific user at Blender ID.
 *
 * Call: bid-refresh.php?blenderid=some@email.address
 */

/** Set up WordPress environment */
$topdir = dirname(__FILE__);
if (!defined('ABSPATH')) {
    require_once("$topdir/wp-load.php");
}
require_once("$topdir/wp-content/plugins/blender-id/subscriptions.php");

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Content-Type: application/json");


// Find the user
$email = $_GET['blenderid'];
$args = array(
    'search' => $email,
    'search_columns' => array('user_email')
);

$user_query = new WP_User_Query($args);
$users = $user_query->get_results();
$count = count($users);
if ($count > 1) {
    http_response_code(400);
    echo json_encode(array('error' => "User not unique, query returned $count users.", 'email' => $email), JSON_PRETTY_PRINT);
    die();
}
if ($count == 0) {
    http_response_code(400);
    echo json_encode(array('error' => "User not found.", 'email' => $email), JSON_PRETTY_PRINT);
    die();
}
$user = $users[0];

$status = _most_active_status($user);
if ($status === false) $status = 'cancelled';

if ($status == 'active' || $status == 'pending-cancel') {
    error_log("bid-refresh: $user->user_email (#$user->ID) subscription confirmed as active; status = $status");
    _bid_badger('grant', 'cloud_subscriber', $user);
    _bid_badger('grant', 'cloud_has_subscription', $user);
} else if ($status == 'on-hold') {
    error_log("bid-refresh: $user->user_email (#$user->ID) subscription confirmed as on-hold");
    _bid_badger('revoke', 'cloud_subscriber', $user);
    _bid_badger('grant', 'cloud_has_subscription', $user);
} else {
    error_log("bid-refresh: $user->user_email (#$user->ID) subscription confirmed as cancelled; status = $status");
    _bid_badger('revoke', 'cloud_subscriber', $user);
    _bid_badger('revoke', 'cloud_has_subscription', $user);
}
echo json_encode(array('subscription_status' => $status, 'email' => $email), JSON_PRETTY_PRINT);
