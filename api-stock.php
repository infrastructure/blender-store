<?php

/** Set up WordPress environment */
$topdir = dirname(__FILE__);
if (!defined('ABSPATH')) {
    require_once("$topdir/wp-load.php");
}

$product_id = $_GET['id'];
$prod = wc_get_product($product_id);
if (!$prod) {
    wp_die("product not found");
}

$max_age = 300;  // seconds
$exp = gmdate('r', time() + $max_age);

header("Cache-Control: public, max-age=$max_age, must-revalidate");
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");
header("Expires: $exp");

$data = array(
    'stock' => $prod->get_stock_quantity(),
    'total_sales' => $prod->get_total_sales(),
    'name' => $prod->get_name(),
    'slug' => $prod->get_slug(),
    'price' => $prod->get_price(),
    'regular_price' => $prod->get_regular_price(),
    'sale_price' => $prod->get_sale_price(),
);

print(json_encode($data));
