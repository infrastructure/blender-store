<?php
/**
 * Blender Cloud sends users here to renew their subscription.
 * They are redirected to /myaccount/subscriptions/<subscription ID>.
 */

/** Set up WordPress environment */
$topdir = dirname(__FILE__);
if (!defined('ABSPATH')) {
    require_once("$topdir/wp-load.php");
}

$user = wp_get_current_user();
if ($user->ID == 0) {
    add_filter('woocommerce_is_account_page', 'renew_says_yes', 10, 0);
    function renew_says_yes() { return true; }

    get_header('activate');
    wc_print_notices();
    do_action( 'woocommerce_before_customer_login_form' );

?>
    <h1>Confirm your login before renewing your subscription</h1>
    <div class="col2-set woocommerce" id="customer_login">
	<div class="col-1">
		<form method="post" class="login_bo">
            <input type='hidden' name='redirect' value='<?php esc_attr_e($_SERVER['REQUEST_URI']) ?>'>

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<p class="form-row form-row-wide">
				<label for="username"><?php _e( 'Blender ID (email)', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="text" class="input-text" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
			</p>
			<p class="form-row form-row-wide">
				<label for="password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input class="input-text" type="password" name="password" id="password" />
			</p>

			<p class="lost_password">
				If you have lost your Blender ID password, <a href="https://www.blender.org/id/password_reset/">please set a new one here</a>.
			</p>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<p class="form-row">
				<?php wp_nonce_field( 'woocommerce-login' ); ?>
				<input type="submit" class="button" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />
				<label for="rememberme" class="inline">
					<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php _e( 'Remember me', 'woocommerce' ); ?>
				</label>
			</p>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>
    </div>
    <div class='col-2'><form><!-- form tag is just for styling purposes. -->
         <p>Before you can renew your Blender Cloud subscription, we need to know how you are.</p>
    </form>
    </div>
    </div>
<?php
    get_footer();
    return;
}

// From this point onward, all we do is redirect.
http_response_code(302);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$subscriptions = wcs_get_users_subscriptions($user->ID);
foreach ($subscriptions as $sub_id => $sub) {
    if (!$sub->can_be_updated_to('active') || !$sub->needs_payment()) {
        continue;
    }

    $orders = $sub->get_related_orders('all', 'renewal');
    if (empty($orders)) {
        // Cannot find any orders; just redirect to the subscription URL.
        $url = $sub->get_view_order_url();
        header("Location: $url");
        return;
    }

    // Directly redirect to checkout of the renewal order.
    $num_orders = count($orders);
    foreach ($orders as $order) {
        if (!$order->needs_payment()) {
            continue;
        }
        $url = $order->get_checkout_payment_url();
        header("Location: $url");
        return;
    }
}

// If all else fails, just redirect to the subscriptions page.
header("Location: /my-account/subscriptions/");
