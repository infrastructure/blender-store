<?php if(!defined('ABSPATH')) exit; // Exit if accessed directly

use Aelia\WC\TaxDisplayByCountry\WC_Aelia_Tax_Display_By_Country;
use Aelia\WC\TaxDisplayByCountry\Definitions;

// $widget_args is passed when widget is initialised
echo get_value('before_widget', $widget_args);
?>
<!-- This wrapper is needed for widget JavaScript to work correctly -->
<div class="aelia_tax_display_by_country widget_wc_aelia_country_selector_widget"><?php
	// Title is set in Aelia\TaxDisplayByCountry\Country_Selector_Widget::widget()
	$widget_title = get_value('title', $widget_args);
	if(!empty($widget_title)) {
		echo get_value('before_title', $widget_args);
		echo apply_filters('widget_title', __($widget_title, $this->text_domain), $widget_args, $this->id_base);
		echo get_value('after_title', $widget_args);
	}
?>
<!-- Tax Display by Country v.' <?php echo WC_Aelia_Tax_Display_By_Country::$version; ?> ' - Country Selector Widget -->
<form method="post" class="country_selector_form">
	<div class="wrapper country_selector">
		<label class="country_label" for="wc_aelia_tdbc_country_selector"><?php
			if(isset($widget_args['country_label'])) {
				echo $widget_args['country_label'];
			}
		?></label>
		<?php
		// The hidden field will allow to know when the user has changed the tax display settings. This is
		// necessary to figure out if the "tax exempt" flag should be processed as well. When the checkbox
		// is unchecked, it's not POSTed back to the server. However, its absence might also mean that the POST
		// arrived from another source and that the "I'm tax exempt" setting should not change.
		?>
		<input type="hidden" name="<?php echo Definitions::ARG_TAX_DISPLAY_SELECTION; ?>" value="1" />
		<select class="countries country_selector" name="<?php echo Definitions::ARG_AELIA_CUSTOMER_COUNTRY; ?>"><?php
			foreach($widget_args['countries'] as $country_code => $country_name) {
				$selected_attr = '';
				if($country_code === $widget_args['selected_country']) {
					$selected_attr = 'selected="selected"';
				}
				echo '<option value="' . $country_code . '" ' . $selected_attr . '>' . __($country_name, $this->text_domain). '</option>';
			}
		?>
		</select>
	</div>

	<?php
	// If required, display the State/county field
	if($widget_args['handle_customer_state']):
		$states = WC()->countries->get_states($widget_args['selected_country']);
		// If WC doesn't have a list of states for the specified country, it returns
		// "false". This is a design flaw, as the WC should always return an
		// array (even if empty), but we can handle this easily.
		// @since 1.9.5.170431
		if(empty($states) || !is_array($states)) {
			$states = array();
		}
		?>
		<div class="wrapper state_selector">
			<label class="state_label" for="wc_aelia_tdbc_state_selector"><?php
				echo $widget_args['state_label'];
			?></label>
			<select id="wc_aelia_tdbc_state_selector" class="states state_selector" name="<?php echo Definitions::ARG_AELIA_CUSTOMER_STATE; ?>"><?php
				foreach($states as $state_code => $state_name) {
					$selected_attr = selected($widget_args['selected_state'], $state_code, false);
					echo '<option value="' . $state_code . '" ' . $selected_attr . '>' . __($state_name, $this->text_domain) . '</option>';
				}
			?>
			</select>
		</div>
	<?php endif;

	// If necessary, display the "I'm tax exempt" option
	if($widget_args['tax_exempt_flag_enabled']): ?>
		<div class="tax_exempt_wrapper">
			<?php $checked_attr = !empty($widget_args['tax_exempt']) ? 'checked="checked"' : ''; ?>
			<input id="tax_exempt" class="tax_exempt" name="<?php echo Definitions::ARG_TAX_EXEMPT; ?>" type="checkbox" value="1" <?php echo $checked_attr; ?> />
			<label for="tax_exempt"><?php
				echo __($widget_args['tax_exempt_flag_label'], $this->text_domain);
			?></label>
		</div>
	<?php endif;
	// Display the "change location" button only when JavaScript is disabled. When it's enabled, selecting a
	// country in the dropdown will automatically trigger the country switch
	?>
		<button type="submit" class="button change_country"><?php
			echo __('Change Location', $this->text_domain);
		?></button>
	</form>
</div>
<?php
echo get_value('after_widget', $widget_args);
