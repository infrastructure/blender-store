<?php
namespace Aelia\WC\TaxDisplayByCountry;
if(!defined('ABSPATH')) exit; // Exit if accessed directly

use \WP_Widget;

/**
 * Displays the country selector widget.
 */
class Customer_Country_Selector_Widget extends WP_Widget {
	protected $text_domain;

	// Widget types
	const TYPE_DROPDOWN = 'dropdown';
	const TYPE_BUTTONS = 'buttons';

	protected function WC() {
		global $woocommerce;
		return $woocommerce;
	}

	protected function default_settings($settings_key = null) {
		return WC_Aelia_Tax_Display_By_Country::settings()->default_settings($settings_key);
	}

	protected function settings($settings_key = null, $default = null) {
		$result = WC_Aelia_Tax_Display_By_Country::settings()->current_settings($settings_key);
		if(empty($result)) {
			$result = $default;
		}
		// Allow 3rd parties to filter the setting value
		// @since 1.9.13.180117
		return apply_filters('wc_aelia_tdbc_country_selector_widget_setting', $result, $settings_key, $default);
	}

	/**
	 * Returns a list of the available widget types and their attributes.
	 *
	 * @return array
	 */
	protected function widget_types() {
		$widget_types = array(
			self::TYPE_DROPDOWN => array(
				'name' => __('Dropdown', $this->text_domain),
				'template' => 'country-selector-widget-dropdown',
				'title' => __('Displays a dropdown with all enabled countries', $this->text_domain),
			),
		);

		$widget_types = apply_filters('wc_aelia_tdbc_country_selector_widget_types', $widget_types);
		return $widget_types;
	}

	/**
	 * Retrieves the template that will be used to render the widget.
	 *
	 * @param string template_type The template type.
	 * @return string
	 */
	protected function get_widget_template($template_type) {
		$widget_types = $this->widget_types();
		$type_info = get_value($template_type, $widget_types);
		// If an invalid type is passed, default to a dropdown widget
		if(empty($type_info)) {
			$type_info = get_value(self::TYPE_DROPDOWN, $this->widget_types());
		}

		return $type_info['template'];
	}

	/**
	 *	Class constructor.
	 */
	public function __construct() {
		$this->text_domain = WC_Aelia_Tax_Display_By_Country::$text_domain;

		parent::__construct(
			'wc_aelia_tdbc_country_selector_widget',
			'WooCommerce Tax Display by Country - Country Selector',
			array('description' => __('Allow to switch country on the fly and ' .
																'display prices with/without tax accordingly.',
																$this->text_domain),)
		);
	}

	/**
	 * Loads the CSS files required by the Widget.
	 */
	private function load_css() {
	}

	/**
	 * Loads the JavaScript files required by the Widget.
	 */
	private function load_js() {
	}

	/**
	 * Front-end display of widget.
	 *
	 * @param array $widget_args Widget arguments.
	 * @param array $instance Saved values from database.
	 * @see WP_Widget::widget()
	 */
	public function widget($widget_args, $instance = array()) {
		$this->load_css();
		$this->load_js();

		if(!is_array($widget_args)) {
			$widget_args = array();
		}

		$widget_args = array_merge(
			$instance,
			$widget_args
		);

		$widget_type = get_value('widget_type', $widget_args, self::TYPE_DROPDOWN);
		$widget_template_name = $this->get_widget_template($widget_type);
		$widget_template_file = WC_Aelia_Tax_Display_By_Country::instance()->get_template_file($widget_template_name);
		// Debug
		//var_dump($widget_template_name, $widget_template_file);

		if(empty($widget_template_file)) {
			$this->display_invalid_widget_type_error($widget_type);
		}
		else {
			$widget_args['title'] = apply_filters('wc_aelia_tdbc_country_selector_widget_title', get_value('title', $widget_args));
			$widget_args['countries'] = apply_filters('wc_aelia_tdbc_country_selector_widget_countries',
																								$this->WC()->countries->get_allowed_countries());

			// Retrieve selected country
			$selected_country = get_value('selected_country', $widget_args);
			if(empty($selected_country)) {
				$widget_args['selected_country'] = WC_Aelia_Tax_Display_By_Country::instance()->get_customer_country();
			}

			// Load "I'm tax exempt" settings
			$widget_args['tax_exempt_flag_enabled'] = $this->settings(Settings::FIELD_TAX_EXEMPT_FLAG_ENABLED);
			if($widget_args['tax_exempt_flag_enabled']) {
				$widget_args['tax_exempt_flag_label'] = $this->settings(Settings::FIELD_TAX_EXEMPT_FLAG_LABEL,
																																$this->default_settings(Settings::FIELD_TAX_EXEMPT_FLAG_LABEL));

				$widget_args['tax_exempt'] = WC_Aelia_Tax_Display_By_Country::instance()->get_tax_exempt_status();
			}

			// If "handle customer State" option is enabled, render the State selector
			$widget_args['handle_customer_state'] = $this->settings(Settings::FIELD_HANDLE_CUSTOMER_STATE);
			if($widget_args['handle_customer_state']) {
				$widget_args['state_label'] = 'State/County';

				// Retrieve selected State/county
				$selected_state = get_value('selected_state', $widget_args);
				if(empty($selected_state)) {
					$widget_args['selected_state'] = WC_Aelia_Tax_Display_By_Country::instance()->get_customer_state();
				}
			}

			// Display the Widget
			include $widget_template_file;
		}
	}

	/**
	 * If an invalid widget type has been set, display an error so that the site
	 * owner is aware of it.
	 *
	 * @param string widget_type The invalid widget type.
	 */
	protected function display_invalid_widget_type_error($widget_type) {
		echo '<div class="error">';
		echo '<h5 class="title">' . __('Error', $this->text_domain) . '</h5>';
		echo sprintf(__('The country selector widget has not been configured properly. A template for ' .
										'wiget type "%s" could not be found. Please review the widget settings and ensure ' .
										'that a valid widget type has been selected. If the issue persists, please ' .
										'<a href="https://aelia.freshdesk.com/helpdesk/tickets/new" title="Contact support">' .
										'contact support</a>.', $this->text_domain),
								 $widget_type);
		echo '</div>';
	}

	/**
	 * Renders Widget's form in Admin section.
	 *
	 * @param array instance Widgets settings passed when submitting the form.
	 */
 	public function form($instance) {
		$title_field_id = $this->get_field_id('title');
		$title_field_name = $this->get_field_name('title');
		echo '<p>';
		echo '<label for="' . $title_field_id . '">' . _e('Widget Title:', $this->text_domain) . '</label>';
		echo '<input type="text" class="widefat" id="' . esc_attr($title_field_id) . '" ' .
				 'name="' . $title_field_name . '" ' .
				 'value="' . esc_attr(get_value('title', $instance, '')) . '" />';
		echo '</p>';

		$country_label_field_id = $this->get_field_id('country_label');
		$country_label_field_name = $this->get_field_name('country_label');
		echo '<p>';
		echo '<label for="' . $country_label_field_id . '">' . _e('Country selector label:', $this->text_domain) . '</label>';
		echo '<input type="text" class="widefat" id="' . esc_attr($country_label_field_id) . '" ' .
				 'name="' . $country_label_field_name . '" ' .
				 'value="' . esc_attr(get_value('country_label', $instance, '')) . '" />';
		echo '</p>';

		// Show the "State/County" label if the "handle customer state" option is
		// enabled
		if($this->settings(Settings::FIELD_HANDLE_CUSTOMER_STATE)) {
			$state_label_field_id = $this->get_field_id('state_label');
			$state_label_field_name = $this->get_field_name('state_label');
			echo '<p>';
			echo '<label for="' . $state_label_field_id . '">' . _e('State selector label:', $this->text_domain) . '</label>';
			echo '<input type="text" class="widefat" id="' . esc_attr($state_label_field_id) . '" ' .
					 'name="' . $state_label_field_name . '" ' .
					 'value="' . esc_attr(get_value('state_label', $instance, '')) . '" />';
			echo '</p>';
		}

		$widget_type_field_id = $this->get_field_id('widget_type');
		$widget_type_field_name = $this->get_field_name('widget_type');

		echo '<p>';
		echo '<label for="' . $widget_type_field_id . '">' . _e('Widget type:', $this->text_domain) . '</label>';
		echo '<select class="widefat" id="' . esc_attr($widget_type_field_id) . '" ' .
				 'name="' . $widget_type_field_name . '">';
		foreach($this->widget_types() as $type_id => $type_info) {
			$selected_attr = '';
			if(esc_attr(get_value('widget_type', $instance, '')) == $type_id) {
				$selected_attr = 'selected="selected"';
			}

			echo '<option value="' . $type_id . '" title="' . $type_info['title'] . '" ' . $selected_attr . '>';
			echo $type_info['name'];
			echo '</option>';
		}
		echo '</select>';
		echo '</p>';
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 * @return array Updated safe values to be saved.
	 * @see WP_Widget::update()
	 */
	public function update($new_instance, $old_instance) {
		$instance = array();
		$instance['title'] = strip_tags(stripslashes($new_instance['title']));
		$instance['widget_type'] = strip_tags(stripslashes($new_instance['widget_type']));
		$instance['country_label'] = strip_tags(stripslashes($new_instance['country_label']));
		$instance['state_label'] = strip_tags(stripslashes($new_instance['state_label']));

		return $instance;
	}

	/**
	 * Renders the country selector widget when invoked using a shortcode.
	 *
	 * @param array widget_args An array of arguments for the widget.
	 * @return string
	 */
	public static function render_country_selector($widget_args) {
		ob_start();

		$class = get_called_class();
		$widget_instance = new $class();
		$widget_instance->widget($widget_args);

		$output = ob_get_contents();
		@ob_end_clean();

		return $output;
	}
}
