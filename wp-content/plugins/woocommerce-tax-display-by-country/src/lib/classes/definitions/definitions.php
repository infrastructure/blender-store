<?php
namespace Aelia\WC\TaxDisplayByCountry;
if(!defined('ABSPATH')) exit; // Exit if accessed directly

use \Aelia\WC\Messages;
use \WP_Error;

/**
 * Implements a base class to store and handle the messages returned by the
 * plugin. This class is used to extend the basic functionalities provided by
 * standard WP_Error class.
 */
class Definitions {
	// @var string The menu slug for plugin's settings page.
	const MENU_SLUG = 'wc_aelia_tax_display_by_country';
	// @var string The plugin slug
	const PLUGIN_SLUG = 'wc-aelia-tax-display-by-country';
	// @var string The plugin text domain
	const TEXT_DOMAIN = 'wc-aelia-tax-display-by-country';

	const ARG_TAX_DISPLAY_SELECTION = 'tax_display_selection';
	const ARG_BILLING_COUNTRY = 'country';
	const ARG_BILLING_STATE = 'state';

	// @since 1.12.3.200203
	const ARG_CHECKOUT_SHIPPING_COUNTRY = 's_country';
	const ARG_CHECKOUT_SHIPPING_STATE = 's_state';


	const ARG_TAX_EXEMPT = 'tax_exempt';
	const ARG_CALC_SHIPPING = 'calc_shipping';
	const ARG_CALC_SHIPPING_COUNTRY = 'calc_shipping_country';
	const ARG_CALC_SHIPPING_STATE = 'calc_shipping_state';

	const ARG_AELIA_BILLING_COUNTRY = 'aelia_billing_country';
	const ARG_AELIA_CUSTOMER_COUNTRY = 'aelia_customer_country';
	const ARG_AELIA_CUSTOMER_STATE = 'aelia_customer_state';

	const SESSION_CUSTOMER_COUNTRY = 'aelia_customer_country';
	const SESSION_CUSTOMER_STATE = 'aelia_customer_state';

	const SESSION_TAX_EXEMPT = 'aelia_tax_exempt';

	const WARN_NEW_COUNTRY_SELECTOR_WIDGET = 2001;
}
