<?php
namespace Aelia\WC\TaxDisplayByCountry;
if(!defined('ABSPATH')) exit; // Exit if accessed directly

/**
 * Handles the settings for the Blacklister plugin and provides convenience
 * methods to read and write them.
 */
class Settings extends \Aelia\WC\Settings {
	/*** Settings Key ***/
	// @var string The key to identify plugin settings amongst WP options.
	const SETTINGS_KEY = 'wc_aelia_tax_display_by_country';

	/*** Settings fields ***/
	const FIELD_TAX_DISPLAY_SETTINGS = 'tax_display_settings';
	const FIELD_TAX_EXEMPT_FLAG_ENABLED = 'tax_exempt_flag_enabled';
	const FIELD_TAX_EXEMPT_FLAG_LABEL = 'tax_exempt_flag_label';
	const FIELD_TAX_EXEMPT_PRICE_SUFFIX = 'tax_exempt_price_suffix';

	const FIELD_KEEP_PRODUCT_PRICES_FIXED = 'keep_product_prices_fixed';
	const FIELD_HANDLE_CUSTOMER_STATE = 'handle_customer_state';

	// @var array A list of tax display settings for each country
	protected $tax_display_for_country = array();

	/**
	 * Returns the default settings for the plugin. Used mainly at first
	 * installation.
	 *
	 * @param string key If specified, method will return only the setting identified
	 * by the key.
	 * @param mixed default The default value to return if the setting requested
	 * via the "key" argument is not found.
	 * @return array|mixed The default settings, or the value of the specified
	 * setting.
	 *
	 * @see WC_Aelia_Settings:default_settings().
	 */
	public function default_settings($key = null, $default = null) {
		$default_options = array(
			self::FIELD_TAX_DISPLAY_SETTINGS => array(
			),
			self::FIELD_TAX_EXEMPT_FLAG_ENABLED => false,
			self::FIELD_TAX_EXEMPT_FLAG_LABEL => __("I'm tax/VAT exempt, show me prices without tax.", $this->textdomain),
			self::FIELD_TAX_EXEMPT_PRICE_SUFFIX => '',
			self::FIELD_KEEP_PRODUCT_PRICES_FIXED => false,
			self::FIELD_HANDLE_CUSTOMER_STATE => false,
		);

		if(empty($key)) {
			return $default_options;
		}
		else {
			return get_value($key, $default_options, $default);
		}
	}

	/**
	 * Indicates if the "I'm tax exempt" feature is enabled.
	 *
	 * @return array
	 */
	public function get_tax_exempt_enabled() {
		return $this->current_settings(self::FIELD_TAX_EXEMPT_FLAG_ENABLED, false);
	}

	/**
	 * Returns the price suffix to display to visitors who declare themselves exempt
	 * from tax.
	 *
	 * @return array
	 */
	public function get_tax_exempt_price_suffix() {
		return $this->current_settings(self::FIELD_TAX_EXEMPT_PRICE_SUFFIX, false);
	}

	/**
	 * Returns an array containing the IP addresses that have been blacklisted.
	 *
	 * @return array
	 */
	public function get_tax_display_settings() {
		$result = $this->current_settings(self::FIELD_TAX_DISPLAY_SETTINGS);
		if(!is_array($result)) {
			$result = array();
		}

		return $result;
	}

	/**
	 * Returns the tax display method (including/excluding taxes) to use for the
	 * specified country.
	 *
	 * @param string country_code The country code.
	 * @param string tax_display_type The display type to retrieve. It can have
	 * one of the following values:
	 * - woocommerce_tax_display_shop
	 * - woocommerce_tax_display_cart
	 * @return string|null
	 */
	public function get_tax_display_for_country($country_code, $tax_display_type) {
		$tax_display_key = sprintf('%s-%s', $country_code, $tax_display_type);
		if(isset($this->tax_display_for_country[$tax_display_key])) {
			return $this->tax_display_for_country[$tax_display_key];
		}

		$result = null;
		foreach($this->get_tax_display_settings() as $index => $settings) {
			// Extract the tax display setting from the first group that matches the
			// passed country
			if(in_array($country_code, $settings['countries'])) {
				$result = get_value($tax_display_type, $settings);

				// If a setting is found, stop and return it. If not, look for another
				// group that might match the country. This is useful in case of "dirty"
				// data, where a group might match the country, but not contain the
				// setting
				if(!empty($result)) {
					break;
				}
			}
		}

		$this->tax_display_for_country[$tax_display_key] = $result;

		return $result;
	}

	/**
	 * Returns the price suffix (including/excluding taxes) to use for the
	 * specified country.
	 *
	 * @param string country_code The country code.
	 * @return string|null
	 */
	public function get_price_suffix_for_country($country_code) {
		$suffix = null;
		foreach($this->get_tax_display_settings() as $index => $settings) {
			// Extract the tax display setting from the first group that matches the
			// passed country
			if(in_array($country_code, $settings['countries'])) {
				$suffix = get_value('price_suffix', $settings);

				// If a setting is found, stop and return it. If not, look for another
				// group that might match the country. This is useful in case of "dirty"
				// data, where a group might match the country, but not contain the
				// setting
				if(!empty($suffix)) {
					break;
				}
			}
		}

		// Pass suffix to WPML for translation before returning it
		$suffix = aelia_t(WC_Aelia_Tax_Display_By_Country::$plugin_name, 'price_suffix_for_country: ' . $suffix, __($suffix));
		return $suffix;
	}

	/**
	 * Indicates if the product prices should be kept fixed, regardless of what
	 * tax rate applies to the customer.
	 *
	 * @return bool
	 */
	public function keep_prices_fixed() {
		return apply_filters('wc_aelia_tdbc_keep_prices_fixed',
												 $this->get(self::FIELD_KEEP_PRODUCT_PRICES_FIXED) && (get_option('woocommerce_prices_include_tax') == 'yes'));
	}

	/**
	 * Indicates if the plugin should handle customer's State/county as well as
	 * the country.
	 *
	 * @since 1.8.0.150729
	 */
	public function handle_customer_state() {
		return $this->get(self::FIELD_HANDLE_CUSTOMER_STATE, false);
	}

	/**
	 * Validates the settings specified via the Options page.
	 *
	 * @param array settings An array of settings.
	 */
	public function validate_settings($settings) {
		// Debug
		//var_dump($settings);die();
		$processed_settings = $this->current_settings();

		$tax_display_settings = get_value(self::FIELD_TAX_DISPLAY_SETTINGS, $settings, array());
		// Remove invalid rows (e.g. ones without selected countries)
		$settings[self::FIELD_TAX_DISPLAY_SETTINGS] = $this->cleanup_invalid_entries($tax_display_settings);

		// Process settings related to the "I'm tax exempt" flag
		$processed_settings = array_merge($processed_settings, $settings);

		/* Invalidate product cache when settings are saved. This is necessary to
		 * ensure that the displayed prices conform with the new settings (cached
		 * prices could be the wrong ones).
		 *
		 * @since 1.7.9.150911
		 * @since WC 2.4
		 */
		if(aelia_wc_version_is('>=', '2.4')) {
			\WC_Cache_Helper::get_transient_version('product', true);
		}

		// Return the array processing any additional functions filtered by this action.
		return apply_filters('wc_aelia_tax_display_by_country_settings', $processed_settings, $settings);
	}

	/**
	 * Class constructor.
	 */
	public function __construct($settings_key = self::SETTINGS_KEY,
															$textdomain = '',
															\Aelia\WC\Settings_Renderer $renderer = null) {
		if(empty($renderer)) {
			// Instantiate the render to be used to generate the settings page
			$renderer = new \Aelia\WC\Settings_Renderer();
		}
		parent::__construct($settings_key, $textdomain, $renderer);

		add_action('admin_init', array($this, 'init_settings'));

		// If no settings are registered, save the default ones
		if($this->load() === null) {
			$this->save();
		}
	}

	/*** Validation methods ***/
	protected function cleanup_invalid_entries($tax_display_settings) {
		$field_error_messages = array();

		foreach($tax_display_settings as $index => $settings) {
			$countries = get_value('countries', $settings);

			if(empty($countries) || !is_array($countries)) {
				$field_error_messages[self::FIELD_TAX_DISPLAY_SETTINGS] =
					sprintf(__('Groups without any selected countries have been removed.',
										 $this->textdomain),
									$index);
					unset($tax_display_settings[$index]);
			}
		}

		return $tax_display_settings;
	}
}
