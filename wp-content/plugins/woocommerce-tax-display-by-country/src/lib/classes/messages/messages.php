<?php
namespace Aelia\WC\TaxDisplayByCountry;
if(!defined('ABSPATH')) exit; // Exit if accessed directly

use Aelia\WC\TaxDisplayByCountry\Definitions;

/**
 * Stores and handles the messages returned by the EDD Currency Switcher plugin.
 */
class Messages extends \Aelia\WC\Messages {
	/**
	 * Loads all the error message used by the plugin.
	 */
	public function load_messages() {
		parent::load_messages();
	}

	public static function admin_message($message, array $params = array()) {
		$params['sender_id'] = Definitions::PLUGIN_SLUG;
		return parent::admin_message($message, $params);
	}
}
