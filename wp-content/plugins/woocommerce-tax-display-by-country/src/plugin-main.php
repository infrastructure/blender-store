<?php
namespace Aelia\WC\TaxDisplayByCountry;
if(!defined('ABSPATH')) { exit; } // Exit if accessed directly

require_once('lib/classes/definitions/definitions.php');

use Aelia\WC\Aelia_Plugin;
use Aelia\WC\Aelia_SessionManager;
use Aelia\WC\IP2Location;
use Aelia\WC\TaxDisplayByCountry\Settings;
use Aelia\WC\TaxDisplayByCountry\Settings_Renderer;
use Aelia\WC\TaxDisplayByCountry\Messages;
use \WC_Customer;

/**
 * Tax Display by Country plugin.
 **/
class WC_Aelia_Tax_Display_By_Country extends Aelia_Plugin {
	public static $version = '1.15.4.201103';

	public static $plugin_slug = Definitions::PLUGIN_SLUG;
	public static $text_domain = Definitions::TEXT_DOMAIN;
	public static $plugin_name = 'Aelia Tax Display by Country for WooCommerce';

	// @var string Customer's location. Used for caching purposes.
	protected $customer_location = null;

	/**
	 * Customer's taxable address. Used for caching.
	 * @var array
	 * @since 1.7.7.150831
	 */
	protected $customer_taxable_address = null;

	// @var IP2Location IP2Location detection class.
	protected $_ip2location;

	/**
	 * Returns an instance of the IP2Location class used for geolocation resolution.
	 *
	 * @return Aelia\WC\IP2Location
	 * @since 1.7.5.150728
	 */
	protected function ip2location() {
		if(empty($this->_ip2location)) {
			$this->_ip2location = IP2Location::factory();
		}
		return $this->_ip2location;
	}

	public static function factory() {
		// Load Composer autoloader
		require_once(__DIR__ . '/vendor/autoload.php');

		$settings_controller = null;
		$messages_controller = null;
		// Example on how to initialise a settings controller and a messages controller
		$settings_page_renderer = new Settings_Renderer();
		$settings_controller = new Settings(Settings::SETTINGS_KEY,
																				self::$text_domain,
																				$settings_page_renderer);
		$messages_controller = new Messages(self::$text_domain);

		$plugin_instance = new self($settings_controller, $messages_controller);
		return $plugin_instance;
	}

	/**
	 * Constructor.
	 *
	 * @param \Aelia\WC\Settings settings_controller The controller that will handle
	 * the plugin settings.
	 * @param \Aelia\WC\Messages messages_controller The controller that will handle
	 * the messages produced by the plugin.
	 */
	public function __construct($settings_controller = null,
															$messages_controller = null) {
		// Load Composer autoloader
		require_once(__DIR__ . '/vendor/autoload.php');

		parent::__construct($settings_controller, $messages_controller);

		// The commented line below is needed for Codestyling Localization plugin to
		// understand what text domain is used by this plugin
		//load_plugin_textdomain(static::$text_domain, false, $this->path('languages') . '/');
	}

	/**
	 * Performs operation when woocommerce has been loaded.
	 */
	public function woocommerce_loaded() {
		parent::woocommerce_loaded();

		if(self::is_frontend()) {
			// Set frontend filters and actions required after WooCommerce is loaded
			// @since 1.9.0.170217
			if(aelia_wc_version_is('<', '2.7')) {
				add_filter('woocommerce_get_price', array($this, 'woocommerce_get_price'), 1, 1);

				add_filter('default_checkout_country', array($this, 'default_checkout_country'));
				if($this->_settings_controller->handle_customer_state()) {
					add_filter('default_checkout_state', array($this, 'default_checkout_state'));
				}
			}
			else {
				add_filter('woocommerce_product_get_price', array($this, 'woocommerce_get_price'), 1, 1);
				add_filter('woocommerce_product_variation_get_price', array($this, 'woocommerce_get_price'), 1, 1);

				// Use new "default_checkout_billing_country" filter in WC3.0 and later
				// @since 1.9.3.170325
				add_filter('default_checkout_billing_country', array($this, 'default_checkout_country'));
				if($this->_settings_controller->handle_customer_state()) {
					// Use new "default_checkout_billing_state" filter in WC3.0 and later
					// @since 1.9.3.170325
					add_filter('default_checkout_billing_state', array($this, 'default_checkout_state'));
				}
			}
		}

		// Update customer's country, to ensure that the correct taxes are applied
		$this->set_customer_location($this->get_customer_location(),
																 isset($_POST[Definitions::ARG_AELIA_CUSTOMER_COUNTRY]));

		// Store the selection made by the user to indicate if they are exempt from tax/VAT
		// @since 1.15.3.201029
		$this->set_tax_exempt_status();
	}

	/**
	 * Determines on what criteria to calculate the tax. Code copied from
	 * WC_Customer::get_taxable_address().
	 *
	 * @return string The criteria to calculate taxes.
	 *
	 * @since 1.6.1.141012
	 * @see WC_Customer::get_taxable_address()
	 */
	protected function tax_based_on() {
		$tax_based_on = get_option('woocommerce_tax_based_on');

		// Check shipping method at this point to see if we need special handling
		if(isset(WC()->cart) &&
			 apply_filters('woocommerce_apply_base_tax_for_local_pickup', true) == true &&
			 WC()->cart->needs_shipping() &&
			 sizeof(array_intersect(WC()->session->get('chosen_shipping_methods', array(get_option('woocommerce_default_shipping_method'))), apply_filters('woocommerce_local_pickup_methods', array('local_pickup')))) > 0) {
			$tax_based_on = 'base';
		}

		return $tax_based_on;
	}

	/**
	 * Sets the billing and shipping country on the user object.
	 *
	 * @param string location An array describing customer's location (country and
	 * State).
	 * @param bool set_shipping_country Indicates if customer's shipping country
	 * should be set as well.
	 */
	protected function set_customer_location($location, $set_shipping_country = false) {
		$woocommerce = $this->wc();
		if(isset($woocommerce->customer)) {
			// Keep the city and postcode, if passed
			// @since 1.9.6.170828

			// Set the billing location
			//
			// IMPORTANT
			// Don't call $woocommerce->customer->set_billing_location(), as
			// that overwrites customer's "address 1" and "addresss 2" fields
			//
			// @since 1.12.0.191028
			// @link https://aelia.freshdesk.com/a/tickets/82243
			$woocommerce->customer->set_billing_country($location['country']);
			$woocommerce->customer->set_billing_state($location['state']);

			$shipping_country = trim($woocommerce->customer->get_shipping_country());
			if($set_shipping_country || empty($shipping_country) || !$woocommerce->session->has_session()) {
				// Set the shipping location
				//
				// IMPORTANT
				// Don't call $woocommerce->customer->set_shipping_location(), as
				// that overwrites customer's "address 1" and "addresss 2" fields
				//
				// @since 1.12.0.191028
				// @link https://aelia.freshdesk.com/a/tickets/82243
				$woocommerce->customer->set_shipping_country($location['country']);
				$woocommerce->customer->set_shipping_state($location['state']);
			}

			/* Trick WooCommerce into thinking that shipping has been calculated,
			 * so that the appropriate tax rate is applied throughout the shop
			 *
			 * The operation to be performed changed in WooCommerce 2.7.
			 */
			if(aelia_wc_version_is('>=', '2.7')) {
				$woocommerce->customer->set_calculated_shipping(true);
			}
			else {
				$woocommerce->customer->calculated_shipping(true);
			}
		}
	}

	/**
	 * Sets the hooks required by the plugin.
	 */
	protected function set_hooks() {
		parent::set_hooks();

		if(self::is_frontend()) {
			// Add hooks to alter the tax display flag depending on user's country
			add_filter('option_woocommerce_tax_display_shop', array($this, 'option_woocommerce_tax_display_shop'));
			add_filter('option_woocommerce_tax_display_cart', array($this, 'option_woocommerce_tax_display_cart'));
			add_filter('woocommerce_get_price_suffix', array($this, 'woocommerce_get_price_suffix'), 10, 4);
			add_filter('woocommerce_countries_ex_tax_or_vat', array($this, 'woocommerce_countries_ex_tax_or_vat'), 10, 1);
			add_filter('woocommerce_countries_inc_tax_or_vat', array($this, 'woocommerce_countries_inc_tax_or_vat'), 10, 1);
			add_filter('woocommerce_check_cart_items', array($this, 'woocommerce_check_cart_items'), 10);

			// Add filters for "fixed product prices" feature
			add_action('woocommerce_before_calculate_totals', array($this, 'woocommerce_before_calculate_totals'), 10, 1);
			// Remove the address override after the cart totals have been calculated
			// @since 1.12.2.200127
			add_filter('woocommerce_order_after_calculate_totals', array($this, 'woocommerce_order_after_calculate_totals'), 1, 1);

			add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'), 15);
		}

		// Register Widgets
		add_action('widgets_init', array($this, 'register_widgets'));

		add_filter('get_user_metadata', array($this, 'get_user_metadata'), 10, 4);

		// WC 2.4+
		// Transient keys
		add_filter('woocommerce_get_variation_prices_hash', array($this, 'woocommerce_get_variation_prices_hash'), 10, 3);

		add_action('admin_init', array($this, 'admin_init'));

		// Handle event in which customer address is changed in My Account page
		// @since 1.9.12.180106
		add_action('woocommerce_customer_save_address', array($this, 'woocommerce_customer_save_address'), 5, 2);

		// Handle the "add item" action for manual orders, to keep prices fixed
		// @since 1.14.0.200608
		add_action('wp_ajax_woocommerce_add_order_item', array($this, 'set_customer_address_for_add_order_item'), 5);
		add_action('wp_ajax_nopriv_woocommerce_add_order_item', array($this, 'set_customer_address_for_add_order_item'), 5);

		add_action('rest_pre_dispatch', array($this, 'set_tax_base_address_for_rest_api'), 5, 3);

		// Add hooks for shortcodes
		$this->set_shortcodes_hooks();
	}

	/**
	 * Returns the country code for the user, detecting it using the IP Address,
	 * if needed.
	 *
	 * @return string
	 * @deprecated since 1.7.5.150728
	 */
	public function get_billing_country() {
		return $this->get_customer_country();
	}

	/**
	 * Stores customer's country in the cookies.
	 *
	 * @param string customer_country customer's country.
	 * @since 1.7.5.150728
	 */
	protected function store_customer_location(array $location) {
		if(isset($location['country'])) {
			Aelia_SessionManager::set_cookie(Definitions::SESSION_CUSTOMER_COUNTRY, $location['country']);
		}
		if(isset($location['state'])) {
			Aelia_SessionManager::set_cookie(Definitions::SESSION_CUSTOMER_STATE, $location['state']);
		}
	}

	/**
	 * Returns the country code for the user, detecting it using the IP Address,
	 * if needed.
	 *
	 * @return string
	 * @since 1.7.5.150728
	 */
	public function get_customer_country() {
		$location = $this->get_customer_location();
		return $location['country'];
	}

	/**
	 * Returns the State/county code for the user, detecting it using the IP Address,
	 * if needed.
	 *
	 * @return string
	 * @since 1.7.5.150728
	 */
	public function get_customer_state() {
		$location = $this->get_customer_location();
		return $location['state'];
	}

	/**
	 * Returns the location information for the user, detecting it using the IP Address,
	 * if needed.
	 * IMPORTANT: WooCommerce stores the billing country in its "customer" property,
	 * while this method uses WooCommerce session when the billing country is selected.
	 * This must be done because the tax display option is retrieved by WooCommerce
	 * BEFORE the "customer" property is initialised. If we relied on such property,
	 * very often it would be empty, and we would return the incorrect country code.
	 *
	 * @return string
	 * @since 1.7.5.150728
	 */
	public function get_customer_location() {
		if(empty($this->customer_location)) {
			$woocommerce = $this->wc();

			$original_country = '';
			$original_state = '';

			// Initialise the customer object, if it's not initialised. This is required
			// since WooCommerce 3.2, which introduced an undocumented breaking change,
			// causing the tax display options to be loaded before the customer object
			// is initialised
			// @since 1.9.10.171127
			// @since WC 3.2
			// @link https://github.com/woocommerce/woocommerce/pull/17898
			if(self::is_frontend()) {
				$customer = !empty($woocommerce->customer) ? $woocommerce->customer : new \WC_Customer(get_current_user_id(), !empty(WC()->session));
			}

			// Keep track of customer's location and state
			// @since 1.9.6.170828
			if(!empty($customer)) {
				$original_country = $customer->get_billing_country();
				$original_state = $customer->get_billing_state();
			}

			$location = array(
				'country' => '',
				'state' => '',
				'city' => !empty($customer) ? $customer->get_billing_city() : '',
				'postcode' => !empty($customer) ? $customer->get_billing_postcode() : '',
			);

			if(self::doing_ajax() &&
				 (isset($_POST['action']) && ($_POST['action'] === 'woocommerce_update_order_review')) ||
				 // Fix WC 2.4. They silently removed the "action" parameter from the POST
				 // and added a "wc-ajax" in the GET. Yet one more nonsensical change from
				 // the WooCommerce "ninjas"
				 (isset($_REQUEST['wc-ajax']) && ($_REQUEST['wc-ajax'] === 'update_order_review'))) {
				// If user is on checkout page and changes the billing country, get the
				// country and State code and store it in the session
				if(check_ajax_referer('update-order-review', 'security', false)) {
					// Fetch the billing or shipping country and state, depending on the tax calculation
					// settings
					// @since 1.12.3.200203
					if($this->tax_based_on() === 'shipping') {
						if(isset($_POST[Definitions::ARG_CHECKOUT_SHIPPING_COUNTRY])) {
							$location['country'] = $_POST[Definitions::ARG_CHECKOUT_SHIPPING_COUNTRY];
						}
						if(isset($_POST[Definitions::ARG_CHECKOUT_SHIPPING_STATE])) {
							$location['state'] = $_POST[Definitions::ARG_CHECKOUT_SHIPPING_STATE];
						}
					}
					else {
						if(isset($_POST[Definitions::ARG_BILLING_COUNTRY])) {
							$location['country'] = $_POST[Definitions::ARG_BILLING_COUNTRY];
						}
						if(isset($_POST[Definitions::ARG_BILLING_STATE])) {
							$location['state'] = $_POST[Definitions::ARG_BILLING_STATE];
						}
					}
				}
			}

			// If changed the country on the cart, take the newly selected country
			if(!empty($_POST[Definitions::ARG_CALC_SHIPPING])) {
				$location['country'] = wc_clean($_POST[Definitions::ARG_CALC_SHIPPING_COUNTRY]);
				if(isset($_POST[Definitions::ARG_CALC_SHIPPING_STATE])) {
					$location['state'] = wc_clean($_POST[Definitions::ARG_CALC_SHIPPING_STATE]);
				}
			}

			// Check if "customer country" argument was posted
			if(empty($location['country'])) {
				if(isset($_POST[Definitions::ARG_AELIA_CUSTOMER_COUNTRY])) {
					$location['country'] = $_POST[Definitions::ARG_AELIA_CUSTOMER_COUNTRY];
				}
			}

			// Check if "customer State" argument was posted
			if(empty($location['state'])) {
				if(isset($_POST[Definitions::ARG_AELIA_CUSTOMER_STATE])) {
					$location['state'] = $_POST[Definitions::ARG_AELIA_CUSTOMER_STATE];
				}
			}

			// If no country selection was posted, check if one was stored in the session
			if(empty($location['country'])) {
				$location['country'] = Aelia_SessionManager::get_cookie(Definitions::SESSION_CUSTOMER_COUNTRY);
				$location['state'] = Aelia_SessionManager::get_cookie(Definitions::SESSION_CUSTOMER_STATE);
			}

			// If no country selection was posted, take it from customer's profile
			if(empty($location['country'])) {
				if(is_user_logged_in() && isset($woocommerce->customer)) {
					if($this->tax_based_on() === 'shipping') {
						// If tax calculation is based on shipping country, take customer's
						// shipping location from the profile
						$location['country'] = $woocommerce->customer->get_shipping_country();
						$location['state'] = $woocommerce->customer->get_shipping_state();
					}
					else {
						// By default, take customer's billing location from the profile
						if(aelia_wc_version_is('>=', '2.7')) {
							$location['country'] = $woocommerce->customer->get_billing_country();
							$location['state'] = $woocommerce->customer->get_billing_state();
						}
						else {
							$location['country'] = $woocommerce->customer->get_country();
							$location['state'] = $woocommerce->customer->get_state();
						}
					}
				}
			}

			// If no valid currency could be retrieved from customer's details, detect
			// it using visitor's IP address
			if(empty($location['country'])) {
				// Fetch the details of the city
				// @since 1.9.6.170828
				$city_data = $this->ip2location()->get_visitor_city();

				$location = array(
					'country' => $this->ip2location()->get_visitor_country(),
					'state' => $this->ip2location()->get_visitor_state(),
					// Try to detect city and postcode
					// @since 1.9.6.170828
					'city' => is_object($city_data) ? $city_data->city->name : '',
					'postcode' => is_object($city_data) ? $city_data->postal->code : '',
				);
			}

			// If everything fails, take shop's base country
			if(empty($location['country'])) {
				$countries = new \WC_Countries();
				$location['country'] = $countries->get_base_country();
				$location['state'] = $countries->get_base_state();
			}

			// Clean location values, for safety
			foreach($location as $key => $value) {
				$location[$key] = wc_clean($value);
			}

			// If the country and/or the state change, reset the city and postcode as
			// well, as most likely they are no longer valid
			// @since 1.9.6.170828
			if(($location['country'] != $original_country) || ($location['state'] != $original_state)) {
				$location['city'] = '';
				$location['postcode'] = '';
			}

			// Store location in user's session
			$this->store_customer_location($location);

			$this->customer_location = $location;
		}

		return apply_filters('wc_aelia_tdbc_customer_location', $this->customer_location);
	}

	/**
	 * Indicates if current user has declared to be exempt from tax/VAT.
	 */
	public function get_tax_exempt_status() {
		return Aelia_SessionManager::get_cookie(Definitions::SESSION_TAX_EXEMPT);
	}

	/**
	 * Stores the selection made by the user to indicate if they are exempt from tax/VAT.
	 *
	 * @since 1.15.3.201029
	 */
	public function set_tax_exempt_status() {
		// @var bool Indicates if the cookie for tax exemption was already stored during this page load.
		static $tax_exempt_cookie_stored = false;

		// Check that the "tax exempt" cookie wasn't already set, before setting it.
		// Setting it multiple times is pointless and can have a negative impact on
		// performance
		// @since 1.9.14.180324
		if(!$tax_exempt_cookie_stored && isset($_POST[Definitions::ARG_TAX_DISPLAY_SELECTION])) {
			$customer_is_tax_exempt = isset($_POST[Definitions::ARG_TAX_EXEMPT]) ? $_POST[Definitions::ARG_TAX_EXEMPT] : false;
			@Aelia_SessionManager::set_cookie(Definitions::SESSION_TAX_EXEMPT, $customer_is_tax_exempt);
			// Track the fact that we already stored the cookie, so that we don't store
			// it again unnecessarily
			// @since 1.9.14.180324
			$tax_exempt_cookie_stored = true;
		}
	}

	/**
	 * Returns the tax display setting (including/excluding tax) for current user.
	 *
	 * @param string setting_type Indicates for what price the setting should be
	 * retrieved. Valid values are the following:
	 * - 'sho_prices'
	 * - 'cart_prices'
	 *
	 * @return string "incl" if prices should be displayed including tax, "excl"
	 * otherwise.
	 */
	protected function get_tax_display_setting($prices_type) {
		// If the "tax exempt selector" feature is enabled, and customer declared
		// himself exempt from tax/VAT, display prices excluding tax
		if(self::settings()->get_tax_exempt_enabled() &&
			 $this->get_tax_exempt_status()) {
			return 'excl';
		}

		// Get user country and retrieve the tax settings configured for it
		$user_country = $this->get_customer_country();
		$result = $this->settings_controller()->get_tax_display_for_country($user_country, $prices_type);

		return $result;
	}

	/**
	 * Processes the "woocommerce_tax_display_shop" option, eventually replacing
	 * it with the one configured for visitor's country.
	 *
	 * @param string value The original value.
	 * @return string
	 */
	public function option_woocommerce_tax_display_shop($value) {
		$result = $this->get_tax_display_setting('shop_prices');

		return (empty($result)) ? $value : $result;
	}

	/**
	 * Processes the "woocommerce_tax_display_cart" option, eventually replacing
	 * it with the one configured for visitor's country.
	 *
	 * @param string value The original value.
	 * @return string
	 */
	public function option_woocommerce_tax_display_cart($value) {
		$result = $this->get_tax_display_setting('cart_prices');
		return (empty($result)) ? $value : $result;
	}

	/**
	 * Returns the suffix to append to prices.
	 *
	 * @param string default_value The default suffix to display if none is found
	 * for current customer country.
	 * @param WC_Product $product
	 * @param float $price
	 * @param int $qty
	 * @return string
	 */
	protected function get_tax_suffix($default_value, $product = null, $price ='', $qty = 1) {
		// If the "tax exempt selector" feature is enabled, and customer declared
		// himself exempt from tax/VAT, display prices excluding tax
		if(self::settings()->get_tax_exempt_enabled() &&
			 $this->get_tax_exempt_status()) {
			$result = self::settings()->get_tax_exempt_price_suffix();
		}
		else {
			$user_country = $this->get_customer_country();
			$result = self::settings()->get_price_suffix_for_country($user_country);
		}

		// Process placeholders
		// @since 1.10.2.190615
		if($product instanceof \WC_Product) {
			$replacements = array(
				'{price_including_tax}' => wc_price(wc_get_price_including_tax($product, array('qty' => $qty, 'price' => $price))),
				'{price_excluding_tax}' => wc_price(wc_get_price_excluding_tax($product, array('qty' => $qty, 'price' => $price))),
			);
		}
		else {
			// If a product instance was not passed, just remove the placeholders
			$replacements = array(
				'{price_including_tax}' => '',
				'{price_excluding_tax}' => '',
			);
		}
		$result = str_replace(array_keys($replacements), array_values($replacements), wp_kses_post($result));

		// Debug
		//var_dump($user_country, $result);
		return (empty($result)) ? $default_value : $result;
	}

	/**
	 * Processes the "woocommerce_price_suffix" option, eventually replacing
	 * it with the one configured for visitor's country.
	 *
	 * @param string value The original value.
	 * @param WC_Product $product
	 * @param float $price
	 * @param int $qty
	 * @return string
	 */
	public function woocommerce_get_price_suffix($price_suffix, $product, $price, $qty) {
		return '<span class="price-suffix">' . $this->get_tax_suffix($price_suffix, $product, $price, $qty) . '</span>';
	}

	public function wp_enqueue_scripts() {
		if(!wp_script_is('select2', 'enqueued')) {
			$wc_assets_path = str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/';

			wp_enqueue_script('select2');
			wp_enqueue_style('select2', $wc_assets_path . 'css/select2.css');
		}

		wp_enqueue_script('wc-country-select');
	}

	/**
	 * Processes the "excluding tax or vat" suffix, eventually replacing
	 * it with the one configured for visitor's country.
	 *
	 * @param string price_suffix The original value.
	 * @return string
	 */
	public function woocommerce_countries_ex_tax_or_vat($price_suffix) {
		return $this->get_tax_suffix($price_suffix);
	}

	/**
	 * Processes the "including tax or vat" suffix, eventually replacing
	 * it with the one configured for visitor's country.
	 *
	 * @param string price_suffix The original value.
	 * @return string
	 */
	public function woocommerce_countries_inc_tax_or_vat($price_suffix) {
		return $this->get_tax_suffix($price_suffix);
	}

	/**
	 * Sets customer's country on cart load.
	 */
	public function woocommerce_check_cart_items() {
		// Set customer's country to the pre-selected one, unless a different one was
		// explicitly chosen
		if(!isset($_POST[Definitions::ARG_CALC_SHIPPING_COUNTRY])) {
			$this->set_customer_location($this->get_customer_location());

			do_action('woocommerce_calculated_shipping');
		}
	}

	/**
	 * Determines if one of plugin's admin pages is being rendered. Override it
	 * if plugin implements pages in the Admin section.
	 *
	 * @return bool
	 */
	protected function rendering_plugin_admin_page() {
		$screen = get_current_screen();
		$page_id = $screen->id;

		return ($page_id == 'woocommerce_page_' . Definitions::MENU_SLUG);
	}

	/**
	 * Registers the script and style files needed by the admin pages of the
	 * plugin. Extend in descendant plugins.
	 */
	protected function register_plugin_admin_scripts() {
		// Scripts
		wp_register_script('chosen',
											 '//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js',
											 array('jquery'),
											 null,
											 true);

		// Styles
		wp_register_style('chosen',
												'//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css',
												array(),
												null,
												'all');
		wp_register_style('jquery-ui',
											'//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css',
											array(),
											null,
											'all');

		wp_enqueue_style('jquery-ui');
		wp_enqueue_style('chosen');

		wp_enqueue_script('jquery-ui-tabs');
		wp_enqueue_script('jquery-ui-sortable');
		wp_enqueue_script('chosen');

		parent::register_plugin_admin_scripts();
	}

	/**
	 * Loads the scripts required in the Admin section.
	 */
	public function load_admin_scripts() {
		parent::load_admin_scripts();
		$this->localize_admin_scripts();
	}

	/**
	 * Loads the settings that will be used by the Admin scripts.
	 */
	protected function localize_admin_scripts() {
		// Prepare parameters for common admin scripts
		$admin_scripts_params = array(
			'european_union_countries' => $this->wc()->countries->get_european_union_countries(),
		);

		wp_localize_script(static::$plugin_slug . '-admin',
											 'aelia_tdbc_params',
											 $admin_scripts_params);
	}

	/**
	 * Loads Styles and JavaScript for the frontend. Extend as needed in
	 * descendant classes.
	 */
	public function load_frontend_scripts() {
		// Enqueue the required Frontend stylesheets
		wp_enqueue_style(static::$plugin_slug . '-frontend');

		// JavaScript
		wp_enqueue_script(static::$plugin_slug . '-frontend');
		$this->localize_frontend_scripts();
	}

	/**
	 * Loads the settings that will be used by the frontend scripts.
	 *
	 * @since 1.7.5.150728
	 */
	protected function localize_frontend_scripts() {
		// Prepare parameters for frontend scripts
		$scripts_params = array(
			'handle_customer_state' => self::settings()->handle_customer_state(),
		);

		wp_localize_script(static::$plugin_slug . '-frontend',
											 'aelia_tdbc_params',
											 $scripts_params);
	}

	/**
	 * Returns the full path and file name of the specified template, if such file
	 * exists.
	 *
	 * @param string template_name The name of the template.
	 * @return string
	 */
	public function get_template_file($template_name) {
		$template = '';

		/* Look for the following:
		 * - yourtheme/woocommerce-aelia-currencyswitcher-{template_name}.php
		 * - yourtheme/woocommerce-aelia-currencyswitcher/{template_name}.php
		 */
		$template = locate_template(array(
			self::$plugin_slug . "-{$template_name}.php",
			self::$plugin_slug . '/' . "{$template_name}.php"
		));

		// If template could not be found, get default one
		if(empty($template)) {
			$default_template_file = $this->path('views') . '/' . "{$template_name}.php";

			if(file_exists($default_template_file)) {
				$template = $default_template_file;
			}
		}

		// If template does not exist, trigger a warning to inform the site administrator
		if(empty($template)) {
			$this->trigger_error(Definitions::INVALID_TEMPLATE,
													 E_USER_WARNING,
													 array(self::$plugin_slug, $template_name));
		}

		return $template;
	}

	/**
	 * Registers all the Widgets used by the plugin.
	 */
	public function register_widgets() {
		$this->register_widget('Aelia\WC\TaxDisplayByCountry\Customer_Country_Selector_Widget');
	}

	/**
	 * Returns the default checkout country.
	 *
	 * @param string checkout_country The country passed by WooCommerce.
	 * @return string
	 */
	public function default_checkout_country($checkout_country) {
		return $this->get_customer_country();
	}

	/**
	 * Returns the default checkout State/county.
	 *
	 * @param string checkout_state The country passed by WooCommerce.
	 * @return string
	 * @since 1.7.5.150728
	 */
	public function default_checkout_state($checkout_state) {
		return $this->get_customer_state();
	}

	/**
	 * Intercepts the fetching of user metadata, to alter the billing address if
	 * needed.
	 *
	 * @param mixed value The original value of the user metadata.
	 * @param int user_id The user for whom the data is being retrieved.
	 * @param string meta_key Optional. Metadata key. If not specified, retrieve
	 * all metadata for the specified object.
	 * @param bool $single Optional, default is false. If true, return only the
	 * first value of the specified meta_key. This parameter has no effect if
	 * meta_key is not specified.
	 * @return string
	 */
	public function get_user_metadata($value, $user_id, $meta_key, $single) {
		if(in_array($meta_key, array('billing_country', 'billing_state')) &&
			 (defined('WOOCOMMERCE_CHECKOUT') || is_checkout()) &&
			 ($user_id === wp_get_current_user()->ID)) {
			// If we are on checkout page and the billing country is requested for
			// current user, retrieve the one he (eventually) selected
			if($meta_key === 'billing_country') {
				return $this->get_customer_country();
			}

			// If we are on checkout page and the billing state is requested for
			// current user, retrieve the one he (eventually) selected
			if($meta_key === 'billing_state') {
				return $this->get_customer_state();
			}
		}
		return $value;
	}

	/**
	 * Alters the transient key to retrieve the prices of variable products,
	 * ensuring that the currency is taken into account.
	 *
	 * @param array cache_key_args The arguments that form the cache key.
	 * @param WC_Product product The product for which the key is being generated.
	 * @param bool display Indicates if the prices are being retrieved for display
	 * purposes.
	 * @return array
	 * @since WooCommerce 2.4+
	 * @since 1.7.10.150914
	 */
	public function woocommerce_get_variation_prices_hash($cache_key_args, $product, $display) {
		$cache_key_args[] = get_option('woocommerce_tax_display_shop');
		$cache_key_args[] = get_option('woocommerce_tax_display_cart');
		return $cache_key_args;
	}

	/**
	 * Triggered before cart totals are calculated.
	 * If the option is enabled, it overrides shops' base location in order to allow
	 * keeping product prices fixed, regardless of what VAT rate applies to the
	 * customer.
	 *
	 * @param WC_Cart cart The cart object.
	 * @since 1.7.0.150109
	 */
	public function woocommerce_before_calculate_totals($cart) {
		if(self::settings()->keep_prices_fixed()) {
			$this->set_shop_base_location_overrides(true);
		}
	}

	/**
	 * Triggered before a product price is retrieved.
	 * This method is just used as an event to override shop's base location for
	 * the "fixed product prices" feature.
	 *
	 * @param float price The price passed by WooCommerce.
	 * @return float The price received, unaltered.
	 * @since 1.7.0.150109
	 */
	public function woocommerce_get_price($price) {
		if(self::settings()->keep_prices_fixed()) {
			$this->set_shop_base_location_overrides(true);
		}
		return $price;
	}

	/**
	 * Triggered after the calculation of cart totals is completed.
	 * This method removes the overrides for shop's base location.
	 *
	 * @param array $cart
	 * @since 1.12.2.200127
	 */
	public function woocommerce_order_after_calculate_totals($cart) {
		$this->set_shop_base_location_overrides(false);
	}

	/**
	 * Sets or unsets the filters that will override shop's base location.
	 *
	 * @param bool enable_overrides Indicates if the filters should be set or unset.
	 * @since 1.7.0.150109
	 */
	protected function set_shop_base_location_overrides($enable_overrides) {
		global $woocommerce;
		// Allow 3rd parties to force the override of the base location for tax calculations
		// @since 1.15.0.200723
		if(empty($woocommerce->customer) && !apply_filters('wc_aelia_tdbc_force_base_location_overrides', false)) {
			$this->log(__('Customer object is empty, base location override skipped.', self::$text_domain));
			return;
		}

		$hooks = array(
			'woocommerce_countries_base_country' => 'override_base_country',
			'woocommerce_countries_base_state' => 'override_base_state',
			'woocommerce_countries_base_city' => 'override_base_city',
			'woocommerce_countries_base_postcode' => 'override_base_postcode',
		);

		/* Enable or disable the overrides, depending on the flag. This method could
		 * be written using call_user_func(), instead of using two identical loops.
		 * The loops have been chosen because they are easier to read and to understand.
		 */
		if($enable_overrides) {
			foreach($hooks as $hook => $filter) {
				add_filter($hook, array($this, $filter), 10, 1);
			}
		}
		else {
			foreach($hooks as $hook => $filter) {
				remove_filter($hook, array($this, $filter), 10, 1);
			}
		}
	}

	/**
	 * Indicates if WooCommerce is calculating the product price (ex. VAT) for
	 * a manual order.
	 *
	 * @return bool
	 * @since 1.13.0.200402
	 */
	protected static function calculating_product_price_for_manual_order() {
		return self::doing_ajax() &&
					 isset($_REQUEST['order_id']) && is_numeric($_REQUEST['order_id']) &&
					 isset($_REQUEST['action']) &&
					 in_array($_REQUEST['action'], array('woocommerce_add_order_item'));
	}

	/**
	 * Returns the taxable address from an order.
	 *
	 * @param int $order_id
	 * @return array
	 * @since 1.13.0.200402
	 */
	protected function get_order_taxable_address($order_id) {
		$order = wc_get_order($order_id);

		// Determine if taxes should be calculated based on the shipping or the
		// billing address
		$tax_based_on = get_option('woocommerce_tax_based_on');
		if(($tax_based_on === 'shipping') && !$order->get_shipping_country()) {
			$tax_based_on = 'billing';
		}

		// Return the shipping address, if taxes are based on it
		if($tax_based_on === 'shipping') {
			return array(
				$order->get_shipping_country(),
				$order->get_shipping_state(),
				$order->get_shipping_postcode(),
				$order->get_shipping_city(),
			);
		}

		// Return the billing address, as a default
		return array(
			$order->get_billing_country(),
			$order->get_billing_state(),
			$order->get_billing_postcode(),
			$order->get_billing_city(),
		);
	}

	/**
	 * Returns an array of elements containing customer's taxable address.
	 *
	 * @return array An array with the elements forming customer's address (country,
	 * state, city and postcode).
	 * @since 1.7.7.150831
	 */
	protected function get_customer_taxable_address() {
		// Fetch the taxable address from an order when adding an item on a
		// manual order
		// @since 1.13.0.200402
		if(self::calculating_product_price_for_manual_order()) {
			return $this->get_order_taxable_address($_REQUEST['order_id']);
		}

		// By default, get the taxable address from the customer instance
		global $woocommerce;
		$customer_taxable_address = isset($woocommerce->customer) ? $woocommerce->customer->get_taxable_address() : array();
		// Allow 3rd parties to override the taxable address
		// @since 1.15.0.200723
		return apply_filters('wc_aelia_tdbc_customer_taxable_address', $customer_taxable_address);
	}

	/**
	 * Overrides shop's base country using the corresponding information from
	 * customer's taxable address.
	 *
	 * @param string country The original base country.
	 * @return string The country, from customer's taxable address.
	 * @since 1.7.0.150109
	 */
	public function override_base_country($country) {
		// WC 2.4+
		// Prevent infinite recursion by using a semaphore
		static $processing = false;
		if($processing) {
			return $country;
		}
		$processing = true;

		$customer_taxable_address = $this->get_customer_taxable_address();
		// Customer's country is the first element in the returned array
		$country = !empty($customer_taxable_address[0]) ? $customer_taxable_address[0] : $country;

		$processing = false;
		return $country;
	}

	/**
	 * Overrides shop's base state using the corresponding information from
	 * customer's taxable address.
	 *
	 * @param string state The original base state.
	 * @return string The state, from customer's taxable address.
	 * @since 1.7.0.150109
	 */
	public function override_base_state($state) {
		// WC 2.4+
		// Prevent infinite recursion by using a semaphore
		static $processing = false;
		if($processing) {
			return $state;
		}
		$processing = true;

		$customer_taxable_address = $this->get_customer_taxable_address();
		// Customer's state is the second element in the returned array
		$state = !empty($customer_taxable_address[1]) ? $customer_taxable_address[1] : $state;

		$processing = false;
		return $state;
	}

	/**
	 * Overrides shop's base postcode using the corresponding information from
	 * customer's taxable address.
	 *
	 * @param string postcode The original base postcode.
	 * @return string The postcode, from customer's taxable address.
	 * @since 1.7.0.150109
	 */
	public function override_base_postcode($postcode) {
		// WC 2.4+
		// Prevent infinite recursion by using a semaphore
		static $processing = false;
		if($processing) {
			return $postcode;
		}
		$processing = true;

		$customer_taxable_address = $this->get_customer_taxable_address();
		// Customer's postcode is the third element in the returned array
		$postcode = !empty($customer_taxable_address[2]) ? $customer_taxable_address[2] : $postcode;

		$processing = false;
		return $postcode;
	}

	/**
	 * Overrides shop's base city using the corresponding information from
	 * customer's taxable address.
	 *
	 * @param string city The original base city.
	 * @return string The city, from customer's taxable address.
	 * @since 1.7.0.150109
	 */
	public function override_base_city($city) {
		// WC 2.4+
		// Prevent infinite recursion by using a semaphore
		static $processing = false;
		if($processing) {
			return $city;
		}
		$processing = true;

		$customer_taxable_address = $this->get_customer_taxable_address();
		// Customer's city is the fourth element in the returned array
		$city = !empty($customer_taxable_address[3]) ? $customer_taxable_address[3] : $city;

		$processing = false;
		return $city;
	}

	/**
	 * Sets hooks to register shortcodes.
	 */
	protected function set_shortcodes_hooks() {
		// Shortcode to render the country selector
		add_shortcode('aelia_tdbc_country_selector_widget',
									array('Aelia\WC\TaxDisplayByCountry\Customer_Country_Selector_Widget', 'render_country_selector'));
		// Shortcode to render the billing country selector, deprecated since 1.7.5.150728
		add_shortcode('aelia_tdbc_billing_country_selector_widget',
									array('Aelia\WC\TaxDisplayByCountry\Customer_Country_Selector_Widget', 'render_country_selector'));
	}

	/**
	 * Registers a widget class.
	 *
	 * @param string widget_class The class to register.
	 * @param bool stop_on_error Indicates if the function should raise an error
	 * if the Widget Class doesn't exist or cannot be loaded.
	 * @return bool True, if the Widget was registered correctly, False otherwise.
	 */
	protected function register_widget($widget_class, $stop_on_error = true) {
		register_widget($widget_class);
		return true;
	}

	/**
	 * Triggers actions when the admin section is initialised.
	 *
	 * @since 1.8.6.151105
	 */
	public function admin_init() {
		// Inform admins that the site uses a new widget
		Messages::admin_message(
			$this->_messages_controller->get_message(Definitions::WARN_NEW_COUNTRY_SELECTOR_WIDGET),
			array(
				'code' => Definitions::WARN_NEW_COUNTRY_SELECTOR_WIDGET,
				'dismissable' => true,
				'permissions' => 'manage_options'
		));
	}

	/**
	 * Sets the active country when it's changed on the My Account page.
	 *
	 * @param int user_id
	 * @param string load_address The modified address ("billing" or "shipping").
	 * @since 1.9.12.180106
	 */
	public function woocommerce_customer_save_address($user_id, $load_address) {
		$customer_location = array();
		$update_active_location = false;
		$set_shipping_country = false;

		$customer = new WC_Customer($user_id);
		switch($this->tax_based_on()) {
			case 'shipping':
				// Update customer's location, taking the shipping address
				if($load_address === 'shipping') {
					$customer_location['country'] = $customer->get_shipping_country();
					$customer_location['state'] = $customer->get_shipping_state();
					$customer_location['city'] = $customer->get_shipping_city();
					$customer_location['postcode'] = $customer->get_shipping_postcode();

					$set_shipping_country = true;
					$update_active_location = true;
				}
				break;
			case 'billing':
				// Update customer's location, taking the billing address
				if($load_address === 'billing') {
					$customer_location['country'] = $customer->get_billing_country();
					$customer_location['state'] = $customer->get_billing_state();
					$customer_location['city'] = $customer->get_billing_city();
					$customer_location['postcode'] = $customer->get_billing_postcode();
					$update_active_location = true;
				}
				break;
		}

		// If the location was changed, store the new location
		if($update_active_location) {
			$this->customer_location = $customer_location;
			$this->store_customer_location($this->customer_location);
		}

		//var_dump($this->tax_based_on(), $customer->get_shipping_country(), $customer->get_billing_country(), $customer_location);die();
	}

	/**
	 * Intercepts the "add order item" Ajax call, to override the billing and shipping
	 * address on the customer entity.
	 *
	 * @since 1.14.0.200608
	 */
	public function set_customer_address_for_add_order_item() {
		// If the "fixed prices" feature is disabled, do nothing
		if(!self::settings()->keep_prices_fixed()) {
			return;
		}

		// SEcurity checks
		if(!check_ajax_referer('order-item', 'security', false) || !current_user_can('edit_shop_orders')) {
			return;
		}

		// Check that the order is valid
		if(!isset($_POST['order_id'])) {
			return;
		}

		$order = wc_get_order($_POST['order_id']);
		if(!$order instanceof \WC_Order) {
			return;
		}

		// Set the billing address against the customer entity. This will ensure that WooCommerce
		// will deduct the taxes applicable to that address, rather than shop's base address
		WC()->customer->set_billing_country($order->get_billing_country());
		WC()->customer->set_billing_state($order->get_billing_state());
		WC()->customer->set_billing_postcode($order->get_billing_postcode());
		WC()->customer->set_billing_city($order->get_billing_city());

		// Set the billing address against the customer entity. This will ensure that WooCommerce
		// will deduct the taxes applicable to that address, rather than shop's base address
		WC()->customer->set_shipping_country(!empty($order->get_shipping_country()) ? $order->get_shipping_country() : $order->get_billing_country());
		WC()->customer->set_shipping_state(!empty($order->get_shipping_state()) ? $order->get_shipping_state() : $order->get_billing_state());
		WC()->customer->set_shipping_postcode(!empty($order->get_shipping_postcode()) ? $order->get_shipping_postcode() : $order->get_billing_postcode());
		WC()->customer->set_shipping_city(!empty($order->get_shipping_city()) ? $order->get_shipping_city() : $order->get_billing_city());
	}

	/**
	 * Intercepts the "create order" and "update order" REST API calls, to override the billing and shipping
	 * address and allow keeping the product prices fixed.
	 *
	 * @param mixed $result
	 * @param WP_REST_Server $rest_server
	 * @param WP_REST_Request $request
	 * @return mixed
	 * @since 1.15.0.200723
	 */
	public function set_tax_base_address_for_rest_api($result, $rest_server, $request) {
		// If the "fixed prices" feature is disabled, do nothing
		if(!apply_filters('wc_aelia_tdbc_keep_prices_fixed_for_api_calls', self::settings()->keep_prices_fixed())) {
			return $result;
		}

		// Only intercept "create order" and "update order" calls
		if(in_array($request->get_method(), array('POST', 'PUT')) && preg_match('/wc\/.+?\/orders/i', $request->get_route())) {
			$request_data = $request->get_params();

			// Ensure that the request contains valid data
			if(!is_array($request_data) || empty($request_data['billing'])) {
				return $result;
			}

			// Load the billing address passed with the API call
			$billing_country = isset($request['billing']['country']) ? $request['billing']['country'] : '';
			$billing_state = isset($request['billing']['state']) ? $request['billing']['state'] : '';
			$billing_postcode = isset($request['billing']['postcode']) ? $request['billing']['postcode'] : '';
			$billing_city = isset($request['billing']['city']) ? $request['billing']['city'] : '';

			if($this->tax_based_on() === 'shipping') {
				// If the tax is based on shipping, take the shipping address, if populated. If empty,
				// fall back to the billing address
				$tax_base_country = isset($request['shipping']['country']) ? $request['shipping']['country'] : $billing_country;
				$tax_base_state = isset($request['shipping']['state']) ? $request['shipping']['state'] : $billing_state;
				$tax_base_postcode = isset($request['shipping']['postcode']) ? $request['shipping']['postcode'] : $billing_postcode;
				$tax_base_city = isset($request['shipping']['city']) ? $request['shipping']['city'] : $billing_city;
			}
			else {
				// By default, take the billing address
				$tax_base_country = $billing_country;
				$tax_base_state = $billing_state;
				$tax_base_postcode = $billing_postcode;
				$tax_base_city = $billing_city;
			}

			// Force the override of the base location for tax calculations. This is necessary because the
			// logic relies on the presence of the WC()->customer object, which is not set during API calls.
			// The absence of the customer object would cause the "fixed prices" function not to run
			add_filter('wc_aelia_tdbc_force_base_location_overrides', '__return_true');

			// Prepare the taxable address
			$customer_taxable_address = array(
				$tax_base_country,
				$tax_base_state,
				$tax_base_postcode,
				$tax_base_city,
			);

			// Override the taxable address with the one prepared using the API request
			add_filter('wc_aelia_tdbc_customer_taxable_address', function($address) use ($customer_taxable_address) {
				return $customer_taxable_address;
			});
		}

		// Return the $result variable as it is, we don't need to modify it
		return $result;
	}
}

$GLOBALS[WC_Aelia_Tax_Display_By_Country::$plugin_slug] = WC_Aelia_Tax_Display_By_Country::factory();