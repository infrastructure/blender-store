<?php if(!defined('ABSPATH')) { exit; } // Exit if accessed directly
/*
Plugin Name: Aelia Tax Display by Country for WooCommerce
Plugin URI: https://aelia.co/shop/tax-display-by-country-for-woocommerce/
Description: Allows to display prices including/excluding tax depending on customer's location.
Author: Aelia
Author URI: https://aelia.co
Version: 1.15.4.201103
Text Domain: wc-aelia-tax-display-by-country
Domain Path: /languages
WC requires at least: 3.0
WC tested up to: 4.7
*/

require_once(dirname(__FILE__) . '/src/lib/classes/install/aelia-wc-taxdisplaybycountry-requirementscheck.php');
// If requirements are not met, deactivate the plugin
if(Aelia_WC_TaxDisplayByCountry_RequirementsChecks::factory()->check_requirements()) {
	require_once dirname(__FILE__) . '/src/plugin-main.php';
}
