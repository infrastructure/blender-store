��    <      �      �      �     �     �          )     C     V  9   s     �  	   �     �  9   �  .        H     V     _     e  8   s  '   �  A   �  *     /   A  M   q     �     �     �     �  I     <   `  *   �  =   �       -        K     h  #   �  "   �     �     �     �  /   �     	     *	     ;	     Q	     Y	     m	     }	     �	  !   �	  /   �	     �	  9   
     H
     T
     c
     {
     �
     �
  #   �
    �
     �     �               7     J  9   g     �  	   �     �  9   �  .        <     J     S     Y  8   g  '   �  A   �  *   
  /   5  M   e     �     �     �     �  I   
  <   T  *   �  =   �     �  -        ?     \  #   u  "   �     �     �     �  /   �               /     E     M     a     q     }  !   �  /   �     �  9        <     H     W     o     �     �  #   �   "I'm tax exempt" price suffix "I'm tax exempt" widget label Activate plugin Activating plugin "%s"... Activation failed. Add European Union countries Allow users to indicate that they are exempt from tax/VAT Change Location Countries Country selector label: Customer object is empty, base location override skipped. Displays a dropdown with all enabled countries Documentation Dropdown Error Excluding tax Groups without any selected countries have been removed. Handle customer's State/County/Province How to customise the look and feel of the country selector widget How to display the country selector widget I'm tax/VAT exempt, show me prices without tax. In this section you can configure the settings related to the user interface. Including tax Install plugin Installing plugin "%s"... Invalid plugin specified: "%s". Keep product price fixed, regardless of what tax rate applies to customer Plugin "%s" could not be loaded due to missing requirements. Plugin "%s" must be version "%s" or later. Plugin "<strong>%s</strong>" must be installed and activated. Plugin already active. Plugin installation failed. See errors below. Plugin is already installed. Plugin is not installed. Plugin requires "%s" PHP extension. Plugin requires PHP %s or greater. Price suffix Remove Save Changes Set the tax display settings for the countries. Show cart prices Show shop prices State selector label: Support Support Information Tax Calculation Tax Display Tax Display by Country Tax Display by Country - Settings The shortcode accepts the following parameters: The widget title (optional) This will activate the plugin. Would you like to proceed? Use default User Interface Using WordPress Widgets Using a shortcode Widget Title: Widget type: displays "dropdown" style selector. Project-Id-Version: Aelia Tax Display by Country for WooCommerce v1.7.0.150109
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: Thu Jul 30 2015 21:24:20 GMT+0100 (GMT Daylight Time)
Last-Translator: dzanella <businessdad75@gmail.com>
Language-Team: 
Language: English (UK)
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: utf-8
X-Generator: Loco - https://localise.biz/
X-Poedit-Language: English
X-Poedit-Country: UNITED KINGDOM
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-Basepath: 
X-Poedit-Bookmarks: 
X-Poedit-SearchPath-0: .
X-Textdomain-Support: yes
X-Loco-Target-Locale: en_GB "I'm tax exempt" price suffix "I'm tax exempt" widget label Activate plugin Activating plugin "%s"... Activation failed. Add European Union countries Allow users to indicate that they are exempt from tax/VAT Change Location Countries Country selector label: Customer object is empty, base location override skipped. Displays a dropdown with all enabled countries Documentation Dropdown Error Excluding tax Groups without any selected countries have been removed. Handle customer's State/County/Province How to customise the look and feel of the country selector widget How to display the country selector widget I'm tax/VAT exempt, show me prices without tax. In this section you can configure the settings related to the user interface. Including tax Install plugin Installing plugin "%s"... Invalid plugin specified: "%s". Keep product price fixed, regardless of what tax rate applies to customer Plugin "%s" could not be loaded due to missing requirements. Plugin "%s" must be version "%s" or later. Plugin "<strong>%s</strong>" must be installed and activated. Plugin already active. Plugin installation failed. See errors below. Plugin is already installed. Plugin is not installed. Plugin requires "%s" PHP extension. Plugin requires PHP %s or greater. Price suffix Remove Save Changes Set the tax display settings for the countries. Show cart prices Show shop prices State selector label: Support Support Information Tax Calculation Tax Display Tax Display by Country Tax Display by Country - Settings The shortcode accepts the following parameters: The widget title (optional) This will activate the plugin. Would you like to proceed? Use default User Interface Using WordPress Widgets Using a shortcode Widget Title: Widget type: displays "dropdown" style selector. 