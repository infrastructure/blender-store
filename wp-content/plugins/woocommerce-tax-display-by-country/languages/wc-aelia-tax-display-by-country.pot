# Loco Gettext template
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Aelia Tax Display by Country for WooCommerce v1.7.0."
"150109\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"POT-Revision-Date: Mon Jan 18 2016 16:25:51 GMT+0000 (GMT Standard Time)\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Aelia <support@aelia.co>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Generator: Loco - https://localise.biz/\n"
"X-Poedit-Language: English\n"
"X-Poedit-Country: UNITED KINGDOM\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;"
"esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;"
"esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2\n"
"X-Poedit-Basepath: \n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-SearchPath-0: .\n"
"X-Textdomain-Support: yes"

#: ../src/plugin-main.php:707
msgid "Customer object is empty, base location override skipped."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:137
#, php-format
msgid "Plugin requires \"%s\" PHP extension."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:204
#, php-format
msgid "Plugin \"%s\" must be version \"%s\" or later."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:217
#, php-format
msgid "Plugin \"<strong>%s</strong>\" must be installed and activated."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:238
#, php-format
msgid "Plugin requires PHP %s or greater."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:397
msgid "Install plugin"
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:406
msgid "Activate plugin"
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:457
#, php-format
msgid "Plugin \"%s\" could not be loaded due to missing requirements."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:498
msgid "This will activate the plugin. Would you like to proceed?"
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:530
#, php-format
msgid "Installing plugin \"%s\"..."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:535
msgid "Plugin is already installed."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:550
msgid "Plugin installation failed. See errors below."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:573
#, php-format
msgid "Activating plugin \"%s\"..."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:578
msgid "Plugin is not installed."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:584
msgid "Plugin already active."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:592
msgid "Activation failed."
msgstr ""

#: ../src/lib/classes/install/aelia-wc-requirementscheck.php:658 ..
#: src/lib/classes/install/aelia-wc-requirementscheck.php:704
#, php-format
msgid "Invalid plugin specified: \"%s\"."
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:36
msgid "Add European Union countries"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:50
msgid "Including tax"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:51
msgid "Excluding tax"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:62 ..
#: src/lib/classes/settings/settings-renderer.php:84
msgid "Tax Display"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:66 ..
#: src/lib/classes/settings/settings-renderer.php:93
msgid "Tax Calculation"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:70
msgid "Documentation"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:74
msgid "Support"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:102
msgid "User Interface"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:120
msgid "Support Information"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:139
msgid "Set the tax display settings for the countries."
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:159
msgid "Keep product price fixed, regardless of what tax rate applies to customer"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:177
msgid "Handle customer's State/County/Province"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:198
msgid "Allow users to indicate that they are exempt from tax/VAT"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:215
msgid "\"I'm tax exempt\" widget label"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:228
msgid "\"I'm tax exempt\" price suffix"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:233 ..
#: src/lib/classes/settings/settings-renderer.php:551 ..
#: src/lib/classes/settings/settings-renderer.php:650
msgid "Use default"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:245
msgid "Tax Display by Country"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:264
msgid "Tax Display by Country - Settings"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:313
msgid "In this section you can configure the settings related to the user interface."
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:319
msgid "How to display the country selector widget"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:331
msgid "Using WordPress Widgets"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:344
msgid "Using a shortcode"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:352
msgid "The shortcode accepts the following parameters:"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:359
msgid "The widget title (optional)"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:375
msgid "How to customise the look and feel of the country selector widget"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:386
msgid "displays \"dropdown\" style selector."
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:450
msgid "Remove"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:465
msgid "Countries"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:466
msgid "Show cart prices"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:469
msgid "Show shop prices"
msgstr ""

#: ../src/lib/classes/settings/settings-renderer.php:470
msgid "Price suffix"
msgstr ""

#: ../src/lib/classes/settings/settings.php:44
msgid "I'm tax/VAT exempt, show me prices without tax."
msgstr ""

#: ../src/lib/classes/settings/settings.php:245
msgid "Groups without any selected countries have been removed."
msgstr ""

#: ../src/lib/classes/widgets/customer-country-selector-widget.php:42
msgid "Dropdown"
msgstr ""

#: ../src/lib/classes/widgets/customer-country-selector-widget.php:44
msgid "Displays a dropdown with all enabled countries"
msgstr ""

#: ../src/lib/classes/widgets/customer-country-selector-widget.php:170
msgid "Error"
msgstr ""

#: ../src/lib/classes/widgets/customer-country-selector-widget.php:189
msgid "Widget Title:"
msgstr ""

#: ../src/lib/classes/widgets/customer-country-selector-widget.php:198
msgid "Country selector label:"
msgstr ""

#: ../src/lib/classes/widgets/customer-country-selector-widget.php:210
msgid "State selector label:"
msgstr ""

#: ../src/lib/classes/widgets/customer-country-selector-widget.php:221
msgid "Widget type:"
msgstr ""

#: ../src/views/country-selector-widget-dropdown.php:77
msgid "Change Location"
msgstr ""

msgid "Save Changes"
msgstr ""
