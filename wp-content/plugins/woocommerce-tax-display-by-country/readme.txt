=== WooCommerce Tax Display by Country ===
Requires at least: 4.0
Tested up to: 5.5.3
Tags: woocommerce, aelia, licensing, automatic updates
License: GPL-3.0

Allows to display prices including/excluding tax depending on customer's location.

== Description ==
This plugin allows to specify how prices should be displayed (with or without tax) to customers from different countries. Thanks to this plugin, you will be able to display prices "all inclusive" to some customers, and prices excluding tax/VAT to others, making your pricing policy clearer.

= Requirements =
* WordPress 3.6 or newer.
* PHP 5.4 or newer.
* WooCommerce 3.0 or newer.
* [AFC plugin for WooCommerce](https://aelia.co/downloads/wc-aelia-foundation-classes.zip) 1.6.10.151105 or later.

== Installation ==
1. Extract the zip file and drop the contents in the wp-content/plugins/ directory of your WordPress installation.
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to WooCommerce > Tax Display by Country to configure how the prices will be displayed, based on the countries you will indicate.
4. Optionally, go to Appearance > Widgets and display the Billing Country Selector widget wherever you like. Alternatively, you can display the widget usinga shortcode. Please refer to the instructions on the Tax Display by Country page for more details.
5. That's it! Now your Customers will be able to select the billing country when they visit your shop, and see the prices with/without taxes, as you specified.

== Changelog ==

= 1.15.4.201103 =
* Removed upgrade message related to the new country selector plugin. The message is no longer relevant in new versions of the plugin.
* Updated supported WooCommerce versions.

= 1.15.3.201029 =
* Tweak - Moved logic to store the "I'm VAT exempt" selection to run earlier. This is to reduce the possibility that the related cookie can't be stored due to other actors sending output to the browser.
* Updated supported WooCommerce versions.

= 1.15.2.201005 =
* Updated supported WooCommerce versions.
* Updated supported WordPress versions.

= 1.15.1.200904 =
* Updated supported WooCommerce versions.

= 1.15.0.200813 =
* Updated supported WordPress and WooCommerce versions.

= 1.15.0.200723 =
* Feature - Extended the "fixed product prices" feature to  support "create order" and "update order" REST API calls.

= 1.14.0.200625 =
* Updated supported WooCommerce versions.

= 1.14.0.200608 =
* Improvement - Extended "fixed product prices" feature to support manual orders.

= 1.13.2.200603 =
* Updated supported WooCommerce versions.

= 1.13.1.200428 =
* Updated requirement checking class.
* Updated requirements.
* Updated supported WooCommerce versions.

= 1.13.0.200402 =
* Improvement - Improved support for manual orders in "fixed product prices" feature.

= 1.12.4.200323 =
* Updated supported WooCommerce versions.

= 1.12.3.200203 =
* Tweak - Improved logic used to detect the country selection when taxes are calculated based on shipping country.

= 1.12.2.200127 =
* Fix - Fixed incorrect tax calculation with "fixed prices" option in WooCommerce 3.9.
* Updated supported WooCommerce versions. Minimum version is now WC 3.5.

= 1.12.1.191220 =
* Tweak - Removed logic used to set city and post code, as they are not used by the plugin.
* Updated supported WooCommerce versions.

= 1.12.0.191028 =
* Fix - Fixed bug caused by function `WC_Customer::set_location()`, which caused customer's address to be empty on the checkout page.

= 1.11.1.191015 =
* Fix - Fixed call to filter `widget_title`.
* Updated supported WooCommerce versions.

= 1.11.0.190719 =
* Tweak - Dropped usage of obsolete cookie "aelia_billing_country".

= 1.10.2.190615 =
* Feature - Added support for placeholders `{price_including_tax}` and `{price_excluding_tax}` in the price suffix.

= 1.10.1.190509 =
* Fix - Fixed bug on the checkout page, which caused the shipping country to be ignored when the "Ship to another address" option was selected.

= 1.10.0.190322 =
* Tweak - Added check on frontend script, to handle the case where the `wc_cart_fragments_params` variable has been removed by disabling WooCommerce's cart fragments.
