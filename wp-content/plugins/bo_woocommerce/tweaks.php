<?php
/*
Plugin Name: A bag of tweaks
Plugin URI: http://en.bainternet.info
Description:
Version:
Author: Bart Veldhuizen
Author URI: http://www.blendernation.com
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

require( ABSPATH . WPINC . '/pluggable.php' );

function fontawesome_dashboard() {
   wp_enqueue_style('fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css', '', '4.0.3', 'all');
}

add_action('admin_init', 'fontawesome_dashboard');

/***
 * Determine the correct merchant account ID for different currencies
 */
function bo_get_braintree_merchant_account_id( $order_id ) {

	$curr = get_post_meta( $order_id, '_order_currency', true );

	switch( $curr ) {
		case 'USD':
			return BO_BRAINTREE_MERCHANT_ID_USD;
		case 'EUR':
			return BO_BRAINTREE_MERCHANT_ID_EUR;
		default:
			// wp_die( 'Invalid currency selected in bo_Gateway_Braintree::get_merchant_account_id()' );
			return BO_BRAINTREE_MERCHANT_ID_EUR;
	}
}
add_filter( 'wc_braintree_get_merchant_account_id', 'bo_get_braintree_merchant_account_id' );

/***
 * login errors
 */


add_filter('login_errors','bo_login_error_message');

function bo_login_error_message($error){

    //check if that's the error you are looking for
    $pos = strpos($error, 'Invalid username');
    if (is_int($pos)) {
        //its the right error so you can overwrite it
        $error = "<strong>Error</strong> Invalid username";
    }

    return $error;
}


/***
 * Blender Cloud API
 */
include( 'api.php' );


/**
 * @param $subscription WC_Subscription
 * @param $manual_or_automatic string either 'manual' or 'automatic'
 * @return bool true when an order item has a 'pa_renewal-type' attribute with the given value.
 */
function bo_has_item_of_renewal_type($subscription, $manual_or_automatic) {
    foreach ($subscription->get_items() as $item_id => $item) {
        $renewal_type = $item->get_meta('pa_renewal-type');
        if ($renewal_type == $manual_or_automatic) {
            return true;
        }
    }

    return false;
}

/**
 * Determines whether this subscription is supposed to be manually renewed using
 * manual payments. This looks at our own custom metadata field 'pa_renewal-type',
 * and not at what WooCommerce thinks about this subscription.
 *
 * @param $subscription WC_Subscription
 * @return bool true when manual, false when not (or unable to determine).
 */
function bo_is_manual_payment($subscription) {
    /* No payment method, no automatic renewal. */
    if (empty($subscription->get_payment_method())) {
        return true;
    }

    /* The subscription could be BACS, which doesn't support automatic payment.
     * I (Sybren) don't know of a way to test this with a proper WooCommerce API
     * call, so I'll just test against the payment gateway ID instead. */
    if ($subscription->get_payment_method() == 'bacs') {
        return true;
    }

    /* If any of the line items are set to manual, the subscription is set to manual. */
    if (bo_has_item_of_renewal_type($subscription, 'manual')) {
        return true;
    }

    /* Fall back to what WooCommerce thinks about this subscription. This is
     * needed to also take their extra checks into account, such as the call
     * to WC_Subscriptions::is_duplicate_site().
     */
    return $subscription->is_manual();
}


/***
 * Forces subscription to manual payment when this is set in its metadata.
 * This is required because WooCommerce Subscriptions will always enable
 * automatic payment when the payment gateway supports this, regardless of
 * the customer's wishes.
 */
add_action( 'woocommerce_checkout_subscription_created', 'bo_subscription_handle_manual_flag' );
add_action( 'woocommerce_subscription_payment_complete', 'bo_subscription_handle_manual_flag' );
function bo_subscription_handle_manual_flag($post_id) {
    $subs = wcs_get_subscription($post_id);
    if ($subs === false) return false;  // not a subscription after all.

    $manual = bo_is_manual_payment($subs);

    if (WC_Subscriptions::is_duplicate_site()) {
        // Don't update the subscription when running on a staging site.
        // WooCommerce Subscriptions disables automatic renewals on staging
        // sites, and thus this would mark all automatic subscriptions passing
        // through this function as manual.
        return $manual;
    }

    // Mark as manual payment when our metadata says it should be.
    $subs->set_requires_manual_renewal($manual);
    $subs->save();

    // Add a note that the subscription will be manual, even though the user requested automatic.
    if ($manual && bo_has_item_of_renewal_type($subs, 'automatic')) {
        $subs->add_order_note(
            "Blender Tweak: subscription is set to manual renewal, " .
            "even though the user requested automatic. This is to " .
            "account for capabilities of the chosen payment method \"$subs->payment_method\"."
        );
    }

    return $manual;
}


/***
 * Update formatted order number.
 */
add_action( 'save_post', 'bo_save_post' );
function bo_save_post( $post_id ) {

    $post = get_post( $post_id );

	// fire only when saving orders
	if ( 'shop_order' != $post->post_type ) {
		return;
	}

	// do not update formatted order numbers that have already been set
	// also, do not update payment status of existing orders
	if( get_post_meta( $post_id, '_order_number_formatted') == '' ) {
		// update invoice number
		$o = new WC_Seq_Order_Number_Pro;
		$o->set_sequential_order_number( $post_id );
		$order_number_a = get_post_meta( $post_id, '_order_number' );
		$order_number = $order_number_a[0];

		$order_number_prefix = '{YY}{MM}{DD}-{EMAIL}-';
		$order_number_suffix = '';
		$order_number_length = 0;

		update_post_meta( $post_id, '_order_number_formatted', WC_Seq_Order_Number_Pro::format_order_number( $order_number, $order_number_prefix, $order_number_suffix, $order_number_length, $post_id ) );
	}
}

/***
 * Remove phone field from checkout.
 *
 * Documentation: http://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
 */
// Hook in
add_filter( 'woocommerce_checkout_fields' , 'bo_custom_override_checkout_fields' );
function bo_custom_override_checkout_fields( $fields ) {
     unset($fields['billing']['billing_phone']);
     return $fields;
}

/***
 * Remove phone field from billing address page
 */
add_filter( 'woocommerce_billing_fields', 'bo_custom_override_billing_fields' );
function bo_custom_override_billing_fields( $fields ) {
     unset($fields['billing_phone']);
     return $fields;
}

/***
 * Allow password of almost any strength.
 */
add_filter('woocommerce_min_password_strength', 'bo_custom_override_min_password_strength');
function bo_custom_override_min_password_strength($strength) {
	return 2;
}


add_filter('wcs_view_subscription_actions', 'bo_custom_wcs_view_subscription_actions', 1);
function bo_custom_wcs_view_subscription_actions($actions) {
    if (array_key_exists('cancel', $actions)) {
         // Change the URL so that it points to a confirmation page, rather than an immediate cancel.
        $query = http_build_query(array(
            'confirm' =>  $actions['cancel']['url'],
            'abort' => $_SERVER['REQUEST_URI'],  // an abort should just go to the current page.
        ), '', '&amp;');
        $actions['cancel']['url'] = '/confirm-cancel-subscription/?' . $query;
    }

    if (array_key_exists('resubscribe', $actions)) {
        // Don't use WooCommerce's resubscribe, just direct the user to a new subscription.
        $actions['resubscribe']['url'] = '/product/membership/';
    }

    unset($actions['reactivate']);
    return $actions;
}

//! Automatically complete orders when they only contain subscriptions.
add_action('woocommerce_order_status_processing', 'bo_custom_autocomplete_subscription_order', 30);
function bo_custom_autocomplete_subscription_order($order_id)
{
    $order = new WC_Order($order_id);

    // Ignore empty orders.
    if (sizeof($order->get_items()) <= 0)
        return;

    $subscription_types = ['subscription', 'subscription_variation', 'variable-subscription'];
    foreach ($order->get_items() as $item) {
        $product = $item->get_product();
        if (!$product)
            continue;

        $is_subscription = $product->is_type($subscription_types);
        if ($is_subscription)
            continue;

        // We found a non-subscription item in the order, so we can't auto-complete it.
        return;
    }

    // Nothing found that hinders auto-completion, so go for it!
    $order->update_status('completed');
}

//! Show links to the user's orders and subscriptions on the user's profile in the admin.
if ( current_user_can('edit_users') ) {
    add_action('personal_options', 'bo_custom_show_orders_subscriptions_links');
    add_action('edit_user_profile', 'bo_custom_show_orders_subscriptions_links');
}
function bo_custom_show_orders_subscriptions_links($user)
{
    $user_id = $user->ID;
    $order_url = htmlentities("edit.php?s&post_type=shop_order&post_status=all&_customer_user=$user_id");
    $subs_url = htmlentities("edit.php?s&post_type=shop_subscription&post_status=all&_customer_user=$user_id");
?>
    <table class="form-table" style="min-height: 86px; background: url(/wp-content/uploads/2014/07/blender-socket-300x86.png) no-repeat right top"><tbody>
        <tr>
            <th scope='row'>Quick Links</th>
            <td><a href='<?php _e($order_url)?>'>Orders</a><br>
                <a href='<?php _e($subs_url)?>'>Subscriptions</a></td>
        </tr>
    </tbody></table>
<?php
}

//! Un-check 'ship to a different address' by default.
add_filter('woocommerce_ship_to_different_address_checked', 'bo_ship_to_different_address_checked');
function bo_ship_to_different_address_checked()
{
    return false;
}

// Force the PDF Invoices temp directory to a path we're allowed to write to.
// Search for "open_basedir restriction" for more info.
add_filter('wpo_wcpdf_tmp_path', 'bo_wcpdf_tmp_path');
function bo_wcpdf_tmp_path()
{
    return '/var/tmp/php/pdf-invoices/';
}

// Show the subscription status according to WooCommerce.
add_action('woocommerce_admin_order_data_after_order_details', 'bo_admin_show_subscription_status');
function bo_admin_show_subscription_status($subs)
{
    // Check if WC Subscriptions plugin is available,
    // to avoid breaking Edit Order page when it's not.
    if (!function_exists('wcs_is_subscription')) return;

    if (!wcs_is_subscription($subs)) return;

    $should = bo_is_manual_payment($subs) ? "manual" : "automatic";
    $renewal = $subs->get_requires_manual_renewal() ? "manual" : "automatic";

    if ($should == $renewal) {
        print("<p>Subscription renewal: $renewal</p>");
        return;
    }

    if (WC_Subscriptions::is_duplicate_site()) {
        // WooCommerce Subscriptions disables automatic renewals on staging
        // sites, and thus this would mark all automatic subscriptions passing
        // through this function as "should be manual".
        print("<p>Subscription renewal: $renewal<br>");
        print("<small>The check on this has been disabled, this is a staging/duplicate site.</small></p>");
        return;
    }

    $subs_id = $subs->get_id();
?>
    <p style='color: red; font-weight: bold;'>
        Subscription renewal is <?php _e($renewal) ?> but should be <?php _e($should)?>!
        <a class='button' href='/fix-subscription-renewal.php?post=<?php _e($subs_id) ?>'>Fix now</a>
    </p>
<?php
}


/**
 * Ensure that there is always a default payment token.
 *
 * The BrainTree payment gateway doesn't set new payment tokens as default,
 * which results in "Add new credit card" being the default option even when
 * somebody already added their credit card.
 *
 * Note that this function does *NOT* set the default in the database; it just
 * changes the in-memory data.
 *
 * This action is called from SV_WC_Payment_Gateway_Payment_Tokens_Handler.
 */
add_action('wc_payment_gateway_braintree_credit_card_payment_tokens_loaded', 'bo_ensure_default_token_after_payment', 10, 2);
add_action('wc_payment_gateway_braintree_paypal_payment_tokens_loaded', 'bo_ensure_default_token_after_payment', 10, 2);
function bo_ensure_default_token_after_payment($tokens, $tokens_handler) {
    if (count($tokens) == 0) {
        return;
    }

    foreach ($tokens as $token) {
        if ($token->is_default()) {
            // If a token has already been marked as default, do nothing.
            return;
        }
    }

    // Set the first token as the default for this payment gateway.
    $first_token = array_values($tokens)[0];
    $first_token->set_default(true);
}


/**
 * When a subscription moves to (pending) cancel status, it is no longer possible to resurrect it.
 * All outstanding orders need to be cancelled as well, otherwise people can still pay for them and
 * assume they'll get their subscription back.
 */
add_action('woocommerce_subscription_status_cancelled', 'bo_cancel_subscription_orders');
add_action('woocommerce_subscription_status_expired', 'bo_cancel_subscription_orders');
add_action('woocommerce_subscription_status_pending-cancel', 'bo_cancel_subscription_orders');
function bo_cancel_subscription_orders($subscription)
{
    $orders = $subscription->get_related_orders('all', array('parent', 'renewal'));

    $order_statuses_to_cancel = array('pending', 'on-hold', 'failed');
    foreach ($orders as $item_id => $item) {
        $status = $item->get_status();
        if (!in_array($status, $order_statuses_to_cancel)) continue;

        $item->set_status('cancelled');
        $item->add_order_note('Order was cancelled as the subscription was cancelled');
        $item->save();
    }
}
