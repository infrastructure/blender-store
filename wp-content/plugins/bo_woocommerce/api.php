<?php
		
function blendercloud_api( $atts ) {
	if(!$_GET['blenderid']) {
		http_response_code(400);
		echo json_encode(array('error'=>"no blenderid given."), JSON_PRETTY_PRINT);
		die();
	}

	$user_data = array(	
		'shop_id' => '0',
		'cloud_access' => 0,
		'paid_balance' => 0,
		'balance_currency' => 'EUR',
		'start_date' => '',
		'expiration_date' => '1970-01-01 00:00:00',
	);

	// map blenderid to userid
	$args = array(
		'search'         => $_GET['blenderid'],
		'search_columns' => array( 'user_email' )
	);

	$user_query = new WP_User_Query( $args );	

 	// Get the results from the query, returning the first user
 	$users = $user_query->get_results();
	$count = count($users);
	if($count > 1) {
		http_response_code(400);
		echo json_encode(array('error'=>"User not unique, query returned $count users."), JSON_PRETTY_PRINT);
		die();
	}

	if( !empty( $users ) ) {

		$user_id = $users[0]->ID;
		
		// Set initial user data
		$user_data['shop_id'] = $user_id;
		
		// Get orders and calculate paid balance
		$orders = blender_get_users_subscription_orders( $user_id );
		
		// Get subscriptions based on the user id
		$subscriptions = wcs_get_users_subscriptions( $user_id );
		
		if( isset( $orders ) && !empty( $orders ) ) {
			
			// Loop through each subscription and calculate order totals
			foreach( $orders as $order_id ) {
				
				$post_statusses = array( 'publish', 'completed', 'wc-completed' );
				$post_status = get_post_status( $order_id );
				$initial_order = get_post_meta( $order_id, '_original_order', true );
				
				// Only proceed if it's a completed order
				if( in_array( $post_status, $post_statusses ) ) {
					
					if( empty( $initial_order ) && $initial_order != $order_id ) {
						
						// Calculate initial order
						$user_data['balance_currency'] = get_post_meta( $order_id, '_order_currency', true );
						$user_data['paid_balance'] += get_post_meta( $order_id, '_order_total', true );
						
					} else {
						
						$_order_recurring_total = get_post_meta( $order_id, '_order_recurring_total', true );
						
						if( empty( $_order_recurring_total ) ) {
							
							// calculate order total instead
							$user_data['paid_balance'] += get_post_meta( $order_id, '_order_total', true );
							
						} else {
							
							// Calculate recurring order
							$user_data['paid_balance'] += get_post_meta( $order_id, '_order_recurring_total', true );
							
						}
						
					}
					
					// If there are no subscriptions found, check orders for special subscription cases
					if( !isset( $subscriptions ) || empty( $subscriptions ) ) {
						
						$order = new WC_Order( $order_id );
						
						foreach ( $order->get_items() as $item_id => $item_details ) {
							
							if( $item_details['name'] == "Pre-Paid membership: 18 Months" ) {
								
								// set start date
								$start_date = new DateTime( $order->order_date );
								$user_data['start_date'] = $start_date->format('Y-m-d H:i:s');
								
								// set expiration date 
								$user_data['expiration_date'] = $start_date->modify('+18 month')->format('Y-m-d H:i:s');
								
								// set cloud access if the expiration date has not been reached
								if( date( 'YmdHis', strtotime( $user_data['expiration_date'] ) ) > date('YmdHis') ) {
									$user_data['cloud_access'] = 1;
								}
								
							}
							
						}
						
					}
				
				}
				
			}
		}
		
		
		// Loop through the subscriptions and determin 
		if( isset( $subscriptions ) && !empty( $subscriptions ) ) {
			
			// Loop through each subscription and calculate order totals
			foreach( $subscriptions as $subscription ) {
				// Don't overwrite status from previously seen subscriptions, unless this is an active subscription.
				// (condition spread over multiple lines, see below).
				if (!isset($user_data['subscription_status'])) {
					$user_data['subscription_status'] = $subscription->post_status;
				}

				// Give cloud access to users with an active subscription
				if($subscription->has_status('active') || $subscription->has_status('pending-cancel')) {
					$user_data['subscription_status'] = $subscription->post_status;

					$user_data['cloud_access'] = 1;
					$user_data['start_date'] = $subscription->order_date;
					
					if ($subscription->has_status('active')) {
						$user_data['expiration_date'] = $subscription->get_date('next_payment');
					} else {
                        $exp_date = $subscription->get_date('end');
                        if ($exp_date == 0)
                            $exp_date = $subscription->get_date('trial_end');
						$user_data['expiration_date'] = $exp_date;
					}
				}

			}
		}
				
	}
    // Sybren: if we can't get the expiration date from the subscription,
    // expire it in a day. We can always check again, and at some point in
    // time the store will set the status to Cancelled anyway.
    if ($user_data['expiration_date'] == 0) {
        $user_data['expiration_date'] = date_create()->modify('+1 day')->format('Y-m-d H:i:s');
        $user_data['expiration_date_approximate'] = true;
    }
	
	echo json_encode($user_data, JSON_PRETTY_PRINT);
	die();
}

add_shortcode('blendercloud_api', 'blendercloud_api');	

function blender_get_users_subscription_orders( $user_id = 0 ) {
	
	global $wpdb;

	if ( 0 === $user_id ) {
		$user_id = get_current_user_id();
	}

	// Get all the customers orders
	$order_ids = $wpdb->get_col( $wpdb->prepare(
		"SELECT ID FROM $wpdb->posts as posts
			LEFT JOIN $wpdb->postmeta as postmeta ON posts.ID = postmeta.post_id
			WHERE post_type = 'shop_order'
			AND postmeta.meta_key = '_customer_user'
			AND postmeta.meta_value = %s;
		",
		$user_id
		)
	);
	
	/*
	// Skip this for now since somehow not all orders show up possibly because of faulty imports
	foreach ( $order_ids as $index => $order_id ) {
		if ( ! WC_Subscriptions_Order::order_contains_subscription( $order_id ) ) {
			unset( $order_ids[ $index ] );
		}
	}
	*/
	
	// Normalise array keys
	$order_ids = array_values( $order_ids );
	
	return $order_ids;
}
	
?>
