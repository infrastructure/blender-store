<?php
/**
 * Aggregation of product sales per day.
 * Author: Sybren A. Stüvel
 */

/* Requires the following table definition:
    CREATE TABLE aggr_product_sales
    (
        ID BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
        day DATE NOT NULL,
        product_name VARCHAR(200),
        items_sold INT(10) UNSIGNED NOT NULL
    );
    CREATE UNIQUE INDEX aggr_product_sales_day_product_name_pk ON aggr_product_sales (day, product_name);
*/

/**
 * Updates or creates a row in the product sales aggregation table.
 * There can be only one row for each ($date, $product_name) tuple.
 *
 * @param $date string
 * @param $product_name string
 * @param $items_sold int
 * @param $increment boolean true to add, or false (the default) to set.
 */
function blpltw_update_sold($date, $product_name, $items_sold, $increment=false) {
    global $wpdb;

    if ($increment) $update = "items_sold=items_sold + $items_sold";
    else $update = "items_sold=$items_sold";

    $query = "
        INSERT INTO aggr_product_sales (day, product_name, items_sold)
        VALUES ('$date', '$product_name', $items_sold)
        ON DUPLICATE KEY UPDATE $update;
    ";
    $wpdb->query($query);
}

/**
 * Aggregates all product sales for today, only counting completed sales.
 *
 * @param string $date
 * @return array of (product, sold quantity) tuples.
 */
function blpltw_aggregate_product_sales($date=null) {
    global $wpdb;

    $handling_today = ($date === null);
    if ($handling_today) $date = strftime('%Y-%m-%d');

    error_log("Aggregating product sales for $date.");

    $wpdb->query("begin");

    // There might be products in the aggregation table that were ordered,
    // but are all still on hold. We should set those to zero.
    $preexisting_objs = $wpdb->get_results("SELECT product_name FROM aggr_product_sales WHERE day='$date'");
    $preexisting = array_map(function($obj) { return $obj->product_name; }, $preexisting_objs);
    $json = array();

    // Only update cloud subscribers when handling 'today'; as that data
    // is not historical/versioned, we can't look into the past and update
    // for 'yesterday'.
    if ($handling_today) {
        $query = "select count(p.ID) AS subscriptions
                  from wp_posts p
                  where p.post_type='shop_subscription' and p.post_status in ('wc-active', 'wc-pending-cancel')";

        $result = $wpdb->get_results($query);
        $subscribers = $result[0]->subscriptions;
        blpltw_update_sold($date, 'cloud', $subscribers);
        $json[] = array('product' => 'cloud', 'items_sold' => $subscribers);
    }

    // Keep track of the fact we've seen this 'product'.
    // We always do this, otherwise yesterday's cloud count will be
    // set to 0 and subsequently be removed.
    // source: http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
    if(($key = array_search('cloud', $preexisting)) !== false) {
        unset($preexisting[$key]);
    }

    $query = "
        SELECT product.post_name as product, sum(om_meta2.meta_value) as items_sold
        FROM wp_posts as product

        LEFT JOIN wp_woocommerce_order_itemmeta as om_meta
        ON om_meta.meta_value = product.ID

        LEFT JOIN wp_woocommerce_order_items as items
        ON om_meta.order_item_id = items.order_item_id

        LEFT JOIN wp_woocommerce_order_itemmeta as om_meta2
          ON om_meta2.order_item_id = items.order_item_id

        LEFT JOIN wp_posts as orders
        ON items.order_id = orders.ID

        WHERE
            product.post_type = 'product'
            AND om_meta.meta_key = '_product_id'
            AND om_meta2.meta_key = '_qty'
            AND orders.post_status = 'wc-completed'
            AND orders.post_date LIKE '$date %'
        GROUP BY product.ID
    ";

    $result = $wpdb->get_results($query, 'OBJECT');

    foreach ($result as $sold) {
        // Keep track of the fact we've seen this product.
        // source: http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
        if(($key = array_search($sold->product, $preexisting)) !== false) {
            unset($preexisting[$key]);
        }

        blpltw_update_sold($date, $sold->product, $sold->items_sold);
        $json[] = $sold;
    }

    // Now reset all pre-existing-but-not-seen-so-far products to zero
    foreach ($preexisting as $product_name) {
        blpltw_update_sold($date, $product_name, 0);
    }

    $wpdb->query("commit");

    return $json;
}

/**
 * Counts this order as if it were completed.
 *
 * This is called from WooCommerce actions that notify an order was placed.
 *
 * @param $order_id
 */
function blpltw_register_order($order_id) {
    global $wpdb;

    $today = strftime('%Y-%m-%d');
    error_log("Incrementing product sales for order $order_id.");

    $wpdb->query("begin");

    $query = "
            SELECT product.post_name as product, sum(om_meta2.meta_value) as items_sold
        FROM wp_posts as product

          LEFT JOIN wp_woocommerce_order_itemmeta as om_meta
            ON om_meta.meta_value = product.ID

          LEFT JOIN wp_woocommerce_order_items as items
            ON om_meta.order_item_id = items.order_item_id

          LEFT JOIN wp_woocommerce_order_itemmeta as om_meta2
            ON om_meta2.order_item_id = items.order_item_id

        WHERE
          product.post_type = 'product'
          AND om_meta.meta_key = '_product_id'
          AND om_meta2.meta_key = '_qty'
          AND items.order_id = $order_id
        GROUP BY product.ID
    ";

    $result = $wpdb->get_results($query, 'OBJECT');

    foreach ($result as $sold) {
        blpltw_update_sold($today, $sold->product, $sold->items_sold, true);
    }

    $wpdb->query("commit");
}

/**
 * Removes all the product sale entries with zero products sold.
 * Only acts on entries from the given $date or older.
 *
 * @param $date string in YYYY-MM-DD format
 */
function blpltw_clean_up_aggr_table($date) {
    global $wpdb;

    $query = "DELETE FROM aggr_product_sales WHERE day <= '$date' AND items_sold = 0";
    $wpdb->query($query);
}

/**
 * Calls blpltw_aggregate_product_sales() with yesterday's date.
 *
 * This ensures that last-minute not-completed orders don't skew the stats.
 * Also cleans up old entries with zero items sold.
 */
function blpltw_aggregate_product_sales_yesterday() {
    $today = time();
    $yesterday = $today - 24 * 3600;
    $date = strftime('%Y-%m-%d', $yesterday);

    blpltw_aggregate_product_sales($date);
    blpltw_clean_up_aggr_table($date);
}

function blpltw_woocommerce_api_create_order($order_id, $data, $api) {
    blpltw_register_order($order_id);
}

function blpltw_woocommerce_thankyou($order_id) {
    blpltw_register_order($order_id);
}

function blpltw_aggr_product_sales_activation() {
    error_log("Setting up product sales aggregation to run periodically.");
    wp_schedule_event(time(), 'hourly', 'blpltw_aggr_product_sales');  // this is action name, not function name.

    // schedule at 1970-01-01 00:15:00; it will be rescheduled at another day, same time.
    wp_schedule_event(900, 'daily', 'blpltw_aggr_product_sales_yesterday');
}

function blpltw_aggr_product_sales_deactivation() {
    error_log("Deactivating periodic produc sales aggregation");
    wp_clear_scheduled_hook('blpltw_aggr_product_sales');
}

//error_log("Loaded " . plugin_basename(__FILE__));
//$timestamp = wp_next_scheduled('blpltw_aggr_product_sales_yesterday');
//error_log("Next aggregation will be performed at " . strftime("%Y-%m-%d %H:%M:%S %z", $timestamp));
//error_log("                      Current time is " . strftime("%Y-%m-%d %H:%M:%S %z"));
