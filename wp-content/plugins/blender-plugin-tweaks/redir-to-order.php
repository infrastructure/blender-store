<?php
$topdir = dirname(dirname(dirname(dirname( __FILE__ ))));

if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once("$topdir/wp-load.php" );
}

require_once("$topdir/wp-content/plugins/bo_woocommerce/tweaks.php");

$ordernr = ltrim(trim($_GET['order'], "# \t\n\r\0\x0B"), '0');

function redir_if_order_exists($search_args) {
    $posts = wc_get_orders($search_args);

    if (count($posts)) {
        $ID = $posts[0]->ID;
        http_response_code(302);
        header("Location: /wp-admin/post.php?post=$ID&action=edit");
        exit(0);
    }
}

/* Search by order number */
redir_if_order_exists(array(
    'post_type' => 'shop_order',
    'meta_key' => '_order_number',
    'meta_value' => $ordernr,
));

/* If it cannot be found, check if this is a post ID of an order instead */
redir_if_order_exists(array(
    'post_type' => 'shop_order',
    'post_id' => $ordernr,
));

print("Order number $ordernr could not be found.");
