<?php

/*
Plugin Name: Blender Plugin Tweaks
Description: This plugin is used to modify plugins being used in the Blender Store without hacking plugins (where possible) to make future updates
Version: 1.1
Author: Nerd Inc & Sybren A. Stüvel
Author URI: http://nerd-inc.com
*/

require_once(plugin_dir_path(__FILE__) . 'aggr_product_sales.php');

register_activation_hook( __FILE__, 'blpltw_aggr_product_sales_activation' );
register_deactivation_hook( __FILE__, 'blpltw_aggr_product_sales_deactivation' );
add_action('blpltw_aggr_product_sales', 'blpltw_aggregate_product_sales');
add_action('blpltw_aggr_product_sales_yesterday', 'blpltw_aggregate_product_sales_yesterday');
add_action('woocommerce_api_create_order', 'blpltw_woocommerce_api_create_order');
add_action('woocommerce_thankyou', 'blpltw_woocommerce_thankyou');



class BlenderPluginTweaks {

	public function __construct() {
		/* Admin menu for Film credits */
		add_action( 'admin_menu', array( $this, 'film_credits_menu' ) );

		/* WooCommerce Subscriptions tweaks */
		// woocommerce-subscriptions/clases/class-wc-subscriptions-manager.php
		add_filter( 'can_subscription_be_changed_to_active', array( $this, 'can_subscription_be_changed_to' ), 10, 3 );
		add_filter( 'can_subscription_be_changed_to_new-payment-date', array( $this, 'can_subscription_be_changed_to' ), 10, 3 );
	}


	/**
	 * Give users with admin rights the ability to change subscriptions
	 *
	 * @access public
	 * @param bool $subscription_can_be_changed Current status of if subscription can be changed
	 * @param string $subscription A subscription key of the form created by @see self::get_subscription_key()
	 * @param mixed $order The WooCommerce order object
	 * @return void
	 */
	public static function can_subscription_be_changed_to( $subscription_can_be_changed, $subscription, $order ) {

		if( is_admin() ) {
			$subscription_can_be_changed = true;
		}

		return $subscription_can_be_changed;
	}


	/**
	 * Removes a filter that has been created within an anonymous class
	 *
	 * @access private
	 * @param string $tag Hook name
	 * @param string $class Class name
	 * @param string $method Methid name
	 * @return void
	 */
	private function remove_filter( $tag, $class, $method ) {

		$filters = $GLOBALS['wp_filter'][ $tag ];

		if ( empty ( $filters ) ) {
			return;
		}

		foreach ( $filters as $priority => $filter ) {

			foreach ( $filter as $identifier => $function ) {

				if ( is_array( $function) && is_a( $function['function'][0], $class ) && $method === $function['function'][1] ) {
					remove_filter( $tag, array ( $function['function'][0], $method ), $priority );
				}
			}
		}
	}


	/**
	 * Create admin menu for film credits
	 *
	 * @access public
	 * @return void
	 */
	function film_credits_menu() {

		add_submenu_page( 'tools.php', 'Film Credits', 'Film Credits', 'administrator', 'film-credits', array( $this, 'get_film_credits' ) );
	}

	function get_film_credits() {

		if ( !is_admin() )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		@apache_setenv('no-gzip', 1);
		@ini_set('zlib.output_compression', 0);
		@ini_set('implicit_flush', 1);

		$users = get_users( array( 'role' => 'customer', 'fields' => array( 'ID', 'user_email' ) ) );

		foreach ( $users as $user ) {

			// Get order ID's of user
			$orders = blender_get_users_subscription_orders( $user->ID );

			// Get subscriptions based on the user id
			$subscriptions = WC_Subscriptions_Manager::get_users_subscriptions( $user->ID );

			if( isset( $orders ) && !empty( $orders ) ) {

				$user_data = array( 'email' => $user->user_email, 'paid_balance' => 0, 'balance_currency' => 'EUR' );

				// Loop through each subscription and calculate order totals
				foreach( $orders as $order_id ) {

					$post_statusses = array( 'publish', 'completed', 'wc-completed' );
					$post_status = get_post_status( $order_id );
					$initial_order = get_post_meta( $order_id, '_original_order', true );

					// Only proceed if it's a completed order
					if( in_array( $post_status, $post_statusses ) ) {

						if( empty( $initial_order ) && $initial_order != $order_id ) {

							// Calculate initial order
							$user_data['balance_currency'] = get_post_meta( $order_id, '_order_currency', true );
							$user_data['paid_balance'] += get_post_meta( $order_id, '_order_total', true );

						} else {

							$_order_recurring_total = get_post_meta( $order_id, '_order_recurring_total', true );

							if( empty( $_order_recurring_total ) ) {

								// calculate order total instead
								$user_data['paid_balance'] += get_post_meta( $order_id, '_order_total', true );

							} else {

								// Calculate recurring order
								$user_data['paid_balance'] += get_post_meta( $order_id, '_order_recurring_total', true );

							}

						}

						// If there are no subscriptions found, check orders for special subscription cases
						if( !isset( $subscriptions ) || empty( $subscriptions ) ) {

							$order = new WC_Order( $order_id );

							foreach ( $order->get_items() as $item_id => $item_details ) {

								if( $item_details['name'] == "Pre-Paid membership: 18 Months" ) {

									$user_data['balance_currency'] = 'EUR';
									$user_data['paid_balance'] = 195;

								}

							}

						}

					}

				}

				// Ouput results

				print implode( ",", $user_data ) . '<br />';
				flush();

			}
		}

		echo '<br /><b>Done</b>';
	}

	/**
	 * Show incorrect VAT orders
	 *
	 * @access public
	 * @return void
	 */
	function incorrect_vat_orders() {

		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		$args = array(
			'post_type' => 'shop_order',
			'post_status' => 'any',
			'posts_per_page' => 500,
			'year' => 2014,
			'meta_query' => array(

				// Orders that are less than 10 euros
				array(
					'key' => '_order_recurring_total',
					'value' => 8.265,
				),

				// Orders that are inside the EU
				array(
					'key' => '_billing_country',
					'value' => array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK', 'IM', 'MC'),
					'compare' => 'IN'
				),

				// Orders that are being paid in Euros
				array(
					'key' => '_order_currency',
					'value' => 'EUR',
				),

				// Only edit original orders
				array(
					'key' => '_original_order',
					'compare' => 'NOT EXISTS'
				),

				// Only orders that are more than 1 euro
				array(
					'key' => '_order_total',
					'value' => 1,
					'compare' => '>'
				),

				// Only get orders that are not EU valid
				array(
					'key' => 'Valid EU VAT Number',
					'compare' => 'NOT EXISTS'
				)

			)
		);

		$incorrect_orders = new WP_Query( $args );

		echo '<div class="wrap">';

		echo '<b>Updatet: ' . $incorrect_orders->found_posts . ' orders</b><br /><br />';

		global $wpdb;
		while ( $incorrect_orders->have_posts() ) : $incorrect_orders->the_post();

			// Check if someone has a validated EU VAT number
			$valid_vat_number = get_post_meta( get_the_ID(), 'Valid EU VAT Number', true );
			$billing_country = get_post_meta( get_the_ID(), '_billing_country', true );
			$order_total = get_post_meta( get_the_ID(), '_order_total', true );
			$recurring_total = get_post_meta( get_the_ID(), '_order_recurring_total', true );
			$recurring_tax = get_post_meta( get_the_ID(), '_order_recurring_tax_total', true );

			// Skip recurring Paypal payments that are not 10 euros
			if( get_post_meta( get_the_ID(), '_recurring_payment_method', true ) == 'paypal' && ( $recurring_total + $recurring_tax ) != 10 ) {
				continue;
			}

			// Get tax based on the billing country of the customer
			$rate = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}woocommerce_tax_rates WHERE tax_rate_country = %s AND tax_rate_class = %s;", $billing_country, 'digital-goods' ) );

			// Calculate the tax to pay
			$monthly_fee = 10;
			$tax = round( ( $monthly_fee / ( 100 + $rate->tax_rate ) ) * $rate->tax_rate, 3 );

			// Set correct order tax total
			update_post_meta( get_the_ID(), '_order_recurring_tax_total', $tax );

			// Set order total to 10 euros
			update_post_meta( get_the_ID(), '_order_recurring_total', 10 );

			// Set the prices of the order to be including tax
			update_post_meta( get_the_ID(), '_prices_include_tax', 'yes' );

			// Save with special key so the incorrect orders could be changed at a later time
			add_post_meta( get_the_ID(), '_manually_modified', true );

		endwhile;

		echo '</div>';
	}
}

$blender_tweaks = new BlenderPluginTweaks();

/**
 * Never let Wordpress send password change emails. All password management
 * is delegated to Blender ID.
 */
add_filter('send_password_change_email', 'blpltw_send_password_change_email', 10, 3);
function blpltw_send_password_change_email($send, $user, $userdata) {
	return false;
}

add_action('woocommerce_admin_order_data_after_order_details', 'blpltw_link_user_after_order_details');
function blpltw_link_user_after_order_details($subscription)
{
    $user = $subscription->get_user();
    $user_email = esc_html($user->user_email);
    print("<p>Edit user <a href='user-edit.php?user_id=$user->ID'>$user_email</a></p>");
}

// Block WP user enumeration scans
// Source: https://perishablepress.com/stop-user-enumeration-wordpress/
if (!is_admin() && isset($_SERVER['QUERY_STRING'])) {
	// default URL format
	if (preg_match('/author=([0-9]*)/i', $_SERVER['QUERY_STRING'])) die('nope');
	add_filter('redirect_canonical', 'shapeSpace_check_enum', 10, 2);
}
function shapeSpace_check_enum($redirect, $request) {
	// permalink URL format
	if (preg_match('/\?author=([0-9]*)(\/*)/i', $request)) die('nope');
	else return $redirect;
}

/**
 * WordPress and WooCommerce both return a HTTP 200 OK on login
 * failure. This makes it impossible to recognise failed logins from the
 * access logs. */
add_action('wp_login_failed', 'blpltw_login_failure_handler');
add_action('woocommerce_login_failed', 'blpltw_login_failure_handler');
function blpltw_login_failure_handler() {
    status_header(403);

    /* Prevent this status header from being overwritten in later processing of this request. */
    add_filter('status_header', 'blpltw_force_403_status_header', 10, 0);
}

/**
 * This filter forces a 403 Forbidden header. It is a dirty hack to work around
 * code in WordPress that overwrites the 403 Forbidden we set in the
 * blpltw_login_failure_handler() function above with a 200 OK.
 *
 * The offending code can be found in class-wp.php, function handle_404(). If
 * $wp_query doesn't report a 404, it tries to force a 200 OK, and this filter
 * prevents this.
 *
 * The filter is not added by default, but it is added in blpltw_login_failure_handler()
 * after a login failure.
*/
function blpltw_force_403_status_header() {
    $code = 403;
    $description = get_status_header_desc($code);
	$protocol = wp_get_server_protocol();
	$status_header = "$protocol $code $description";
	return $status_header;
}

require_once('admin-extra-searches.php');
require_once('subscriptions-admin-extras.php');
