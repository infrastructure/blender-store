<?php

add_action('admin_menu', 'blender_custom_menu_item');
function blender_custom_menu_item()
{
    add_submenu_page(
        'woocommerce',  // menu to add submenu page to (parent slug)
        'Blender stuff', // page title
        'Blender stuff', // menu title
        'manage_woocommerce', // capability required to show
        'blender-stuff',  // page slug
        'blender_custom_admin_page' // callback function to render page
    );
}

add_filter('woocommerce_screen_ids', 'blender_fake_woocommerce_pages');
function blender_fake_woocommerce_pages($screen_ids)
{
    // format: "{parent slug}_page_{page slug}"
    $screen_ids[] = 'woocommerce_page_blender-stuff';
    return $screen_ids;
}

// Copy of WC_Admin_List_Table_Orders::render_filters()
// We can't call it directly any more because they moved the code to a protected function...
function _blender_render_filters() {
    $user_string = '';
    $user_id     = '';

    if ( ! empty( $_GET['_customer_user'] ) ) { // WPCS: input var ok.
        $user_id = absint( $_GET['_customer_user'] ); // WPCS: input var ok, sanitization ok.
        $user    = get_user_by( 'id', $user_id );

        $user_string = sprintf(
            /* translators: 1: user display name 2: user ID 3: user email */
            esc_html__( '%1$s (#%2$s &ndash; %3$s)', 'woocommerce' ),
            $user->display_name,
            absint( $user->ID ),
            $user->user_email
        );
    }
    ?>
    <select class="wc-customer-search" name="_customer_user" data-placeholder="<?php esc_attr_e( 'Filter by registered customer', 'woocommerce' ); ?>" data-allow_clear="true">
        <option value="<?php echo esc_attr( $user_id ); ?>" selected="selected"><?php echo wp_kses_post( $user_string ); ?><option>
    </select>
    <?php
}

function blender_custom_admin_page()
{
    if (!current_user_can('list_users')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }

    // To ensure that the _blender_render_filters() thingy works.
    $wc_admin_assets = new WC_Admin_Assets();
    $wc_admin_assets->admin_styles();
    $wc_admin_assets->admin_scripts();

    // The "post-type-shop_order" and "tablenav" CSS classes are to make sure the user filter CSS works.
    ?>
    <div class="wrap post-type-shop_order">
        <img src='/wp-content/uploads/2014/07/blender-socket-300x86.png'
             alt='Blender logo'
             style='float: right'>
        <h1>Blender stuff</h1>

        <p>Here we can do Blender stuff!</p>

        <table class="form-table tablenav">
            <tr valign="top">
                <th scope="row">Order search</th>
                <td>
                    <form action='edit.php?s' method='get' style='width: 20em'>
                        <input type='text' name='s' value='' placeholder='Generic search terms' style='width: 100%'>
                        <input type='hidden' name='post_type' value='shop_order'>
                        <input type='hidden' name='post_status' value='all'>
                        <?php _blender_render_filters() ?>
                        <button type='submit'>Search</button>
                    </form>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Go directly to order</th>
                <td>
                    <form action='/wp-content/plugins/blender-plugin-tweaks/redir-to-order.php?' method='get' style='width: 20em'>
                        <input type='text' name='order' value='' placeholder='Order number' style='width: 100%'>
                        <button type='submit'>Go to order</button>
                    </form>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Subscription search</th>
                <td>
                    <form action='edit.php?s' method='get' style='width: 20em'>
                        <input type='text' name='s' value='' placeholder='Generic search terms' style='width: 100%'>
                        <input type='hidden' name='post_type' value='shop_subscription'>
                        <input type='hidden' name='post_status' value='all'>
                        <?php _blender_render_filters() ?>
                        <button type='submit'>Search</button>
                    </form>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Go directly to subscription</th>
                <td>
                    <form action='post.php?' method='get' style='width: 20em'>
                        <input type='text' name='post' value='' placeholder='Subscription number' style='width: 100%'>
                        <input type='hidden' name='action' value='edit'>
                        <button type='submit'>Go to subscription</button>
                    </form>
                </td>
            </tr>
        </table>

        <h2>PayPal debugging</h2>
        <dl>
            <dt>IPN URL used by WooCommerce PayPal gateway</dt>
            <dd><tt>WC()-&gt;api_request_url( 'WC_Gateway_Paypal' ) →
                "<?php echo WC()->api_request_url( 'WC_Gateway_Paypal' ) ?>"</tt></dd>
        </dl>
    </div>
    <?php
}
