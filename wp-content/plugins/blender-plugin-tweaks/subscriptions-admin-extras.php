<?php

/* Add some extra filters to the Subscriptions admin.
 *
 * The 'restrict_manage_posts' action adds the filter as select box to the UI.
 * The 'parse_query' action actually performs the filtering.
 */

add_action('restrict_manage_posts', 'blpltw_admin_subscriptions_extra_filter__ui');
function blpltw_admin_subscriptions_extra_filter__ui() {
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }

    if ($type != 'shop_subscription') return;

    $values = array(
        'STILL_PENDING_CANCEL' => 'Still-Pending Cancellations',
    );
    ?>
    <select name="BLENDER_EXTRA_FILTER">
    <option value="">Blender-specific Filters</option>
    <?php
        $current_v = isset($_GET['BLENDER_EXTRA_FILTER'])? $_GET['BLENDER_EXTRA_FILTER']:'';
        foreach ($values as $value => $label) {
            printf
                (
                    '<option value="%s"%s>%s</option>',
                    esc_html($value),
                    esc_html($value == $current_v ? ' selected="selected"' : ''),
                    esc_html($label)
                );
            }
    ?>
    </select>
    <?php
}


add_filter('parse_query', 'blpltw_admin_subscriptions_extra_filter__filter');
function blpltw_admin_subscriptions_extra_filter__filter($query) {
    global $pagenow;

    if (!isset($_GET['post_type']) || $_GET['post_type'] != 'shop_subscription') return $query;
    if (!isset($_GET['BLENDER_EXTRA_FILTER']) || $_GET['BLENDER_EXTRA_FILTER'] == '') return $query;
    if (!is_admin() || $pagenow != 'edit.php') return $query;

    $query->query['post_status'] = ['wc-pending-cancel'];
    $query->query_vars['post_status'] = ['wc-pending-cancel'];
    $query->query_vars['meta_key'] = '_schedule_end';
    $query->query_vars['meta_value'] = date("Y-m-d H:i:s");
    $query->query_vars['meta_compare'] = '<';
    return $query;
}

add_filter('woocommerce_order_actions', 'blpltw_woocommerce_order_actions');
function blpltw_woocommerce_order_actions($actions) {
    global $post_type, $post;

    if ($post_type != 'shop_subscription') return $actions;
    if ($post->post_status != 'wc-pending-cancel' && $post->post_status != 'wc-on-hold') return $actions;

    // See the WC_Meta_Box_Order_Actions class definition.
    $actions["blpltw_fix_cancelled_date"] = 'Hack Cancel/End Date to Last Payment';
    return $actions;
}

add_action('woocommerce_order_action_blpltw_fix_cancelled_date', 'blpltw_fix_cancelled_date');
function blpltw_fix_cancelled_date($subscription) {
    $cancel_date = $subscription->schedule_cancelled;

    $dates = array();
    foreach ( wcs_get_subscription_date_types() as $date_type => $type_label ) {
        $dates[$date_type] = $subscription->get_date($date_type);
    }

    // Add one second to make WooCommerce happy. They mix up their <= and < symbols.
    $target_timestamp = new DateTime($dates['last_payment']);
    $target_timestamp->add(new DateInterval('PT1S'));  // Period of Time 1 Second
    $timestamp = $target_timestamp->format('Y-m-d H:i:s');

    $note = "Subscription dates set to last payment date <code>$timestamp</code>. Old values:\n" .
        "Cancel: <code>$dates[cancelled]</code>\n" .
        "End: <code>$dates[end]</code>\n";

    $dates['cancelled'] = $timestamp;
    $dates['end'] = $timestamp;

    $subscription->update_dates($dates);
    $subscription->add_order_note($note, 0, true);
}
