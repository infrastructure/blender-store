/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "../../../../../../../../../Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../../../../../../../../../Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/arrayWithoutHoles.js":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** D:/Users/Diego/Documents/Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/arrayWithoutHoles.js ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

module.exports = _arrayWithoutHoles;

/***/ }),

/***/ "../../../../../../../../../Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/iterableToArray.js":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** D:/Users/Diego/Documents/Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/iterableToArray.js ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

module.exports = _iterableToArray;

/***/ }),

/***/ "../../../../../../../../../Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/nonIterableSpread.js":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** D:/Users/Diego/Documents/Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/nonIterableSpread.js ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

module.exports = _nonIterableSpread;

/***/ }),

/***/ "../../../../../../../../../Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/toConsumableArray.js":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** D:/Users/Diego/Documents/Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/toConsumableArray.js ***!
  \******************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithoutHoles = __webpack_require__(/*! ./arrayWithoutHoles */ "../../../../../../../../../Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/arrayWithoutHoles.js");

var iterableToArray = __webpack_require__(/*! ./iterableToArray */ "../../../../../../../../../Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/iterableToArray.js");

var nonIterableSpread = __webpack_require__(/*! ./nonIterableSpread */ "../../../../../../../../../Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/nonIterableSpread.js");

function _toConsumableArray(arr) {
  return arrayWithoutHoles(arr) || iterableToArray(arr) || nonIterableSpread();
}

module.exports = _toConsumableArray;

/***/ }),

/***/ "../../../../../../../../../Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/src/index.js":
/*!**********************************************************************************************************************************************************************************************!*\
  !*** D:/Users/Diego/Documents/Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/src/index.js ***!
  \**********************************************************************************************************************************************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/toConsumableArray */ "../../../../../../../../../Projects/Web/personal/Wordpress/Plugins/WooCommerce/AeliaCS_AFC/woocommerce-aelia-currencyswitcher/src/lib/classes/integration/woocommerce_admin/node_modules/@babel/runtime/helpers/toConsumableArray.js");
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/hooks */ "@wordpress/hooks");
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__);



/**
 * Adds a "currency" filter to the WooCommerce Analytics.
 *
 * @param array filters
 * @return array
 * @since 4.8.2.200310
 */

function add_currency_filter(filters) {
  var settings = wcSettings.aelia_cs_woocommerce_admin_integration || {};

  if (settings === {}) {
    return filters;
  }

  return [].concat(_babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default()(filters), [{
    label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__["__"])('Currency', settings.text_domain),
    staticParams: [],
    param: settings.arg_report_currency,
    showFilters: function showFilters() {
      return true;
    },
    defaultValue: settings.default_report_currency,
    filters: settings.report_currency_options
  }]);
}

;
Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__["addFilter"])('woocommerce_admin_revenue_report_filters', 'Aelia/WC/CurrencySwitcher', add_currency_filter);
Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__["addFilter"])('woocommerce_admin_orders_report_filters', 'Aelia/WC/CurrencySwitcher', add_currency_filter);
Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__["addFilter"])('woocommerce_admin_products_report_filters', 'Aelia/WC/CurrencySwitcher', add_currency_filter);
Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__["addFilter"])('woocommerce_admin_categories_report_filters', 'Aelia/WC/CurrencySwitcher', add_currency_filter);
Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__["addFilter"])('woocommerce_admin_taxes_report_filters', 'Aelia/WC/CurrencySwitcher', add_currency_filter); // Add currency filter to coupons report
// @since 4.8.11.200524

Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__["addFilter"])('woocommerce_admin_coupons_report_filters', 'Aelia/WC/CurrencySwitcher', add_currency_filter); // Add currency filter to Dashboard report
// @since 4.8.14.200805

Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__["addFilter"])('woocommerce_admin_dashboard_filters', 'Aelia/WC/CurrencySwitcher', add_currency_filter); // Add currency filter for customers
// @since 4.9.0.200917

Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__["addFilter"])('woocommerce_admin_customers_report_filters', 'Aelia/WC/CurrencySwitcher', add_currency_filter); // Add currency filter for variations
// @since x.x

Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__["addFilter"])('woocommerce_admin_variations_report_filters', 'Aelia/WC/CurrencySwitcher', add_currency_filter);
/**
 * Returns the currency settings that will be used to format the data
 * displayed by the Analytics.
 *
 * @param object config
 * @param object query
 * @since 4.8.7.200417
 * @link https://woocommerce.wordpress.com/2020/02/20/extending-wc-admin-reports/
 */

function update_report_currency(config, query) {
  var settings = wcSettings.aelia_cs_woocommerce_admin_integration || {};

  if (settings === {}) {
    return config;
  } // Fetch a list of the available currencies, with their respective settings


  var currencies = settings.currencies; // Fetch the selected currency. The "query" variable contains the arguments
  // passed with the request

  var selected_currency = query['report_currency'] || ''; // If the selected currency matches one of the currencies in the list, return
  // the formatting settings for that currency

  if (selected_currency && currencies[selected_currency]) {
    config = currencies[selected_currency];
  }

  return config;
}

;
Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__["addFilter"])('woocommerce_admin_report_currency', 'Aelia/WC/CurrencySwitcher', update_report_currency);
/**
 * Add "currency" to the list of persisted queries so that the parameter remains
 * when navigating between different reports.
 *
 * @param array
 * @return array
 * @since 4.8.11.200524
 */

function persist_query_args(params) {
  params.push('report_currency');
  return params;
}

;
Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_1__["addFilter"])('woocommerce_admin_persisted_queries', 'Aelia/WC/CurrencySwitcher', persist_query_args);

/***/ }),

/***/ "@wordpress/hooks":
/*!****************************************!*\
  !*** external {"this":["wp","hooks"]} ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["hooks"]; }());

/***/ }),

/***/ "@wordpress/i18n":
/*!***************************************!*\
  !*** external {"this":["wp","i18n"]} ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["i18n"]; }());

/***/ })

/******/ });
//# sourceMappingURL=index.js.map