<?php
namespace Aelia\WC\CurrencySwitcher\WC36;
if(!defined('ABSPATH')) exit; // Exit if accessed directly

use \WC_Product;
use \WC_Product_Simple;
use \WC_Product_Variation;
use \WC_Product_External;
use \WC_Product_Grouped;
use \WC_Cache_Helper;
use \Aelia\WC\CurrencySwitcher\WC_Aelia_CurrencySwitcher;
use \Aelia\WC\CurrencySwitcher\Definitions;

interface IWC_Aelia_CurrencyPrices_Manager {
	public function convert_product_prices(WC_Product $product, $currency);
	public function convert_external_product_prices(WC_Product_External $product, $currency);
	public function convert_grouped_product_prices(WC_Product_Grouped $product, $currency);
	public function convert_simple_product_prices(WC_Product $product, $currency);
	public function convert_variable_product_prices(WC_Product $product, $currency);
	public function convert_variation_product_prices(WC_Product_Variation $product, $currency);
}

/**
 * Handles currency conversion for the various product types.
 * Due to its architecture, this class should not be instantiated twice. To get
 * the instance of the class, call WC_Aelia_CurrencyPrices_Manager::Instance().
 *
 * @since 4.7.7.190706
 */
class WC_Aelia_CurrencyPrices_Manager extends \Aelia\WC\CurrencySwitcher\WC32\WC_Aelia_CurrencyPrices_Manager {
	/**
	 * Removes the hooks for the conversion of product prices.
	 *
	 * @since 4.7.7.190706
	 */
	protected function remove_product_price_hooks() {
		// Remove filters to convert variation prices
		remove_filter('woocommerce_variation_prices_price', array($this, 'woocommerce_variation_prices_price'), 5, 3);
		remove_filter('woocommerce_variation_prices_regular_price', array($this, 'woocommerce_variation_prices_regular_price'), 5, 3);
		remove_filter('woocommerce_variation_prices_sale_price', array($this, 'woocommerce_variation_prices_sale_price'), 5, 3);

		// Remove filters to convert product prices, based on selected currency
		remove_filter('woocommerce_product_get_price', array($this, 'woocommerce_product_get_price'), 5, 2);
		remove_filter('woocommerce_product_get_regular_price', array($this, 'woocommerce_product_get_regular_price'), 5, 2);
		remove_filter('woocommerce_product_get_sale_price', array($this, 'woocommerce_product_get_sale_price'), 5, 2);

		remove_filter('woocommerce_product_variation_get_price', array($this, 'woocommerce_product_get_price'), 5, 2);
		remove_filter('woocommerce_product_variation_get_regular_price', array($this, 'woocommerce_product_get_regular_price'), 5, 2);
		remove_filter('woocommerce_product_variation_get_sale_price', array($this, 'woocommerce_product_get_sale_price'), 5, 2);

		// WC 2.7+
		remove_filter('woocommerce_product_get_variation_prices_including_taxes', array($this, 'woocommerce_product_get_variation_prices_including_taxes'), 5, 2);
	}

	/**
	 * Sets the hooks required by the class.
	 */
	protected function set_hooks() {
		parent::set_hooks();
		add_action('wp_ajax_woocommerce_add_order_item', array($this, 'wp_ajax_woocommerce_add_order_item'), 1);
	}

	/**
	 * Given a product, restores the price, regular price and sale price
	 * properties to their original base currency value.
	 * This method can be used to prevent the currency-specific prices
	 * set against a product instance from being saved to the database,
	 * in case someone calls $product->save() against that instance.
	 *
	 * @param WC_Product $product
	 * @link https://github.com/woocommerce/woocommerce/issues/23952
	 */
	protected function restore_base_product_prices($product) {
		$original_product = wc_get_product($product->get_id());

		// Debug
		// var_dump(
		// 	$original_product->get_price('edit'),
		// 	$original_product->get_regular_price('edit'),
		// 	$original_product->get_sale_price('edit')
		// );

		// Reset the product prices to the base one. This will ensure that
		// the values of meta _price, _regular_price and _base_price are
		// preserved
		$product->set_price($original_product->get_price('edit'));
		$product->set_regular_price($original_product->get_regular_price('edit'));
		$product->set_sale_price($original_product->get_sale_price('edit'));
		// Keep track of the fact that product prices are now in the original currency
		$product->currency = $this->base_currency();
	}

	/**
	 * Perform actions when an item is added to an order via Ajax (manual orders).
	 * - Adds hook to intercept the adding of an item to an order. That will allow
	 *   to set the order item's price in the correct currency, without risking to
	 *   save the product prices to the database when the stock is updated
	 *
	 * @since WC 3.6.3
	 */
	public function wp_ajax_woocommerce_add_order_item() {
		// Remove the product conversion hooks before an item is added to a manual
		// order. This is to avoid setting the prices against a product instance that
		// will also be used to update the stock, causing the prices stored in the
		// database to be overwritten
		// @link https://github.com/woocommerce/woocommerce/issues/23952
		$this->remove_product_price_hooks();

		//add_action('woocommerce_before_product_object_save', array($this, 'woocommerce_before_product_object_save'), 10, 2);
		add_action('woocommerce_ajax_add_order_item_meta', array($this, 'before_woocommerce_ajax_add_order_item_meta'), 1, 3);
		add_action('woocommerce_ajax_add_order_item_meta', array($this, 'after_woocommerce_ajax_add_order_item_meta'), 9999, 3);
	}

	// /**
	//  * Intercept the saving of a product instance before its stock is updated. This
	//  * is required since WC 3.6.3, due to the new logic that "recycles" a product instance,
	//  * whose prices might have changed during the "add item" setp of an order creation.
	//  * The stock update function saved the product instance with the new attributes, causing
	//  * the new prices to overwrite the meta in the database.
	//  *
	//  * @param WC_Product $product
	//  * @param WC_Data_Store $data_store
	//  * @since WC 3.6.3
	//  */
	// public function woocommerce_before_product_object_save($product, $data_store) {
	// 	if(isset($product->currency) && ($product->currency != $this->base_currency())) {
	// 		$this->restore_base_product_prices($product);
	// 	}
	// }

	/**
	 * Performs actions after an item has been added to an order from the Edit Order
	 * page, to ensure that the item is created with the correct price:
	 * - Enables the product price conversion filters.
	 * - Replaces a newly added item with an item with same ID, but with the correct
	 *   currency prices.
	 *
	 * @param int $item_id
	 * @param WC_Order_Item $item
	 * @param WC_Order $order
	 */
	public function before_woocommerce_ajax_add_order_item_meta($item_id, $item, $order) {
		// Enable the price conversion filters and get a new product instance for the
		// item. This will allow to set the correct currency prices, without affecting the
		// "main" product instance used in WC_AJAX::add_order_item(), which is the one
		// used to update the product's stock
		$this->set_product_price_hooks();
		$product = $item->get_product();

		// Replace the item just added with a new item with the same
		// product, but with the prices in order's currency.
		// The new item will also take the same ID as the original one, so that plugins
		// like Bundles can remove it after replacing it with bundled items.
		// @link https://github.com/woocommerce/woocommerce/issues/24089
		$new_item_id = $order->add_product($product, $item->get_quantity(), array(
			'id' => $item_id,
		));
		// Save the order to update the items against the order instance
		$order->save();
	}

	/**
	 * Performs actions after an item has been added to an order from the Edit Order
	 * page, and after other actors had the chance to do their part:
	 * - Removes the product price conversion filters.
	 *
	 * @param int $item_id
	 * @param WC_Order_Item $item
	 * @param WC_Order $order
	 */
	public function after_woocommerce_ajax_add_order_item_meta($item_id, $item, $order) {
		// Remove the price conversion filters, to prevent them from affecting the product
		// instance for the next order item
		$this->remove_product_price_hooks();
	}
}
