<?php

defined('ABSPATH') or die("No access");

// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

 /*
  * Create a new table class that will extend the WP_List_Table
  */

class WPFortune_Plugins_Table_NI extends WP_List_Table
{

    private $data;

    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        $data = $this->table_data();
        usort( $data, array( &$this, 'sort_data' ) );

        $perPage = 20;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);

        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );

        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);

        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }

    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
        $columns = array(
            'name' => __("Name", 'wpfortune'),
            'description' => __("Description", 'wpfortune'),
            'download' => __("Download", 'wpfortune'),
        );

        return $columns;
    }

    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns()
    {
        return array('name' => array('name', false));
    }

    /**
     * Get the table data
     *
     * @return Array
     */
    private function table_data()
    {


        if (is_array($this->data))
            return $this->data;
        else
            return array();

    }

    /*
     * Set table data
     *
     * @return void
     */

    public function set_data($data)
    {

        foreach ($data AS $id => $info) {

            $this->data[] = array(
                'id' => $id,
                'name' => $info['name'],
                'description' => $info['description'],
                'download' => $info['url'],
            );

        }

    }

    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {

        //$plugin_data = get_option('wpfortune_pld_'.$item['id']);

        switch( $column_name ) {
            case 'name':
                return '<b>'.$item[$column_name].'</b>' . '<input type="hidden" name="Plugin['.$item['id'].'][pluginName]" value="'.$item[$column_name].'" />';
            case 'description':
                return $item[ $column_name ];
            case 'download':
                return '<a href="'.$item[ $column_name ].'" target="_blank" title="'.__('Download this plugin').'" class="button button-small button-primary">'.__('Download').'</a>';

        }
    }

    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'name';
        $order = 'asc';

        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }

        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }


        $result = strnatcmp( $a[$orderby], $b[$orderby] );

        if($order === 'asc')
        {
            return $result;
        }

        return -$result;
    }

}

?>