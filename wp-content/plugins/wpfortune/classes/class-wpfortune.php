<?php

defined('ABSPATH') or die("No access");

require_once(plugin_dir_path( __FILE__ ) . 'class-wpfortune-plugins-table.php');
require_once(plugin_dir_path( __FILE__ ) . 'class-wpfortune-plugins-table-ni.php');
require_once(plugin_dir_path( __FILE__ ) . 'class-wpfortune-api-manager.php');

/*
 * Main class for the WPFortune plugin
 */

class WPFortune extends WP_Fortune_Api_Manager {

    /*
     * @var array $wp_fortune_plugins
     */

    private $wp_fortune_plugins;

    /*
     * @var array $render_data Contains data that can be used in a rendered file
     */

    public $render_data;

    /*
     * @param array $wp_fortune_plugins List of all available WPFortune plugins
     * @return void
     */

    public function __construct($wp_fortune_plugins)
    {

        if (session_id() === '')
            session_start();

        $this->wp_fortune_plugins = $wp_fortune_plugins;

        // Version
        // @since 1.0.4
        $current_version = get_option('wpfortune_version');

        if (!$current_version || $current_version != WPF_PLUGIN_VERSION) {
            update_option('wpfortune_version', WPF_PLUGIN_VERSION);
        }

        // Run the plugin activation process
		register_activation_hook(WPF_PLUGIN_FILE, array( $this, 'plugin_activation' ) );

        add_action('admin_menu', array($this, 'add_menu_page'));

        add_action('admin_enqueue_scripts', array($this, 'register_plugin_styles'));

        $this->render_data['error'] = array();
        $this->render_data['success'] = array();

        // Run the plugin deactivation process
		register_deactivation_hook(WPF_PLUGIN_FILE, array( $this, 'plugin_deactivation' ) );

        if ( is_admin() ) {

            if (!class_exists('WPFUpdateAPICheck'))
			    require_once( plugin_dir_path( __FILE__ ) . 'am/class-wc-plugin-update.php' );

			// Check for software updates
            $this->update_check(
			    'https://www.wpfortune.com',
				untrailingslashit(plugin_basename(WPF_PLUGIN_FILE)),
				'WP Fortune',
				'wpf_order_540ed262d7058_am_0DiBSTVLoYKy',
				'plugin@wpfortune.com',
				'https://www.wpfortune.com/my-account/',
				'4kMlfjs04nns1kT3',
				'https://www.wpfortune.com',
				WPF_PLUGIN_VERSION,
				'plugin',
				'wpfortune'
			);

		}

    }

    /**
	 * Update Check Class.
	 *
	 * @return WPFUpdateAPICheck
	 */
	public function update_check( $upgrade_url, $plugin_name, $product_id, $api_key, $activation_email, $renew_license_url, $instance, $domain, $software_version, $plugin_or_theme, $text_domain, $extra = '' ) {

		return WPFUpdateAPICheck::instance( $upgrade_url, $plugin_name, $product_id, $api_key, $activation_email, $renew_license_url, $instance, $domain, $software_version, $plugin_or_theme, $text_domain, $extra );
	}

    /*
     * Registers the CSS file for this plugin
     *
     * @return void
     */

     public function register_plugin_styles()
     {

            wp_register_style('wpfortune', plugins_url('wpfortune/assets/css/style.css' ) );
	        wp_enqueue_style('wpfortune');

            wp_register_script('wpfortune_admin', plugins_url('wpfortune/assets/js/admin.js'), array('jquery'));
            wp_enqueue_script('wpfortune_admin');

     }

    /*
     * Creates an extra admin menu item for this plugin
     * @return void
     */

    public function add_menu_page()
    {

        add_menu_page('WP Fortune', 'WP Fortune', 'install_plugins', 'wpfortune', array($this, 'render_main_page'), plugin_dir_url(WPF_PLUGIN_FILE).'images/admin_icon.png');

    }

    /*
     * Renders the admin page
     * @return void
     */

    public function render_main_page()
    {

        $wpf_plugins = $this->get_wp_fortune_plugins();

        // Check and update status of plugins
        if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($wpf_plugins['installed'])) {

            $this->status_update($wpf_plugins['installed']);

        }

        //$this->render_data['not-installed-plugins'] = $wpf_plugins['not-installed'];

         // Perform the license keys activation process (POST)
        $this->license_user_activate();

        // Perform the license keys deactivation process (GET)
        $this->license_user_deactivate();

        // Set up installed plugins table
        $plugins_table = new WPFortune_Plugins_Table;

        if (isset($wpf_plugins['installed']))
            $plugins_table->set_data($wpf_plugins['installed']);

        $plugins_table->prepare_items();
        $this->render_data['pluginsTable'] = $plugins_table;

        // Set up not installed plugins table
        $plugins_table_ni = new WPFortune_Plugins_Table_NI;

        if (isset($wpf_plugins['not-installed']))
            $plugins_table_ni->set_data($wpf_plugins['not-installed']);

        $plugins_table_ni->prepare_items();
        $this->render_data['pluginsTableNI'] = $plugins_table_ni;


        // Render the page
        include_once(WPF_PLUGIN_DIR . 'pages/main.php');

    }

    /*
     * Activates the license remotely based on user input and saves the result to the database
     *
     * @return array Response data, containing the result of the process
     */

    private function license_user_activate()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['Plugin']) && check_admin_referer('plugins-actions')) {

            $plugins = $this->get_wp_fortune_plugins();
            $installed_plugins = $plugins['installed'];

            if (is_array($_POST['Plugin'])) {

                $success = 0;

                foreach($_POST['Plugin'] AS $plugin_id => $data) {

                    $current_plugin = $installed_plugins[$plugin_id];

                    $plugin_name = trim($current_plugin['name']);

                    if (!empty($data['license_key'])) {

                        if (is_email($data['license_email'])) {

                            $license_email = sanitize_email($data['license_email']);
                            $license_key = trim($data['license_key']);

                            // Connect to server and activate the license
                            $result = $this->activate_license(array(
                                'email' => $license_email,
                                'licence_key' => $license_key,
                                'product_id' => $plugin_name,
                            ));

                            if ($result) {

                                $result = json_decode($result);


                                // On successfull activation
                                if ($result->activated === true) {

                                    // Save status into database
                                    $this->save_activate_plugin_data($current_plugin, $license_key, $license_email);
                                    $success++;

                                } elseif (!empty($result->error)) {

                                    $this->render_data['error'][] = '<b>'.$plugin_name.':</b> '.$result->error;

                                }

                                // Check status again

                                else {

                                    $result = $this->status(array(
                                        'name' => $plugin_name,
                                        'email' => $license_email,
                                        'licence_key' => $license_key,
                                        'product_id' => $plugin_name,
                                    ));


                                    if ($result) {

                                        $result = json_decode($result);

                                        // If status of activated license is active already and product names match
                                        if (isset($result->status_check) && $result->status_check == 'active' && strtolower($result->product_id) == strtolower($plugin_name)) {

                                            $this->save_activate_plugin_data($current_plugin, $license_key, $license_email);

                                            $success++;

                                        }  elseif ($result->error !== null) {

                                            $this->render_data['error'][] = sprintf( __( 'License for %1$s could not be activated. Error: %2$s', 'wpfortune' ), '<b>'.$plugin_name.'</b>', $result->error );

                                        } else {

                                            $this->render_data['error'][] = sprintf( __( 'License for %s could not be activated. Please check your license key again!', 'wpfortune' ), '<b>'.$plugin_name.'</b>' );

                                        }

                                    }

                                }

                            } else {

                                $this->render_data['error'][] = sprintf( __( 'There was an error during the remote connection for %s. This is probably caused by an outdated version of cURL and/or OpenSSL on your server. You can read more about this issue <a href="https://wpfortune.com/documentation/licence-activation/licence-troubleshouting/#connectionerrorwhileactivatinglicenceinwpfortuneplugin" target="blank">at our helpdesk</a>.', 'wpfortune'), '<b>'.$plugin_name.'</b>' );

                            }

                        } else {

                            $this->render_data['error'][] = sprintf( __( 'No valid e-mail given for %s.', 'wpfortune' ), '<b>'.$plugin_name.'</b>' );

                        }

                    } else {

                        //$this->render_data['error'][] = sprintf( __( 'The license key for %s may not be empty.', 'wpfortune'), '<b>'.$plugin_name.'</b>' );

                    }

                    unset($result);

                }

            }

            if ($success)
                $this->render_data['success'][] = sprintf(_n('1 License successfully activated', '%d Licenses successfully activated', $success, 'wpfortune' ), $success);

        }

    }

    /*
     * Deactivate a license
     *
     * @return void
     */

    private function license_user_deactivate()
    {

        if (!empty($_GET['deactivate'])) {

            $plugin_id = strtolower($_GET['deactivate']);

            $option = get_option('wpfortune_pld_' . $plugin_id);

            if ($option) {

                $option_data = $option;

                // Only deactivate is plugin is active
                if ($option_data['active'] == 1) {

                    $result = $this->deactivate_license(array(
                        'email' => $option_data['license_email'],
                        'licence_key' => $option_data['license_key'],
                        'product_id' => $option_data['name'],
                    ));


                    if ($result) {



                        $result = json_decode($result);

                        if ($result->deactivated === true) {

                            $this->clear_plugin_data($plugin_id);

                            $this->render_data['success'][] = sprintf( __( 'License for %s successfully deactivated.', 'wpfortune' ), '<b>'.$option_data['name'].'</b>');

                        } else {

                            $this->render_data['error'][] = sprintf( __( 'License for %1$s could not be deactivated. Please try to refresh the plugins by clicking on the refresh button. Error: %2$s', 'wpfortune' ), '<b>'.$option_data['name'].'</b>', $result->error );

                        }

                    }



                }

                // Refresh page afterwards
                //wp_redirect(admin_url('admin.php?page='.$_GET["page"]));

            } else {

                $this->render_data['error'][] = __('License could not be deactivated here. Please try to deactivate this license on WPFortune in \'My account\'.');

            }

        }

    }

    /*
     * Check status of installed plugins and update database if needed
     *
     * @param array $WPFPlugins Installed plugins
     *
     */

    public function status_update($wpf_plugins)
    {

        // Manual renew status
        if (isset($_GET['renewstatus']) && $_GET['renewstatus']) {

            if (check_admin_referer('renew-status')) {
                unset( $_SESSION['last-wp-fortune-check']);
            } else {
                die('Security check');
            }

        }

        // Do check once in a while
        if (!isset($_SESSION['last-wp-fortune-check']) || strtotime($_SESSION['last-wp-fortune-check']) < strtotime('-25 minute')) {

            $_SESSION['last-wp-fortune-check'] = date('d-m-Y H:i:s');

            foreach ($wpf_plugins AS $plugin_id => $data) {

                $option_name = 'wpfortune_pld_' . $plugin_id;
                $option_data = get_option($option_name);

                $plugin_name = trim($data['name']);

                if ($option_data) {

                    // Only check if currently activated licenses are deactivated remotely
                    if ($option_data['active'] == 1) {

                        // License deactivated check
                        $result = $this->status(array(
                            'email' => $option_data['license_email'],
                            'licence_key' => $option_data['license_key'],
                            'product_id' => $plugin_name,
                        ));

                        $result = json_decode($result);

                        // If status of activated license is changed to inactive or license key is removed
                        if ($result->status_check == 'inactive') {

                            $this->clear_plugin_data($plugin_id);

                        } elseif (!isset($result->status_check) && empty($result->activated)) {

                            $this->clear_plugin_data($plugin_id);

                        }

                        // Do update check
                         $plugin_info = $this->plugin_information(array(
                            'plugin_name'		=>	$plugin_name,
                			'version'			=>	$data['version'],
                			'product_id'		=>	$plugin_name,
                			'api_key'			=>	$option_data['license_key'],
                			'activation_email'	=>	$option_data['license_email']
                         ));



                         if ($plugin_info) {

                             $latest_version = $plugin_info->new_version;
                             // Save latest version into database
                             $this->save_plugin_data($plugin_id, array(
                                'latest_version' => $latest_version
                             ));
                         }

                    }

                }

            }

            // Refresh the page without renewstatus get parameter
            if (isset($_GET['renewstatus']) && $_GET['renewstatus'])
                wp_redirect(admin_url('admin.php?page='.$_GET["page"]));

        }

    }

    /*
     * Saves the data into the database upon succesfull activation of a plugin.
     * A version update check is also performed
     *
     * @return void
     */


    private function save_activate_plugin_data($current_plugin, $license_key, $license_email)
    {
         $plugin_name = trim($current_plugin['name']);

         // Do update check
         $plugin_info = $this->plugin_information(array(
            'plugin_name'		=>	$plugin_name,
			'version'			=>	$current_plugin['version'],
			'product_id'		=>	$plugin_name,
			'api_key'			=>	$license_key,
			'activation_email'	=>	$license_email
         ));

         if ($plugin_info) {
           $latest_version = $plugin_info->new_version;
         }

         // Save status into database
         $this->save_plugin_data($current_plugin['id'], array(
            'name' => $plugin_name,
            'active' => 1,
            'license_key' =>$license_key,
            'license_email' => $license_email,
            'version' => $current_plugin['version'],
            'latest_version' => $latest_version
         ));

    }

    /*
     * Clears the plugin data in the database, usually for deactivation purposes
     *
     * @param string $plugin_id The id for the plugin
     */

    private function clear_plugin_data($plugin_id)
    {

        $this->save_plugin_data($plugin_id, array(
            'active' => 0,
            'license_key' => null,
            'license_email' => null,
        ));

    }

    /*
     * Save plugin data
     * @param string $plugin_id
     * @param array $args array of arguments to be saved (optionName => value pairs)
     * @return void
     */

    private function save_plugin_data($plugin_id, $args)
    {

        $option_name = 'wpfortune_pld_' . $plugin_id;
        $option = get_option($option_name);

        if (!$option)
            add_option($option_name, '', '', 'no');
        else
            $option_data = $option;


        if (is_array($args)) {

            foreach ($args AS $name => $value) {
              $option_data[$name] = $value;
            }

            update_option($option_name, $option_data);

        }

    }


    /*
     * On plugin activation
     * @return void
     */

     public function plugin_activation()
     {

        // Generate a random key, unique for this blog, used for de API instance ID
        add_option('wpfortune_instance', wp_generate_password(12, false));

     }

    /*
     * On plugin deactivation
     * @return void
     */

     public function plugin_deactivation()
     {
        global $wpdb;

        // Delete all wpfortune options
        $wpdb->query( "DELETE FROM {$wpdb->options} WHERE option_name LIKE 'wpfortune_%'" );

     }

    /*
     * Get all installed WPFortune plugins
     * @return array Installed WPFortune plugins
     */

    private function get_wp_fortune_plugins()
    {

        // Get already installed WPFortune plugins
        $installed_plugins = get_plugins();

        $non_premium_plugins = $this->get_non_premium_plugins();

        $plugin_list = array();

        if (is_array($installed_plugins)) {

            foreach ($installed_plugins AS $file => $data) {

                if (strtolower(str_replace(' ', '', $data['Author'])) == 'wpfortune' && !in_array($data['Name'], $non_premium_plugins)) {

                    $dir = explode('/', $file);

                    $plugin_list['installed'][$dir[0]] = array(
                        'id' => $dir[0],
                        'file' => $file,
                        'name' => $data['Name'],
                        'url' => $data['PluginURI'],
                        'author' => $data['Author'],
                        'version' => $data['Version'],
                        'is_active' => (is_plugin_active($file))?1:0,
                    );
                }

            }

        }

        // Get not installed yet WPFortune plugins
        if (is_array($this->wp_fortune_plugins)) {

              foreach ($this->wp_fortune_plugins AS $id => $data) {

                  // If not installed yet
                  if (!is_array($plugin_list['installed'][$id])) {

                      $plugin_list['not-installed'][$id] = $data;

                  }

              }

        }

        return $plugin_list;

    }

    /*
     * Get and convert plugin help html
     *
     * @param string $plugin_id
     * @param string $plugin_name
     * @return string|boolean Formatted html or false if no help file is found
     */

    public function get_help_html($plugin_id, $plugin_name)
    {

        $help_dir = WP_PLUGIN_DIR.'/'.$plugin_id.'/assets/help/';
        $help_file = $help_dir.'help.txt';

        if (file_exists($help_file)) {

            $content = file_get_contents($help_file);

            $content_exp = explode("\n", $content);
            $links = '';
            foreach ($content_exp AS $link_row) {

                $link_exp = explode('|', $link_row);

                $links .= '<li><a href="'.$link_exp[1].'" target="_blank">'.$link_exp[0].'</a></li>';

            }

            if (empty($links)) {

              return false;

            } else {

                $return = '<img src="'.plugins_url($plugin_id).'/assets/help/img.png"  class="wpf-help-img" />';
                $return .= '<h3>'.$plugin_name.'</h3>';
                $return .=  '<ul>';
                $return .= $links;
                $return .= '</ul>';

                return $return;

            }

        } else {
            return false;
        }

    }

    /*
     * Returns non premium plugins
     *
     * @return array Non premium plugins
     * @since 1.0.5
     */

    public static function get_non_premium_plugins()
    {

        return array(

            'WP Fortune',
            'WooCommerce Uploads Restore Settings',

        );

    }

    /*
     * Get plugin data of a specific plugin file
     * @param string $file File of the plugin
     * @return array The plugin data
     */

    private function get_plugin_data($file)
    {
        if (file_exists($file)) {
            get_plugin_data($file);
        }
    }

}