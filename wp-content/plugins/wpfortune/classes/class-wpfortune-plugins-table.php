<?php

defined('ABSPATH') or die("No access");

// WP_List_Table is not loaded automatically so we need to load it in our application
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

 /*
  * Create a new table class that will extend the WP_List_Table
  */

class WPFortune_Plugins_Table extends WP_List_Table
{

    private $data;

    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        $data = $this->table_data();
        usort( $data, array( &$this, 'sort_data' ) );

        $perPage = 20;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);

        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );

        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);

        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }

    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns()
    {
        $columns = array(
            'name' => __("Name", 'wpfortune'),
            'version' => __("Version", 'wpfortune'),
            'latestversion' => __("Latest version", 'wpfortune'),
            'license-email' => __("License E-mail", 'wpfortune'),
            'license' => __("License Key", 'wpfortune'),
            'active' => __("License Status", 'wpfortune'),
            'url' => __("More info", 'wpfortune'),
        );

        return $columns;
    }

    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns()
    {
        return array('name' => array('name', false));
    }

    /**
     * Get the table data
     *
     * @return Array
     */
    private function table_data()
    {


        if (is_array($this->data))
            return $this->data;
        else
            return array();

    }

    /*
     * Set table data
     *
     * @return void
     */

    public function set_data($data)
    {
        foreach ($data AS $id => $info) {

            $this->data[] = array(
                'id' => $id,
                'name' => $info['name'],
                'version' => 'v'.$info['version'],
                'url' => $info['url'],
                'active' => $info['is_active'],
            );

        }

    }

    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {

        $plugin_data = get_option('wpfortune_pld_'.$item['id']);

        switch( $column_name ) {
            case 'name':
                return '<b>'.$item[$column_name].'</b>' . '<input type="hidden" name="Plugin['.$item['id'].'][pluginName]" value="'.$item[$column_name].'" />';
            case 'version':
                return $item[ $column_name ];
            case 'latestversion':
                return ($plugin_data['active'] == 1 && !empty($plugin_data['latest_version']))?'v'.$plugin_data['latest_version']:__('Not available');
            case 'active':
                return ($plugin_data['active'] == 1)?'<div class="dashicons dashicons-yes" style="color: #40C970; font-size: 24px;"></div> <a href="'.admin_url('admin.php?page='.$_GET["page"].'&deactivate='.$item['id']).'" class="button button-secondary wpf-deactivate">'.__('Deactivate', 'wpfortune').'</a>':'<div class="dashicons dashicons-no" style="color: #CC0B1E;"></div>';
            case 'license':
                return '<input type="text" name="Plugin['.$item['id'].'][license_key]" value="'.$plugin_data['license_key'].'" placeholder="' . __("Your license key here", 'wpfortune') . '" '. (($plugin_data['license_key'] != false) ? 'DISABLED' : '') .' />';
            case 'license-email':
                return '<input type="text" name="Plugin['.$item['id'].'][license_email]" value="'.$plugin_data['license_email'].'" placeholder="' . __("Your license e-mail here", 'wpfortune') . '" '. (($plugin_data['license_email'] != false) ? 'DISABLED' : '') .' />';
            case 'url':
                return '<a href="'.$item[ $column_name ].'" target="_blank" title="'.__('More details about this plugin').'" class="button button-small button-primary">'.__('More info').'</a>';

        }
    }

    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'name';
        $order = 'asc';

        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }

        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }


        $result = strnatcmp( $a[$orderby], $b[$orderby] );

        if($order === 'asc')
        {
            return $result;
        }

        return -$result;
    }

}

?>