<?php

defined('ABSPATH') or die("No access");

/*
 * Class that performs the API calls
 * The Woo API Manager classes are required for correct use
 */

class WP_Fortune_Api_Manager {

    /*
     * @var string $wp_fortune_url
     */
    public $wp_fortune_url = 'https://www.wpfortune.com';

    // API Key URL
	public function create_software_api_url( $args ) {

		$api_url = add_query_arg( 'wc-api', 'am-software-api', $this->wp_fortune_url );

		return $api_url . '&' . http_build_query( $args );
	}

    // Upgrade API URL
	private function create_upgrade_api_url( $args ) {
		$upgrade_url = add_query_arg( 'wc-api', 'upgrade-api', $this->wp_fortune_url );

		return $upgrade_url . '&' . http_build_query( $args );
	}

    /*
     * Activates a license
     *
     * @param array $args Array of arguments passed to the API
     */


	public function activate_license( $args ) {
        
		$defaults = array(
			'request' 			=> 'activation',
			'instance' 			=> get_option('wpfortune_instance') . substr(md5($args['product_id']), 0, -16),
			'platform' 			=> home_url(),
		);

		$args = wp_parse_args( $defaults, $args );

		$target_url = self::create_software_api_url( $args );

		$request = wp_remote_get( $target_url );

		if( is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) != 200 ) {
		// Request failed
			return false;
		}

		$response = wp_remote_retrieve_body( $request );

		return $response;
	}

    /*
     * Deactivates a license
     *
     * @param array $args Array of arguments passed to the API
     */

	public function deactivate_license( $args ) {

		$defaults = array(
			'request' 		=> 'deactivation',
			'instance' 		=> get_option('wpfortune_instance') . substr(md5($args['product_id']), 0, -16),
			'platform' 		=> home_url(),
		);

		$args = wp_parse_args( $defaults, $args );

		$target_url = self::create_software_api_url( $args );

		$request = wp_remote_get( $target_url );

		if( is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) != 200 ) {
		// Request failed
			return false;
		}

		$response = wp_remote_retrieve_body( $request );

		return $response;
	}

	/**
	 * Checks if the software is activated or deactivated
	 * @param  array $args
	 * @return array
	 */
	public function status( $args ) {

		$defaults = array(
			'request' 		=> 'status',
			'instance' 			=> get_option('wpfortune_instance') . substr(md5($args['product_id']), 0, -16),
			'platform' 			=> home_url(),
		);

		$args = wp_parse_args( $defaults, $args );

		$target_url = self::create_software_api_url( $args );

		$request = wp_remote_get( $target_url );

		if( is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) != 200 ) {
		// Request failed
			return false;
		}

		$response = wp_remote_retrieve_body( $request );

		return $response;
	}

    /**
	 * Sends and receives data to and from the server API
	 *
	 * @access public
	 * @since  1.0.0
	 * @return object $response
	 */
	public function plugin_information( $args )
    {

        $defaults = array(
		    'request'			=>	'pluginupdatecheck',
			'instance'			=>	get_option('wpfortune_instance') . substr(md5($args['product_id']), 0, -16),
			'domain'			=>	home_url(),
		);

        $args = wp_parse_args($defaults, $args);

		$target_url = $this->create_upgrade_api_url( $args );

		$request = wp_remote_get( $target_url, array( 'timeout' => 45 ) );

		if ( is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) != 200 ) {
			return false;
		}

		$response = unserialize( wp_remote_retrieve_body( $request ) );


		/**
		 * For debugging errors from the API
		 * For errors like: unserialize(): Error at offset 0 of 170 bytes
		 * Comment out $response above first
		 */
		// $response = wp_remote_retrieve_body( $request );
		// print_r($response); exit;


		if ( is_object( $response ) ) {
			return $response;
		} else {
			return false;
		}
	}

}