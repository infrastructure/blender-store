<div id="wpf" class="wrap">

    <h2>WPFortune</h2>

    <div id="poststuff">

        <div id="post-body">

            <div class="postbox-container">

            <h2 id="wpf-settings-tabs" class="nav-tab-wrapper">
                <a href="#1" id="wpf-settings-tab-1" data-id="1" class="nav-tab nav-tab-active"><?php _e('Licenses', $this->plugin_id); ?></a>
                <a href="#2" id="wpf-settings-tab-2" data-id="2" class="nav-tab"><?php _e('Help', $this->plugin_id); ?></a>
            </h2>

            <div id="wpf-settings-container-1" class="wpf-settings-container">

                  <h3 class="wpf-left"><?php echo __("Installed WPFortune plugins", 'wpfortune'); ?></h3>

                  <a href="<?php echo admin_url('admin.php?page='.$_GET["page"].'&renewstatus=1&_wpnonce='.wp_create_nonce('renew-status')); ?>" class="wpf-right button button-small button-secondary" title="<?php echo __('Refresh the plugins'); ?>"><div class="dashicons dashicons-update"></div> Refresh</a>

                  <div class="clear"></div>

                  <?php if (is_array($this->render_data['success'])): ?>

                      <?php foreach ($this->render_data['success'] AS $success): ?>

                          <div class="updated">
                              <p><?php echo $success; ?></p>
                          </div>

                      <?php endforeach; ?>

                  <?php endif; ?>

                  <?php if (is_array($this->render_data['error'])): ?>

                      <?php foreach ($this->render_data['error'] AS $error): ?>

                          <div class="error">
                              <p><?php echo $error; ?></p>
                          </div>

                      <?php endforeach; ?>

                  <?php endif; ?>

                  <form action="<?php echo admin_url('admin.php?page='.$_GET["page"]); ?>" method="post">

                      <?php $this->render_data['pluginsTable']->display(); ?>

                      <?php wp_nonce_field('plugins-actions'); ?>

                      <div style="float: right;">
                          <input type="submit" value="<?php echo __("Activate license keys", 'wpfortune'); ?>" class="button button-large button-green"/>
                      </div>

                      <div class="clear"></div>

                  </form>

                  <?php /*
                  <div class="wpf-not-installed">

                      <h3><?php echo __("More interesting plugins from WPFortune", 'wpfortune'); ?></h3>

                      <?php $this->render_data['pluginsTableNI']->display(); ?>


                  </div>
                  */ ?>

            </div>

            <div id="wpf-settings-container-2" class="wpf-settings-container hidden">

                <?php include('_help.php'); ?>

            </div>

        </div>

        </div>

    </div>

</div>