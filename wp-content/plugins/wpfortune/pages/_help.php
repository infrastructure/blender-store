<div class="about-wrap">

    <div id="wpf-help" class="feature-section col three-col">

        <div class="wpf-single-help">

            <img src="<?php echo plugins_url('wpfortune'); ?>/images/help-getting-started.png" class="wpf-help-img" />

            <h3><?php _e('Getting started', 'wpfortune'); ?></h3>

            <ul>

                <li><a href="http://wpfortune.com/documentation/" target="_blank"><?php _e('Documentation', 'wpfortune'); ?></a></li>
                <li><a href="https://support.wpfortune.com/" target="_blank"><?php _e('Helpdesk', 'wpfortune'); ?></a></li>
                <li><a href="http://wpfortune.com/wordpress-plugins/" target="_blank"><?php _e('Our plugins', 'wpfortune'); ?></a></li>

            </ul>

        </div>

        <?php

        if (is_array($wpf_plugins['installed'])):

        foreach ($wpf_plugins['installed'] AS $plugin_id => $plugin_data):

        $html = $this->get_help_html($plugin_id, $plugin_data['name']);

        if (!empty($html)):  ?>

        <div class="wpf-single-help">

            <?php echo $html; ?>

        </div>

        <?php

        endif;

        endforeach;

        endif; ?>

    </div>

</div>