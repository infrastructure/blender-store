/*
 * @author WPFortune
 */

jQuery(document).ready(function($) {

    /*
     * Navigation tabs
     */

     // On page load
     if (window.location.hash != '') {

        var current_tab_id = window.location.hash.replace('#', '');
        switch_tabs(current_tab_id);

     }

     // On click

     var form_action;

     $('#wpf-settings-tabs a').click(function() {

          var tab_id = $(this).data('id');

          switch_tabs(tab_id);
     });


     function switch_tabs(tab_id) {

        $('#wpf-settings-tabs .nav-tab').removeClass('nav-tab-active');
        $('a#wpf-settings-tab-'+tab_id).addClass('nav-tab-active');

        $('.wpf-settings-container').hide();

        $('#wpf-settings-container-'+tab_id).show();

     }

});