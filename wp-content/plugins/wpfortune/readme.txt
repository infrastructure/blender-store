=== WP Fortune ===
Contributors: wpfortune
Tags: plugin, manage, licence
Requires at least: 3.8
Tested up to: 4.5
Stable tag: 1.0.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Manage all of your WP Fortune licences

== Description ==

Manage easily all of your WP Fortune licences. Active and deactivate all of your licences.

== Installation ==

1. Download the .zip file.
1. Upload the entire �wpfortune� directory into the �/wp-content/plugins/� directory.
1. Activate the plugin through the �Plugins� menu in WordPress.
1. Configure the plugin by going to WP Fortune in the admin menu


== Frequently Asked Questions ==

For the WP Fortune FAQ, please check the Frequently Asked Questions section in our [helpdesk](https://wpfortune.com/documentation/licence-activation/)

== Changelog ==
= 2016.04.29 - 1.0.8 =
* Changed some helpdesk urls

= 2016.02.03 - 1.0.7 =
* Added: Notification for unsupported versions of cURL and/or OpenSSL

= 2015.01.12 - 1.0.6 =
* Fix: Minor tweaks for new activation API

= 2014.10.13 - 1.0.5 =
* Fix: Removed non premium plugins from plugin overview

= 2014.09.12 - 1.0.4 =
* Fix: Update on activation API

= 2014.09.10 - 1.0.3 =
* Fix: Fixed problem with licence activation when having two products in one order

= 2014.09.09 - 1.0.2 =
* Fix: The plugin can now update itself
* Fix: Plugin is now compatible with https version of wpfortune.com

= 2014.09.01 - 1.0.0 =
* Stable release of WP Fortune