<?php
/*
Plugin Name: WP Fortune
Plugin URI: http://www.wpfortune.com
Description: Manage updates and licenses for WP Fortune plugins
Version: 1.0.8
Author: WP Fortune
Author URI: http://www.wpfortune.com/
License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/
/*  Copyright 2014  WP Fortune  (email : info@wpfortune.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

defined('ABSPATH') or die("No access");

define('WPF_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
define('WPF_PLUGIN_FILE', __FILE__);
define('WPF_PLUGIN_VERSION', '1.0.8');

ob_start();

$wp_fortune_plugins = array();

if (is_admin()) {
    require_once('classes/class-wpfortune.php');
    $wp_fortune = new WPFortune($wp_fortune_plugins);
}


?>