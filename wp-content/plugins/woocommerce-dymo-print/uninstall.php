<?php
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) )	exit ();

if(get_option('wc_dymo_uninstall_files')==1) {
	$upload_dir   = wp_upload_dir();
	$upload_dir=$upload_dir['basedir'].'/wc-dymo-labels/';
	require_once ABSPATH . '/wp-admin/includes/class-wp-filesystem-base.php';
	require_once ABSPATH . '/wp-admin/includes/class-wp-filesystem-direct.php';
	WP_Filesystem_Direct::rmdir($upload_dir, true);
}

if(get_option('wc_dymo_uninstall')==1) {
	delete_option('wc_dymo_labelfile' );
	delete_option('wc_dymo_option' );
	delete_option('wc_dymo_option_extras' );
	delete_option('wc_dymo_debug' );
	delete_option('wc_dymo_order_item');
	delete_option('wc_dymo_printers');
	delete_option('wc_dymo_printers_args');
	delete_option('wc_dymo_products');
	delete_option('wc_dymo_labelfile_fallback');
	/* New for 5.0.0 */
	delete_option( 'wc_dymo_notice-old_dymo');
	delete_option('wc_dymo_labeltime');
	delete_option('wc_dymo_combo');
	delete_option('wc_dymo_type');
	delete_option('wc_dymo_uninstall');
	delete_option('wc_dymo_uninstall_files');
}