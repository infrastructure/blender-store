<?php
/* 
 *	Settings Footer and sidebar
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
				</div>
			</div>
			<div style="float:right; width:25%;">
				<div class="postbox">
					<div class="inside">
						<h3><?php _e( 'Need Support?', 'woocommerce-dymo' ); ?></h3>
						<p><?php _e( 'If you are having problems with this plugin, please contact us via our ', 'woocommerce-dymo' ); ?> <a target=_blank href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/"><?php _e('Helpdesk','woocommerce-dymo');?></a>.</p>
						<p><?php _e( 'We will try to support you as soon as possible, mostly within 48 hours.', 'woocommerce-dymo' ); ?></p>
						<p><?php _e( 'On our website you will also find some basic support information for this plugin.', 'woocommerce-dymo' ); ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>