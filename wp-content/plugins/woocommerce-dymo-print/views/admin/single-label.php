<?php
/*
 *	Settings Single label
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<div id="<?php echo $label;?>" class="tab">
	<form name="chooselabel" method="post" enctype="multipart/form-data">
		<input type="hidden" name="label" value="<?php echo $label;?>">
		<input type="hidden" name="dymo_fields_submitted" value="submitted">
		<?php wp_nonce_field('dymo_nonce_action','dymo_nonce_field'); ?>

		<div class="postbox">
			<div class="inside">
				<h3><?php _e('Step 1: DYMO label file','woocommerce-dymo');?></h3>
				<p><em><?php echo sprintf(__('For more information about how to create a new DYMO label, see %s.', 'woocommerce-dymo'),'<a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/create-label/" target="_blank">'.__('our documentation','woocommerce-dymo').'</a>');?></em></p>

				<table class="form-table">
					<tr>
						<th>
							<label><?php _e('Label type','woocommerce-dymo');?>:</label>
						</th>
						<td>

						<?php
							switch($args['type']) {
								case 'order_item':
									_e('Order item label: Prints a label from each order item (line) from an order','woocommerce-dymo');
									break;
								default:
									_e('Standard order label: Prints WooCommerce order information.','woocommerce-dymo');
							}
						?>

						</td>
					</tr>

					<?php
					if(isset($chosenFile[$label]) && in_array($chosenFile[$label],$files)) {
					?>
					<tr class="dymo-preview-row">
						<th class="left_label">
							<label for="currentLabel"><?php _e('Current DYMO label file:','woocommerce-dymo');?></label>
						</th>
						<td>
							<div id="<?php echo $label;?>_preview" class="dymo-preview"></div>
						</td>
					</tr>
					<?php }

					if(count($files) > 0) {
					?>
					<tr>
						<th class="left_label">
							<label for="labelfile_<?php echo $label;?>"><?php _e('Select DYMO label file:','woocommerce-dymo');?></label>
						</th>
						<td>
							<select name="wc_dymo_labelfile" id="labelfile_<?php echo $label;?>" class="dymo-select-label">
								<option value="">-- <?php _e('Choose label','woocommerce-dymo');?> --</option>
								<?php
									foreach ($files as $file) {
										if(isset($chosenFile[$label])) {
											echo '<option value="'.$file.'" '.selected($file,$chosenFile[$label],false).'>'.$file.'</option>';
										} else {
											echo '<option value="'.$file.'">'.$file.'</option>';
										}
									}
								?>
							</select>
							<p class="description"><?php _e( 'Select a label which is already uploaded.', 'woocommerce-dymo' ); ?></p>
						</td>
					</tr>
					<?php } ?>

					<tr>
						<th class="left_label">
							<label for="uploadfile_<?php echo $label;?>"><b><?php _e( 'Upload new label file:', 'woocommerce-dymo' ); ?></b></label>
						</th>
						<td>
							<input type="file" name="uploadfile_<?php echo $label;?>" id="uploadfile_<?php echo $label;?>" size="35" class="uploadfile" />
							<p class="description"><?php _e( 'Upload your .dymo file, which you have made with the DYMO Connect (DCD).', 'woocommerce-dymo' ); ?></p>
						</td>
					</tr>
					<tr>
						<th colspan="2">
							<input type="submit" name="Submit" class="button-primary" value="<?php echo sprintf(__( 'Save %s label layout', 'woocommerce-dymo' ),$args['name']); ?>" />
						</th>
					</tr>
				</table>
			</div>
		</div>

		<div class="postbox">
			<div class="inside">
				<h3><?php _e('Step 2: Change label settings','woocommerce-dymo');?></h3>
				<p><em><?php echo sprintf(__('Need help setting up your label? %s in our documentation.', 'woocommerce-dymo'),'<a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/label-setup/" target="_blank">'.__('Read instructions','woocommerce-dymo').'</a>');?>
					<br><?php _e('Select which WooCommerce data you want to asign to a label-object','woocommerce-dymo');?>
				</em></p>

				<?php
					// Load XML label
						if(isset($chosenFile[$label]) && in_array($chosenFile[$label],$files)) {
							echo '<table class="form-table">
									<tbody>';

									$xml=simplexml_load_file($path.$chosenFile[$label]);

									if(isset($xml->ObjectInfo)) {
										$XMLobjects=$xml->ObjectInfo;
									} elseif(isset($xml->DYMOLabel->DynamicLayoutManager->LabelObjects)) {
										$XMLobjects=$xml->DYMOLabel->DynamicLayoutManager->LabelObjects;
									} else {
										$xml=false;
									}
									

									$loadObjects=array('AddressObject','TextObject','CircularTextObject','BarcodeObject');

									if(!$xml) {
										echo '<div class="dymo-error dymo-xml-error"><p>'.sprintf(__( 'XML data could not be loaded. Please check your directory permissions on %s.','woocommerce-dymo'),'<em>'.$path.$chosenFile[$label]).'</em>'.'</a></p></div>';
									} else{
										foreach($XMLobjects as $object) {
											foreach($object as $objectType=>$data) {
												if(in_array($objectType,$loadObjects)){
													$objectName=(string)$data->Name;

													if(!isset($objects[$objectName])) {
														$objects[$objectName]='';
													}

													if($objectType=='BarcodeObject' && $data->Type=='QRCode') $objectType='QRcode';

													echo '<tr class="dymo-object-row"><th><strong>Object: '.$objectName.'</strong> <span class="dymo-object-type">'.$objectType.'</span></th><td>';

														// get select field
														echo '<select class="dymo-object-data-select" id="wc_dymo_option_'.$label.'_'.$objectName.'" name="wc_dymo_option['.$objectName.']"><option value="">--'.__('Select WooCommerce data','woocommerce-dymo').'--</option>';

														//Order specific data
														echo '<optgroup label="'.__('Order data','woocommerce-dymo').'">
															<option value="19" '.selected(19,$objects[$objectName],false).'>'.__('Order ID', 'woocommerce-dymo' ).'</option>
															<option value="1" '.selected(1,$objects[$objectName],false).'>'.__('Order number', 'woocommerce-dymo' ).'</option>
															<option value="3" '.selected(3,$objects[$objectName],false).'>'.__('Order total (incl. TAX)', 'woocommerce-dymo' ).'</option>
															<option value="4" '.selected(4,$objects[$objectName],false).'>'.__('Shipping method', 'woocommerce-dymo' ).'</option>
															<option value="2" '.selected(2,$objects[$objectName],false).'>'.__('Product list','woocommerce-dymo').'</option>
															<option value="18" '.selected(18,$objects[$objectName],false).'>'.__('Customer message','woocommerce-dymo').'</option>';
															if(is_plugin_active('woocommerce-pip/woocommerce-pip.php') || is_plugin_active('woocommerce-pdf-invoice/woocommerce-pdf-invoice.php')) {
																echo '	<option value="invoice" '.selected('invoice',$objects[$objectName],false).'>'.__( 'Invoice number', 'woocommerce-dymo' ).'</option>';
															}
														echo '</optgroup>';
														
														echo '<optgroup label="'.__('Order dates','woocommerce-dymo').'">
																<option value="15" '.selected(15,$objects[$objectName],false).'>'.__('Order date & time', 'woocommerce-dymo' ).'</option>
																<option value="16" '.selected(16,$objects[$objectName],false).'>'.__('Order date', 'woocommerce-dymo' ).'</option>
																<option value="17" '.selected(17,$objects[$objectName],false).'>'.__('Order time', 'woocommerce-dymo' ).'</option>
																<option value="order_completed_datetime" '.selected('order_completed_datetime',$objects[$objectName],false).'>'.__('Order completed date & time', 'woocommerce-dymo' ).'</option>
																<option value="order_completed_date" '.selected('order_completed_date',$objects[$objectName],false).'>'.__('Order completed date', 'woocommerce-dymo' ).'</option>
																<option value="order_completed_time" '.selected('order_completed_time',$objects[$objectName],false).'>'.__('Order completed time', 'woocommerce-dymo' ).'</option>
																<option value="order_paid_datetime" '.selected('order_paid_datetime',$objects[$objectName],false).'>'.__('Order paid date & time', 'woocommerce-dymo' ).'</option>
																<option value="order_paid_date" '.selected('order_paid_date',$objects[$objectName],false).'>'.__('Order paid date', 'woocommerce-dymo' ).'</option>
																<option value="order_paid_time" '.selected('order_paid_time',$objects[$objectName],false).'>'.__('Order paid time', 'woocommerce-dymo' ).'</option>
															  </optgroup>';

														//Customer specific data
														echo '<optgroup label="'.__('Customer data (billing)','woocommerce-dymo').'">
																<option value="5" '.selected(5,$objects[$objectName],false).'>'.__('Billing address (default format)','woocommerce-dymo').'</option>
																<option value="6" '.selected(6,$objects[$objectName],false).'>'.__('Billing address (custom format)','woocommerce-dymo').'</option>
																<option value="9" '.selected(9,$objects[$objectName],false).'>'.__('Billing state (full)','woocommerce-dymo').'</option>
																<option value="10" '.selected(10,$objects[$objectName],false).'>'.__('Billing country (full)','woocommerce-dymo').'</option>
																<option value="7" '.selected(7,$objects[$objectName],false).'>'.__('Customer Email address','woocommerce-dymo').'</option>
																<option value="8" '.selected(8,$objects[$objectName],false).'>'.__('Customer telephone number','woocommerce-dymo').'</option>
															</optgroup>';
														echo '<optgroup label="'.__('Customer data (shipping)','woocommerce-dymo').'">
																<option value="11" '.selected(11,$objects[$objectName],false).'>'.__('Shipping address (default format)','woocommerce-dymo').'</option>
																<option value="12" '.selected(12,$objects[$objectName],false).'>'.__('Shipping address (custom format)','woocommerce-dymo').'</option>
																<option value="13" '.selected(13,$objects[$objectName],false).'>'.__('Shipping state (full)','woocommerce-dymo').'</option>
																<option value="14" '.selected(14,$objects[$objectName],false).'>'.__('Shipping country (full)','woocommerce-dymo').'</option>
															</optgroup>';
														if($args['type']=='order_item') {
														echo '<optgroup label="'.__('Order item product data','woocommerce-dymo').'">
																<option value="order_item_sku" '.selected('order_item_sku',$objects[$objectName],false).'>'.__('Order item SKU','woocommerce-dymo').'</option>
																<option value="order_item_name" '.selected('order_item_name',$objects[$objectName],false).'>'.__('Order item name','woocommerce-dymo').'</option>
																<option value="order_item_qty" '.selected('order_item_qty',$objects[$objectName],false).'>'.__('Order item quantity','woocommerce-dymo').'</option>
																<option value="order_item_price" '.selected('order_item_price',$objects[$objectName],false).'>'.__('Order item price','woocommerce-dymo').'</option>
																<option value="order_item_line_total" '.selected('order_item_line_total',$objects[$objectName],false).'>'.__('Order item line total','woocommerce-dymo').'</option>
																<option value="order_item_line_total_tax" '.selected('order_item_line_total_tax',$objects[$objectName],false).'>'.__('Order item line total incl. tax','woocommerce-dymo').'</option>
																<option value="order_item_short_descr" '.selected('order_item_short_descr',$objects[$objectName],false).'>'.__('Order item short description','woocommerce-dymo').'</option>
																<option value="order_item_attr_new" '.selected('order_item_attr_new',$objects[$objectName],false).'>'.__('Order item attributes','woocommerce-dymo').' '.__('(requires WC 3.1)','woocommerce-dymo').'</option>
																<option value="order_item_attr" '.selected('order_item_attr',$objects[$objectName],false).'>'.__('Order item attributes','woocommerce-dymo').' '.__('(< WC 3.1)','woocommerce-dymo').'</option>
																<option value="order_item_url" '.selected('order_item_url',$objects[$objectName],false).'>'.__('Order item url','woocommerce-dymo').'</option>
															</optgroup>';
														}

														echo '<optgroup label="'.__('Advanced users only options','woocommerce-dymo').'">
																<option value="order_function" '.selected('order_function',$objects[$objectName],false).'>'.__('Custom PHP function','woocommerce-dymo').'</option>
															  </optgroup>';

														//All order metadata
														if(is_array($order_meta) && count($order_meta)>0) {
															$serialized=array();
															echo '<optgroup label="'.__('Order metadata','woocommerce-dymo').'">';
															foreach($order_meta as $key) {
																echo '<option value="order_meta_'.$key.'" '.selected($objects[$objectName],'order_meta_'.$key,false).' data-array="'.$key.'">'.$key.'</option>';
															}
														}

														echo '</optgroup>';
														if(!isset($object_extras['serialized'][$objectName])) {$object_extras['serialized'][$objectName]='';}
														echo '</select>';

														  if((int)apply_filters('wpf_woocommerce_dymo_advanced_metadata', 0)) {
															echo '<a href="#" class="dymo-advanced-options dymo-hide">'.__('Toggle advanced options','woocommerce-dymo').'</a>';
															echo '<div class="dymo-serialized dymo-hide">';
																echo '<table>';
																echo '<tr><th>'.__('Order ID to get order metadata','woocommerce-dymo').'</th><td>';
																echo '<input type="text" class="small-input dymo-orderID" value="'.$latest_order.'" >';
																echo '<a href="#" class="dymo-serialized-build">'.__('Build query string','woocommerce-dymo').'</a>';
																echo '</td></tr>';
																echo '<tr><th>'.__('Query string for serialized data','woocommerce-dymo').'</th><td>';
																echo '<input type="text" name="wc_dymo_option_extras[serialized]['.$objectName.']" class="regular-text dymo-metakey" value="'.esc_attr($object_extras['serialized'][$objectName]).'" >';
																echo '<div class="dymo-serialized-query">';
																echo '<pre>';
																echo '</pre>';
																echo '</div>';
																echo '</td></tr>';
															echo '</table></div>';
														  }

														// Textarea for custom address - by default hidden. Shown by jQuery call on change select
														echo '<div class="dymo-custom-address dymo-hide"><strong>'.__('Customer address (custom format)','woocommerce-dymo').'</strong>
															<textarea name="wc_dymo_option_extras[address]['.$objectName.']" cols="45" rows="3" class="regular-text" placeholder="[FIRSTNAME] [LASTNAME] [ADDRESS1] [POSTCODE] [CITY]">'.(isset($object_extras['address'][$objectName]) ? stripslashes($object_extras['address'][$objectName]):'').'</textarea><br />
															<span class="description">'.__( 'Create your own address format which will override the default WooCommerce address format.', 'woocommerce-dymo' ).'
																<br><strong>'. __( 'Possible tags:', 'woocommerce-dymo' ).'</strong>
																<code>[COMPANY]</code>, <code>[FIRSTNAME]</code>, <code>[LASTNAME]</code>, <code>[ADDRESS1]</code>, <code>[ADDRESS2]</code>, <code>[POSTCODE]</code>, <code>[CITY]</code>, <code>[STATE]</code>, <code>[STATE_ABBR]</code>, <code>[COUNTRY]</code>, <code>[COUNTRY_ABBR]</code>
																<br><strong>'. __( 'Conditional tags:', 'woocommerce-dymo' ).'</strong>
																'.sprintf( __( 'use %s and %s to hide something if no company name is available.', 'woocommerce-dymo' ),'<code>{COMPANY}</code>','<code>{/COMPANY}</code>').'<br>
																'.sprintf( __( 'use %s and %s to hide something if no address2 is available.', 'woocommerce-dymo' ),'<code>{ADDR}</code>','<code>{/ADDR}</code>').'</span>';

														// Support for Brazil checkout fields - added 2.5.3
														if(is_plugin_active('woocommerce-extra-checkout-fields-for-brazil/woocommerce-extra-checkout-fields-for-brazil.php')) {

															echo '<br><br><strong>'.__('Extra checkout fields for Brazil','woocommerce-dymo').'</strong><br>';
															echo '<span class="description"><strong>'. __( 'Possible tags:', 'woocommerce-dymo' ).'</strong> ';
															echo '<code>[NUMBER]</code>, <code>[NEIGHBORHOOD]</code>';
															echo '</span>';
														}
														echo '</div>';


														// Textarea for product list - by default hidden. Shown by jQuery call on change select
														echo '<div class="dymo-product-list dymo-hide"><strong>'.__('Product list layout','woocommerce-dymo').'</strong>
																<textarea name="wc_dymo_option_extras[productlist]['.$objectName.']" cols="45" rows="3" class="regular-text" placeholder="[QTY]x [SKU]: [PRODUCT] - [VARIATION] - [PRICE]">'.(isset($object_extras['productlist'][$objectName]) ? stripslashes($object_extras['productlist'][$objectName]):'').'</textarea><br />
																<span class="description">'.__( 'Combine SKU, product and variations into one field', 'woocommerce-dymo' ).'
																<p><strong>'. __( 'Possible tags:', 'woocommerce-dymo' ).'</strong>
																<code>[SKU]</code> <code>[PRODUCT]</code> <code>[VARIATION]</code> <code>[QTY]</code> <code>[PRICE]</code> <code>[VAR_SKU]</code> <code>[WEIGHT]</code> <code>[WEIGHT_LINETOTAL]</code> <code>[DIMENSIONS]</code></p>
																<p><strong>'. __( 'Conditional tags:', 'woocommerce-dymo' ).'</strong>
																'.sprintf( __( 'Use %s and %s to hide something if no variations available.', 'woocommerce-dymo' ),'<code>{VAR}</code>','<code>{/VAR}</code>').'<br>
																'.sprintf( __( 'Use %s or %s to create a new line.', 'woocommerce-dymo' ),'<code>{ENTER}</code>','<code>|</code>').'</span></p>
																<p>'.sprintf(__('Check %s for more information about product list layout.','woocommerce-dymo'),'<a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/label-setup/#printalistofallpurchasedproducts">'.__('our documentation','woocommerce-dymo').'</a>').'</p>';
														echo '</div>';

														// Input field for customer message wordwrap
														echo '<div class="dymo-customer-note dymo-hide"><strong>'.__('Customer message wordwrap','woocommerce-dymo').'</strong>
																<input type="number" name="wc_dymo_option_extras[customernote]['.$objectName.']" class="small-text" placeholder="80" value="'.(isset($object_extras['customernote'][$objectName]) ? (int)stripslashes($object_extras['customernote'][$objectName]):'80').'"><br />
																<span class="description">'.__( 'DYMO Label Software does not know any automatic wordwrapping. Therefore you need to manually define wordwrap for your label. Define wordwrap for customer message. Wraps customer message to given number of characters.', 'woocommerce-dymo' ).'</span>';
														echo '</div>';

														// Input field for order_item product descr wordwrap
														echo '<div class="dymo-product-descr dymo-hide"><strong>'.__('Short description wordwrap','woocommerce-dymo').'</strong>
																<input type="number" name="wc_dymo_option_extras[short_descr]['.$objectName.']" class="small-text" placeholder="80" value="'.(isset($object_extras['short_descr'][$objectName]) ? (int)stripslashes($object_extras['short_descr'][$objectName]):'80').'"><br />
																<span class="description">'.__( 'DYMO Label Software does not know any automatic wordwrapping. Therefore you need to manually define wordwrap for your label. Define wordwrap for order item short description. Wraps short description to given number of characters.', 'woocommerce-dymo' ).'</span>';
														echo '</div>';

													echo '</td></tr>';
												}
											}
										}
												}
										if($args['type']=='order_item') {
											?>
											<tr>
												<th>
													<?php _e('Print label by quantity','woocommerce-dymo');?>
												</th>
												<td>
													<label for="wc_dymo_print_qty_yes"><input type="radio" name="wc_dymo_order_item[qty]" value="1" id="wc_dymo_print_qty_yes" <?php checked($repeat,1);?>> <?php _e('Yes');?></label><br>
													<label for="wc_dymo_print_qty_no"><input type="radio" name="wc_dymo_order_item[qty]" value="0" id="wc_dymo_print_qty_no" <?php checked($repeat,0);?>> <?php _e('No');?></label><br><br>
													<span class="description">
														<strong><?php _e('Yes');?>:</strong> <?php _e('For each ordered product a label is printed. Product quantity is used. Example: If order item total is 5, 5 labels are printed.','woocommerce-dymo');?><br>
														<strong><?php _e('No');?>:</strong> <?php _e('For each order item line a label is printed. Product quantity is ignored. Example: If order item total is 5, but order contains just 3 order item lines (unique product), 3 labels are printed.','woocommerce-dymo');?>
													</span>
												</td>
											</tr>
											<?php
										}

										echo '</tbody>
											<tfoot>
												<tr><th colspan="2">
													<input type="submit" name="Submit" class="button-primary" value="'.sprintf(__('Save %s label objects','woocommerce-dymo'),$args['name']) .'" />
												</th></tr>
											</tfoot>
										</table>';
						} else {
							echo '<p>'.__('No DYMO label selected. Please select a DYMO label first.','woocommerce-dymo').'</p>';
						}

				?>
			</div>
		</div>

		<div class="postbox">
			<div class="inside">
				<h3><?php echo sprintf(__( 'Step 3: Set LabelWriter for %s labels', 'woocommerce-dymo' ),$args['name']); ?></h3>

				<?php
				if(!isset($dymoPrinters[$label]) || $dymoPrinters[$label]=="") {
					echo '<div class="dymo-error">';
					_e('No DYMO LabelWriter selected OR saved. Please select and save your printer','woocommerce-dymo');
					echo '</div>';
				} else {
					echo '<div class="dymo-error dymo-hide">';
					_e('Current printer is not connected to this computer. Please make sure your DYMO LabelWriter is connected to this computer before printing.','woocommerce-dymo');
					echo '</div>';
				}
				?>

				<table class="form-table">
					<tbody>
						<tr>
							<th>
								<label for="woocommerce_dymo_use_<?php echo $label;?>_elements"><b><?php _e( 'Choose your printer:', 'woocommerce-dymo' ); ?></b></label>
							</th>
							<td valign="top">
								<input type="text" name="wc_dymo_printers" value="<?php if(isset($dymoPrinters[$label]) && $dymoPrinters[$label]!="") { echo $dymoPrinters[$label];} ?>" class="regular-text dymo-printerField">
								<p class="description"><?php _e( 'Please choose your DYMO LabelWriter.', 'woocommerce-dymo' );?></p>
							</td>
							<td width="40%">
								<b><?php _e('Available printers:','woocommerce-dymo');?></b>
								<ul class="dymo-printersSelect"></ul>
								<span class="dymo-printerSelect-description description"><?php _e( 'Click on printer name or copy paste it', 'woocommerce-dymo' );?></span>
								<span class="dymo-printerSelect-error"><?php _e( 'DYMO Label Framework not active - no printers available.', 'woocommerce-dymo' );?></span>
							</td>
    					</tr>
						<tr class="dymo-turbo">
							<th>
								<label for="twinroll"><b><?php _e( 'DYMO Twin Turbo roll:', 'woocommerce-dymo' ); ?></b></label>
							</th>

							<td colspan="2">
								<?php if(!isset($dymoPrintersArgs[$label]) || $dymoPrintersArgs[$label]=="") $dymoPrintersArgs[$label]='auto';?>
								<input type="radio" name="wc_dymo_printers_args" value="auto" id="dymo-twin-roll_<?php echo $label;?>_auto" class="input-radio" <?php checked($dymoPrintersArgs[$label],'auto');?> /><label for="dymo-twin-roll_<?php echo $label;?>_auto"><?php _e( 'Auto', 'woocommerce-dymo' ); ?></label><br />
								<input type="radio" name="wc_dymo_printers_args" value="left" id="dymo-twin-roll_<?php echo $label;?>_left" class="input-radio" <?php checked($dymoPrintersArgs[$label],'left');?> /><label for="dymo-twin-roll_<?php echo $label;?>_left"><?php _e( 'Left', 'woocommerce-dymo' ); ?></label><br />
								<input type="radio" name="wc_dymo_printers_args" value="right" id="dymo-twin-roll_<?php echo $label;?>_right" class="input-radio" <?php checked($dymoPrintersArgs[$label],'right');?> /><label for="dymo-twin-roll_<?php echo $label;?>_right"><?php _e( 'Right', 'woocommerce-dymo' ); ?></label>
								<p class="description"><?php _e( 'When using a DYMO LabelWriter 450 Twin Turbo, please specify default roll', 'woocommerce-dymo' );?></p>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th colspan="3">
								<input type="submit" name="Submit" class="button-primary" value="<?php echo sprintf(__('Save %s label printer', 'woocommerce-dymo' ),$args['name']); ?>" />
							</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

		<?php
		$step=4;

		?>

		<div class="postbox">
			<div class="inside">
				<h3><?php _e('Step 4: Other settings','woocommerce-dymo');?></h3>
				<?php $update_status='updateStatus'.rand(0,999);?>
				<table class="form-table">
					<tbody>
						<tr>
							<th>
								<label for="<?php echo $update_status;?>"><?php _e('Update order status','woocommerce-dymo');?>:</label>
							</th>
							<td>
								<select name="wc_dymo_option_extras[orderstatus]" id="<?php echo $update_status;?>">
									<option value="">-- <?php _e('No change','woocommerce-dymo');?> --</option>
									<?php
									foreach ($order_status as $key=>$status) {
										if(!isset($object_extras['orderstatus'])) $object_extras['orderstatus']='';
										echo '<option value="'.$key.'" '.selected($key,$object_extras['orderstatus'],false).'>'.$status.'</option>';
									}
									?>
								</select>
								<p class="description"><?php _e( 'Automatically update order status after printing. Please note: this could influence other actions.', 'woocommerce-dymo' ); ?></p>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th colspan="2">
								<input type="submit" name="Submit" class="button-primary" value="<?php echo sprintf(__('Save %s label other settings','woocommerce-dymo'),$args['name']);?>" />
							</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

		<?php
		$step++;
	 ?>
	</form>

	<div class="postbox dymo-testlabel">
		<div class="inside">
			<h3><?php echo sprintf(__('Step %d: Test label','woocommerce-dymo'),$step);?></h3>

			<?php
			if(isset($latest_order) && $latest_order>0 && isset($chosenFile[$label]) && in_array($chosenFile[$label],$files)) { ?>

				<p><em><?php _e('Click the button to print a test label from the most recent WooCommerce order.','woocommerce-dymo');?></em></p>

				<?php

					if($args['type']=='order_item' && $repeat==1) {
						$count=$test_order->get_item_count();
						echo '<p><strong>'.__('Note: this will print labels for each product inside the latest order.','woocommerce-dymo').'<br>'.sprintf(__('Latest order has a total product count of %s, %s labels will be printed for testing.','woocommerce-dymo'),$count,$count).'</strong></p>';
					} elseif($args['type']=='order_item' && $repeat==0) {
						$count=count($test_order->get_items());
						echo '<p><strong>'.__('Note: this will print labels for each order item line inside the latest order.','woocommerce-dymo').'<br>'.sprintf(__('Latest order contains %s item lines, %s labels will be printed for testing.','woocommerce-dymo'),$count,$count).'</strong></p>';
					}
				?>
				<?php printf( '<a href="#" class="button wc-action-button-dymo wc-action-button tips dymo-link wc-action-button-%1$s dymo-button-%1$s-%3$s" data-tip="Print %2$s label" data-printid="%3$s" data-request="%1$s" data-nonce="%4$s"><span class="dymo-spinner"></span>'.__('%5$s Print test %2$s label','woocommerce-dymo').'</a>', $label,$args['name'], $latest_order, $test_nonce,$this->dymo->get_icon($args['color']));?>
				
				<?php } else {
					echo '<p><em>'.__('No test label available.','woocommerce-dymo').'</em></p>';
				}
				?>

				<span class="dymo-animate dymo-progress dymo-progress-<?php echo $latest_order;?>" data-dymo-id="<?php echo $latest_order;?>"><span></span></span>
				<div class="dymo-errors order_notes dymo-hidden"></div>
		</div>
	</div>
</div>
