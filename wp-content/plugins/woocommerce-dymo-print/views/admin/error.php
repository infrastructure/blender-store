<?php
/*
 *	Settings Single label
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<div id="<?php echo $label;?>" class="tab">
	<div class="postbox">
		<div class="inside">
			<h3><?php _e('Unknown label','woocommerce-dymo');?></h3>
			<p><?php _e('Unknown label, please check your label settings.','woocommerce-dymo');?></p>
		</div>
	</div>
</div>