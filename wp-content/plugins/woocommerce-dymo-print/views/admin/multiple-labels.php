<?php
/*
 *	Settings Single label
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<div id="<?php echo $label;?>" class="tab">
	<form name="chooselabel" method="post" enctype="multipart/form-data">
		<input type="hidden" name="dymo_fields_submitted" value="combo">
		<?php wp_nonce_field('dymo_nonce_action','dymo_nonce_field'); ?>

		<?php if(isset($this->product)) { ?>
		<div class="postbox">
			<div class="inside">
				<h3><?php _e('Order or Product label combination?','woocommerce-dymo');?></h3>
				<p><em><?php _e('Do you want to create an order OR a product label combination?', 'woocommerce-dymo');?></em></p>
				<table class="form-table">
					<tr>
						<th valign="top">
							<strong><?php _e('Order or Product labels?','woocommerce-dymo');?></strong>
						</th>
						<td>
							<input type="radio"  <?php checked( $combos[$label]['labeltype'], 'order' ); ?> value="order" required name="wc_dymo_combo[<?php echo $label;?>][labeltype]" id="combo-type-<?php echo $label;?>-order"><label for="combo-type-<?php echo $label;?>-order">Order label combination</label><br>
							<input type="radio"  <?php checked( $combos[$label]['labeltype'], 'product' ); ?> value="product" required name="wc_dymo_combo[<?php echo $label;?>][labeltype]" id="combo-type-<?php echo $label;?>-product"><label for="combo-type-<?php echo $label;?>-product">Product label combination</label>
						</td>
					</tr>
					<tr>
						<th colspan="2">
							<input type="submit" name="Submit" class="button-primary" value="<?php _e( 'Change combination type', 'woocommerce-dymo' ); ?>" />
						</th>
					</tr>
				</table>
			</div>
		</div>
	<?php  } else {
		echo '<input type="hidden" name="wc_dymo_combo['.$label.'][labeltype]" value="order">';
		$combos[$label]['labeltype']='order';
	}
		if($combos[$label]['labeltype']=='order') {
	?>
		<div class="postbox">
			<div class="inside">
				<h3><?php _e('Create order label combination','woocommerce-dymo');?></h3>
				<p><em><?php printf(__('Create a label combination if you want to print multiple labels in one single click or action. For more information about creating label combinations, see %s.', 'woocommerce-dymo'),'<a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/create-label-combinations/" target="_blank">'.__('our documentation','woocommerce-dymo').'</a>');?></em></p>
				<p><em><?php _e('You can create a label combination by defining the order of printing for each label.','woocommerce-dymo');?></em></p>

				<table class="form-table">
					<?php
					$c=0;
					ob_start();
					foreach($labels as $id=>$data) {
						if(in_array($data['type'],array('combo','product'))) {continue;}
						if(isset($chosenFile[$id]) && in_array($chosenFile[$id],$files)) {
							
					?>
					<tr>
						<th>
							<label id="<?php echo $label.'_'.$id;?>"><?php printf(__( '%s label', 'woocommerce-dymo' ), ucwords($data['name']));?></label>
						</th>
						<td>
							<input type="number" value="<?php if(isset($combos[$label][$id])) echo $combos[$label][$id];?>" name="wc_dymo_combo[<?php echo $label;?>][<?php echo $id;?>]" min="0" max="<?php echo count($labels);?>">
							
							<p class="description"><?php _e('Leave empty or set 0 (zero) to not print this label.', 'woocommerce-dymo' );?></p>
						</td>
					</tr>
					<?php 
						$c++;
						}
					}
					?>
					<tr>
						<td colspan="2">
							<p class="description"><?php printf(__('Set order of printing by entering a number (1 to %d), where 1 is printed first.', 'woocommerce-dymo' ),$c);?></p>
						</td>
					</tr>
					<tr>
						<th colspan="2">
							<input type="submit" name="Submit" class="button-primary" value="<?php printf(__( 'Save %s label combination', 'woocommerce-dymo' ),$args['name']); ?>" />
						</th>
					</tr>
					<?php
					$label_rows=ob_get_contents();
					ob_end_clean();
					if($c<2) {
						?>
							<tr>
								<th colspan="2">
								<?php _e('To create a label combination you need to have at least 2 order or order-item labels.','woocommerce-dymo');?>
								</th>
							</tr>
						<?php
					} else {
						echo $label_rows;
					}
					?>
					
				</table>
			</div>
		</div>

  <?php } elseif(isset($this->product) && $combos[$label]['labeltype']=='product') { ?>
		<div class="postbox">
			<div class="inside">
				<h3><?php _e('Create product label combination','woocommerce-dymo');?></h3>
				<p><em><?php printf(__('Create a label combination if you want to print multiple labels in one single click or action. For more information about creating label combinations, see %s.', 'woocommerce-dymo'),'<a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/create-label-combinations/" target="_blank">'.__('our documentation','woocommerce-dymo').'</a>');?></em></p>
				<p><em><?php _e('You can create a label combination by defining the order of printing for each label.','woocommerce-dymo');?></em></p>
				<table class="form-table">
					<?php
					$c=0;
					ob_start();
					foreach($labels as $id=>$data) {
						if(in_array($data['type'],array('combo','order','order_item'))) {continue;}
						if(isset($chosenFile[$id]) && in_array($chosenFile[$id],$files)) {
					?>
					<tr>
						<th>
							<label id="<?php echo $label.'_'.$id;?>"><?php printf(__( '%s label', 'woocommerce-dymo' ), ucwords($data['name']));?></label>
						</th>
						<td>
							<input type="number" value="<?php if(isset($combos[$label][$id])) echo $combos[$label][$id];?>" name="wc_dymo_combo[<?php echo $label;?>][<?php echo $id;?>]" min="0" max="<?php echo count($labels);?>">
							<p class="description"><?php _e('Leave empty or set 0 (zero) to not print this label.', 'woocommerce-dymo' );?></p>
						</td>
					</tr>
					<?php
						$c++;
						}
					}
					?>
					<tr>
						<td colspan="2">
							<p class="description"><?php printf(__('Set order of printing by entering a number (1 to %d), where 1 is printed first.', 'woocommerce-dymo' ),$c);?></p>
						</td>
					</tr>
					<tr>
						<th colspan="2">
							<input type="submit" name="Submit" class="button-primary" value="<?php printf(__( 'Save %s label combination', 'woocommerce-dymo' ),$args['name']); ?>" />
						</th>
					</tr>
					<?php
					$label_rows=ob_get_contents();
					ob_end_clean();
					if($c<2) {
						?>
							<tr>
								<th colspan="2">

								<?php _e('To create a label combination you need to have at least 2 product labels.','woocommerce-dymo');?>
								</th>
							</tr>
						<?php
					} else {
						echo $label_rows;
					}
					?>
				</table>
				
			</div>
		</div>
	<?php } ?>
	</form>
</div>