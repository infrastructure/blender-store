<?php
/**
 * WordPress Settings Page
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<style>
#dymo-debug-log-output {display:block;width:100%;max-width:500px;height:200px;margin:0 0 20px;overflow:auto;border:1px solid #666;padding:10px;}
#dymo-debug-log-output .error {color:red;}
#dymo-debug-log-output .undefined {color:orange;}
#dymo-debug-log-output .bold {font-weight:700;margin-top:20px;}
.dymo-turbo{display:none}
#poststuff h2.nav-tab-wrapper{padding-bottom:0px;}
.tab{display:none !important;}
.tab.active{display:block !important;}table td p {padding:0px !important;}
table.dymocheck{width:100%;border:1px solid #ccc !important;text-align:left;margin:0 0 20px 0}
.dymocheck tr th{border-bottom:1px solid #ccc !important;background:#ccc;}
table th {text-align:left !important;}
.postbox ul li label {vertical-align:top}
.dymo-error {color:red;border:1px solid #eee;border-left:4px solid red;background:#f8f8f8;display:block;padding:5px 10px}
.printerField {background-position:98% 50%;}
.labelxml_descr {font-size:75% !important;float:right;color:#666;text-decoration:underline}
.dymo-preview {background:url(<?php echo plugins_url( '/assets/img/icon-load.gif',$this->plugin_file);?>) no-repeat center center #eee;padding:30px;border:1px solid #ccc;width:auto;display:inline-block}
.dymo-object-row td{vertical-align:top}
.dymo-object-row strong{display:block}
.dymo-object-type {display:inline-block;background:#333;color:#fff;text-transform:uppercase;font-size:10px;padding:2px 5px;margin-right:4px}
.dymo-hide {display:none}
.dymo-excerpt,.dymo-product-descr,.dymo-customer-note,.dymo-serialized,.dymo-product-list,.dymo-custom-address {border:1px solid #999;padding:10px;margin:10px 0}
.dymo-printerSelect-error {color:red;display:none}
.nav-tab svg,.button svg {width:20px;height:20px;margin:3px 4px 0 -3px;display:inline-block;float:left}
body .dymo-framework-error-notice {display:none}
body.dymo-framework-error .dymo-framework-error-notice,
body.dymo-framework-error .dymo-printerSelect-error {display:block}
body.dymo-framework-error .dymo-printerSelect-description {display:none}
.dymo-icon-found {background:url(<?php echo plugins_url( '/assets/img/icon-accept.png',$this->plugin_file);?>) no-repeat 5px 50%;padding-left:2em !important;}
.dymo-icon-notfound{background:url(<?php echo plugins_url( '/assets/img/icon-cross.png',$this->plugin_file);?>) no-repeat 5px 50%;padding-left:2em !important;border-color:red !important}
.dymo-serialized table tr th,.dymo-serialized table tr td {padding-bottom:0px}
.dymo-serialized-build {text-decoration:underline;display:block;margin-bottom:10px}
.dymo-serialized-query {color:#999}
.dymo-serialized-element {color:#000;text-decoration:underline;cursor:pointer}
.addPrinter {cursor:pointer;}
.addPrinter:hover,.addPrinter:focus {text-decoration:underline;}
.addPrinter.choose {font-weight:700;}

#wc_dymo_labeltypes fieldset {margin:20px 0;}
#wc_dymo_labeltypes fieldset p {display:inline-block;float:left;max-width:25%;margin-right:5px;}
#wc_dymo_labeltypes fieldset p.dymo-color input {width:100px;}
#wc_dymo_labeltypes fieldset svg {width:20px;height:20px;display:inline-block;float:left;line-height:1;}
#wc_dymo_labeltypes fieldset legend {font-weight:700;}
#wc_dymo_labeltypes fieldset label {font-weight:400;display:block;clear:both;}
#wc_dymo_labeltypes fieldset .color-picker {clear:both;max-width:230px;}
#wc_dymo_labeltypes fieldset p.dymo-color span {display:block;clear:both;color:#cc0000;font-size:0.8em;}

</style>

<div class="wrap">

  <div id="icon-options-general" class="icon32"></div>

	<h2><?php _e( 'WooCommerce - Print DYMO labels', 'woocommerce-dymo' ); ?></h2>

	<?php if ( isset( $_POST['dymo_fields_submitted'] ) && $_POST['dymo_fields_submitted'] == 'submitted' ) { ?><div id="message" class="updated fade"><p><strong><?php _e( 'Your settings have been saved.', 'woocommerce-dymo' ); ?></strong></p></div><?php } ?>
	<?php if ( isset( $_POST['dymo_fields_submitted'] ) && $_POST['dymo_fields_submitted'] == 'delete' && !empty($_POST['wc_dymo_delete_label'])) { ?><div id="message" class="updated fade"><p><strong><?php _e( 'Your labels are succesfully deleted.', 'woocommerce-dymo' ); ?></strong></p></div><?php } ?>
	
	<?php if(isset($uploadError)) { echo '<div id="message" class="error fade"><p>'.$uploadError.'</p></div>'; }?>

	<div id="message" class="error fade dymo-framework-error-notice">
		<p>
			<strong><?php echo sprintf(__( 'DYMO Label Framework is not installed or DYMO WebService is not running on your system. Please see %s for more information about this problem.', 'woocommerce-dymo' ),'<a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/debug-dymo-framework/" target="_blank">'.__('our documentation','woocommerce-dymo').'</a>'); ?></strong>
		</p>
	</div>

	<div id="content" class="dymo-settings-content" >
		<div id="poststuff">
			<div style="float:left; width:72%; padding-right:3%;">
			  <div id="tabs">
				<h2 class="nav-tab-wrapper">
					<?php
					foreach ($labels as $label=>$args) {
						if(isset($chosenFile[$label])) { $clickClass='dymo-label-tab'; } else {$clickClass='';}
						echo '<a id="'.$label.'-tab" class="nav-tab '.$clickClass.' hideError" href="#top#'.$label.'">'.$this->dymo->get_icon($args['color']).' '.sprintf(__( '%s label', 'woocommerce-dymo' ), ucwords($args['name'])).'</a>';
					}
					?>
					<a id="settings-tab" class="nav-tab" href="#top#settings"><?php _e( 'General settings', 'woocommerce-dymo' ); ?></a>
					<a id="check-tab" class="nav-tab" href="#top#check"><?php _e( 'Debug & Help', 'woocommerce-dymo' ); ?></a>
				</h2>
