<?php
/*
 *	Settings General settings tab
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<div id="settings" class="tab">
	<div class="postbox hideError">
		<div class="inside">
			<h3><?php _e( 'Label management', 'woocommerce-dymo' ); ?></h3>
			<p><?php _e('Below you can modify, add or delete labels.','woocommerce-dymo');?></p>
			<form method="post" action="" id="wc_dymo_labeltypes">
				<input type="hidden" name="dymo_types" value="true">
				<?php echo wp_nonce_field('dymo_nonce_action','dymo_nonce_field');?>

			<?php
				foreach($labels as $type=>$label) {
					?>
					<fieldset>
						<legend><?php echo $this->dymo->get_icon($label['color']).' '.sprintf(__( '%s label', 'woocommerce-dymo' ), ucwords($label['name']));?></legend>
						<input id="type_<?php echo $type;?>_default" name="wc_type[<?php echo $type;?>][default]" type="hidden" value="<?php if(isset($label['default'])) echo $label['default'];?>">
						<input id="type_<?php echo $type;?>_id" name="wc_type[<?php echo $type;?>][id]" type="hidden" value="<?php echo $type;?>" disabled>
						<p>
							<label for="type_<?php echo $type;?>_name"><?php _e('Name','woocommerce-dymo');?><span class="screen-reader-text"><?php _e('Required','woocommerce');?></span></label>
							<input id="type_<?php echo $type;?>_name" name="wc_type[<?php echo $type;?>][name]" type="text" value="<?php echo $label['name'];?>" required>
						</p>
						<p class="dymo-color">
							<label for="type_<?php echo $type;?>_color"><?php _e('Color','woocommerce');?><span class="screen-reader-text"><?php _e('Required','woocommerce');?></span></label>
							<input id="type_<?php echo $type;?>_color" class="popup-colorpicker" name="wc_type[<?php echo $type;?>][color]" type="text" value="<?php echo $label['color'];?>" required>
						</p>
						<p>
							<label for="type_<?php echo $type;?>_type"><?php _e('Label type','woocommerce');?><span class="screen-reader-text"><?php _e('Required','woocommerce');?></span></label>
							<select id="type_<?php echo $type;?>_type" name="wc_type[<?php echo $type;?>][type]" required>
								<?php
								foreach($labelTypes as $typeID=>$typeName) {
									echo '<option value="'.$typeID.'" '.selected($label['type'],$typeID,false).'>'.$typeName.'</option>';
								}
								?>
							</select>
						</p>
						<?php
						if($type!='shipping' && (!isset($label['default']) || $label['default']==false)) {
							?>
							<p>
								<label for="type_<?php echo $type;?>_delete"><?php _e('Delete','woocommerce');?></label>
								<input id="type_<?php echo $type;?>_delete" name="wc_type[<?php echo $type;?>][delete]" type="checkbox" value="1">
							</p>
							<?php
						}
						?>
						<div id="type_<?php echo sanitize_title($type);?>_colorpicker" class="color-picker"></div>
					</fieldset>
					<?php
				}
			?>
				<fieldset>
				<legend><?php _e('Add new label');?></legend>
				<p>
					<label for="type_new_name"><?php _e('New label','woocommerce-dymo');?> <?php _e('Name','woocommerce-dymo');?></label>
					<input id="type_new_name" name="wc_type[new][name]" type="text" value="" >
				</p>
				<p class="dymo-color">
					<label for="type_new_color"><?php _e('New label','woocommerce-dymo');?> <?php _e('Color','woocommerce');?></label>
					<input id="type_new_color" class="popup-colorpicker" name="wc_type[new][color]" type="text" value="#000000">
				</p>
				<p>
					<label for="type_new_type"><?php _e('New label','woocommerce-dymo');?> <?php _e('Type','woocommerce');?></label>
					<select id="type_new_type" name="wc_type[new][type]">
						<?php
						foreach($labelTypes as $typeID=>$typeName) {
							echo '<option value="'.$typeID.'">'.$typeName.'</option>';
						}
						?>
					</select>
				</p>
				<div id="type_new_colorpicker" class="color-picker"></div>
				</fieldset>

				<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e( 'Save labels', 'woocommerce-dymo' ); ?>" />
			</form>
		</div>
	</div>
	<div class="postbox hideError">
		<div class="inside">
			<h3><?php _e( 'Label file management', 'woocommerce-dymo' ); ?></h3>
			<p><?php _e('Here you see all available DYMO .labels. Select the file to remove the label. ','woocommerce-dymo');?></p>

			<?php
				if(isset($files) && count($files)>0) {
					
					echo '<form method="post" action="" id="wc_dymo_files_'.rand(0,999).'">';
					echo' <input type="hidden" name="dymo_fields_submitted" value="delete">';

					wp_nonce_field('dymo_nonce_action','dymo_nonce_field');
					$used=array();

					if(is_array($chosenFile)) {
						$used=array_intersect_key($chosenFile,$labels);

						$labelsUsed=array();
						foreach($used as $key=>$value) {
							$labelsUsed[$value][]=$key;
						}
					}

					echo '<ul>';

					foreach ($files as $file) {

						$labelId='wc_dymo_delete_'.strtolower(str_replace('.','',$file));
						echo '<li>';

						if(in_array($file,$used)) {
							$labelNames='';
							foreach($labelsUsed[$file] as $label) {
								$labelNames.='"'.$labels[$label]['name'].'", ';
							}

							echo '<input type="checkbox" name="wc_dymo_delete_label[]" disabled value="'.$file.'" id="'.$labelId.'">';

							$labelUsed='| <strong>'.sprintf(__('Label used for %s labels','woocommerce-dymo'),substr($labelNames,0,-2)).'</strong>';
						} else {
							echo '<input type="checkbox" name="wc_dymo_delete_label[]" value="'.$file.'" id="'.$labelId.'">';
								$labelUsed='';
						}

						echo '<label for="'.$labelId.'">'.$file.'</label> (<a href="'.$dir.$file.'" target="_blank" rel="nofollow">'.__('Download').'<span class="screen-reader-text"> '.$file.'</span></a>) '.$labelUsed.'</li>';
					}

					echo '</ul>';

					echo '<input type="submit" name="Submit" class="button-primary" value="'.__('Delete selected labels','woocommerce-dymo').'" >';
					echo '</form>';

				} else {

					echo '<p>'.__('No DYMO label uploaded. Please upload a DYMO Label first.','woocommerce-dymo').'</p>';

				}
				?>

				<p>&nbsp;</p>

				<h3><?php _e('Sample labels','woocommerce-dymo');?></h3>
				<p><?php _e('We created 4 sample labels which you can use. Click the button to copy them to the label directory.','woocommerce-dymo');?> <a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/sample-labels/" target="_blank"><?php _e('Or visit our website to download more sample labels.','woocommerce-dymo');?></a></p>
				<form method="post" action="" id="wc_dymo_files">
					<input type="hidden" name="dymo_fields_submitted" value="copy">
					<?php echo wp_nonce_field('dymo_nonce_action','dymo_nonce_field'); ?>
					<input type="submit" name="submit" class="button-secondary" value="<?php _e('Copy sample labels','woocommerce-dymo');?>" >
				</form>

		</div>
	</div>
	<div class="postbox hideError">
		<div class="inside">
			<h3><?php _e( 'Other settings', 'woocommerce-dymo' ); ?></h3>
			<form method="post" action="" id="dymo_settings_1">
				<input type="hidden" name="dymo_fields_submitted" value="submitted">
				<?php wp_nonce_field('dymo_nonce_action','dymo_nonce_field'); ?>

				<table class="form-table">
					<tr>
						<th>
							<label for="wc_dymo_debug"><b><?php _e( 'Time (ms) between labels', 'woocommerce-dymo' ); ?>:</b></label>
						</th>
						<td>
							<input type="text" class="small-input" name="wc_dymo_labeltime" value="<?php echo $labeltime;?>" >
							<p class="description"><?php _e('DYMO needs some time between printing labels. If you experience problems during printing define more seconds between every print.','woocommerce-dymo');?></p>
							<p class="description"><?php _e('Define time in milli seconds (1000ms = 1s). Minimum time: 1000ms','woocommerce-dymo');?></p>
						</td>
					</tr>
					<tr>
						<th colspan="2">
							<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e( 'Save other settings', 'woocommerce-dymo' ); ?>" />
						</th>
					</tr>
				</table>
			</form>

		</div>
	</div>
</div>