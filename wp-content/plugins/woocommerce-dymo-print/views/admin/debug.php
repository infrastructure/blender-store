<?php
/* 
 *	Settings Debug tab
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<div id="check" class="tab">
	<div class="postbox hideError">
		<div class="inside">
			<h3><?php _e( 'DYMO LabelWriter properties', 'woocommerce-dymo' ); ?></h3>
			<div class="dymo-check">
				<p class="dymo-printerSelect-description"><?php _e( 'If you have any problems, please check below data.', 'woocommerce-dymo' );?></p>
				<p class="dymo-printerSelect-error"><?php echo sprintf(__( 'DYMO Label Framework is not installed or DYMO WebService is not running on your system. Please see %s for more information about this problem.', 'woocommerce-dymo' ),'<a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/debug-dymo-framework/" target="_blank">'.__('our documentation','woocommerce-dymo').'</a>'); ?></p>
				
				<div id="printersInfoContainer"></div>
				
				<div class="printControls dymo-printerSelect-description">
					<button class="button" style="float:right;" id="updateTableButton"><?php _e( 'Refresh', 'woocommerce-dymo' ); ?></button>
					<button class="button-primary" id="printButton"><?php _e( 'Print printers information on', 'woocommerce-dymo' ); ?></button>
					<select id="dymo-printersSelect"></select>
				</div>
				
			</div>
		</div>
	</div>
					  				  
	<div class="postbox">
		<div class="inside">
			<h3><?php _e( 'DYMO Javascript Framework debug log', 'woocommerce-dymo' ); ?></h3>
			
			<div class="dymo-debug-log">
				<div class="dymo-debug-log-container">
					<ul id="dymo-debug-log-output"></ul>
					<button id="debugClear" class="button"><?php _e('Clear debug log','woocommerce-dymo');?></button>
				</div>
			</div>
		</div>
	</div>
					  
	<div class="postbox">
		<div class="inside">
			<form method="post" action="" id="dymo_settings">
				<input type="hidden" name="dymo_fields_submitted" value="submitted">
				<?php wp_nonce_field('dymo_nonce_action','dymo_nonce_field'); ?>
				
				<h3><?php _e( 'Other advanced settings', 'woocommerce-dymo' ); ?></h3>
				<?php
					if (in_array('better-wp-security/better-wp-security.php',get_option('active_plugins')) || is_plugin_active_for_network( 'better-wp-security/better-wp-security.php' )) {
						echo '<div class="dymo-error dymo-ithemes-error"><p>'.__( 'iThemes Security plugin is installed and may prevent any connections to your DYMO LabelWriter.', 'woocommerce-dymo' ).' <a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print-3/debug-dymo-framework/#ithemessecurityplugin" target="_blank">'.__('See our documentation for a solution.','woocommerce-dymo').'</a></p></div>';
					}
				?>

				<table class="form-table">
					<tr>
						<th>
							<label for="wc_dymo_debug"><b><?php _e( 'Debug mode', 'woocommerce-dymo' ); ?>:</b></label>
						</th>
						<td>
							<input type="radio" name="wc_dymo_debug" id="wc_dymo_debug_no" value="0" <?php checked(get_option( 'wc_dymo_debug' )==0 || get_option( 'wc_dymo_debug' )=="");?>> <label for="wc_dymo_debug_no"><?php _e('Disabled','woocommerce');?></label><br>
							<input type="radio" name="wc_dymo_debug" id="wc_dymo_debug_yes" value="1" <?php checked(get_option( 'wc_dymo_debug' )==1);?>> <label for="wc_dymo_debug_yes"><?php _e('Enabled','woocommerce');?></label><br>
							<p class="description"><?php _e('When set to true, it will put tracing messages in the browser developer console.' , 'woocommerce-dymo');?></p>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wc_dymo_uninstall"><b><?php _e( 'Delete settings at uninstall', 'woocommerce-dymo' ); ?>:</b></label>
						</th>
						<td>
							<input type="radio" name="wc_dymo_uninstall" id="wc_dymo_uninstall_no" value="0" <?php checked(get_option( 'wc_dymo_uninstall' )==0 );?>> <label for="wc_dymo_uninstall_no"><?php _e('Keep plugin settings','woocommerce');?></label><br>
							<input type="radio" name="wc_dymo_uninstall" id="wc_dymo_uninstall_yes" value="1" <?php checked(get_option( 'wc_dymo_uninstall' )==1 || get_option( 'wc_dymo_uninstall' )=="");?>> <label for="wc_dymo_uninstall_yes"><?php _e('Delete all settings','woocommerce');?></label><br>
							<p class="description"><?php _e('Do you want to delete all plugin settings at plugin uninstall?' , 'woocommerce-dymo');?></p>
						</td>
					</tr>
					<tr>
						<th>
							<label for="wc_dymo_uninstall_files"><b><?php _e( 'Delete label files at uninstall', 'woocommerce-dymo' ); ?>:</b></label>
						</th>
						<td>
							<input type="radio" name="wc_dymo_uninstall_files" id="wc_dymo_uninstall_files_no" value="0" <?php checked(get_option( 'wc_dymo_uninstall_files' )==0 || get_option( 'wc_dymo_uninstall_files' )=="");?>> <label for="wc_dymo_uninstall_files_no"><?php _e('Keep label files','woocommerce');?></label><br>
							<input type="radio" name="wc_dymo_uninstall_files" id="wc_dymo_uninstall_files_yes" value="1" <?php checked(get_option( 'wc_dymo_uninstall_files' )==1);?>> <label for="wc_dymo_uninstall_files_yes"><?php _e('Delete all label files','woocommerce');?></label><br>
							<p class="description"><?php _e('This will delete all uploaded .label files at plugin uninstall.' , 'woocommerce-dymo');?></p>
							<p class="description"><?php printf(__('It will remove directory and all files at path: %s','woocommerce-dymo'),'<code>'.$path).'</code>';?>
						</td>
					</tr>
					<tr>
						<th colspan="2">
							<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e( 'Save advanced settings', 'woocommerce-dymo' ); ?>" />
						</th>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>