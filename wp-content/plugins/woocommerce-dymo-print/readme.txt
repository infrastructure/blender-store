﻿=== WooCommerce DYMO Print ===
Contributors: WP Fortune
Tags: woocommerce
Requires at least: 5.5
Tested up to: 5.5.3
Stable tag: 6.0.0
WC requires at least: 4.5.0
WC tested up to: 4.6.1
License: GPLv2

This plugin adds the possibility to print WooCommerce order labels on your DYMO label printer.

== Description ==
***Quickly print your DYMO labels from within your WooCommerce order overview.***

The _WooCommerce Dymo Print plugin_ adds a quick connection with your DYMO labelwriter within your WooCommerce shop.
Now you are able to print shipping address labels in a second!

This plugin is compatible with WordPress 5.5.x and WooCommerce 4.6.x

== Installation ==
1. Install WooCommerce DYMO Print PRO either via the WordPress.org plugin directory or by uploading the files to the '/wp-content/plugins/' directory.
2. Activate the plugin  through the 'Plugins' menu in WordPress.

== Upgrade Notice ==
Please backup first.

== Usage ==
Go to WooCommerce > DYMO print, configure and start printing. Simple!

== Changelog ==

***WooCommerce DYMO Print***
= 2020.11.05 - version 6.0.0 =
* Changed: DYMO Framework to CONNECT Software
* Fixed: Bulk printing for orders moved to admin_footer hook
* Fixed: Several notices on order data direct lookup
* Fixed: Scripts not loading on Hebrew language

= 2019.10.07 - version 5.0.9 =
* Fixed: Issue with printing product list variations when variation data is custom attribute

= 2019.07.02 - version 5.0.8 =
* Tweak: Keep sorting during bulk print
* Fixed: solved notice on missing order_meta data in function get_order_meta in class-wc-dymo.php
* Fixed: solved notice on missing print_type in function get_label_data in class-wc-dymo.php
* Fixed: Order item short description not working
* Fixed: Order item attributes not working
* Fixed: Product list: [variation] not showing variation data
* Tweak: Product list: show item name instead of product name to reflect variation data
* Tweak: Product list: use parent SKU if no VARIATION SKU is found

= 2019.06.06 - version 5.0.7 =
* Fixed: issue with & (ampersand) in label texts

= 2019.01.18 - version 5.0.6 =
* Fixed: Error during printing in some cases
* Tweak: Progressbar script changed for product printing

= 2019.01.14 - version 5.0.5 =
* Fixed: entity decode of special characters inside get_product_attr function
* Tweak: The way how product stock printing is handled

= 2019.01.12 - version 5.0.4 =
* Fixed: warning with undefined variable on class-wc-dymo.php line 644

= 2018.12.12 - version 5.0.3 =
* Fixed: Label combinations for products not working
* Fixed: Order item price output not working for order item labels
* Added: Option to delete settings at uninstall
* Added: Option to delete .label files at uninstall
* Tweak: Better visual for bulk printing

= 2018.12.05 - version 5.0.2 =
* Fixed: several small bugs related to Product add-on

= 2018.12.04 - version 5.0.1 =
* Fixed: bug with incorrect loaded filter

= 2018.12.01 - version 5.0.0 =
* Complete rewrite of the plugin
* Added: Better error notifications
* Added: Inline printing with ajax (no pop-ups anymore!)
* Added: Option to print multiple labels in one click (label combinations)
* Added: Option to overwrite label output with add_filter('wc_dymo_order',....
* Fix: compatibility with Jetpack spellcheck
* Improvement: Better debug logs

= 2108.06.01 - version 4.0.0 =
* Fixed: "Synchronous XMLHttpRequest" deprecated error on several requests by using Async requests
* Updated: DYMO Javascript Framework 3.0
* Updated: Rewritten complete debug section on plugin settings page for better debug-messages and links to documentation
* Updated: Rewritten label preview generation on plugin settings page
* Removed: Several old code which was no longer in use
* Removed: Some small documentation section on settings page

= 2108.05.17 - version 3.2.1 =
* Fixed: Fallback labels were not created correctly

= 2108.04.30 - version 3.2.0 =
* Tweak: Better error handling
* Fixed: Issue with new lines on static objects

= 2108.04.23 - version 3.1.9 =
* Tweak: Better support for Windows 10 and chrome 67
* Fixed: Issue with WooCommerce PIP Invoices 

= 2018.02.01 - version 3.1.8 =
* Added: Fallback label XML code is now saved to plugin settings. Fallback label XML code is used when label file can not opened directly.
* Tweak: Created a new javascript function for printing so we can better handle error requests
* Added: Use a filter to output all order data from a custom PHP function, see https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/filters/#codeoutputallorderdataandorderitemdatawithacustomphpfunction

= 2018.01.09 - version 3.1.7 =
* Fixed: Keep query args on reloading page after bulk print
* Added: Select other printer directly from print dialog when selected printer is not available.

= 2017.12.20 - version 3.1.6 =
* Added: Support for multi currency on order total price and order item price (Aelia currency switcher)
* Added: Support for WooCommerce 3.3 new order view page

= 2017.12.11 - version 3.1.5 =
* Fixed: Removed "MediaType" from labelfile which is added in some versions of DYMO Label Software and is not recognized by DYMO Framework
* Fixed: Printer loading on settings page

= 2017.12.07 - version 3.1.4 =
* Added: Order ID
* Added: Filter to adjust Order ID, see https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/filters/

= 2017.12.01 - version 3.1.3 =
* Added: WP Fortune license notification

= 2017.06.11 - version 3.1.2 =
* Fix: order status was not automatically changed in some situations

= 2017.08.10 - version 3.1.1 =
* Added: Option to override order item attribute output with filter, see https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/filters/

= 2017.07.25 - version 3.1.0 =
* Added: Option to automatically change order status after printing
* Fixed: Make order action overview buttons readible for screenreaders (.screen-reader-text)

= 2017.05.16 - version 3.0.6 =
* Fix: Problem with order items not showing all attributes in WC 3.0.

= 2017.04.12 - version 3.0.5 =
* Fix: replaced new WC_Product with wc_get_product

= 2017.04.03 - version 3.0.4 =
* Fix: order->id for WooCommerce 3.0
* Fix: replaced new WC_Order with wc_get_order

= 2017.03.08 - version 3.0.3 =
* Fix: issue with order item labels not printing based on order quantity
* Fix: remove html (strip tags) from order item short description

= 2017.02.27 - version 3.0.2 =
* Fix: Custom shipping address layoutbox was not loaded since version 3.0.1.
* Added: Order item (product) short description
* Added: Wordwrapping for order item (product) short description
* Tweak: HTML entities decode for order item product name and attributes

= 2017.02.01 - version 3.0.1 =
* Fix: better check if data is already stored or not to prevent PHP error notices
* Added: print customer note

= 2017.01.16 - version 3.0.0 =
* Complete rewritten plugin - limited backwards compatibility

= 2016.12.11 - version 2.5.6 =
* Fixed: Corrected loading of printerlist in some browsers
* Added: DYMO Framework check on debug tab.
* Removed: DYMO Software notice

= 2016.09.28 - version 2.5.5 =
* Added: Filter to use raw meta data (useful for ACF plugin)

= 2016.04.12 - version 2.5.4 =
* Added: Better support for multisite environments

= 2016.03.07 - version 2.5.3 =
* Added: Partial support for plugin: "WooCommerce Extra Checkout Fields for Brazil"
* Added: NUMBER tag for custom address layout for Brazil
* Added: NEIGHBORHOOD tag for custom address layout for Brazil
* Fixed: Order meta data shipping

= 2016.02.03 - version 2.5.2 =
* Fix: Special characters in latest version of DYMO Label Framework
* Added: Debug console log on debug tab
* Added: Print state abbreviation in custom address format
* Tweak: Several improvements to language strings.

= 2016.01.11 - version 2.5.1 =
* Updated: DYMO Javascript Framework to version 2.0.2. (Please also update DYMO Label Software to latest version)
* Tweak: Better error handling during printing if printer is not connected.

= 2015.12.17 - version 2.5.0 =
* Add: Check if printer exists on printing and at the settings panel
* Fixed: Removed NPAPI support and added new driver connections with DLS 8.5.3.
* Tweak: New and improved DYMO Javascript Framework to connect with printer drivers.
* Tweak: Only show Twin Turbo roll settings when Dymo Label Writer Twin Turbo is found on system.
* Tweak: Only show printers connected to current computer.
* Tweak: If data-object not exists on label, skip data.
* Tweak: Several improvements to the settings panel and language strings.

= 2015.12.01 - version 2.4.0-beta-4 =
* Added: Support for order item labels, documentation will be added in stable release
* Tweak: new svg icons for print buttons
* Tweak: better error handling when DYMO Labelwriter is not found
* Fix: bulk printing with new multiple label print filter

= 2015.08.25 - version 2.4.0-beta-3 =
* Added: Filter to print multiple labels based on order

= 2015.08.25 - version 2.4.0-beta-2 =
* Fixed: Support for Network printers
* Added: Support for HTTPS
* Added: Support for Variation SKU on product lists layout
* Tweak: Better string new line escaping for faulty XML code

= 2015.08.25 - version 2.4.0-beta =
* Tweak: Renewed DYMO Framework to version 2.0 BETA

= 2015.04.29 - version 2.3.9.3 =
* Tweak: Better support for label import with images
* Added: Support Product Label add-on
* Added: Support for add-ons in WPFortune updater

= 2015.03.07 - version 2.3.9.2 =
* Fixed: Support for > WC 2.3 for states in custom address layout
* Fixed: Support for > WC 2.3 for printing billing phone number on labels with CUSTEL label object

= 2015.03.02 - version 2.3.9.1 =
* Fixed: Support for special characters in states

= 2015.01.12 - version 2.3.9 =
* Added: Support for WPFortune

= 2014.08.02 - version 2.3.8.3 =
* Added: Support for product labels (future release improvement)

= 2014.07.14 - version 2.3.8.2 =
* Updated: DYMO Javascript framework
* Fixed: empty line {COMPANY} check
* Fixed: close popup after print

= 2014.06.17 - version 2.3.8.1 =
* Fixed: Small bugfix when updating

= 2014.05.15 - version 2.3.8 =
* Fixed: Wrong address print after updating to WooCommerce 2.1.19

= 2014.01.24 - version 2.3.7 =
* Fixed: Ajax calls on ssl enabled servers
* Fixed: check if sample labelfile exists

= 2014.10.12 - version 2.3.6 =
* Several small fixes and changes
* Ready for WooCommerce 2.1 (tested on WooCommerce 2.1 Beta 3)

= 2013.12.12 - version 2.3.5.1 =
* Fixed: Strip slash before single quotes
* Checked and small layout modifications for WordPress 3.8

= 2013.12.06 - version 2.3.4 =
* Fixed: Bug with updater

= 2013.11.07 - version 2.3.3 =
* Fixed: couple of bugs which occurs when there was no shipping address available. When no shipping address is available, billing address will be used for shipping label.

= 2013.10.30 - version 2.3.2 =
* Fixed: a small bug with a double declared function on first time activation (thanks to Richard Shaylor for reporting this).

= 2013.10.24 - version 2.3.1 =
* Fixed: a small bug when order data is empty.

= 2013.10.24 - version 2.3 =
* Added: Support for printing ALL order data en order meta data on your labels
* Added: Price variable on product lists
* Tweak: Print full country name instead of country code on custom address
* Tweak: Better (bulk) printing support
* Removed: Option Company name, Company Extra and Picture from settings, just import them on your label file as these are static info.
* Removed: Class-check-woocommerce.php

= 2013.07.29 - version 2.2.2 =
* Added: Support for WooCommerce PDF Invoice - http://www.woothemes.com/products/pdf-invoices/

= 2013.07.27 - version 2.2.1 =
* Added: Option to print product list on one line
* Fixed: Several small bugfixes

= 2013.07.11 - version 2.2.0 =
* Tweak: Hidden XML textarea for better security
* Added: Address options: manually create your address on a label
* Added: Support for Geev Updater plugin
* Removed: License Key tab
* Fixed: Bug: now the option "Use shipping label elements" actually works ;-)

= 2013.07.09 - version 2.1.6 =
* Fixed: error on printing while no printer selected.

= 2013.07.03 - version 2.1.5 =
* Fixed: bug on "EXTRA" field which causes errors when you use multiple lines. Thanks to Barry Passchier!

= 2013.06.19 - version 2.1.4 =
* Fixed: small bug which causes errors while printing on some installations

= 2013.06.15 - version 2.1.3 =
* Added: basic check if label contains required object fields
* Added: support for quotes in addresses (thanks to Evert-Jan for reporting this)
* Added: better error handling when label file contains wrong objects.
* Minor bugfixes

= 2013.06.05 - version 2.1.2 =
* Added: Quantity to product lists
* Added: Support for WooCommerce Sequential Order Numbers plugin

= 2013.05.29 - version 2.1.1 =
* Fix: Wrong description beneath products and variations: [VARIATIONS] --> [VARIATION]

= 2013.05.21 - version 2.1.0 =
* Added: Layout settings for products & skus
* Fix: Bulk printing products and skus
* Removed: SKU field

= 2013.05.20 - version 2.0.1 =
* Fix: License key error (not saved)

= 2013.05.16 - version 2.0 =
* New: Use a different label for shipping and billing labels
* New: Choose which DYMO printer you want to use for your labels
* Tweak: Changed layout for different shipping and billing label settings
* Fix: Corrected support for DYMO Labelwriter Twin Turbo

= 2013.05.07 - version 1.6 =
* New: Bulk printing

= 2013.05.03 - version 1.5.1 =
* Tweak: Check for active shipping

= 2013.03.26 - version 1.5 =
* Added tabbed layout
* Added DYMO Javascript Framework check
* Added DYMO label import
* Added customer telephone number on labels
* Added customer e-mail address on labels
* Fix update-check

= 2013.03.19 - version 1.1.5 =
* Minor bug-fixes.

= 2013.01.01 - version 1.1 =
* Minor bug-fixes
* Removed template directory (not necessary to customize output)

= 2012.12.19 - version 1.0 =
* Stable release

= 2012.12.19 - version 0.5 =
* Minor bug-fixes

= 2012.12.18 - version 0.1 =
* First release



== Frequently Asked Questions ==
= Where can I find more information about this plugin? =
You can find more information on [our website](https://wpfortune.com/shop/plugins/woocommerce-dymo-print/).

= What is the difference between the Free and Pro versions of this plugin? =
You may use the free version as it is. When you buy WooCommerce DYMO Print PRO you get a lot more options: print billing & shipping labels, bulk printing, customize your own labels, choose your label size, print your company logo on your labels, use a DYMO Labelwriter 450 Twin Turbo.
For a full list of features, please check out [our website](https://wpfortune.com/shop/plugins/woocommerce-dymo-print/).

= Why is there a PRO version? =
We want to give everyone the opportunity to use and try our plugins, but if you want to get more options and access to our support section you can buy our PRO version. WooCommerce DYMO Print Pro costs only **€ 25,00**.