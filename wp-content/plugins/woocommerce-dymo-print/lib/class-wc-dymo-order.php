<?php
if (! defined('ABSPATH')) {
    exit;
}

class WPF_DYMO_Order
{
	/*
	 * Define default label types
	 */
	public function get_labels() {
		$labels=array(
			'shipping'=>array(
				'name'=>'Shipping',
				'color'=>'#8f4399',
				'type'=>'order',
			),
			'billing'=>array(
				'name'=>'Billing',
				'color'=>'#2286C7',
				'type'=>'order',
			),
		);

		$activate_order_item = apply_filters('wc_dymo_order_items', 0);

		if($activate_order_item==true) {
			$labels['order-item']=array(
				'name'=>'Order item',
				'color'=>'#FF7F55',
				'type'=>'order_item',
			);
		}
		$labels = apply_filters( 'wpf_woocommerce_dymo_labels', $labels);
		
		if(is_array($labels) && !empty($labels)) {
			$this->fix_array_keys($labels);
			array_change_key_case($labels,CASE_LOWER);
		} else {
			$labels=array();
		}
		
		return $labels;
	}

	/*
	 * Make sure the apply_filter label Types does not contain wrong array keys
	 */
	public function fix_array_keys(&$arr)
	{
		$arr = array_combine(
			array_map(
				function ($str) {
					return str_replace(" ", "_", strtolower($str));
				},
				array_keys($arr)
			),
			array_values($arr)
		);

		foreach ($arr as $key => $val) {
			if (is_array($val)) {
				$this->fix_array_keys($arr[$key]);
			}
		}
	}

	/*
	 * Get all labels
	 */
	function get_label_files(){
		$uploadDir=wp_upload_dir();
		$path= $uploadDir['basedir'].'/wc-dymo-labels/';
		$files=array();

		if (wp_mkdir_p($path)) {

			$dh  = opendir($path);

			while (false !== ($filename = readdir($dh))) {

				$ext = substr($filename, strrpos($filename, '.') + 1);
				if(in_array($ext, array("label","dymo")))
					$files[] = $filename;
			}

			$files=array_diff($files, array('..', '.'));
			sort($files);
			return $files;
		}
		return false;
	}

  /*
	 * Output print icon
	 */
	public function get_icon($color="#8F4399") {
		$rand=str_replace('#','',$color).'_'.rand(0,99999);
		return '<svg viewBox="0 0 20 14" style="enable-background:new 0 0 20 14;" xml:space="preserve" x="0px" y="0px"><g>
			<path class="icon_'.$rand.'" d="M17.6,7.6v3.8c0,0.1,0,0.1-0.1,0.2c-0.1,0.1-0.1,0.1-0.2,0.1h-2v1.5c0,0.2-0.1,0.5-0.3,0.6c-0.2,0.2-0.4,0.3-0.6,0.3H5.6
				c-0.2,0-0.4-0.1-0.6-0.3c-0.2-0.2-0.3-0.4-0.3-0.6v-1.5h-2c-0.1,0-0.1,0-0.2-0.1c-0.1-0.1-0.1-0.1-0.1-0.2V7.6
				c0-0.5,0.2-0.9,0.5-1.2C3.3,6,3.7,5.8,4.2,5.8h0.6v-5c0-0.2,0.1-0.4,0.3-0.6S5.4,0,5.6,0h6.1c0.2,0,0.5,0.1,0.8,0.2
				s0.5,0.3,0.7,0.4L14.6,2c0.2,0.2,0.3,0.4,0.4,0.7s0.2,0.6,0.2,0.8v2.3h0.6c0.5,0,0.9,0.2,1.2,0.5C17.4,6.7,17.6,7.1,17.6,7.6z
				M5.9,7h8.2V3.5h-1.5c-0.2,0-0.4-0.1-0.6-0.3c-0.2-0.2-0.3-0.4-0.3-0.6V1.2H5.9V7z M5.9,12.8h8.2v-2.3H5.9V12.8z M16.2,8
				c0.1-0.1,0.2-0.3,0.2-0.4c0-0.2-0.1-0.3-0.2-0.4C16.1,7.1,16,7,15.8,7c-0.2,0-0.3,0.1-0.4,0.2c-0.1,0.1-0.2,0.3-0.2,0.4
				c0,0.2,0.1,0.3,0.2,0.4c0.1,0.1,0.3,0.2,0.4,0.2C16,8.2,16.1,8.1,16.2,8z"/>
		</g><style>.icon_'.$rand.' {fill:'.$color.'}</style>
		</svg>';
	}
	
	/*
	 * Format product price
	 * @since 2.3
	 */
	public function format_price($price,$currency='') {

		$args=array();
		if($currency!="") {
			$args=array('currency'=>$currency);
		}
		$price=wc_price($price,$args);
		return html_entity_decode(strip_tags($price));

	}
	
	

}