<?php
if (! defined('ABSPATH')) {
    exit;
}

require_once('class-wpfortune-base.php');
require_once('class-wc-dymo-order.php');

if (in_array('woocommerce-dymo-print-product/woocommerce-dymo-print-product.php',get_option('active_plugins')) 
		&& file_exists(WP_PLUGIN_DIR. '/woocommerce-dymo-print-product/lib/class-wc-dymo-product-settings.php')) {
	require_once(WP_PLUGIN_DIR. '/woocommerce-dymo-print-product/lib/class-wc-dymo-product-settings.php');
}

class WPF_DYMO extends WPFortune_Base
{
	protected $dymo,$product;
	/*
     * When class is called, perform base actions
     *
     * @params string $settings_plugin_name The plugin name
     * @params string $settings_plugin_version The plugin version
     * @params string $settings_plugin_slug The plugin slug
     * @params string $settings_plugin_file The plugin file
     * @params string $settings_plugin_dir The plugin directory
     * @params string $settings_upgrade_url The plugin upgrade url
     * @params string $settings_renew_url The plugin renew subscription url
     * @params string $settings_docs_url The plugin docs url
     * @params string $settings_support_url The plugin support url
     */
	public function __construct($settings_plugin_name, $settings_plugin_version, $settings_plugin_id, $settings_plugin_slug, $settings_plugin_dir, $settings_plugin_file, $settings_upgrade_url, $settings_renew_url, $settings_docs_url, $settings_support_url)
    {
        parent::__construct($settings_plugin_name, $settings_plugin_version, $settings_plugin_id, $settings_plugin_slug, $settings_plugin_dir, $settings_plugin_file, $settings_upgrade_url, $settings_renew_url, $settings_docs_url, $settings_support_url);
		
		$this->dir_path=$settings_plugin_dir;

        add_action('plugins_loaded', array($this, 'translation_load_textdomain'));

		if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
        }

        if (!in_array('woocommerce/woocommerce.php',get_option('active_plugins')) && !is_plugin_active_for_network( 'woocommerce/woocommerce.php' )) {

            add_action('admin_notices', array($this,'show_admin_messages'));

        } elseif (is_admin()) {

			$this->dymo=new WPF_DYMO_Order();

			if(class_exists('WPF_DYMO_Product_Settings')) {
				$this->product=new WPF_DYMO_Product_Settings();
			}

            add_action('admin_enqueue_scripts', array($this, 'enqueue_scripts'));
			add_action('woocommerce_admin_order_actions_end', array($this,'alter_order_actions'), 99);
			add_action('admin_menu', array($this,'create_admin_menu'));
			add_action('add_meta_boxes', array($this,'create_meta_box'));
			add_action('admin_notices', array($this,'create_error_container'));

			//AJAX calls
			add_action( 'wp_ajax_wc_dymo_dismiss_update_notice', array($this,'dismiss_update_notice') );
			add_action( 'wp_ajax_wc_dymo_get_label_data', array($this,'get_label_data') );
			add_action( 'wp_ajax_wc_dymo_get_label_variables', array($this,'get_label_variables') );
			add_action( 'wp_ajax_wc_dymo_get_label_combo', array($this,'get_label_combo') );
			add_action( 'wp_ajax_wc_dymo_order_status', array($this,'update_order_status'));
			add_action( 'wp_ajax_wc_dymo_build_query', array($this,'build_query'));

			add_action( 'all_admin_notices', array($this,'update_notice' ));
			

			add_action( 'admin_footer', array($this,'bulk_actions_notice') );
			add_filter( 'bulk_actions-edit-shop_order',array($this,'register_bulk_actions'),10,1 );
			add_filter( 'handle_bulk_actions-edit-shop_order', array($this,'process_bulk_actions'), 10, 3 );

			add_filter('wpf_woocommerce_dymo_labels',array($this,'label_management'),9999,1);

			add_filter('wc_dymo_order',array($this,'create_array_output'),10,5);
			

        }

    }

	/*
	 * Load translations
	 */
	public function translation_load_textdomain()
    {

        load_plugin_textdomain($this->plugin_id, false, dirname( $this->plugin_file) . '/languages/');

    }

	/*
	* Load scripts and styles on specific admin pages
	*/
	public function enqueue_scripts($hook) {
		$current_screen=get_current_screen();

		wp_register_script( 'dymo-connect', plugins_url( '/assets/js/dymo.connect.framework.js', dirname(__FILE__) ) );
		
		$version=filemtime($this->dir_path.'/assets/js/woocommerce-dymo.js');
		wp_register_script( 'woocommerce-dymo-js', plugins_url( '/assets/js/woocommerce-dymo.js', dirname(__FILE__) ),array('jquery','wp-a11y','dymo-connect'),$version);

		$labeltime=get_option( 'wc_dymo_labeltime',3000);
		if($labeltime<1000) $labeltime=1000;

		$dymo_data=array(
			'ajax_nonce'=>wp_create_nonce('dymo_nonce'),
			'no_printers_installed'=>sprintf(__('No printers are installed or recognized by your browser. Please see %s for more information.', 'woocommerce-dymo'),'<a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/" target="_blank">'.__('our documentation','woocommerce-dymo').'</a>'),
			'no_printers_connected'=>esc_html__('DYMO Label Writer is not connected. Please re-connect your DYMO Label Writer.', 'woocommerce-dymo'),
			'default_printer'=>esc_html__('Default printer selected. Printing on:', 'woocommerce-dymo'),
			'general_error'=>esc_html__('There was an error while printing your label.', 'woocommerce-dymo'),
			'no_framework'=>esc_html__('Uncaught Error: DYMO Label Framework is not installed. Please check DYMO Web Service.','woocommerce-dymo'),
			'label_printed'=>esc_html__('1 DYMO label printed.','woocommerce-dymo'),
			'post_ID'=>esc_html__('Post ID:','woocommerce-dymo'),
			'no_data'=>__('No WooCommerce data found. Did you assign WooCommerce data to label objects?', 'woocommerce-dymo').' <a href="admin.php?page=woocommerce_dymo" target="_blank">'.__('Check your label settings','woocommerce-dymo').'</a>',
			'skipped'=>__('Skipped label(s) during printing.','woocommerce-dymo'),
			'status_changed'=>__('Order status changed.','woocommerce-dymo'),
			'reload_page'=>__('Reload page after printing to see changes.','woocommerce-dymo'),
			'labeltime'=>$labeltime,
			'debug'=>get_option('wc_dymo_debug',0),
			'combo_error'=>__('Error creating label combination.','woocommerce-dymo'),
			'finished'=>__('Finished printing all labels.', 'woocommerce-dymo'),
			'wait_redirect'=>__('Wait until page is reloaded.','woocommerce-dymo'),
			'job_title'=>__('WooCommerce DYMO Print','woocommerce-dymo-print'),
		);
		
		wp_localize_script('woocommerce-dymo-js','dymo_data',$dymo_data);

		if(in_array($hook,array('edit.php','post.php')) && $current_screen->post_type=='shop_order') {

			wp_enqueue_script( 'dymo-connect' );
			wp_enqueue_script( 'woocommerce-dymo-js' );

			wp_enqueue_style('woocommerce-dymo',plugins_url( '/assets/css/woocommerce-dymo.css', dirname(__FILE__) ) );

		}

		if(strpos($current_screen->base,'_page_woocommerce_dymo')) {

			wp_enqueue_script( 'jquery-ui-tabs' );
			wp_enqueue_script( 'farbtastic' );

			wp_enqueue_script( 'dymo-connect', array('jquery','farbtastic' ) );
			wp_enqueue_script( 'woocommerce-dymo-js');

			$version=filemtime($this->dir_path.'/assets/js/woocommerce-dymo-settings.js');
			wp_register_script( 'woocommerce-dymo-settings', plugins_url( '/assets/js/woocommerce-dymo-settings.js', dirname(__FILE__) ),array('jquery','jquery-ui-tabs','dymo-connect'),$version );

			wp_enqueue_style( 'farbtastic');
			wp_enqueue_style('woocommerce-dymo',plugins_url( '/assets/css/woocommerce-dymo.css', dirname(__FILE__) ) );

			$debugmode=get_option( 'wc_dymo_debug' );

			$labels=$this->dymo->get_labels();
			$chosenFile=get_option( 'wc_dymo_labelfile' );
			$files=$this->dymo->get_label_files();

			$uploadDir=wp_upload_dir();
			$dir= $uploadDir['baseurl'].'/wc-dymo-labels/';

			$labelArray=array();
			
			foreach($labels as $label=>$args) {
				if(isset($chosenFile[$label]) && in_array($chosenFile[$label],$files)) {
					$labelArray[$label]=$dir.$chosenFile[$label];
				}
			}

			$labelArray=json_encode($labelArray);
			
			$settingsJS = array(
				'security'=>wp_create_nonce( "wc-dymo-ajax" ),
				'autocut'=>__('The printer supports auto-cut','woocommerce-dymo'),
				'no_autocut'=>__('The printer does NOT supports auto-cut','woocommerce-dymo'),
				'twinturbo'=>__('The printer is TwinTurbo','woocommerce-dymo'),
				'no_twinturbo'=>__('The printer is NOT TwinTurbo','woocommerce-dymo'),
				'printer'=>__('Printer:','woocommerce-dymo'),
				'printerType'=>__('Printer type','woocommerce-dymo'),
				'is_local'=>__('Local printer','woocommerce-dymo'),
				'local'=>__('Local','woocommerce-dymo'),
				'connected'=>__('Connected','woocommerce-dymo'),
				'modelname'=>__('Model name','woocommerce-dymo'),
				'printername'=>__('Printer name','woocommerce-dymo'),
				'is_online'=>__('Printer connected:','woocommerce-dymo'),
				'isBrowserSupported'=>__('Browser supports DYMO Framework:','woocommerce-dymo'),
				'isFrameworkInstalled'=>__('DYMO Framework is installed:','woocommerce-dymo'),
				'isWebServicePresent'=>__('DYMO WebService is running:','woocommerce-dymo'),
				'errorDetails'=>__('Error details:','woocommerce-dymo'),
				'ServiceDiscovery'=>sprintf(__('Error: No connection to DYMO Web Service. Check if DYMO Web Service is installed and running. For more information see %s.','woocommerce-dymo'),'<a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/dymo-web-service/#debuginformation" target="_blank">'.__('our documentation','woocommerce-dymo')),
				'environmentCheck'=>__('DYMO Javascript Framework environment check...','woocommerce-dymo'),
				'pluginVersion'=>sprintf(__('WooCommerce DYMO Print plugin PRO version: %s','woocommerce-dymo'),$this->version),
				'frameworkVersion'=>__('DYMO Javascript Framework version:','woocommerce-dymo'),
				'dymoconnect'=>__('Using DYMO Connect JS Library','woocommerce-dymo'),
				'debugmode'=>$debugmode,
				'debugmodeTxt'=>__('Debug mode is active','woocommerce-dymo'),
				'labelArray'=>$labelArray,
				'noFramework'=>__('DYMO Javascript Framework not active or DYMO Web Service not running on your system','woocommerce-dymo'),
				'noPreview'=>__('Can not create preview from label','woocommerce-dymo'),
				'clearCache'=>__('This might be a caching issue. Clear your browser history and use CTRL+F5 (Windows) or Cmd+Shift+R (Mac) to Hard Refresh this page.','woocommerce-dymo'),
				'seeDebugInfo'=>__('See debug information in documentation.','woocommerce-dymo'),
				'noFramework'=>sprintf(__('DYMO Javascript Framework not active or DYMO Web Service not running on your system. Make sure DYMO Label Software version 8.7.2 or higher is installed. For more information see %s.','woocommerce-dymo'),'<a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/debug-dymo-framework/" target="_blank">'.__('our documentation','woocommerce-dymo')),
				'savePreview'=>__('Save label layout to generate preview.','woocommerce-dymo'),
				'noMetaData'=>__('Could not retrieve metadata for this order and metakey.','woocommerce-dymo'),
				'noSerializedData'=>__('Data found, but not serialized. No advanced options possible.','woocommerce-dymo'),
			);

			wp_localize_script( 'woocommerce-dymo-settings', 'objects', $settingsJS );

			wp_enqueue_script( 'woocommerce-dymo-settings');

		}
	}

	/*
	* Admin messages
	* Since 1.2
	*/
	public function showDymoMessage($message, $errormsg = false)
	{
		if ($errormsg) {
		echo '<div id="message" class="error">';
		} else {
		echo '<div id="message" class="updated fade">';
		}
		echo "<p>$message</p></div>";

	}

	/*
	 * Show admin message if WooCommerce is not active
	 */
	public function show_admin_messages() {
		$this->showDymoMessage(__( 'WooCommerce is not active. Please activate WooCommerce plugin before using WooCommerce DYMO Print plugin.', 'woocommerce_dymo'), true);
	}


	/*
	* WordPress Administration Menu
	*/
	public function create_admin_menu() {
		add_submenu_page('woocommerce', $this->plugin_name, __( 'DYMO Print', $this->plugin_id), 'manage_woocommerce', $this->plugin_slug, array($this, 'admin_render'));
	}

    /*
     * Renders the admin page
     *
     * @return void
     */

    public function admin_render()
    {

		// Check the user capabilities
		if ( !current_user_can( 'manage_woocommerce' ) ) {
			wp_die( __( 'You do not have sufficient permissions to access this page.', 'woocommerce-dymo' ) );
		}

		// Get upload directory
		$uploadDir=wp_upload_dir();
		$dir= $uploadDir['baseurl'].'/wc-dymo-labels/';
		$path= $uploadDir['basedir'].'/wc-dymo-labels/';


		// Process uploaded and deleted label files/*// Save the field values
		if ( isset( $_POST['dymo_fields_submitted'] ) && $_POST['dymo_fields_submitted'] == 'submitted' && wp_verify_nonce($_POST['dymo_nonce_field'],'dymo_nonce_action')) {

			// Upload label files
			if(isset($_POST['label']) && $_POST['label']!="") {
				$label=$_POST['label'];
				$upload=$this->upload_label($label);

				if($upload['status']==200) {
					$_POST['wc_dymo_labelfile']=str_replace($path,'',$upload['file']);
				} elseif($upload['status']==501) {
					$uploadError=__('Wrong file type. Upload file created with DYMO Connect (DCD)','woocommerce-dymo');
				}else {

					$filetmp = $path.$_POST['wc_dymo_labelfile'];
					$labelContent=file_get_contents($filetmp);

					$fallback=get_option('wc_dymo_labelfile_fallback');
					$fallback[$label]=esc_attr($labelContent);
					update_option('wc_dymo_labelfile_fallback',$fallback);

				}

			}

			// Update settings
			foreach ( $_POST as $key => $value ) {

				if (strpos($key, 'wc_dymo_') !== false) {

					if(isset($_POST['label']) && $_POST['label']!="") {
						$value=array($label=>$value);

						$current= get_option($key);
						if(isset($current) && is_array($current) && !empty($current)) {
							$value=array_merge($current,$value);
						}
					}

					update_option( $key, $value);

				}
			}
		} elseif(isset( $_POST['dymo_fields_submitted'] ) && $_POST['dymo_fields_submitted'] == 'delete' && wp_verify_nonce($_POST['dymo_nonce_field'],'dymo_nonce_action') && !empty($_POST['wc_dymo_delete_label'])){

			// Delete label files
			foreach($_POST['wc_dymo_delete_label'] as $label) {

				wp_delete_file($path.$label);

			}

		} elseif(isset( $_POST['dymo_fields_submitted'] ) && $_POST['dymo_fields_submitted'] == 'copy' && wp_verify_nonce($_POST['dymo_nonce_field'],'dymo_nonce_action')){

			// Copy sample labels
			echo $this->copy_labels($path);

		} elseif(isset( $_POST['dymo_fields_submitted'] ) && $_POST['dymo_fields_submitted'] == 'combo' && !empty($_POST['wc_dymo_combo']) && wp_verify_nonce($_POST['dymo_nonce_field'],'dymo_nonce_action')){

				$combos=get_option('wc_dymo_combo',true);
				if(is_array($combos)) {
					$update=array_merge($combos,$_POST['wc_dymo_combo']);
				} else {
					$update=$_POST['wc_dymo_combo'];
				}

				update_option('wc_dymo_combo', $update);

		}elseif(isset( $_POST['dymo_types'] ) && $_POST['dymo_types'] == true && wp_verify_nonce($_POST['dymo_nonce_field'],'dymo_nonce_action')){
			$new_labels=array();

			foreach($_POST['wc_type'] as $type => $data) {
			
				if(!isset($data['delete']) && $data['name']!="") {

					if($type=='new') {
						$type=sanitize_title($data['name']);
					}

					$new_labels[$type]=$data;
				}

			}
			
			update_option( 'wc_dymo_type', $new_labels);

		}

		// Get all label information
		$files=$this->dymo->get_label_files();
		$labels=$this->dymo->get_labels();
		$chosenFile=get_option( 'wc_dymo_labelfile' );
		$combos=get_option( 'wc_dymo_combo',true);

		$labelObjects=get_option( 'wc_dymo_option' );
		
		$order_status=wc_get_order_statuses();

		//Receive all order_meta from the last WC order
		$args=array(
			'post_type'   => 'shop_order',
			'post_status' => array_keys( $order_status ),
			'numberposts'=>1
		);

		$shop_orders=get_posts($args);
		$latest_order=$shop_orders[0]->ID;
		$test_order=new WC_Order($latest_order);

		$test_nonce = wp_create_nonce('print-dymo');

		$order_meta = $this->get_order_metakeys();

		$dymoPrinters=get_option('wc_dymo_printers');
		$dymoPrintersArgs=get_option('wc_dymo_printers_args');

		$repeatLabel=get_option('wc_dymo_order_item');

		$labeltime=get_option( 'wc_dymo_labeltime',3000);
		if($labeltime<1000) {
			$labeltime=1000;
		}


		// Load views
		include_once($this->plugin_dir . 'views/admin/header.php');

		foreach($labels as $label=>$args) {
			$objects=array();
			if(isset($labelObjects[$label])) {
				$objects=$labelObjects[$label];
			}

			if($repeatLabel!="" && is_array($repeatLabel) && !empty($repeatLabel) && isset($repeatLabel[$label]['qty'])) {
				$repeat=$repeatLabel[$label]['qty'];
			} else {
				$repeat =0;
			}

			if(!isset($args['type']) || $args['type']=="") {
				$args['type']='order';
			}

			$object_extras=get_option( 'wc_dymo_option_extras' );

			if(isset($object_extras[$label])) {
				$object_extras=$object_extras[$label];
			} else {
				$object_extras['address']=array();
				$object_extras['productlist']=array();
				$object_extras['serialized']=array();
			}

			if($args['type']=='combo') {
				if(!isset($combos[$label]['labeltype'])) {
					if(!isset($combos[$label]) && !is_array($combos)) {
						$combos=array();
						$combos[$label]=array();
					}
					$combos[$label]['labeltype']='order';
				}
				include($this->plugin_dir . 'views/admin/multiple-labels.php');
			} elseif($args['type']=='product' && isset($this->product)) {
				$this->product->render_settings($label,$files,$args,$chosenFile,$objects,$object_extras,$dymoPrinters,$dymoPrintersArgs);
			}elseif(in_array($args['type'],array('order','order_item'))) {
				include($this->plugin_dir . 'views/admin/single-label.php');
			} else {
				include($this->plugin_dir . 'views/admin/error.php');
			}
		}

		$default_types=array(
			'order'=>__('Order','woocommerce'),
			'order_item'=>__('Order item','woocommerce-dymo'),
			'combo'=>__('Combination','woocommerce-dymo'),
		);


		$labelTypes=apply_filters('wc_dymo_label_types', $default_types);

		include_once($this->plugin_dir . 'views/admin/settings.php');
		include_once($this->plugin_dir . 'views/admin/debug.php');
		include_once($this->plugin_dir . 'views/admin/footer.php');
    }

	/*
	* Create order meta box
	*/
	public function create_meta_box() {
		add_meta_box( 'woocommerce-dymo-box', __( 'Print DYMO labels', 'woocommerce-dymo' ), array($this,'render_meta_box'), 'shop_order', 'side', 'default' );
	}

	/*
	*Create order meta box content
	*/
	public function render_meta_box() {
		global $post_id;
		$order = new WC_Order($post_id);

		$labels=$this->dymo->get_labels();
		$files=$this->dymo->get_label_files();
		$chosenFile=get_option( 'wc_dymo_labelfile' );
		$combos=get_option('wc_dymo_combo',true);
		$nonce = wp_create_nonce('print-dymo');
		
		if(empty($labels)) return false;

		foreach($labels as $label => $args) {
			if(isset($chosenFile[$label]) && in_array($chosenFile[$label],$files) && (!isset($args['type']) || $args['type']!='product')) {
				printf( '<a href="#" class="button wc-action-button-dymo wc-action-button tips dymo-link wc-action-button-%1$s dymo-button-%1$s-%3$s" data-tip="Print %2$s label" data-printid="%3$s" data-request="%1$s" data-type="%6$s" data-nonce="%4$s"><span class="dymo-spinner"></span>%5$s Print %2$s label</a>', $label,$args['name'], $order->get_ID(), $nonce,$this->dymo->get_icon($args['color']),$args['type']);
			} 
			if($args['type']=='combo' && isset($combos[$label]['labeltype']) && $combos[$label]['labeltype']=='order') {
				printf( '<a href="#" class="button wc-action-button-dymo wc-action-button tips dymo-link wc-action-button-%1$s dymo-button-%1$s-%3$s" data-tip="Print %2$s label" data-printid="%3$s" data-request="%1$s" data-type="%6$s" data-nonce="%4$s"><span class="dymo-spinner"></span>%5$s Print %2$s label</a>', $label,$args['name'], $order->get_ID(), $nonce,$this->dymo->get_icon($args['color']),$args['type']);
			} 
		}
		?>

		<span class="dymo-animate dymo-progress dymo-progress-<?php echo $order->get_ID();?>" data-dymo-id="<?php echo $order->get_ID();?>"><span></span></span>

		<div class="dymo-errors order_notes dymo-hidden"></div>
		<div class="dymo-success order_notes dymo-hidden"></div>
		<style>#woocommerce-dymo-box h4 {margin-bottom:4px} #woocommerce-dymo-box .dymo-link {margin:2px 4px 2px 0!important;}.dymo-link svg {width:20px;height:20px;margin:3px 4px 0 -3px;display:inline-block;float:left}</style>
		<?php
	}

	/*
	* Convert address to javascript string
	* @since 1.1.1
	*/
	public function sanitize_address($address) {
		$address= preg_replace('/<br\/(\s+)?\/?>/i', "|", preg_replace("/[\n\r]/","|",$address));
		$address= preg_replace('/<br(\s+)?\/?>/i', "", preg_replace("/[\n\r]/","|",$address));
		$address= str_replace("||","|",$address);
		$address=rtrim(str_replace("'", "\'", htmlspecialchars_decode($address,ENT_QUOTES)),"|");
		return stripslashes($address);
	}

	/*
	* Admin notice for DYMO Print version 2.0
	*/
	public function update_notice() {
		
		if ( ! get_option('wc_dymo_notice-old_dymo', FALSE ) ) { 
		?>
		<div class="updated notice error dymo-update-notice is-dismissible" data-notice="old_dymo">
			<p><?php _e( 'WooCommerce DYMO Print 5.0 does not work with older versions of DYMO Label Software (DLS). Please update your DYMO Label Software (DLS) to version 8.7.2. or above.', 'woocommerce-dymo' ); ?> <a href="https://wordpress.org/plugins/woocommerce-dymo-print/installation/" target="_blank"><?php _e('Read installation instructions','woocommerce-dymo-print');?></a></p>
		</div>
		<script>
			jQuery(function($) {
				$( document ).on( 'click', '.dymo-update-notice .notice-dismiss', function () {
       
					var type = $( this ).closest( '.dymo-update-notice' ).data( 'notice' );
			
					$.ajax( ajaxurl,
					{
					type: 'POST',
					data: {
						action: 'wc_dymo_dismiss_update_notice',
						type: type,
					}
					} );
				} );
			});
		</script>
       <?php }
	}

	/*
	* Close admin notice and do not show it again
	*/
	public function dismiss_update_notice() {
		if(isset($_POST) && $_POST['action']=='wc_dymo_dismiss_update_notice' && $_POST['type']!="") {
			$type = esc_attr($_POST['type']);
			
			// Store it in the options table
			update_option( 'wc_dymo_notice-'.$type, TRUE );
		}
		wp_die();
	}


	/*
	 * Show action buttons in order overview table
	 */
	public function alter_order_actions($order) {

		if (empty($order)) return;

		$labels=$this->dymo->get_labels();
		$files=$this->dymo->get_label_files();
		$chosenFile=get_option( 'wc_dymo_labelfile' );
		$combos=get_option('wc_dymo_combo',true);

		$nonce = wp_create_nonce('print-dymo');
		
		
		if(empty($labels)) return false;
		
		foreach($labels as $label => $args) {
			if(isset($chosenFile[$label]) && in_array($chosenFile[$label],$files) && (!isset($args['type']) || $args['type']!='product')) {
				printf( '<a href="#" class="button wc-action-button-dymo wc-action-button tips dymo-link wc-action-button-%1$s dymo-button-%1$s-%3$s" data-tip="Print %2$s label" data-printid="%3$s" data-request="%1$s" data-type="%6$s" data-nonce="%4$s"><span class="dymo-spinner"></span>%5$s Print %2$s label</a>', $label,$args['name'], $order->get_ID(), $nonce,$this->dymo->get_icon($args['color']),$args['type']);
			}

			if($args['type']=='combo' && isset($combos[$label]['labeltype']) && $combos[$label]['labeltype']=='order') {
				printf( '<a href="#" class="button wc-action-button-dymo wc-action-button tips dymo-link wc-action-button-%1$s dymo-button-%1$s-%3$s" data-tip="Print %2$s label" data-printid="%3$s" data-request="%1$s" data-type="%6$s" data-nonce="%4$s"><span class="dymo-spinner"></span>%5$s Print %2$s label</a>', $label,$args['name'], $order->get_ID(), $nonce,$this->dymo->get_icon($args['color']),$args['type']);
			}
		}

		echo '<span class="animate dymo-progress dymo-progress-'.$order->get_ID().'" data-dymo-id="'.$order->get_ID().'"><span></span></span>';

	}

	/*
	 * AJAX call to get order data (address) from specific order
	 */
	public function get_label_data() {

		check_ajax_referer( 'dymo_nonce', 'security' );
		
		$item='';
		$output=array();

		$printID=explode(',',$_GET['printID']);
		
		
		$labelID=$_GET['label'];

		$objects=get_option( 'wc_dymo_option' );
		$object=array_filter($objects[$labelID]);

		$allLabels=$this->dymo->get_labels();

		$order_item_info=get_option('wc_dymo_order_item');

		if(isset($_GET['print_type'])) {
			$print_type=$_GET['print_type'];
		} else {
			$print_type=false;
		}

		$currentLabel=$allLabels[$labelID];
		if(!isset($currentLabel['type'])) {
			$labelType='order';
		} else {
			$labelType=$currentLabel['type'];
		}
		
		if($labelType=='product') {
			
			$output=$this->product->get_product_labels($printID,$object,$labelID,$item,$print_type);
			
		} else {
			$j=10;
			foreach($printID as $order_id) {
				
				$order = new \WC_Order($order_id);
	
				$i=0;
				$output[$j.'_'.$order_id]['amount'] = apply_filters('woocommerce_dymo_amount_per_order', 1, $order, $labelID);
				
	
				if($labelType=='order_item') {
	
					foreach($order->get_items() as $item) {
						
						if($order_item_info[$labelID]['qty']==1) {
							$x=0;
							while($x < $item['qty']) {
	
								$order_output=apply_filters('wc_dymo_order',$this->order_output($object,$order,$labelID,$item),$object,$order,$labelID,$item);
									
								foreach($order_output as $data) {
									$output[$j.'_'.$order_id][$i]=$data;
									$i++;
								}
	
								$x++;
							}
						} else {
	
							$order_output=apply_filters('wc_dymo_order',$this->order_output($object,$order,$labelID,$item),$object,$order,$labelID,$item);
	
							foreach($order_output as $data) {
								$output[$j.'_'.$order_id][$i]=$data;
								$i++;
							}
	
						}
		
					}
				} else {
	
					$order_output=apply_filters('wc_dymo_order',$this->order_output($object,$order,$labelID,$item),$object,$order,$labelID,$item);
	
					foreach($order_output as $data) {
						$output[$j.'_'.$order_id][$i]=$data;
						$i++;
					}
	
				}
				$j++;
			}
		}
		
		if(empty($output)) {
			$output[0]='nofields';
		}
		echo json_encode($output);
		
		wp_die();
	}

	/*
	 * AJAX call to get label variables
	 */
	public function get_label_variables() {
		check_ajax_referer( 'dymo_nonce', 'security' );
		$return['fail']='';

		$labelID=$_GET['labelID'];

		$xml=get_option('wc_dymo_labelfile_fallback');

		$printers=get_option('wc_dymo_printers');
		$printerArgs=get_option('wc_dymo_printers_args');
		
		$return['printer']=$printers[$labelID];
		$return['printerArgs']=$printerArgs[$labelID];
		
		$productOptions=get_option('wc_dymo_products');
		if(isset($productOptions[$labelID]['stock'])) $return['print_stock']=$productOptions[$labelID]['stock'];	
		//if(isset($productOptions[$labelID]['bulk'])) $return['print_bulk']=$productOptions[$labelID]['bulk'];	
		

		if(isset($xml[$labelID])) {
			/*/$return['template']=trim(str_replace('<?xml version="1.0" encoding="utf-8"?>','',html_entity_decode($xml[$labelID])));*/
			
			//$xml[$labelID]=str_replace('&amp;','AMPREPLACEDYMO',$xml[$labelID]);
			//$labelTemplate=html_entity_decode($xml[$labelID]);
			//$labelTemplate=str_replace('AMPREPLACEDYMO','&amp;',$labelTemplate);
			
			$labelTemplate=$xml[$labelID];
			
			$return['template']=trim(str_replace('<?xml version="1.0" encoding="utf-8"?>','',$labelTemplate));
			$return['template']=trim($labelTemplate);
			
			$allLabels=$this->dymo->get_labels();
			$currentLabel=$allLabels[$labelID];
			
			$chosenFile=get_option( 'wc_dymo_labelfile' );
			
			$uploadDir=wp_upload_dir();
			$dir= $uploadDir['baseurl'].'/wc-dymo-labels/';
			
			$return['labelfile']=$dir.$chosenFile[$labelID];
		} else {
			$return['fail'].= __('There was a problem opening your label. No label XML found. Upload your label-file again.', 'woocommerce-dymo');
		}

		wp_send_json($return);

		die();
	}

	/*
	 * Show errors above order table
	 */
	public function create_error_container() {
		$current_screen=get_current_screen();
		if($current_screen->id=='edit-shop_order') {
			echo '<div class="error dymo-errors dymo-hidden"></div>';
			echo '<div class="updated dymo-success dymo-hidden"></div>';
		}
	}

	/*
	 * Get all order meta keys and save them in transients
	 */
	public function save_order_metakeys($post_type='shop_order'){
		global $wpdb;

		$query = "
			SELECT DISTINCT($wpdb->postmeta.meta_key)
			FROM $wpdb->posts
			LEFT JOIN $wpdb->postmeta
			ON $wpdb->posts.ID = $wpdb->postmeta.post_id
			WHERE $wpdb->posts.post_type = '%s'
			AND $wpdb->postmeta.meta_key != ''
		";

		$meta_keys = $wpdb->get_col($wpdb->prepare($query, $post_type));
		sort($meta_keys);

		set_transient('wc_dymo_metadata_'.$post_type, $meta_keys, 60*60*24); # create 1 Day Expiration

		return $meta_keys;
	}

	/*
	 * Get all order meta keys from transients
	 */
	public function get_order_metakeys(){
		$cache = get_transient('wc_dymo_metadata_shop_order');
		$meta_keys = $cache ? $cache : $this->save_order_metakeys('shop_order');
		return $meta_keys;
	}

	/*
	 * Copy sample labels to new label dir
	 */
	public function copy_labels($path) {

		$sample_labels= array(
			'sample-simple-address.label',
			'sample-barcode-label.label',
			'sample-qrcode-label.label',
			'sample-productlist.label',
		);

		$plugin_dir=plugin_dir_path( dirname(__FILE__) );
		$success=0;
		foreach($sample_labels as $label ) {
			if (!copy($plugin_dir.'assets/labels/'.$label, $path.$label)) {
				echo "failed to copy $plugin_dir to $path...\n";
			} else {
				$success++;
			}
		}
		if($success==count($sample_labels)) {
			return '<div id="message" class="updated fade"><p><strong>'.__( 'Sample labels successfully copied.', 'woocommerce-dymo' ).'</strong></p></div>';
		} else {
			return '<div id="message" class="error fade"><p><strong>'.__( 'Error: We could not copy all sample labels.', 'woocommerce-dymo' ).'</strong></p></div>';
		}
	}

	/*
	 * Label file upload process
	 */
	public function upload_label($label='') {
		if(isset($_FILES['uploadfile_'.$label])) {
			$uploadfile = $_FILES['uploadfile_'.$label];
		}

		if (isset($uploadfile) && is_array($uploadfile)) {

			if ($uploadfile['error'] == 0) {
				$filetmp = $uploadfile['tmp_name'];

				$labelContent=file_get_contents($filetmp);
				
				$fallback=get_option('wc_dymo_labelfile_fallback');
				
				$fallback[$label]=esc_attr($labelContent);
				update_option('wc_dymo_labelfile_fallback',$fallback);

				$re = "/MediaType=\"[\\s\\S]*?\"/";
				$str = $labelContent;
				$subst = "";
				$labelContent = trim(preg_replace($re, $subst, $str));

				$filetype = wp_check_filetype( $uploadfile['name'],array('label'=>'application/xml','dymo'=>'application/xml') );
				if($filetype['ext']=='label' || $filetype['ext']=='dymo') {

					$upload_dir   = wp_upload_dir();
					$upload_dir=$upload_dir['basedir'].'/wc-dymo-labels/';
					if(wp_mkdir_p($upload_dir)) {
						$labelFile=$upload_dir.$uploadfile['name'];

						if(file_exists($labelFile)) {
							$labelFile=$upload_dir.str_replace('.label','-'.rand(0,9999).'.label',$uploadfile['name']);
							$labelFile=$upload_dir.str_replace('.dymo','-'.rand(0,9999).'.dymo',$labelFile);
						}

						file_put_contents($labelFile, $labelContent);
						return array('status'=>200,'file'=>$labelFile);
					} else {
						return array('status'=>500);
					}

				} else {
					return array('status'=>501);
				}
			}
		}
	}

	/*
	 * Generate output by object
	 */
	public function order_output($objects=array(),$order,$label,$item=array()) {
		foreach($objects as $object=>$data) {

			switch($data) {
				case 1: // Order number
					$output[$object]=$this->order_number($order);
					break;
				case 2: // Product list
					$output[$object]=$this->get_product_list($order,$label,$object);
					break;
				case 3: // Order total (incl. TAX)
					$output[$object]=$this->dymo->format_price($order->get_total(),$order->get_currency());
					break;
				case 4: // Shipping method
					$output[$object]=$order->get_shipping_method();
					break;
				case 5: //Billing: Default Customer address
					$output[$object]=$this->get_default_address($order,$label,'billing');
					break;
				case 6: // Billing: Custom Customer address
					$output[$object]=$this->get_custom_address($order,$label,$object,'billing');
					break;
				case 7: // Billing: Customer Email address
					$output[$object]=nl2br(stripslashes($order->get_billing_email()));
					break;
				case 8: // Billing: Customer Phone number
					$output[$object]=nl2br(stripslashes($order->get_billing_phone()));
					break;
				case 9: // Billing: Customer State (full)
					$state=$this->get_country_state($order,$label,'billing');
					$output[$object]=$state['state'];
					break;
				case 10: // Billing: Customer Country (full)
					$country=$this->get_country_state($order,$label,'billing');
					$output[$object]=$country['country'];
					break;
				case 11: // Shipping: Default Customer address
					$output[$object]=$this->get_default_address($order,$label,'shipping');
					break;
				case 12: // Shipping: Custom Customer address
					$output[$object]=$this->get_custom_address($order,$label,$object,'shipping');

					break;
				case 13: // Shipping: Customer State (full)
					$state=$this->get_country_state($order,$label,'shipping');
					$output[$object]=$state['state'];
					break;
				case 14: // Shipping: Customer State (full)
					$country=$this->get_country_state($order,$label,'shipping');
					$output[$object]=$country['country'];
					break;
				case 15: // Order date & time
					//$output[$object]=$order->get_date_created();
					$output[$object]=date_i18n(apply_filters('wc_dymo_date_format',get_option('date_format'),$label,'order_datetime').' '.apply_filters('wc_dymo_time_format',get_option('time_format'),$label,'order_datetime'),strtotime($order->get_date_created()));
					break;
				case 16: // Order date
					$output[$object]=date_i18n(apply_filters('wc_dymo_date_format',get_option('date_format'),$label,'order_date'),strtotime($order->get_date_created()));
					//$output[$object]=$date[0];
					break;
				case 17: // Order time
					//$time=explode(' ',$order->get_date_created());
					$output[$object]=date_i18n(apply_filters('wc_dymo_time_format',get_option('time_format'),$label,'order_time'),strtotime($order->get_date_created()));
					break;
				case 'order_completed_datetime': // Order completed date & time
					//$output[$object]=$order->get_date_created();
					$output[$object]=date_i18n(apply_filters('wc_dymo_date_format',get_option('date_format'),$label,$data).' '.apply_filters('wc_dymo_time_format',get_option('time_format'),$label,$data),strtotime($order->get_date_completed()));
					break;
				case 'order_completed_date': // Order completed date
					$output[$object]=date_i18n(apply_filters('wc_dymo_date_format',get_option('date_format'),$label,$data),strtotime($order->get_date_completed()));
					//$output[$object]=$date[0];
					break;
				case 'order_completed_time': // Order completed time
					//$time=explode(' ',$order->get_date_created());
					$output[$object]=date_i18n(apply_filters('wc_dymo_time_format',get_option('time_format'),$label,$data),strtotime($order->get_date_completed()));
					break;
				case 'order_paid_datetime': // Order completed date & time
					//$output[$object]=$order->get_date_created();
					$output[$object]=date_i18n(apply_filters('wc_dymo_date_format',get_option('date_format'),$label,$data).' '.apply_filters('wc_dymo_time_format',get_option('time_format'),$label,$data),strtotime($order->get_date_paid()));
					break;
				case 'order_paid_date': // Order completed date
					$output[$object]=date_i18n(apply_filters('wc_dymo_date_format',get_option('date_format'),$label,$data),strtotime($order->get_date_paid()));
					//$output[$object]=$date[0];
					break;
				case 'order_paid_time': // Order completed time
					//$time=explode(' ',$order->get_date_created());
					$output[$object]=date_i18n(apply_filters('wc_dymo_time_format',get_option('time_format'),$label,$data),strtotime($order->get_date_paid()));
					break;
				case 18: // Customer note
					$object_extras=get_option( 'wc_dymo_option_extras' );
					if(isset($object_extras[$label]['customernote'][$object])) {
						$wordwrap=$object_extras[$label]['customernote'][$object];
					} else {
						$wordwrap=80;
					}
					$output[$object]=wordwrap($order->get_customer_note(),$wordwrap,"\n");
					break;
				case 19: // Order ID
					$output[$object]=(string)apply_filters( 'wpf_woocommerce_dymo_orderid', $order->get_ID());
					break;
				case 'order_item_sku':
					$output[$object]=$this->get_sku($item);
					break;
				case 'order_item_name':
					$output[$object]=html_entity_decode($item['name']);
					break;
				case 'order_item_qty':
					$output[$object]=(string)$item['qty'];
					break;
				case 'order_item_price':
					$output[$object]=$this->get_product_price($item);
					break;
				case 'order_item_line_total':
					$output[$object]=$this->dymo->format_price($item['line_total'],$order->get_currency());
					break;
				case 'order_item_line_total_tax':
					$output[$object]=$this->dymo->format_price($item['line_total']+$item['line_tax'],$order->get_currency());
					break;
				case 'order_item_short_descr':
					$object_extras=get_option( 'wc_dymo_option_extras' );
					if(isset($object_extras[$label]['short_descr'][$object])) {
						$wordwrap=$object_extras[$label]['short_descr'][$object];
					} else {
						$wordwrap=80;
					}
					$output[$object]=wordwrap($this->get_product_short_descr($item,$order),$wordwrap,"\n");
					break;
				case 'order_item_attr':
					$output[$object]=$this->get_product_attr($item,$order);
					break;
				case 'order_item_attr_new': // attributes with label
					$output[$object]=$this->get_product_attr($item,$order,1);
					break;
				case 'order_item_url':
					$output[$object]=esc_url(get_permalink($item['product_id']));
					break;
				case 'order_function':
					$result=apply_filters('wc_dymo_order_function', $order->get_ID(),$label,$object);
					$output[$object]=html_entity_decode( stripslashes($result), ENT_QUOTES, "utf-8" ).' ';
					break;
				case stristr($data,'order_meta_'): // Order meta data
					$output[$object]=$this->get_order_meta($order->get_ID(),$data,$label,$object);
					break;
				case 'invoice':
					if(is_plugin_active('woocommerce-pip/woocommerce-pip.php')) {
						$invoice_number    = $order->get_order_number();
						$invoice_number = get_option( 'wc_pip_invoice_number_prefix', '' ) . $invoice_number . get_option( 'wc_pip_invoice_number_suffix', '' );
						$output[$object]=nl2br(stripslashes($invoice_number)).' ';
					}
					if(is_plugin_active('woocommerce-pdf-invoice/woocommerce-pdf-invoice.php') && get_post_meta($order->get_ID(), '_invoice_number_display', TRUE )!="") { $invoicenr=get_post_meta($order->get_ID(), '_invoice_number_display', TRUE ); $output[$object]=nl2br(stripslashes($invoicenr)).' '; }

					break;
				default:
					$output[$object]='FAILED';
			}
		}
		return $output;
	}

	/*
	 * Convert Order number without hash
	 * @since 2.1.2
	 */
	public function order_number($order) {
		return nl2br(stripslashes(ltrim( $order->get_order_number(), _x( '#', 'hash before order number', 'woocommerce' ) )));
	}

	/*
	 * Get product list from order
	 */
	public function get_product_list($order,$label,$object){
		$objects=get_option( 'wc_dymo_option_extras' );
		$product_layout=$objects[$label]['productlist'][$object];

		$products='';
		if ( sizeof( $order->get_items() ) > 0 ) {

		foreach ($order->get_items() as $item) {
			$var_sku=$item_meta_var=$product_variation='';
			$attribute=$variation_attributes=array();

			if ( $item['product_id'] > 0 ) {
				$product_price=$this->dymo->format_price($item['line_total'],$order->get_currency());
				$product=wc_get_product($item['product_id']);
				$product_sku=stripslashes($product->get_sku());
				
				$product = $item->get_product();
				//$product_title=stripslashes($product->get_title());
				$product_title=$item->get_name();
				
				if( version_compare( WC_VERSION, '3.0', '<' ) ) {
					$item_meta = new WC_Order_Item_Meta($item['item_meta'] );
					$item_meta_var=$item_meta->display($flat=true,$return=true);
				} else {
					  // Get an instance of the WC_Product object (can be a product variation  too)

					  // Only for product variation
					if($product->is_type('variation')){
						
						$variation_attributes=$product->get_attributes();
						
						
						$meta_data=$item->get_formatted_meta_data( '' );
						
						if(is_array($meta_data) && !empty($meta_data)) {
							foreach($meta_data as $meta_id => $meta) {
								$attribute[] =  $meta->display_key .': '. $meta->value ;
							}
						} else {
						
						
						foreach($variation_attributes as $attribute_taxonomy => $term_slug){
							$attribute[] = wc_attribute_label($attribute_taxonomy,$product).': '.$term_slug;
						}
						
						}
						$separator=apply_filters('wc_dymo_product_list_variation_separator',', ');
						$item_meta_var=implode($separator,$attribute);
						
					}
					
				}

				$product_variation= stripslashes(trim(preg_replace('/\s+/', ' ', $item_meta_var)));

				$product_qty=stripslashes($item['qty']);
				$product_dimensions=$product_weight=$product_line_weight='';


				if ( ! $product->is_virtual() ) {
					$product_weight=$product->get_weight();

					if ( $product_weight > 0 ) {
						$product_line_weight=($product_weight * $item['qty']).' ' . esc_attr( get_option('woocommerce_weight_unit' ) );
						$product_weight=$product_weight . ' ' . esc_attr( get_option('woocommerce_weight_unit' ) );
					}
					if($product->get_dimensions()!="") {
						$product_dimensions=$product->get_dimensions();
					}
				}

				$layout_arr = array ('[SKU]' => $product_sku, '[PRODUCT]' => $product_title, '[VARIATION]' => $product_variation,'[QTY]'=>$product_qty,'[PRICE]'=>$product_price,'[WEIGHT]'=>$product_weight,'[WEIGHT_LINETOTAL]'=>$product_line_weight,'[DIMENSIONS]'=>$product_dimensions,'[VAR_SKU]'=>'');

				if(isset($item['variation_id']) && $item['variation_id']!="") {

					$product_variation_id = $item['variation_id'];

					$var=wc_get_product($item['variation_id']);

					$var_sku = $var->get_sku();
					
					
				}
				
				if($var_sku!="") {
					$layout_arr['[VAR_SKU]']=$var_sku;
				} else {
					$layout_arr['[VAR_SKU]']=$product_sku;	
				}

				$key = array_keys($layout_arr);
				$value = array_values($layout_arr);

				$product_line = str_replace($key, $value, $product_layout);
				if($product_variation=="") {
				$product_line=$this->delete_all_between('{VAR}','{/VAR}',$product_line,1);
				} else {
				$product_line=$this->delete_all_between('{VAR}','{/VAR}',$product_line,0);
				}
				$products.=$product_line;

				$products=str_replace('{ENTER}','|',$products);
			}
		}
		}
		return html_entity_decode( stripslashes($products), ENT_QUOTES, "utf-8" ).' ';
	}


	/*
	 * Get default WooCommerce address
	 */
	public function get_default_address($order,$label,$type='billing'){
		if($type=='shipping'){
			$address=$order->get_formatted_shipping_address();
		} else {
			$address=$order->get_formatted_billing_address();
		}
		return $this->sanitize_address(html_entity_decode(stripslashes($address),ENT_COMPAT,'UTF-8'));
	}

	/*
	 * Get custom customer address from order
	 */
	public function get_custom_address($order,$label,$object,$type='billing') {

		$objects=get_option( 'wc_dymo_option_extras' );
		$adres_layout=$objects[$label]['address'][$object];
		if($adres_layout!="") {

			$adres='""';
			if ($type == 'billing' || $order->get_formatted_shipping_address()=="") {
				$first_name=stripslashes($order->get_billing_first_name());
				$last_name=stripslashes($order->get_billing_last_name());
				$company=stripslashes($order->get_billing_company());
				$address1=stripslashes($order->get_billing_address_1());
				$address2=stripslashes($order->get_billing_address_2());
				$postcode=stripslashes($order->get_billing_postcode());
				$city=stripslashes($order->get_billing_city());

				$state_abbr=$order->get_billing_state();
				$country_abbr=$order->get_billing_country();

				// Support for Brazil checkout fields - added 2.5.3
				if(is_plugin_active('woocommerce-extra-checkout-fields-for-brazil/woocommerce-extra-checkout-fields-for-brazil.php')) {
					$number=$order->billing_number;
					$neighborhood=$order->billing_neighborhood;
				}

			} else {
				$first_name=stripslashes($order->get_shipping_first_name());
				$last_name=stripslashes($order->get_shipping_last_name());
				$company=stripslashes($order->get_shipping_company());
				$address1=stripslashes($order->get_shipping_address_1());
				$address2=stripslashes($order->get_shipping_address_2());
				$postcode=stripslashes($order->get_shipping_postcode());
				$city=stripslashes($order->get_shipping_city());
				$state_abbr=$order->get_shipping_state();
				$country_abbr=$order->get_shipping_country();

				// Support for Brazil checkout fields - added 2.5.3
				if(is_plugin_active('woocommerce-extra-checkout-fields-for-brazil/woocommerce-extra-checkout-fields-for-brazil.php')) {
					$number=$order->shipping_number;
					$neighborhood=$order->shipping_neighborhood;
				}

			}

			$state_country=$this->get_country_state($order,$label,$type);
			$state=$state_country['state'];
			$country=$state_country['country'];

			$layout_arr = array ('[COMPANY]' => $company, '[FIRSTNAME]' => $first_name, '[LASTNAME]' => $last_name,'[ADDRESS1]'=>$address1,'[ADDRESS2]'=>$address2,'[POSTCODE]'=>$postcode,'[CITY]'=>$city,'[STATE]'=>$state,'[STATE_ABBR]'=>$state_abbr,'[COUNTRY]'=>$country,'[COUNTRY_ABBR]'=>$country_abbr);

			// Support for Brazil checkout fields - added 2.5.3
			if(is_plugin_active('woocommerce-extra-checkout-fields-for-brazil/woocommerce-extra-checkout-fields-for-brazil.php')) {
				$layout_arr['[NUMBER]']=$number;
				$layout_arr['[NEIGHBORHOOD]']=$neighborhood;
			}

			$key = array_keys($layout_arr);
			$value = array_values($layout_arr);
			$adres_line = str_replace($key, $value, $adres_layout);
				if($company=="" || $company==" ") {
				$adres_line=$this->delete_all_between('{COMPANY}','{/COMPANY}',$adres_line,1);
			} else {
				$adres_line=$this->delete_all_between('{COMPANY}','{/COMPANY}',$adres_line,0);
			}
			if($address2=="" || $address2==" ") {
				$adres_line=$this->delete_all_between('{ADDR}','{/ADDR}',$adres_line,1);
			} else {
				$adres_line=$this->delete_all_between('{ADDR}','{/ADDR}',$adres_line,0);
			}
			$adres=nl2br($adres_line);
		}

		return $this->sanitize_address($adres);
	}

	/*
	 * Get full country and state
	 */
	public function get_country_state($order,$label,$type){
		global $woocommerce;
		$countries=new WC_Countries();

		if($type=='billing' || empty($order->get_shipping_country())) {
			if($order->billing_country=="") {
				$country=$woocommerce->countries->get_base_country();
			} else {
				$country=$order->get_billing_country();
			}

			$country=__($woocommerce->countries->countries[$country],'woocommerce').' ';
			$state   = ( $order->get_billing_country() && $order->get_billing_state() && isset( $countries->states[ $order->get_billing_country() ][ $order->get_billing_state() ] ) ) ? $countries->states[ $order->get_billing_country() ][ $order->get_billing_state() ] : $order->get_billing_state();

		} else {
			$country=__($woocommerce->countries->countries[$order->get_shipping_country()],'woocommerce').' ';
			$state   = ( $order->get_shipping_country() && $order->get_shipping_state() && isset( $countries->states[ $order->get_shipping_country() ][ $order->get_shipping_state() ] ) ) ? $countries->states[ $order->get_shipping_country() ][ $order->get_shipping_state() ] : $order->get_shipping_state();
		}

		return array('country'=>html_entity_decode($country,ENT_COMPAT,'UTF-8'),'state'=>html_entity_decode($state,ENT_COMPAT,'UTF-8'));
	}

	/*
	 * Get product SKU from order item
	 */
	public function get_sku($item) {
		global $woocommerce;
		if($item['variation_id']) {
			$product=wc_get_product($item['variation_id']);
			$sku=$product->get_sku();
			if($sku=="" ) {
				$product=wc_get_product($item['product_id']);
			}
		} else {
			$product=wc_get_product($item['product_id']);
		}
		$sku=$product->get_sku();
		return $sku;
	}

	/*
	 * Get product price from order item
	 */
	public function get_product_price($item) {
		global $woocommerce;
		
		if($item['variation_id']) {
			$product=wc_get_product($item['variation_id']);
		} else {
			$product=wc_get_product($item['product_id']);
		}
		
		$price=$product->get_price();
		return $this->dymo->format_price($price);
	}

	/*
	 * Get product short description from order item
	 */
	public function get_product_short_descr($item,$order) {
		
		//$_product     = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
		
		$product_id = $item->get_product_id();
		$product_variation_id = $item->get_variation_id();
		$_product = new WC_Product($product_id);
		
		$descr='';
		if($product_variation_id!="") {
			$_product_variation=new WC_Product_Variation($product_variation_id);
			$descr=$_product_variation->get_description();
		}
		
		if($descr=='') {
			$descr=$_product->get_short_description();
		}
		
		return html_entity_decode(strip_tags($descr));
	}

	/*
	 * Get product attributes from order item
	 */
	public function get_product_attr($item,$order,$new=0) {
		if($new==1) {
			$item_meta='';
			$formatted_meta_data=$item->get_formatted_meta_data(true,true);
			foreach ($formatted_meta_data as $meta_id => $meta ) {
				$default_output=$meta->display_key.': '.$meta->value.PHP_EOL;
				$item_meta.=strval(apply_filters('wpf_woocommerce_dymo_item_attr_fix',$default_output,$meta->value,$meta->display_key));
			}
			return html_entity_decode($item_meta);
		} else {

			if( version_compare( WC_VERSION, '3.0', '<' ) ) {
				$_product     = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
				$item_meta    = new WC_Order_Item_Meta( $item, $_product );
				return html_entity_decode($item_meta->display( true, true ));
			} else {

				$args=array(
					'before'=>'',
					'after'=>'',
					'separator'=>'|',
					'echo'=>false,
					'autop'=>false,
				);
				$item_meta= strip_tags(wc_display_item_meta($item,$args));

				return html_entity_decode($item_meta);
			}
		}
	}

	/*
	 * Get Order meta data for order
	 */
	function get_order_meta($orderID,$key,$label='',$object=''){
		$key=str_replace('order_meta_','',$key);

		$order_meta=get_post_meta($orderID,$key);
		if(isset($order_meta[0])) {
			$output=$order_meta[0];
			if(is_array($output)) {
				$object_extras=get_option( 'wc_dymo_option_extras' );
				$array_keys=$object_extras[$label]['serialized'][$object];
				$array_keys=explode(']',$array_keys);
				$array_keys=array_filter($array_keys);
	
				foreach ($array_keys as $key) {
					$key=str_replace('[','',$key);
					if($key=="") {
						$output=current($output);
					} else {
						$output=$output[$key];
					}
				}
			}
			return str_replace('ORDER_META_','',$output);
		} else {
			return '';
		}
	}

	/*
	 * Update order status after printing
	 */
	function update_order_status() {
		check_ajax_referer( 'dymo_nonce', 'security' );

		$labelID=$_GET['label'];

		$allLabels=$this->dymo->get_labels();
		$currentLabel=$allLabels[$labelID];
		if(!isset($currentLabel['type'])) {
			$labelType='order';
		} else {
			$labelType=$currentLabel['type'];
		}

		if($labelType=='order_item' || $labelType=='order') {

			$object_extras=get_option( 'wc_dymo_option_extras' );
			$output=array();
			$orderID=(int)$_GET['id'];
			$label=esc_attr($labelID);

			if(isset($object_extras[$label]['orderstatus']) && $object_extras[$label]['orderstatus']!="") {
				$new_status=strtolower($object_extras[$label]['orderstatus']);
				$new_status=$object_extras[$label]['orderstatus'];
				$order=wc_get_order($orderID);
				$order->update_status($new_status,'Status changed by WooCommerce DYMO Print.');
				echo 'changed';
			} else {
				echo 'no_change';
			}

		} else {
			echo 'product';
		}
		die();
	}

	/*
	 * Strip content between conditional template for product line
	 * Since 2.1.0
	 */
	private function delete_all_between($beginning, $end, $string,$option) {
		$beginningPos = strpos($string, $beginning);
		$endPos = strpos($string, $end);

		if (($beginningPos=="" && $beginningPos!="0") || !$endPos) {
			return $string;
		}

		// Check if has conditional, if not remove only conditional tags from string
		if($option==1) {
			$textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);
		} else {
			$textToDelete=array($beginning,$end);
		}
		return str_replace($textToDelete, '', $string);
	}

	/*
	 * Add bulk actions to action-list
	 */
	public function register_bulk_actions($bulk_actions) {

		$labels=$this->dymo->get_labels();
		$files=$this->dymo->get_label_files();
		$chosenFile=get_option( 'wc_dymo_labelfile' );
		$combos=get_option('wc_dymo_combo',true);
		
		if(empty($labels)) return false;

		foreach($labels as $label =>$args) {
			if(isset($chosenFile[$label]) && in_array($chosenFile[$label],$files) && (!isset($args['type']) || in_array($args['type'],array('order','order_item')))) {
				$bulk_actions['wc_dymo_order_'.$label] = sprintf(__( 'Print %s label', 'woocommerce-dymo' ), $args['name']);
			}
			if($args['type']=='combo' && isset($combos[$label]['labeltype']) && $combos[$label]['labeltype']=='order') {
				$bulk_actions['wc_dymo_combo_'.$label] = sprintf(__( 'Print %s labels', 'woocommerce-dymo' ), $args['name']);
			}
		}

		return $bulk_actions;
	}

	/*
	 * Handle bulk actions
	 */
	public function process_bulk_actions( $redirect_to, $doaction, $post_ids ) {
		if (strpos($doaction, 'wc_dymo_order_') !== false) {

			$count=count($post_ids);
			$printID=implode(',',$post_ids);
			$redirect_to = remove_query_arg( array('bulk_action','changed','ids'), $redirect_to );
			$redirect_to = add_query_arg(array('post_type' => 'shop_order','printID'=>$printID,'type'=>'order','label'=>str_replace('wc_dymo_order_','',$doaction),'bulk_printed'=>$count), $redirect_to);
			return $redirect_to;

		} elseif (strpos($doaction, 'wc_dymo_combo_') !== false) {
			$redirect_to = remove_query_arg( array('bulk_action','changed','ids'), $redirect_to );
			$count=count($post_ids);
			$printID=implode(',',$post_ids);

			$redirect_to = add_query_arg(array('post_type' => 'shop_order','printID'=>$printID,'type'=>'combo','label'=>str_replace('wc_dymo_combo_','',$doaction),'bulk_printed'=>$count), $redirect_to);
			return $redirect_to;

		}else {
			return $redirect_to;
		}

	}

	/*
	 * Display notification and init javascript printing
	 */
	public function bulk_actions_notice() {
		global $post_type;

		if ( !empty($_REQUEST['printID']) && ! empty( $_REQUEST['bulk_printed']) && $post_type=='shop_order' && !isset($_REQUEST['bulk_action'])) {


			?>
				<script>
				jQuery(document).ready(function($) {

					var printID='<?php echo esc_attr($_REQUEST['printID']);?>';
					var labelID='<?php echo esc_attr($_REQUEST['label']);?>';
					var type='<?php echo esc_attr($_REQUEST['type']);?>';

					if(type=='combo') {
						start_combo(printID,labelID,false,false);
					} else {
						frameworkInitShim(printID,labelID,false,false,false);
					}


				});
				</script>
			<?php

			$count = intval($_REQUEST['bulk_printed']);
			printf( '<div id="message" class="updated fade"><p>' .
			_n( 'Printing labels for %s order. Do not close this page during printing.',
				'Printing labels for %s orders. Do not close this page during printing. This might take a while.',$count,'woocommerce-dymo') . '</p></div>', $count );
		} 
	}

	/*
	 * Get all labels
	 */
	public function label_management($labels) {
		$labels=get_option('wc_dymo_type',$labels);
		return $labels;
	}

	/*
	 * Convert output to single array-element
	 */
	public function create_array_output($output,$object,$order,$labelID,$item) {
		return array($output);
	}

	/*
	 * Get combo data
	 */
	public function get_label_combo() {
		check_ajax_referer( 'dymo_nonce', 'security' );

		$labelID=esc_attr($_GET['labelID']);
		$combos=get_option('wc_dymo_combo',true);

		$return=$combo=array();

		foreach($combos[$labelID] as $name=>$key) {
			if($key!=0 && $key!="") {
				$combo[$key]=$name;
			}
		}

		ksort($combo);

		foreach($combo as $label) {
			$return[]=$label;
		}

		wp_send_json($return);
		wp_die();
	}
	
	/* 
	 * Build serialized array data for settings 
	 */
	public function build_query() {
		check_ajax_referer( 'wc-dymo-ajax', 'security' );
		$output='';
		
		if(isset($_GET['action']) && $_GET['action']=='wc_dymo_build_query' && isset($_GET['orderID']) && $_GET['orderID']>0 && isset($_GET['metakey']) && $_GET['metakey']!="") {
			$orderID=(int)$_GET['orderID'];
			$metakey=str_replace('order_meta_','',$_GET['metakey']);
			$metadata=get_post_meta($orderID,$metakey);
	
			if(is_array($metadata[0])) {
				$output=$metadata[0];
			} else {
				$output='nodata';
			}
			
			//Do not remove print_r -- not for DEBUG purposes
			print_r($output);
			exit();
		}
	
		return '';
		
	}
	
}