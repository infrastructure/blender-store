jQuery(document).ready(function($) {
	$('.dymo-link').click(function (e){
		e.preventDefault();

		if(!$(this).hasClass('dymo-disabled')) {
			var printID=$(this).data('printid');
			var labelID=$(this).data('request');
			var type=$(this).data('type');

			if(type=='combo') {
				start_combo(printID,labelID,false,type);
			} else {
				frameworkInitShim(printID,labelID,false,false,type);
			}

		}

	});
});

function WCDymostartupCode(printID,labelID,combo,qty,type) {

	var errorbox=jQuery('.dymo-errors');
	var okbox=jQuery('.dymo-success');
	var labeltime=parseInt(dymo_data.labeltime);
	var connectedPrinters=[];

	dymo.label.framework.getPrintersAsync().then(function(printers) {
		var printParams = {};

		if (printers.length != 0) {

			// Get variables from PHP
			var ajax_data = {
				'security': dymo_data.ajax_nonce,
				'action': 'wc_dymo_get_label_variables',
				'labelID': labelID,
			};

			jQuery.ajax({
				url:ajaxurl,
				data: ajax_data,
				dataType: 'json',
				success: function(response) {
					if(response.fail!="") {
						get_dymo_error(errorbox,response.fail,printID,'error');

					} else {

						var xml= dymo.label.framework.openLabelFile(response.labelfile);
						
						var label = dymo.label.framework.openLabelXml(xml);
						
						if(!label.isValidLabel()) {
							label = dymo.label.framework.openLabelXml(response.template);
						}
						
						if(!label.isValidLabel()) {
							get_dymo_error(errorbox,response.fail,printID,'error');
						}
						
						var labelObjects=label.getObjectNames();
						
						var printer=printers[0];
						var selectedPrinter=response.printer;
						var print_stock=response.print_stock;

						if(selectedPrinter=="") { selectedPrinter=printer.name;}

						var pr=0;
						for (var p = 0; p < printers.length; p++)
						{
							var printerName = printers[p].name;

							if(printers[p].isConnected) {
								if(printers[p].name==selectedPrinter) {
									printer=printers[p];
								} else {
									connectedPrinters[pr]=printers[p];
									pr++;
								}
							}

						}

						if(!printer.isConnected && connectedPrinters.length>0) {
							get_dymo_error(errorbox,dymo_data.default_printer + ' ' +connectedPrinters[0].name,printID,'stay');
							printer=connectedPrinters[0];
						}

						if (typeof printer.isTwinTurbo != "undefined") {
							if (printer.isTwinTurbo) {
								if(response.printerArgs=='left') {
									printParams.twinTurboRoll = dymo.label.framework.TwinTurboRoll.Left;
								} else if(response.printerArgs=='right') {
									printParams.twinTurboRoll = dymo.label.framework.TwinTurboRoll.Right;
								} else {
									printParams.twinTurboRoll = dymo.label.framework.TwinTurboRoll.Auto;
								}

							}

						}

						function handleErrors(response) {
							if (!response.ok) {
								throw Error(response.statusText);
							}
							return response;
						}

						if(printer.isConnected) {

							var ajax_data = {
								'security': dymo_data.ajax_nonce,
								'action': 'wc_dymo_get_label_data',
								'printID': printID,
								'print_type':type,
								'label':labelID,
							};

							jQuery.ajax({
								url:ajaxurl,
								data: ajax_data,
								dataType: 'json',
								success: function(response) {

									if(response[0]=='nofields') {
										get_dymo_error(errorbox,dymo_data.no_data,printID,'error');

									} else {

										var delay=0;

										var printCount=Object.keys(response).length;
										var c=1;
										jQuery.each(response,function(id,labeldata) {
   										  id=id.split("_").pop();
										  if(labeldata[0].error!='skipped') {

											var percent=0;

                                            var progress_bar=jQuery('.dymo-progress-'+id);
                                            progress(0, progress_bar);
                                            jQuery(progress_bar).animate({opacity:1},500);

											if(type!="product_bulk" && type!='variations') { print_stock=0; }

                                            if(qty>0 && print_stock==0) {
												var labelCount=qty;
											} else {
												var labelCount=labeldata.amount;
											}


											var dataLength=Object.keys(labeldata).length-1;

											percent=percent+(100/(dataLength*labelCount));

											jQuery.each(labeldata,function(label_key,label_value) {

												if(label_key=='amount') { return true;}

												setTimeout(function() {

													j=0;

													if(labelCount>0) {
														(function printLabels (i,delay) {
															setTimeout(function () {

																jQuery.each(label_value,function(key,val) {
																	if(val) {
																		val= val.replace(/\|/g, "\n");
																	} else {
																		val=' ';
																	}

																	if (labelObjects.indexOf(key) >= 0) {
																		label.setObjectText(key, val);
																	}

																});

																errorbox.hide();
																okbox.hide();

																try {
																	
																	printParams.jobTitle=dymo_data.job_title + dymo_data.post_ID;
																	
																	label.printAsync(printer.name, dymo.label.framework.createLabelWriterPrintParamsXml(printParams));
																	progress(percent, progress_bar);

																	wp.a11y.speak(dymo_data.label_printed + ' '+ dymo_data.post_ID + ' ' + id, 'polite' );
																} catch (ex) {
																	console.log(ex);
																	get_dymo_error(errorbox,dymo_data.general_error,id,'error');

																	get_dymo_error(errorbox,ex.message,id,'error');

																}

																percent=percent+(100/(dataLength*labelCount));
																j++;

																if (--i) {
																	printLabels(i,delay);
																}
															}, delay)
														})(labelCount,labeltime);

													} else {
														get_dymo_error(errorbox,dymo_data.skipped,id,'error');
													}

												//},delay*labelCount);
												},delay);

												delay +=labeltime;

											});


											// Remove progress bar 3 seconds after printing
											var delayProgress=delay+(labelCount*labeltime);
											delay=delayProgress-labeltime;

											setTimeout(function() {
												progress_bar.animate({opacity:0},100);
												jQuery('.dymo-button-'+labelID+'-'+id+',.post-'+id+' .column-name .dymo-spinner').removeClass('dymo-loading');
												jQuery('.post-'+id).addClass('dymo-print-finished');

												// Change order status after printing
												jQuery.ajax({
													url: ajaxurl,
													data: ({action : 'wc_dymo_order_status', security:dymo_data.ajax_nonce, id:id,label:labelID}),
													success: function(data) {
														if(data!="") {

															if(combo[1] && c==printCount) {
																combo.shift();
																frameworkInitShim(printID,combo[0],combo,qty,type);
															} else if(c==printCount) {
																get_dymo_error(okbox,dymo_data.finished,printID,'success');
																jQuery('.wc-action-button-dymo').removeClass('dymo-disabled');

																if(data=='changed') {
																	wp.a11y.speak(dymo_data.status_changed + ' '+ dymo_data.post_ID + ' ' + id, 'polite' );
																	get_dymo_error(okbox,dymo_data.status_changed + ' '+ dymo_data.reload_page,id,'stay');
																}

																var url=removeURLParameter(window.location.href, 'label');

																if( url.indexOf('bulk_printed') >= 0){

																	wp.a11y.speak(dymo_data.wait_redirect, 'polite' );
																	get_dymo_error(okbox,dymo_data.wait_redirect,id,'stay');

																	setTimeout(function() {

																		url=removeURLParameter(url, 'printID');
																		url=removeURLParameter(url, 'type');
																		url=removeURLParameter(url, 'bulk_printed');
																		window.location.replace(url);
																	},labeltime);
																}
															}

														}
														c++;
													}
												});


											},delayProgress);

										  } else {
											  //hier spinner';

											  jQuery('.post-'+id+' .column-name .dymo-spinner').removeClass('dymo-loading');
											  jQuery('.post-'+id).addClass('dymo-print-error');

											  get_dymo_error(errorbox,labeldata[0].reason +' '+dymo_data.post_ID+ id,printID,'stay');
											  c++;
										  }

										});
									}
								},error: function(data) {
									get_dymo_error(errorbox,dymo_data.general_error,printID,'error');
								}
							});
						} else {
							get_dymo_error(errorbox,dymo_data.no_printers_connected,printID,'error');
						}

					}

				},error: function(data) {
					get_dymo_error(errorbox,dymo_data.general_error,printID,'error');
				}
			});


		} else {
			get_dymo_error(errorbox,dymo_data.no_printers_installed,printID,'error');
		}
	},function (errorMessage) {
			get_dymo_error(errorbox,'Framework Error: '+errorMessage,printID,'error');
			console.log('Framework error: '+errorMessage);
	});
}

function get_dymo_error($element,message,printID,type) {
	if((jQuery('body').hasClass('edit-php') || type=='success') && type!='stay') {
		$element.show().html('<p>'+message+'</p>');
	} else {
		$element.show().append('<p>'+message+'</p>');
	}

	if(type=='stay') {
		$element.show().addClass('dymo-show-always');
	}

	var setClass=String(printID);

	if(type=='error') {
		if (setClass.indexOf(',') > -1) {
			setClass=setClass.split(",");

			jQuery.each(setClass,function(key,id) {
				jQuery('.dymo-progress-'+id).addClass('dymo-progress-error');
			});
		} else {
			jQuery('.dymo-progress-'+setClass).addClass('dymo-progress-error');
		}
	}

	wp.a11y.speak( message, 'polite' );
}

function frameworkInitShim(printID,labelID,combo,qty,type) {

		var errorbox=jQuery('.dymo-errors');
		var okbox=jQuery('.dymo-success');

		if(dymo_data.debug==1) {
			dymo.label.framework.trace = 1;
			window.addEventListener('error', function (evt) {
				console.log(evt.message);
				evt.preventDefault();
			});
		}

		try {

			var setClass=String(printID);

			if (setClass.indexOf(',') > -1) {
				setClass=setClass.split(",");

				jQuery.each(setClass,function(key,id) {
					jQuery('.dymo-button-'+labelID+'-'+id+',.post-'+id+' .column-name .dymo-spinner').addClass('dymo-loading');
				});
			} else {
				jQuery('.dymo-button-'+labelID+'-'+setClass+',.post-'+setClass+' .column-name .dymo-spinner').addClass('dymo-loading');
			}
			dymo.label.framework.init(WCDymostartupCode(printID,labelID,combo,qty,type));

		} catch (ex) {
			console.log(ex);
		}
}

function start_combo(printID,labelID,qty,type) {
	var errorbox=jQuery('.dymo-errors');
	var ajax_data = {
		'security': dymo_data.ajax_nonce,
		'action': 'wc_dymo_get_label_combo',
		'labelID': labelID,
	};

	jQuery.ajax({
		url:ajaxurl,
		data: ajax_data,
		dataType: 'json',
		success: function(response) {
			frameworkInitShim(printID,response[0],response,qty,type);

		},error: function(data) {
			get_dymo_error(errorbox,dymo_data.combo_error,printID,'error');
		}
	});
}

function progress(percent, $element) {

	var progressBarWidth = percent * $element.width() / 100;
	var speed=1000;

    if(percent==0) {
        $element.css('display','block');
    }

	percent=percent.toFixed(0);
	$element.css('opacity',1);

	if(percent==0) { speed=0;}

	$element.find('span').animate({ width: progressBarWidth }, speed).html(percent + "%&nbsp;");
	$element.removeClass('dymo-progress-error');

	jQuery('.wc-action-button-dymo').addClass('dymo-disabled');

    var labeltime=parseInt(dymo_data.labeltime);

    if(percent==100) {
        setTimeout(function() {
            $element.css('display','none');
        },labeltime)

    }

}

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts= url.split('?');
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(parameter)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url= urlparts[0]+'?'+pars.join('&');
        return url;
    } else {
        return url;
    }
}
