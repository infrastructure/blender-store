jQuery(document).ready(function() {

    // Load all printers for printers tab and debug tab
    function loadPrinters()
    {

	
		// Load all printers for printers tab
        jQuery('.dymo-printersSelect').each(function() {
			var printersSelect=jQuery(this);
        
			var show_error=1;		
			var closestDiv=jQuery(this).closest('.postbox .inside');		
			var currentPrinter=jQuery(closestDiv).find('.dymo-printerField').val();
			
			var currentBox=jQuery(this);
			
			dymo.label.framework.getPrintersAsync().then(function(printers) {
				
				if (printers.length == 0) {
					jQuery('.nav-tab-wrapper a').removeClass('nav-tab-active');			
					jQuery('.tab').removeClass('active');			
					jQuery('#check').addClass('active');			
					jQuery('#check-tab').addClass('nav-tab-active');			
					jQuery(closestDiv).find('.dymo-hide').show();			
					jQuery(closestDiv).find('.dymo-printerField').addClass('dymo-icon-notfound');            
					add_log('Error: No supported printers found','error');
					return;        
				}
		
				for (var i = 0; i < printers.length; i++)
				{
					var printerName = printers[i].name;
					
					if(printers[i].isConnected) {
					
						currentBox.append('<li><a href="#" class="dymo-addPrinter" data-printer="' + printerName+ '">'+printerName+'</a></li>');
					
						if (typeof printers[i].isTwinTurbo != "undefined")
						{
							if (printers[i].isTwinTurbo) { 
								var TwinTurbo=jQuery(closestDiv).find('.dymo-turbo');
								jQuery(TwinTurbo).show();
							} 
						}
						
						if(currentPrinter==printerName) {
							show_error=0;
						}
					}			
				}
				
				if(show_error==1) {
					jQuery(closestDiv).find('.dymo-hide').show();
					jQuery(closestDiv).find('.dymo-printerField').addClass('dymo-icon-notfound');
				} else {
					
					jQuery(closestDiv).find('.dymo-printerField').addClass('dymo-icon-found');
				}
			});
			
			AddPrinter();
		});
		
		
		//Create table for debug information
		var table = document.createElement("table");
		table.className='dymocheck';
	
		// Create the header row of <th> elements in a <tr> in a <thead>
		var thead = document.createElement("thead");
		var header = document.createElement("tr");
	
		var createTableHeader = function(name)
		{
			var cell = document.createElement("th");
			cell.appendChild(document.createTextNode(name));
			header.appendChild(cell);
		};
		
		createTableHeader(objects.printerType);
		createTableHeader(objects.printername);
		createTableHeader(objects.modelname);
		createTableHeader(objects.local);
		createTableHeader(objects.connected);
		createTableHeader("TwinTurbo");
	
		// Put the header into the table
		thead.appendChild(header);
		table.appendChild(thead);
	
		// The remaining rows of the table go in a <tbody>
		var tbody = document.createElement("tbody");
		table.appendChild(tbody);
		
		var createPrinterRow = function(printer, row, propertyName)	{
			var cell = document.createElement("td");
	
			// Put the text data into the HTML cell
			if (typeof printer[propertyName] != "undefined")
				cell.appendChild(document.createTextNode(printer[propertyName]));
			else
				cell.appendChild(document.createTextNode("n/a"));
				// Add the cell to the row
				row.appendChild(cell);
		};
		
		
		
		dymo.label.framework.getPrintersAsync().then(function(printers) {
			if (printers.length != 0) {
				for (var i = 0; i < printers.length; i++)
				{
					var printerName = printers[i].name;
					if(printers[i].isConnected) {
						jQuery('#dymo-printersSelect').append('<option value="' + printerName+ '">'+printerName+'</option>'); 
						
						var printer = printers[i];
						// Create an HTML element to display the data in the row
						var row = document.createElement("tr");
		
						createPrinterRow(printer, row, "printerType");
						createPrinterRow(printer, row, "name");
						createPrinterRow(printer, row, "modelName");
						createPrinterRow(printer, row, "isLocal");
						createPrinterRow(printer, row, "isConnected");
						createPrinterRow(printer, row, "isTwinTurbo");
		
						// And add the row to the tbody of the table
						tbody.appendChild(row);

						add_log('-','ok');
						if(printer.isTwinTurbo) {
							add_log('Printer type: '+printer.printerType+' TwinTurbo','ok');
						}
						add_log('Success: Found connected printer '+printerName,'ok');
						
						
						
					} else {
						add_log('Error: '+printerName+ ' installed, but not connected. Try to reconnect this printer.','error');
					}
				}
			} else {
				add_log('Error: No supported printers found.','error');
			}
		});
		return table;
		
    }
	
	// Add printer to printers input-field on click
	function AddPrinter() {
		jQuery('.dymo-printersSelect').on('click','.dymo-addPrinter',function(e) {
			e.preventDefault();
			var printer=jQuery(this).data('printer');
			var parent=jQuery(this).closest('tr');
			var field=jQuery(parent).find('.dymo-printerField');
			jQuery(field).val(printer);
			jQuery(parent).find('.dymo-printersSelect a').removeClass('choose');
			jQuery(this).addClass('choose');
		});
	}
	
	// updates printers information and insert it into the document 
    function updatePrintersTable()
    {
		jQuery('.dymo-printersSelect li').remove();
		jQuery('#dymo-printersSelect option').remove(); 
        var container = document.getElementById("printersInfoContainer");

        // remove previous table
        while (container.firstChild)
            container.removeChild(container.firstChild);

        container.appendChild(loadPrinters());
    }
	
	// Print printers information on label
	// Possible lable layouts
    var dieCutLabelLayout = '<?xml version="1.0" encoding="utf-8"?>\<DieCutLabel Version="8.0" Units="twips">\<PaperOrientation>Landscape</PaperOrientation>\<Id>Address</Id>\<PaperName>30252 Address</PaperName>\<DrawCommands/>\<ObjectInfo>\    <TextObject>\        <Name>Text</Name>\        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />\        <LinkedObjectName></LinkedObjectName>\        <Rotation>Rotation0</Rotation>\        <IsMirrored>False</IsMirrored>\        <IsVariable>True</IsVariable>\        <HorizontalAlignment>Left</HorizontalAlignment>\        <VerticalAlignment>Middle</VerticalAlignment>\        <TextFitMode>AlwaysFit</TextFitMode>\        <UseFullFontHeight>True</UseFullFontHeight>\        <Verticalized>False</Verticalized>\        <StyledText/>\    </TextObject>\    <Bounds X="332" Y="150" Width="4455" Height="1260" />\</ObjectInfo>\
    </DieCutLabel>';

    var continuousLabelLayout = '<?xml version="1.0" encoding="utf-8"?>\<ContinuousLabel Version="8.0" Units="twips">\    <PaperOrientation>Landscape</PaperOrientation>\<Id>Tape19mm</Id>\    <PaperName>19mm</PaperName>\    <LengthMode>Auto</LengthMode>\    <LabelLength>0</LabelLength>\    <RootCell>\    <TextObject>\        <Name>Text</Name>\        <ForeColor Alpha="255" Red="0" Green="0" Blue="0" />\        <BackColor Alpha="0" Red="255" Green="255" Blue="255" />\        <LinkedObjectName></LinkedObjectName>\        <Rotation>Rotation0</Rotation>\        <IsMirrored>False</IsMirrored>\        <IsVariable>True</IsVariable>\        <HorizontalAlignment>Left</HorizontalAlignment>\        <VerticalAlignment>Middle</VerticalAlignment>\        <TextFitMode>AlwaysFit</TextFitMode>\        <UseFullFontHeight>False</UseFullFontHeight>\        <Verticalized>False</Verticalized>\        <StyledText/>\    </TextObject>\    <ObjectMargin Left="0" Top="0" Right="0" Bottom="0" />\<Length>0</Length>\<LengthMode>Auto</LengthMode>\<BorderWidth>0</BorderWidth>\<BorderStyle>Solid</BorderStyle>\<BorderColor Alpha="255" Red="0" Green="0" Blue="0" />\
    </RootCell>\</ContinuousLabel>';

    // prints printers information
    function print(printerName)
    {
        dymo.label.framework.getPrintersAsync().then(function(printers) {
        var printer = printers[printerName];
        if (!printer)
        {
            alert("Printer '" + printerName + "' not found");
            return;
        }

        // select label layout/template based on printer type
        var labelXml;
        if (printer.printerType == "LabelWriterPrinter")
            labelXml = dieCutLabelLayout;
        else if (printer.printerType == "TapePrinter")
            labelXml = continuousLabelLayout;
        else
        {
            alert("Unsupported printer type");
            throw "Unsupported printer type";
        }

        // create label set to print printers' data
        var labelSetBuilder = new dymo.label.framework.LabelSetBuilder();
        for (var i = 0; i < printers.length; i++)
        {
            var printer = printers[i];

            // process each printer info as a separate label
            var record = labelSetBuilder.addRecord();

            // compose text data
            // use framework's text markup feature to set text formatting
            // because text markup is xml you can use any xml tools to compose it
            // here we will use simple text manipulations t oavoid cross-browser compatibility.
            var info = "<font family='Courier New' size='14'>"; // default font
            info = info + objects.printer+" <b>" + printer.name + "\n</b>"; 
            info = info + objects.printerType + ': '+ printer.printerType;
            info = info + "\n<font size='10'>"+objects.is_local+": " + printer.isLocal;
            info = info + "\n"+objects.is_online+" " + printer.isConnected + "</font>";

            if (typeof printer.isTwinTurbo != "undefined")
            {
                if (printer.isTwinTurbo) {
                    info = info + "<i><u><br/>"+objects.twinturbo+"</u></i>";
				}else {
                    info = info + "<font size='6'><br/>"+objects.no_twinturbo+"</font>";
				}
            }

            if (typeof printer.isAutoCutSupported != "undefined")
            {
                if (printer.isAutoCutSupported)
                    info = info + "<i><u><br/>"+objects.autocut+"</u></i>";
                else
                    info = info + "<font size='6'><br/>"+objects.no_autocut+"</font>";
            }

            info = info + "</font>";

            // when printing put info into object with name "Text"
            record.setTextMarkup("Text", info);
        }

        // finally print label with default printing parameters
        dymo.label.framework.printLabelAsync(printerName, "", labelXml, labelSetBuilder);
		add_log('Success: Print printer information on '+printerName,'ok');
		});
    }
	
	// Debug log
	function add_log(message,type) {
		var ul = jQuery('#dymo-debug-log-output');
		jQuery(ul).prepend('<li class="'+type+'">'+message+'</li>');
	}
	
	// Clear debug logs
	function clear_log() {
		jQuery('#dymo-debug-log-output li').remove();
	}
	
	// Load label previews
	function dymoInitPreview(label) {
	  if(!jQuery('body').hasClass('dymo-framework-error')) {
		try
		{
			var printerName='';
			var labelUri=objects.labelArray;
			var json = JSON.parse(labelUri);
			
			labelUri=json[label];
			
			var elementExists = document.getElementById("dymo_preview_"+label);
				
			if(!elementExists && json[label]) {

				var image = document.createElement('img');
				  dymo.label.framework.openLabelFileAsync(labelUri).then(function(labelXML) {
					var labelXml = labelXML.getLabelXml();
					  dymo.label.framework.renderLabelAsync(labelXml, "", printerName).then(function(pngData) {
						image.src = "data:image/png;base64," + pngData;
						var element = document.getElementById(label+"_preview");
						element.appendChild(image);
						image.setAttribute("id", "dymo_preview_"+label);
					  });
					add_log('Success: Label preview created for label: "' + label + '"','ok');
				  }),function(err) { 
					add_log('Error: '+ err,'error');
				  } 
				  
			}
			
		}
        catch(e)
        {
            console.log('settings: '+e.message || e);
			add_log('Error: '+ e.message || e,'error');

			if(e.message==objects.noFramework) {
				jQuery('.dymo-preview-row').hide();
				jQuery('body').addClass('dymo-framework-error');
			}
			setTimeout(function() {
			var warningExists = document.getElementById('dymo_preview_warning'+label);
			var elementExists = document.getElementById('dymo_preview_'+label);
			var preview = document.getElementById(label+"_preview");
			if(e.message=='dymo.label.framework.openLabelFileAsync(...).then(...).catch is not a function' && !elementExists && !warningExists && preview) {
					var warning=document.createElement('div');
					warning.setAttribute('id','dymo_preview_warning'+label);
					warning.innerHTML = '<em>'+objects.noPreview+' <a href="https://wpfortune.com/documentation/plugins/woocommerce-dymo-print/debug-dymo-framework#nolabelpreview" target="_blank">'+ objects.seeDebugInfo + '</a></em>';
					preview.parentNode.appendChild(warning);
					document.getElementById(label+"_preview").style.display = 'none';
				}
			},6000);
        }
		
	  }
	}

    // Init all settings scripts
    function InitCheck()
    {	  
	
		if(!jQuery('body').hasClass('dymo-framework-error')) {
			var printButton = document.getElementById('printButton');
			var updateTableButton = document.getElementById('updateTableButton');
			var printersSelect = document.getElementById('dymo-printersSelect');
			var clearLogs = document.getElementById('debugClear');
			var active_tab = window.location.hash.replace('#top#','');
			if ( active_tab == '' || active_tab=='printers' || active_tab=='check')
				active_tab = 'shipping';
			
			
			// load printers list on startup		
			updateTableButton.onclick = updatePrintersTable;
			updatePrintersTable();		
			
			// Print printers information on click
			printButton.onclick = function(){
				print(printersSelect.value);
			}
			
			// Print printers information on click
			clearLogs.onclick = function(){
				clear_log();
			}
			
			try
            {
                var result = dymo.label.framework.checkEnvironment();
				
                add_log(objects.isBrowserSupported + ' '+ result.isBrowserSupported,'ok');
                add_log(objects.isFrameworkInstalled + ' '+ result.isFrameworkInstalled,'ok');
				if(dymo.label.framework.init){
					add_log(objects.isWebServicePresent + ' '+ result.isWebServicePresent,'ok');
				}
				if(result.errorDetails!="")
					add_log(objects.errorDetails + ' '+ result.errorDetails,'error');
				
				add_log(objects.pluginVersion,'ok');				
				add_log(objects.frameworkVersion + ' '+dymo.label.framework.VERSION,'ok');
				add_log(objects.dymoconnect,'ok');
				if(objects.debugmode==1) {
					add_log(objects.debugmodeTxt);
				} 
				add_log(objects.environmentCheck,'bold');
				
				if(result.errorDetails=="" && dymo.label.framework.init) {
					dymoInitPreview(active_tab);
				}
            }
            catch(e)
            {
                add_log(e.message || e);
            }
			
			
			
			jQuery('.dymo-label-tab').click(function() {
				var tab=jQuery(this).attr('id');
				tab=tab.replace('-tab','');
				dymoInitPreview(tab);
			});
			
			
			
		} 
    };
	
	// Init Shim needed for DYMO Javascript Framework
	function frameworkInitShim() {
		window.addEventListener('error', function (evt) {
    
		if(evt.message=='Uncaught Error: DYMO Label Framework Plugin is not installed') {
			console.log(evt.message);
			add_log(evt.message,'error');
			add_log(objects.noFramework,'error');
			jQuery('.nav-tab-wrapper a').removeClass('nav-tab-active');			
			jQuery('.tab').removeClass('active');			
			jQuery('#check').addClass('active');			
			jQuery('#check-tab').addClass('nav-tab-active');			
			jQuery('.hideError').remove();
					
		}
	
		evt.preventDefault();
		});
		
		try {
			dymo.label.framework.init(InitCheck); 
		} catch (ex) {
			console.log(ex);
			
			if(ex=='Error: DYMO Label Framework service discovery is in progress.') {
				ex = objects.ServiceDiscovery
			}
			add_log(ex,'error');
			
			
		}
	}

	setTimeout(function() {frameworkInitShim();},200);
   
});


jQuery(document).ready(function($){

	var $color_inputs = $('input.popup-colorpicker');

	$color_inputs.each(function(){
		
		var $input = $(this);
		var $pickerId = "#" + $(this).attr('id') + "picker";
		try {	
			$($pickerId).hide();
			$($pickerId).farbtastic($input);
			$($input).click(function(event){event.stopPropagation(); 
			$($pickerId).slideToggle()});
			$($pickerId).click(function(event){event.stopPropagation();});
		} catch(err) {
			$('<span>Error: Cannot load colorpicker</span>').insertAfter($input);
		}
	});
	
	$(window).click(function() {
		$('.color-picker').slideUp();
	});

	var active_tab = window.location.hash.replace('#top#','');

	if ( active_tab == '' )

		active_tab = 'shipping';

		jQuery('#'+active_tab).addClass('active');
		jQuery('#'+active_tab+'-tab').addClass('nav-tab-active');
		jQuery('.nav-tab-wrapper a').click(function() {

		jQuery('.nav-tab-wrapper a').removeClass('nav-tab-active');

		jQuery('.tab').removeClass('active');

		var id = jQuery(this).attr('id').replace('-tab','');

		jQuery('#'+id).addClass('active');

		jQuery(this).addClass('nav-tab-active');

	});

	// New for 3.0
	jQuery('.postbox').on('change','.dymo-object-data-select',function() {
		
		jQuery(this).parent().find('.dymo-hide').hide();
		var value=jQuery(this).val();
		
		var array=jQuery(this).find('option:selected').data('array');
		if(value==2) {
			jQuery(this).parent().find('.dymo-product-list').show();
		}else if (value==6 || value==12) {
			jQuery(this).parent().find('.dymo-custom-address').show();
		}else if (value==18) {
			jQuery(this).parent().find('.dymo-customer-note').show();
		}else if (value=='product_short_descr') {
			jQuery(this).parent().find('.dymo-excerpt').show();
		}else if(value=='order_item_short_descr') {
			jQuery(this).parent().find('.dymo-product-descr').show();
		}
		if(array!=undefined) {
			jQuery(this).parent().find('.dymo-advanced-options').show();	
		}
	});
	
	jQuery('.dymo-object-data-select').each(function () {
		var value=jQuery(this).val();
		var array=jQuery(this).find('option:selected').data('array');
		if(value==2) {
			jQuery(this).parent().find('.dymo-product-list').show();
		}
		if(value==6 || value==12) {
			jQuery(this).parent().find('.dymo-custom-address').show();	
		}
		if(value==18) {
			jQuery(this).parent().find('.dymo-customer-note').show();	
		}
		if(value=='product_short_descr') {
			jQuery(this).parent().find('.dymo-excerpt').show();	
		}
		if(value=='order_item_short_descr') {
			jQuery(this).parent().find('.dymo-product-descr').show();
		}
		if(array!=undefined) {
			jQuery(this).parent().find('.dymo-advanced-options').show();	
		}
	});
	
	jQuery('.postbox').on('change','.dymo-select-label',function() {
		var value=jQuery(this).val();
		var preview=jQuery(this).closest('table').find('.dymo-preview-row td');
		jQuery(preview).find('img').fadeOut(200);
		jQuery(preview).html('<em>'+objects.savePreview+'</em>');
	});
	
	jQuery('.dymo-serialized-build').click(function(e) {
		e.preventDefault();
		
		var parentRow=jQuery(this).closest('.dymo-object-row');
		var orderID=jQuery(parentRow).find('.dymo-orderID').val();
		var metakey=jQuery(parentRow).find('.dymo-object-data-select').val();
		
		//var ajaxurl=objects.adminAjax;
			jQuery.ajax({
				url: ajaxurl,
				data: ({action : 'wc_dymo_build_query', security:objects.security,orderID:orderID,metakey:metakey}),
				label: 'GET',
				
				//dataType: 'json',
				success: function(data) {
					if(data=="" || data.length==0) {
						output='<span class="dymo-error">'+objects.noMetaData+'</span>';
					} else if(data=='nodata') {
						output='<span class="dymo-error">'+objects.noSerializedData+'</span>';
					} else {
						output='<pre>'+data.replace(/\[+(.*?)\]+/g, '<span class="dymo-serialized-element">[$1]</span>')+'</pre>';
					}
					jQuery(parentRow).find('.dymo-serialized-query').html(output);
				},
				error: function(data) {
					jQuery(parentRow).find('.dymo-serialized-query').html('<span class="dymo-error">'+objects.noMetaData+'</span>');
				}
		});
	});
	jQuery('.dymo-advanced-options').click(function(e) {
		e.preventDefault();
		jQuery(this).next('.dymo-serialized').toggle();
	});	

	jQuery('.dymo-object-row').on('click','.dymo-serialized-element',function() {
		var element=jQuery(this).text();
		var inputBox=jQuery(this).closest('td').find('input');
		jQuery(inputBox).val(jQuery(inputBox).val() + element);
	});
	
	jQuery('.tab form').on('submit', function(){
		if (jQuery('.dymo-serialized input:focus').length){return false;}
	});
});