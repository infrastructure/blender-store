<?php
/**
 * Blender ID Webhook API end-point.
 *
 * The URL of this page can be given to Blender ID to push user updates to us.
 */

if (!isset($_SERVER['REQUEST_METHOD']) || $_SERVER['REQUEST_METHOD'] != 'POST') {
    http_response_code(405);
    print('Unsupported HTTP request method');
    return;
}

if (!isset($_SERVER['CONTENT_TYPE']) || $_SERVER['CONTENT_TYPE'] != 'application/json') {
    http_response_code(422);
    print('Unsupported content type');
    return;
}

// 100 kB will be plenty.
if (!isset($_SERVER['CONTENT_LENGTH']) || $_SERVER['CONTENT_LENGTH'] > 102400) {
    http_response_code(422);
    print('Content-Length too large');
    return;
}

if (!isset($_SERVER['CONTENT_LENGTH']) || empty($_SERVER['CONTENT_LENGTH'])) {
    http_response_code(422);
    print('Content-Length too large');
    return;
}

/** Set up WordPress environment because we need the config */
$topdir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
if (!defined('ABSPATH')) {
    require_once("$topdir/wp-load.php");
}

$body = file_get_contents('php://input');
$mac = hash_hmac('sha256', $body, BLENDERID_WEBHOOK_USER_CHANGED_SECRET);
if (!isset($_SERVER['HTTP_X_WEBHOOK_HMAC']) || $_SERVER['HTTP_X_WEBHOOK_HMAC'] != $mac) {
    $got = isset($_SERVER['HTTP_X_WEBHOOK_HMAC']) ? $_SERVER['HTTP_X_WEBHOOK_HMAC'] : '-none-';
    error_log("Bad HMAC from Blender ID webhook; got $got, expected $mac");
    print('Bad HMAC');
    return;
}

$payload = json_decode($body);
if ($payload == null) {
    http_response_code(400);
    print('Unable to parse JSON');
    return;
}

error_log("INFO: received new user info for $payload->old_email from Blender ID");

header("Content-Type: text/plain");

// We only handle changes in email address and name, and ignore the roles.
$user = get_user_by('email', $payload->old_email);
if ($user === false) {
    print("User $payload->old_email not found, ignoring");
    return;
}

$first_name = '';
$last_name = '';
_bid_split_full_name($payload->full_name, $first_name, $last_name);

$user_id = wp_update_user(array(
    'ID' => $user->ID,
    'user_login' => $payload->email,
    'user_email' => $payload->email,
    'display_name' => $payload->full_name,
    'first_name' => $first_name,
    'last_name' => $last_name,
));
if (is_wp_error($user_id)) {
    print("Error updating user: " . $user_id->get_error_messages());
    return;
}

// Update the user_login with the email (in case it contains chars that are
// stripped by wordpress, like +)
global $wpdb;
$wpdb->update($wpdb->users, array('user_login' => $payload->email), array('ID' => (int) $user_id));

update_user_meta($user_id, 'billing_first_name', $first_name);
update_user_meta($user_id, 'billing_last_name', $last_name);
update_user_meta($user_id, 'billing_email', $payload->email);
update_user_meta($user_id, 'blender_id', $payload->id);
update_user_meta($user_id, 'nickname', $payload->email);

if ($payload->date_deletion_requested) {
    // Just store as metadata, so that a cron job can handle the actual deletion
    // after some grace period.
    update_user_meta($user_id, 'blender_id_date_deletion_requested', $payload->date_deletion_requested);
} else {
    // If for some reason the user is no longer marked as 'deletion requested',
    // remove the metadata so that the local user is kept.
    delete_user_meta($user_id, 'blender_id_date_deletion_requested');
}

print('ok');
