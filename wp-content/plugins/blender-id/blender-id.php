<?php
/*
Plugin Name: Blender ID Integration
Description: Creation of users and pushing subscription status to Blender ID.
Author: Sybren A. Stüvel
Author URI: https://blender.studio/
License: GPL2
Version: 1.0
WC requires at least: 2.6.14
WC tested up to: 3.2
*/

const BID_HTTP_TIMEOUT_SEC = 5;

require_once('subscriptions.php');
require_once('blender-id-queue.php');
require_once('user-creation.php');
require_once('authentication.php');
require_once('admin-tweaks.php');


// Activation & deactivation hooks need to be in the plugin top level file (i.e. this one).
register_activation_hook(__FILE__, 'bidq_install');
function bidq_install()
{
    // Add custom capabilities to admin role.
    $role = get_role('administrator');

    $role->add_cap('read_bid_req');
    $role->add_cap('read_private_bid_reqs');
    $role->add_cap('edit_bid_req');
    $role->add_cap('edit_bid_reqs');
    $role->add_cap('edit_others_bid_reqs');
    $role->add_cap('edit_private_bid_reqs');
    $role->add_cap('edit_published_bid_reqs');
    $role->add_cap('delete_bid_req');
    $role->add_cap('delete_bid_reqs');
    $role->add_cap('delete_others_bid_reqs');
    $role->add_cap('delete_published_bid_reqs');
    $role->add_cap('delete_private_bid_reqs');
    $role->add_cap('publish_bid_reqs');
}

register_deactivation_hook(__FILE__, 'bidq_deactivation');
function bidq_deactivation()
{
    // Our post type will be automatically removed, so no need to unregister it
    wp_clear_scheduled_hook('flush_bid_queue_hook');

    // Clear the permalinks to remove our post type's rules
    flush_rewrite_rules();
}


// Prevent "your email address/password changed" mails from being sent.
// If we do this at all, it should be done at the Blender ID side, and
// not by the Store.
add_filter('send_password_change_email', 'bid_says_no', 10, 0);
add_filter('send_email_change_email', 'bid_says_no', 10, 0);
function bid_says_no()
{
    return false;
}


// Register our CRON job
if (!wp_next_scheduled('flush_bid_queue_hook')) {
    wp_schedule_event(time(), 'bid_queue_schedule', 'flush_bid_queue_hook');
}


// Adds authentication headers and performs a HTTP POST request.
function _bid_authed_post($url, $req)
{
    $req['method'] = 'POST';
    $req['timeout'] = BID_HTTP_TIMEOUT_SEC;

    $authed_req = _bid_add_auth_header($req);
    return wp_remote_post($url, $authed_req);
}

// Adds authentication headers and performs a HTTP GET request.
function _bid_authed_get($url, $req)
{
    $req['method'] = 'GET';
    $req['timeout'] = BID_HTTP_TIMEOUT_SEC;

    $authed_req = _bid_add_auth_header($req);
    return wp_remote_get($url, $authed_req);
}

function _bid_add_auth_header($req)
{
    $authed_req = array_merge($req, array(
        'headers' => array(
            'User-Agent' => 'Store/Blender ID plugin',
            'Authorization' => 'Bearer ' . BLENDERID_AUTH_TOKEN,
        )
    ));
    return $authed_req;
}

function _bid_split_full_name($full_name, &$first_name, &$last_name){
    $name_parts = explode(' ', $full_name);
    $mid = ceil(count($name_parts) / 2);
    $first_name = implode(' ', array_slice($name_parts, 0, $mid));
    $last_name = implode(' ', array_slice($name_parts, $mid));
}
