<?php


/* WooCommerce tries to guess a username from the email address when the email
 * address is used to log in. Since we don't even support logging in with a
 * user name, this is disallowed using this filter. */
add_filter('woocommerce_get_username_from_email', 'bid_get_username_from_email', 30, 0);
function bid_get_username_from_email()
{
    return false;
}

add_action('wp_authenticate', 'bid_auth_check_login', 1, 2);
function bid_auth_check_login(&$username, &$password)
{
    // Strip slashes off the username and password; we don't even use SQL here.
    $sane_username = stripslashes($username);
    $sane_password = stripslashes($password);

    if ($sane_username == "") {
        /* Change the password in such a way that the Wordpress login process will fail. */
        wp_clear_auth_cookie();
        $password = '!';
        return;
    }

    $req = array(
        'timeout' => 5,
        'body' => array(
            'email' => $sane_username,
            'password' => $sane_password,
        ),
    );
    $resp = _bid_authed_post(BLENDERID_AUTHENTICATE_ENDPOINT, $req);
    if (is_wp_error($resp)) {
        /* There was another error from Blender ID. If they are failing for
         * some reason, just return and let Wordpress deal with the user.
         * The account may actually exist already with this password. */
        error_log("bid_auth_check_login($username) ERROR: error in POST request to Blender ID: " .
            $resp->get_error_code() . ", " . $resp->get_error_message());
        return;
    }

    $resp_code = $resp['response']['code'];
    if ($resp_code == 403) {
        /* This error can be due to an unknown account at the Blender ID side
         * (such accounts exist); if that is the case, fall through and let
         * Wordpress deal with it. */
        $payload = json_decode($resp['body']);
        if ($payload->error == 'no-such-account') {
            return;
        }

        /* Actual login error; change the password in such a way that
         * the Wordpress login process will fail too. */
        wp_clear_auth_cookie();
        $password = '!';
        return;
    }
    if ($resp_code != 200) {
        error_log("bid_auth_check_login($username) WARNING: error $resp_code in POST request.");
        /* There was another error from Blender ID. If they are failing for
         * some reason, just return and let Wordpress deal with the user.
         * The account may actually exist already with this password. */
        return;
    }

    // User is found to be okay, parse the data we got from Blender ID so we can update the local user.
    $userinfo = json_decode($resp['body']);

    // Fall back to the email address in case there is no full name.
    if (trim($userinfo->full_name) == "") {
        $userinfo->full_name = $sane_username;
    }

    // Wordpress wants first/last name, so split up the full name to accomplish that.
    $first_name = '';
    $last_name = '';
    _bid_split_full_name($userinfo->full_name, $first_name, $last_name);

    // The username and password MUST be the escaped versions.
    $userarray = array(
        'user_login' => $username,
        'user_pass' => wp_hash_password($password),
        'first_name' => $first_name,
        'last_name' => $last_name,
        'user_email' => $sane_username,
        'description' => '',
        'aim' => '',
        'yim' => '',
        'jabber' => '',
        'display_name' => $userinfo->full_name,
    );

    /* The value we pass to username_exists() is pasted AS-IS into a SQL query,
     * so we MUST keep the auto-quoting in place. */
    if ($id = username_exists($username)) {
        // Update the user in our database.
        $userarray['ID'] = $id;
    }

    $user_id = wp_insert_user($userarray);
    if (is_wp_error($user_id)) {
        $p = $user_id->get_error_message();
        error_log($p);
    } else {
        // Update the user_login with the email (in case it contains chars that are
        // stripped by wordpress, like +)
        global $wpdb;
        $wpdb->update($wpdb->users, array('user_login' => $sane_username), array('ID' => (int) $user_id));
    }

    // Some metadata won't be updated with wp_insert_user, so we use other DB calls for that.
    update_user_meta($user_id, 'billing_first_name', $first_name);
    update_user_meta($user_id, 'billing_last_name', $last_name);
}
