<?php

/**
 * Queues requests to Blender ID in a regularly flushed queue.
 *
 * Requests are performed immediately, and when they fail they are placed in
 * the queue. This queue is flushed every BID_CRON_INTERVAL_SECS seconds, and
 * failed deliveries remain in the queue. When a request receives a 422
 * response (for the badger service this indicates that there is no Blender ID
 * user with the given email address), the message is only kept in the queue
 * for BID_QUEUE_KEEP_422_SECS seconds (default: a month). This ensures that
 * messages that are delivered out of order (for example giving a subscriber
 * role before the Blender ID user is created) are re-tried later, but that
 * the queue won't overflow with these messages.
 */

add_action('init', 'bidq_setup_post_type');
function bidq_setup_post_type()
{
    $type = register_post_type('bid_queued_request',
        array(
            'label' => __('Blender ID Request'),
            'labels' => array(
                'name' => __('Blender ID Requests', 'blender-id'),
                'singular_name' => __('Blender ID Request', 'blender-id'),
                'add_new' => __('Add new Blender ID Request', 'blender-id'),
                'add_new_item' => __('New Blender ID Request', 'blender-id'),
                'edit_item' => __('Edit Blender ID Request', 'blender-id'),
                'search_items' => __('Search Blender ID Requests', 'blender-id'),
                'not_found' => __('No Blender ID Requests found', 'blender-id'),
                'not_found_in_trash' => __('No Blender ID Requests found in trash', 'blender-id'),
                'all_items' => __('All Blender ID Requests', 'blender-id'),
                'archives' => __('Blender ID Requests', 'blender-id'),
            ),
            'public' => false,
            'show_ui' => true,  // show in admin
            'menu_position' => 100, // below second separator
            'has_archive' => false,
            'rewrite' => array('slug' => 'bid-req'),
            'supports' => array('title', 'editor', 'comments'),
            'taxonomies' => array(),
            'menu_icon' => 'dashicons-carrot',
            'capability_type' => 'bid_req',
            'map_meta_cap' => true,
            'capabilities' => array(
                'read' => 'read_bid_req', // don't allow the standard 'read' capability.
            ),
        )
    );
    if (is_wp_error($type)) {
        die($type->get_error_message());
    }

    // Clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}

// Add custom columns to admin bid_reqs
add_filter('manage_bid_queued_request_posts_columns', 'bid_req_posts_columns');
function bid_req_posts_columns($columns)
{
    return array_merge($columns, array(
        'author' => __('Author', 'blender-id'),
    ));
}


/**
 * Queues a message for POSTing to Blender ID.
 *
 * Immediate delivery is attempted, and queueing only happens when this fails.
 *
 * Returns true when immediate delivery was OK, and false when queued.
 */
function bidq_post($url, $req)
{
    $q_size = _bidq_size();
    if ($q_size > 0) {
        error_log("bidq_post($url) WARNING: already $q_size items in queue, immediately queueing");
        _bidq_queue_post($url, $req, "error: items already in queue");
        return false;
    }

    $resp = _bid_authed_post($url, $req);
    if (is_wp_error($resp)) {
        error_log("bidq_post($url) WARNING: error in POST request, queueing; " .
            $resp->get_error_code() . ", " . $resp->get_error_message());
        _bidq_queue_post($url, $req, "error: " . $resp->get_error_code() . ", " . $resp->get_error_message());
        return false;
    }

    $resp_code = $resp['response']['code'];
    if ($resp_code < 200 || $resp_code > 204) {
        error_log("bidq_post($url) WARNING: error $resp_code in POST request, queueing.");
        _bidq_queue_post($url, $req, "error $resp_code in POST request, queueing");
        return false;
    }

    return true;
}

/* Actually queue the request for POSTing to Blender ID later. */
function _bidq_queue_post($url, $req, $reason)
{
    $content = json_encode($req);
    $post_id = wp_insert_post(array(
        'post_title' => $url,
        'post_content' => $content,
        'post_type' => 'bid_queued_request',
        'post_status' => 'private',
    ));
    if ($post_id == 0 || is_wp_error($post_id)) {
        error_log("_bidq_queue_post($url): ERROR creating post: $post_id");
        return;
    }

    _bidq_queue_add_comment($post_id, $reason);
}

/* Add a comment to a queued item to indicate why it was queued. */
function _bidq_queue_add_comment($post_id, $content)
{
    $commentdata = array(
        'comment_post_ID' => $post_id, // to which post the comment will show up
        'comment_content' => current_time('mysql', false) . ": $content",
        'comment_type' => '', //empty for regular comments, 'pingback' for pingbacks, 'trackback' for trackbacks
        'comment_parent' => 0,
    );

    /* Prevent notices from WC_Comments::check_comment_rating() */
    $old_level = error_reporting();
    error_reporting($old_level && ~E_NOTICE);

    /* Ignore errors adding comments; this happens when we comment the same
     * thing twice or more to prevent duplicates. */
    wp_new_comment($commentdata, true);
    error_reporting($old_level);
}

// Make the 'bid_queue_schedule' cron schedule available.
add_filter('cron_schedules', 'bid_cron_schedules');
function bid_cron_schedules($schedules)
{
    $schedules['bid_queue_schedule'] = array(
        'interval' => BID_CRON_INTERVAL_SECS,
        'display' => __('Blender ID Queue')
    );
    return $schedules;
}

add_action('flush_bid_queue_hook', '_bidq_flush_bid_queue');
function _bidq_flush_bid_queue()
{
//    error_log('INFO: Blender ID Queue is flushing.');

    /* Time-limit the queue sending so that we are guaranteed to
     * stop before the next cron job comes in to flush again. */
    $start_time = time();
    $end_time = $start_time + BID_CRON_INTERVAL_SECS - 10;
    if ($end_time < $start_time) {
        error_log('ERROR: BID_CRON_INTERVAL_SECS is too small');
        return;
    }

    $posts = get_posts(array(
        'post_type' => 'bid_queued_request',
        'orderby' => 'ID',
        'order' => 'ASC',
        'post_status' => array('private', 'publish'),
        'numberposts' => 250,  // this probably fits in memory
        'cache_results' => false,
    ));
    $count = count($posts);
    if ($count == 0) return;

    $count_ok = 0;
    $count_bad = 0;
    $count_skipped = 0;
    $last_id = -1;

    // Only try POSTing to a failing host once.
    $failing_hosts = array();

    error_log("INFO: Blender ID Queue is flushing $count items.");
    foreach ($posts as $post) {
        if (time() > $end_time) {
            error_log('WARNING: Blender ID Queue flushing stopped due to time limit.');
            return;
        }
        // Assert that we are reading in the correct order.
        if ($last_id >= $post->ID) {
            error_log("ERROR: Blender ID Queue was not returned with incrementing IDs, read $last_id before $post->ID, aborting!");
            return;
        }
        $last_id = $post->ID;
        $url = $post->post_title;

        // Skip failing hosts.
        $hostname = parse_url($url, PHP_URL_HOST);
        if (isset($failing_hosts[$hostname]) && $failing_hosts[$hostname]) {
            $count_skipped++;
            continue;
        }

        $req = json_decode($post->post_content, true);
        if ($req == null) {
            $err = json_last_error_msg();
            error_log("bidq_post($url) ERROR: unable to decode JSON $post->post_content: $err");
            _bidq_queue_add_comment($post->ID, "unable to decode JSON: $err");
            $count_bad++;
            continue;
        }

        $resp = _bid_authed_post($url, $req);
        if (is_wp_error($resp)) {
            $failing_hosts[$hostname] = true;

            $msg = $resp->get_error_code() . ", " . $resp->get_error_message();
            error_log("bidq_post($url) WARNING: error in POST request $post->ID, leaving queued; $msg");
            _bidq_queue_add_comment($post->ID, "error: $msg");
            $count_bad++;
            continue;
        }

        $resp_code = $resp['response']['code'];

        if ($resp_code == 422 && BID_QUEUE_KEEP_422_SECS > 0) {
            /* Email address of user is unknown; try for a while to avoid
             * synchronisation issues (like user creation msg queued after
             * subscription activation msg). */
            $count_bad++;
            $post_date = mysql2date('U', $post->post_date_gmt);
            $post_age = time() - $post_date;
            if ($post_age <= BID_QUEUE_KEEP_422_SECS) {
                $time_left = round(BID_QUEUE_KEEP_422_SECS - $post_age);
                $msg = "error $resp_code in POST request $post->ID, " .
                    "address is unknown to Blender ID, leaving queued for $time_left seconds.";
                error_log("bidq_post($url) INFO: $msg");
                _bidq_queue_add_comment($post->ID, $msg);
                continue;
            }
            // Otherwise fall through and de-queue.
            error_log("bidq_post($url) WARNING: error $resp_code in POST request $post->ID, " .
                "address is unknown to Blender ID and queued for $post_age seconds; de-queueing.");
        } else if ($resp_code < 200 || $resp_code > 204) {
            // Check if this is a create-user call for a user that already exists.
            if ($resp_code == 409 && substr($url, -17) == '/api/create-user/') {
                $email = $req['body']['email'];
                error_log("bidq_post($url) WARNING: error $resp_code in POST request $post->ID, address $email already exists; skipping");
                if (wp_delete_post($post->ID, true) === false) {
                    error_log("WARNING: unable to de-queue $post->ID.");
                    _bidq_queue_add_comment($post->ID, "unable to de-queue $post->ID");
                }
                $count_skipped++;
                continue;
            }

            $failing_hosts[$hostname] = true;
            $body = $resp['body'];
            error_log("bidq_post($url) WARNING: error $resp_code in POST request $post->ID, leaving queued: $body");
            _bidq_queue_add_comment($post->ID, "error $resp_code in POST request, leaving queued: $body");
            $count_bad++;
            continue;
        } else {
            error_log("INFO: succesfully POSTed $post->ID to $url, de-queueing.");
            $count_ok++;
        }

        if (wp_delete_post($post->ID, true) === false) {
            error_log("WARNING: unable to de-queue $post->ID.");
            _bidq_queue_add_comment($post->ID, "unable to de-queue $post->ID");
        }
    }

    error_log("INFO: Blender ID Queue is flushed; ok=$count_ok; bad=$count_bad; skipped=$count_skipped");
}


// Show a little counter in the Blender Queue admin menu, indicating the size of the queue.
// Hidden when there are no items queued.
add_action('admin_menu', 'bidq_show_queued_count_in_menu');
function bidq_show_queued_count_in_menu()
{
    global $menu;

    $count = _bidq_size();
    if ($count == 0) return;

    foreach ($menu as $priority => $item) {
        // Find our menu item
        if ($item[1] != "edit_bid_reqs") continue;

        $menu[$priority][0] .= "<span class='update-plugins count-$count'><span class='update-count'>$count</span></span>";

        // Found it, skip the rest of the menu.
        break;
    }
}

/**
 * @return int the size of the queue at this moment.
 */
function _bidq_size()
{
    // We don't care about the status, just sum everything.
    $counts = wp_count_posts('bid_queued_request');

    $count = 0;
    foreach ($counts as $status => $status_count) {
        if ($status == 'trash' || $status == 'auto-draft') continue;  // ok I lied, we do care about some statuses.
        $count += $status_count;
    }

    return $count;
}
