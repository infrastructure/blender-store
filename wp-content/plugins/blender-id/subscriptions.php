<?php
/***
 * Notify Blender ID of subscription status mutations.
 */
add_action('woocommerce_subscription_status_active', 'bid_notify_subscribe');
function bid_notify_subscribe($subscription)
{
    // $user is a WP_User object: https://codex.wordpress.org/Class_Reference/WP_User
    $user = $subscription->get_user();
    $status = $subscription->get_status();

    error_log("bid_notify_subscribe: $user->user_email (#$user->ID) subscription changed to '$status'");
    _bid_badger('grant', 'cloud_subscriber', $user);
}

add_action('woocommerce_subscription_status_cancelled', 'bid_notify_unsubscribe');
add_action('woocommerce_subscription_status_expired', 'bid_notify_unsubscribe');
add_action('woocommerce_subscription_status_on-hold', 'bid_notify_unsubscribe');
function bid_notify_unsubscribe($subscription)
{
    // $user is a WP_User object: https://codex.wordpress.org/Class_Reference/WP_User
    $user = $subscription->get_user();

    /* One subscription may be cancelled, but they could still have another one
     * active. This shouldn't happen in real life, but we'd better check anyway. */
    $other_status = _most_active_status($user);
    $status = $subscription->get_status();

    if ($other_status == 'active' || $other_status == 'pending-cancel') {
        error_log("bid_notify_unsubscribe: $user->user_email subscription changed to '$status', " .
            "but this user also has a subscription that is $other_status; ignoring cancellation!");
        return;
    }

    // The subscription certainly isn't active at this time.
    error_log("bid_notify_unsubscribe: $user->user_email (#$user->ID) subscription changed to '$status'");
    _bid_badger('revoke', 'cloud_subscriber', $user);

    // Possibly this status change means the subscription cannot be renewed any more.
    if ($status != 'on-hold' && $other_status != 'on-hold') {
        error_log("bid_notify_unsubscribe: $user->user_email (#$user->ID) lost their subscription");
        _bid_badger('revoke', 'cloud_has_subscription', $user);
    }
}


/***
 * Notify Blender ID of subscription renewability mutations.
 */
add_action('woocommerce_subscription_status_pending', 'bid_notify_has_subscription');
add_action('woocommerce_subscription_status_active', 'bid_notify_has_subscription');
add_action('woocommerce_subscription_status_on-hold', 'bid_notify_has_subscription');
function bid_notify_has_subscription($subscription)
{
    // $user is a WP_User object: https://codex.wordpress.org/Class_Reference/WP_User
    $user = $subscription->get_user();

    error_log("bid_notify_has_subscription: $user->user_email (#$user->ID) has a subscription");
    _bid_badger('grant', 'cloud_has_subscription', $user);
}

/***
 * Grant or revoke a role from/to a specific user.
 *
 * @param $action
 * @param $role
 * @param $user WP_User object: https://codex.wordpress.org/Class_Reference/WP_User
 */
function _bid_badger($action, $role, $user)
{
    if ($user->user_email == '' || strpos($user->user_email, '@') === false) {
        /* This really shouldn't be happening, but it did in the past and when it does
         * it'll block the entire Blender ID queue. Better just complain and ignore.
         */
        error_log("_bid_badger($action, $role, $user): $user->ID has no email; UNABLE TO SEND TO BLENDER ID, MANUAL INTERVENTION IS NEEDED");
        return;
    }
    $email = $user->user_email;

    // Perform the HTTP POST to the Blender ID Badger service.
    $req = array(
        'method' => 'POST',
    );
    $url = BLENDERID_BADGER_ENDPOINT . "/$action/$role/$email";
    $delivered = bidq_post($url, $req);
    if (!$delivered) {
        error_log("_bid_badger($action, $role, $email) WARNING: request was queued");
    }
}

// Returns the status of the most active, false if all are cancelled/expired.
function _most_active_status($user) {
    $statuses = array();
    $all_subs = wcs_get_users_subscriptions($user->ID);
    if (!empty($all_subs)) {
        foreach ($all_subs as $other_sub) {
            // Not interesting.
            if ($other_sub->has_status('cancelled') || $other_sub->has_status('expired')) {
                continue;
            }
            $status = $other_sub->get_status();
            $statuses[$status] = true;
        }
    }

    if (empty($statuses)) return false;

    if (count($statuses) > 1) {
        if ($statuses['active']) return 'active';
        if ($statuses['pending-cancel']) return 'pending-cancel';
        if ($statuses['on-hold']) return 'on-hold';
    }

    return array_keys($statuses)[0];
}
