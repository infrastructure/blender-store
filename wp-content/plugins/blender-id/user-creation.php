<?php


// Handle an already existing Blender ID as a registration error.
add_action('woocommerce_register_post', 'bid_process_registration_post', 2000, 3);
function bid_process_registration_post($username, $email, $validation_error)
{
    // Refuse to go any further if we haven't been able to find an email address.
    if (strlen(trim($email)) == 0) {
        error_log("bid_process_registration_post: Refusing to handle user without email address");
        $validation_error->add('missing-email', 'Please enter a valid email address to register');
        return;
    }

    // The create-user-upon-checkout form uses 'account_password', whereas the user registration form
    // uses 'password'. Here we unify this so that the rest of the code doesn't have to care.
    if (isset($_POST['account_password']) && !isset($_POST['password'])) {
        $_POST['password'] = $_POST['account_password'];
    }

    // Collect data to create a record in the external database.
    // Since we know how to not create SQL injection attacks, we have to strip slashes from $_POST.
    $post = array_map('stripslashes_deep', $_POST);
    $password = $post['password'];
    $first_name = isset($_POST['billing_first_name']) ? $_POST['billing_first_name'] : '';
    $last_name = isset($_POST['billing_last_name']) ? $_POST['billing_last_name'] : '';
    $full_name = trim("$first_name $last_name");

    $req = array(
        'method' => 'POST',
        'timeout' => 30,
        'body' => array(
            'email' => $email,
            'full_name' => $full_name,
            'password' => $password,
        ),
    );
    $resp = _bid_authed_post(BLENDERID_CREATE_USER_ENDPOINT, $req);
    if (is_wp_error($resp)) {
        error_log("woocommerce_register_post($email) WARNING: error in POST request, aborting registration; " .
            $resp->get_error_code() . ", " . $resp->get_error_message());
        $validation_error->add('bid-comm-error', 'There was an error communicating with Blender ID to create your account, please try again by reloading the page.');
        return;
    }

    $resp_code = $resp['response']['code'];
    if ($resp_code == 409) {
        error_log("woocommerce_register_post($email) WARNING: error $resp_code in POST request; account already exists.");
        $msg = _('You already have a Blender ID, just log in with it or <a href="%s" target="_blank">reset your password</a> if you don\'t remember it.');
        $validation_error->add('registration-error-email-exists', sprintf($msg, "https://www.blender.org/id/password_reset/"));
        return;
    }

    if ($resp_code < 200 || $resp_code > 204) {
        error_log("woocommerce_register_post($email) WARNING: error $resp_code in POST request, not creating account: " . $resp['body']);

        if ($resp['headers']['content-type'] == 'application/json') {
            $msg = 'Blender ID did not create your account';
            $payload = json_decode($resp['body'], true);
            if (!empty($payload)) {
                $msg .= ':<dl>';
                foreach ($payload as $key => $value) {
                    $msg .= "<dt>$key:</dt><dd>$value</dd>";
                }
                $msg .= '</dl>';
            } else {
                $msg .= '.';
            }
        } else {
            $msg = 'Blender ID did not create an account for you, and said: ' . $resp['response']['message'];
        }

        $validation_error->add('bid-error', $msg .
            "<br>\nPlease try again by reloading the page and confirming. " .
            "If this problem persists, please <a href='/contact/'>contact us</a>.");
        return;
    }
}
