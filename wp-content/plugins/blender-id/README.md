# Blender ID integration

This plugin provides authentication against Blender ID. It features the following:

- Password authentication against Blender ID.
- Local Wordpress user creation & update upon login.
- Pushing Blender ID user creation via the registration form and checkout flow.
- Pushing subscription status changes to Blender ID.


## In case of failure

When Blender ID cannot be reached (or responds with errors like 500 Internal Server Error), the
following will occur.

- When logging in, the regular Wordpress authentication still happens. This means that the user will
  be able to log in just fine if they ever logged in before, using the password still known by
  Wordpress. If they changed their password on Blender ID since the last login this will not be
  known yet to Wordpress, so they'll have to log in using their previous password.

- Messages to be sent to Blender ID ('create this user', 'update this subscription status') will be
  queued. Every BID_CRON_INTERVAL_SECS the plugin will try to deliver queued messsages. Messages
  will remain queued until succesfully delivered or manually removed from the queue.

- If there are any messages queued, new messages will be queued immediately without trying to
  deliver them.


## TO DO

### WooCommerce

The billing info isn't updated from Blender ID, so on the checkout page the
first/last name fields are empty even though we do know someone's name.

### Wordpress-only accounts

This plugin makes the assumption that everybody with a Wordpress account also has a Blender ID
account. If the user has a Wordpress account but *not* a Blender ID account, it will be impossible
to register (because the Wordpress account already exists) and it will be impossible to log in
(because the Blender ID password check will fail).
