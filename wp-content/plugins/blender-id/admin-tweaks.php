<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
require( ABSPATH . WPINC . '/pluggable.php' );

//! Show info about users who requested their Blender ID to be deleted.
if ( current_user_can('edit_users') ) {
    add_action('personal_options', 'bid_user_admin_deletion');
    add_action('edit_user_profile', 'bid_user_admin_deletion');
}
function bid_user_admin_deletion($user)
{
    $user_id = $user->ID;

    $deletion_requested_meta = get_user_meta($user_id, 'blender_id_date_deletion_requested', true);
    $deletion_requested_date = new DateTimeImmutable($deletion_requested_meta);
    $delete_on = $deletion_requested_date->add(new DateInterval('P14D'));
    $now = new DateTimeImmutable();
    $remaining = $now->diff($delete_on);
?>
    <table class="form-table"><tbody>
        <tr>
            <th scope='row'>User Deletion</th>
            <td><table>
<?php if ($deletion_requested_meta != ""): ?>
                <tr>
                    <td>User Requested Deletion On:</td>
                    <td><?php _e($deletion_requested_date->format('Y-m-d')) ?></td>
                </tr>
  <?php if ($remaining->invert): ?>
                <tr style="color: darkred; font-weight: bold;">
                    <td>Deletion Should Have Happened On:</td>
  <?php else: ?>
                <tr>
                    <td>Deletion Will Happen On:</td>
  <?php endif; ?>
                    <td><?php _e($delete_on->format('Y-m-d')) ?></td>
                </tr>
<?php endif; ?>
<?php if (current_user_can('delete_users')): ?>
                <tr><td colspan='2'>
                  <a class='button button-secondary' href='<?php _e(wp_nonce_url("users.php?action=delete&amp;user=$user_id", 'bulk-users' )) ?>'>Delete User Now</a>
                </td></tr>
<?php endif; ?>
            </table></td>
        </tr>
    </tbody></table>
<?php
}
