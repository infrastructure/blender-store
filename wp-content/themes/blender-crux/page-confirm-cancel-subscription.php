<?php
/**
 * The template for displaying the subscription cancellation page.
 *
 * Make sure there is an appropriate page with the slug "/confirm-cancel-subscription/"
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main"<?php stag_markup_helper( array( 'context' => 'content', 'post_type' => 'page' ) ); ?>>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<div style='text-align: center'>
                <p><a class='button cancel' href='<?php esc_attr_e($_GET['confirm'])?>'>Yes, I really want to cancel my Blender Cloud subscription</a></p>
                <p><a href='<?php esc_attr_e($_GET['abort'])?>'>No, I want to keep my Blender Cloud subscription</a>.</p>
				</div>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

if ( stag_should_load_sidebar() ) {
	get_sidebar();
}

?>

<?php get_footer(); ?>
