<?php

/* Added Magnific Popup for product images. For documentation, see:
 * http://dimsemenov.com/plugins/magnific-popup/documentation.html */
add_action('wp_enqueue_scripts', 'blender_crux_enqueue_scripts');
function blender_crux_enqueue_scripts()
{
    // Magnific Popup stuff
    wp_register_script('magnific-popup', get_stylesheet_directory_uri() . '/magnific-popup/jquery.magnific-popup.min.js', array('jquery'), '1.1.0');
    wp_register_script('magnific-popup-activate', get_stylesheet_directory_uri() . '/magnific-popup/magnific-popup-activate.js', array('jquery', 'magnific-popup'), '1.1.0');
    wp_enqueue_script('magnific-popup');
    wp_enqueue_script('magnific-popup-activate');
    wp_enqueue_style('magnific-popup', get_stylesheet_directory_uri() . '/magnific-popup/magnific-popup.css', false, '1.1.0');
}


add_filter('woocommerce_checkout_must_be_logged_in_message', 'blender_crux_checkout_must_be_logged_in_message', 10, 0);
function blender_crux_checkout_must_be_logged_in_message()
{
    return '';
}


// Add a second password field to the registration page.
// Priority set to 5 to put the "Confirm password" field above the reCaptcha field.
add_action('woocommerce_register_form', 'blender_crux_register_form', 5);
function blender_crux_register_form()
{
    if (get_option('woocommerce_registration_generate_password') == 'yes')
        return;

    ?>
    <p class="form-row form-row-wide">
        <label for="reg_password_2"><?php _e('Confirm password', 'woocommerce'); ?> <span
                    class="required">*</span></label>
        <input type="password" class="input-text" name="password2" id="reg_password_2"/>
    </p>
    <?php
}

// Add a link to Blender ID to the login form.
add_action('woocommerce_login_form_start', 'blender_crux_login_form_start');
function blender_crux_login_form_start()
{
    ?>
	<p class="lost_password">
		In order to login to the store you need a <a href="https://www.blender.org/id/register/" target="_blank">Blender-ID</a>. <br>
		No Blender-ID? You can simply use the Register form and we will make one for you!
	</p>
    <?php
}
add_action('woocommerce_login_form', 'blender_crux_login_form');
function blender_crux_login_form()
{
    ?>
    <p class="woocommerce-LostPassword lost_password">
        If you have lost your Blender ID password, <a href="https://www.blender.org/id/password_reset/">please reset it here</a>.
    </p>
    <?php
}

// Check the password and confirm password fields match before allow registration to proceed.
add_filter('woocommerce_process_registration_errors', 'blender_crux_process_registration_errors', 10, 3);
function blender_crux_process_registration_errors($validation_error, $username, $password)
{
    $password2 = isset($_POST['password2']) ? $_POST['password2'] : '';
    if (strcmp($password2, $password) != 0)
        $validation_error->add('password-validation', _('Passwords did not match'));

    return $validation_error;
}

// The login form handler checks our 'redirect' field, but the registration form handler does not.
add_filter('woocommerce_registration_redirect', 'blender_crux_woocommerce_registration_redirect');
function blender_crux_woocommerce_registration_redirect($redirect)
{
    if (isset($_POST['redirect']) && !empty($_POST['redirect']))
        return $_POST['redirect'];
    return $redirect;
}


// Hide the "Have a coupon?" thing on the checkout page until the user is logged in.
add_filter('woocommerce_coupons_enabled', 'blender_crux_woocommerce_coupons_enabled');
function blender_crux_woocommerce_coupons_enabled($is_enabled)
{
    return is_user_logged_in() && $is_enabled;
}


// Override what Crux is doing, as it sets 'Proceed to Checkout' as the 'Order Now' button text.
add_filter('woocommerce_order_button_text', 'blender_change_order_button_text', 30);
function blender_change_order_button_text() {
	return 'Order Now';
}
