<?php
/**
 * Lost password form
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.2
 */
?>
<script>
    window.location.replace('https://www.blender.org/id/password_reset/');
</script>
<p>Please visit the <a href='https://www.blender.org/id/password_reset/'>Password Reset</a>
    page of Blender ID to reset your password.</p>
