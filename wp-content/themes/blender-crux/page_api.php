<?php
/*
 * Template Name: API Output
 */
header('Content-Type: application/json');
while ( have_posts() ) : the_post(); ?>				
<?php the_content(); ?>
<?php endwhile; // end of the loop. ?>
