<?php
/**
 * Shows a counter for the number of projects sold.
 *
 * User: sybren
 * Date: 11-4-2016
 * Time: 12:49
 */
global $wpdb;

$product = isset($_GET['prod']) ? $_GET['prod'] : '';
$aggr = isset($_GET['aggr']) ? $_GET['aggr'] : '';
$callback = isset($_GET['callback']) ? $_GET['callback'] : '';  // JSONP callback


function print_json($content) {
    global $callback;

    $encoded = json_encode($content);

    // Enable cross-origin resource sharing; see http://enable-cors.org/
    if (isset($_SERVER["HTTP_ORIGIN"]) && !empty($_SERVER["HTTP_ORIGIN"])) {
        header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
    }

    // Expire in 15 minutes.
    header('Cache-Control: max-age=900');

    // Wait with setting the content type header until the last moment, as
    // PHP will print errors as HTML and not as JSON.
    if (strlen($callback)) {
        header('Content-Type: application/javascript; charset=UTF-8');
        print("$callback($encoded)");
    } else {
        header('Content-Type: application/json; charset=UTF-8');
        print($encoded);
    }
}

function no_such_product_error() {
    print_json(array(
         'status' => 'fail',
         'error' => 'No such product available'));
    die();
}

if ($aggr == 'all') {
    // Performs aggregation for today. Shouln't be necessary, as it's automatically
    // done each hour anyway, but it might be nice to have a way to manually trigger.
    $json = blpltw_aggregate_product_sales();  // defined in plugins/bo_woocommerce/aggr_product_sales.php
    print_json($json);
    die();
}
if ($aggr == 'daily') {
    // Performs the daily aggregation, ensuring stats for 'yesterday' are correct.
    blpltw_aggregate_product_sales_yesterday();
    print('OK');
    die();
}

// query to see if we need to run the first hourly aggregation run.
$query = "SELECT items_sold
          FROM aggr_product_sales
          WHERE day=date(now()) AND product_name='cloud'";
$result = $wpdb->get_results($query);
if (count($result) == 0 || (int)$result[0]->items_sold == 0) {
    blpltw_aggregate_product_sales();
}

// query for product sales from the aggregation table.
$product = esc_sql(sanitize_title_for_query($product));
$query = "SELECT product_name, items_sold
          FROM aggr_product_sales
          WHERE day=date(now()) AND items_sold > 0";
if (strlen($product)) {
    $query .= " AND product_name='$product'";
}

$result = $wpdb->get_results($query);

if (strlen($product)) {
    // asking for a specific product, return items sold for today and in total.
    $items_sold = (int)$result[0]->items_sold;
    if ($items_sold === null) $items_sold = 0;

    $json = array(
        'status' => 'success',
        'product_count' => $items_sold,
    );

    if ($product == 'cloud') {
        $json['total_sold'] = $items_sold;
    } else {
        $query = "SELECT meta_value FROM wp_postmeta pm
                      LEFT JOIN wp_posts p ON (p.ID = pm.post_id)
                    WHERE p.post_type = 'product'
                      AND p.post_name='$product'
                      AND pm.meta_key='total_sales'";
        $result = $wpdb->get_results($query);
        $total_sold = (int)$result[0]->meta_value;
        if ($total_sold === null) $total_sold = 0;

        $json['total_sold'] = $total_sold;
    }

    print_json($json);
    die();
}

// Asking for all products, show an overview.
$product_sales = array();
foreach ($result as $sale) {
    $product_sales[$sale->product_name] = (int)$sale->items_sold;
}

print_json(array(
    'status' => 'success',
    'product_count' => $product_sales
));
