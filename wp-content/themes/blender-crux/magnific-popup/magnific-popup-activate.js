jQuery(function ($) {
    // See http://dimsemenov.com/plugins/magnific-popup/documentation.html#options

    $('.main-product-wrapper .product-details-left').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: true,
        gallery: {enabled: true}
    });
});
