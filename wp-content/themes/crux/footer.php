<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package StagFramework
 * @subpackage Crux
 */
?>
		</div><!-- #content -->
	</div><!-- .content-wrapper -->

	<?php
		/**
		 * @hooked stag_display_static_content() - Displays static content in footer.
		 */
		do_action( 'before_footer' );
	?>

	<footer id="colophon" class="site-footer"<?php stag_markup_helper( array( 'context' => 'footer' ) ); ?>>

		<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
			<div class="inside">
				<div class="grids footer-widget-area">
					<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php

		$grid_class = 'grid-12';
		if ( has_nav_menu( 'footer' ) && has_nav_menu( 'jetpack-social-menu' ) ) {
			$grid_class = 'grid-6';
		}

		?>
		<div class="footer-navigation">
			<div class="inside">
				<div class="grids">
					<div class="<?php echo esc_attr( $grid_class ); ?>">
						<?php if ( has_nav_menu( 'footer' ) ) : ?>
						<nav class="footer-menu-wrapper"<?php stag_markup_helper( array( 'context' => 'nav' ) ); ?>>
							<?php
							wp_nav_menu(
								array(
									'theme_location' => 'footer',
									'menu_class'     => 'footer-menu navigation',
									'container'      => false,
								)
							);
							?>
						</nav>
						<?php endif; ?>
					</div>

					<?php if ( function_exists( 'jetpack_social_menu' ) && has_nav_menu( 'jetpack-social-menu' ) ) : ?>
					<div class="<?php echo esc_attr( $grid_class ); ?>">
						<?php jetpack_social_menu(); ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<?php if ( '' !== $footer_text = stag_theme_mod( 'general_settings', 'general_footer_text' ) ) : ?>
		<div class="site-info">
			<div class="inside">
				<?php echo stripslashes( $footer_text ); ?>
			</div>
		</div><!-- .site-info -->
		<?php endif; ?>

	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
