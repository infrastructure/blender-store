<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Stag_Customizer
 * @subpackage Crux
 */

if ( ! function_exists( 'crux_sanitize_text' ) ) :
	/**
	 * Sanitize a string to allow only tags in the allowedtags array.
	 *
	 * @since  2.1.0.
	 *
	 * @param  string $string The unsanitized string.
	 * @return string The sanitized string.
	 */
	function crux_sanitize_text( $string ) {
		global $allowedtags;
		return wp_kses( $string, $allowedtags );
	}
endif;

if ( ! function_exists( 'crux_sanitize_multicheck' ) ) :
	function crux_sanitize_multicheck( $values ) {

		$multi_values = ! is_array( $values ) ? explode( ',', $values ) : $values;

		return ! empty( $multi_values ) ? array_map( 'sanitize_text_field', $multi_values ) : array();
	}
endif;

if ( ! function_exists( 'stag_allowed_tags' ) ) :
	/**
	 * Allow only the allowedtags array in a string.
	 *
	 * @since  1.0.
	 *
	 * @param  string $string   The unsanitized string.
	 * @return string           The sanitized string.
	 */
	function stag_allowed_tags( $string ) {
		global $allowedtags;
		return wp_kses( $string, $allowedtags );
	}
endif;

/**
 * Output the tracking code.
 */
function stag_tracking_code() {
	$analytics = stag_theme_mod( 'general_settings', 'general_tracking_code' );
	if ( '' !== $analytics ) {
		echo stripslashes( $analytics );
	}
}
add_action( 'wp_footer', 'stag_tracking_code' );

/**
 * Return the WordPress array of allowed tags, with a few things added.
 *
 * @since 2.2.0.
 *
 * @return mixed|void
 */
function crux_allowed_html() {
	$expandedtags = wp_kses_allowed_html();

	// Span.
	$expandedtags['span'] = array();

	// H1 - H6.
	$expandedtags['h1'] = array();
	$expandedtags['h2'] = array();
	$expandedtags['h3'] = array();
	$expandedtags['h4'] = array();
	$expandedtags['h5'] = array();
	$expandedtags['h6'] = array();

	// Enable id, class, and style attributes for each tag.
	foreach ( $expandedtags as $tag => $attributes ) {
		$expandedtags[ $tag ]['id']    = true;
		$expandedtags[ $tag ]['class'] = true;
		$expandedtags[ $tag ]['style'] = true;
	}

	// br (doesn't need attributes).
	$expandedtags['br'] = array();

	// img.
	$expandedtags['img'] = array(
		'src'    => true,
		'height' => true,
		'width'  => true,
		'alt'    => true,
		'title'  => true,
	);

	/**
	 * Customize the tags and attributes that are allows during text sanitization.
	 *
	 * @since 2.2.0
	 *
	 * @param array     $expandedtags    The list of allowed tags and attributes.
	 * @param string    $string          The text string being sanitized.
	 */
	return apply_filters( 'crux_allowed_html', $expandedtags );
}
