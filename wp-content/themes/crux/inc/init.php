<?php

$inc = get_template_directory() . '/inc/';

/**
 * Load components.
 */
require_once $inc . 'customizer.php';
require_once $inc . 'extras.php';

