<?php
/**
 * Crux theme customizer settings.
 *
 * @package Stag_Customizer
 * @subpackage Crux
 */
function stag_customize_register_sections( $wp_customize ) {
	$wp_customize->add_panel(
		'crux_options_panel',
		array(
			'title'       => esc_html__( 'Theme Options', 'crux' ),
			'description' => esc_html__( 'Configure your theme settings', 'crux' ),
			'priority'    => 999,
		)
	);

	$wp_customize->add_section(
		'general_settings',
		array(
			'title'       => _x( 'General Settings', 'Theme customizer section title', 'crux' ),
			'priority'    => 30,
			'panel'       => 'crux_options_panel',
			'description' => __( 'Configure general settings of your theme and insert your analytics tracking code.', 'crux' ),
		)
	);

	$wp_customize->add_section(
		'styling_options',
		array(
			'title'       => _x( 'Styling Options', 'Theme customizer section title', 'crux' ),
			'priority'    => 40,
			'panel'       => 'crux_options_panel',
			'description' => __( 'Configure the visual appearance of your theme.', 'crux' ),
		)
	);

	$wp_customize->add_section(
		'typography',
		array(
			'title'       => _x( 'Typography Options', 'Theme customizer section title', 'crux' ),
			'priority'    => 60,
			'panel'       => 'crux_options_panel',
			'description' => sprintf(
				esc_html__( 'The list of Google fonts is long! You can %s before making your choices.', 'crux' ),
				sprintf(
					'<a href="%1$s" target="_blank" class="external-link">%2$s</a>',
					esc_url( 'https://fonts.google.com' ),
					esc_html__( 'preview', 'crux' )
				)
			),
		)
	);

	$wp_customize->add_section(
		'sidebar_locations',
		array(
			'title'       => _x( 'Sidebar Location', 'Theme customizer section title', 'crux' ),
			'priority'    => 70,
			'panel'       => 'crux_options_panel',
			'description' => __( 'Configure how Sidebars appear on your site. You can change the setting of each individual page, post when editing that page.', 'crux' ),
		)
	);

	$wp_customize->add_section(
		'shop_settings',
		array(
			'title'           => _x( 'Shop Settings', 'Theme customizer section title', 'crux' ),
			'priority'        => 80,
			'panel'           => 'crux_options_panel',
			'description'     => __( 'Customize your shop settings.', 'crux' ),
			'active_callback' => 'stag_is_woocommerce_active',
		)
	);

	$wp_customize->add_section(
		'static_content',
		array(
			'title'       => _x( 'Static Content', 'Theme customizer section title', 'crux' ),
			'priority'    => 90,
			'panel'       => 'crux_options_panel',
			'description' => __( 'Display Static Content right before the footer widget.', 'crux' ),
		)
	);

	$wp_customize->add_section(
		'stag_footer',
		array(
			'title'    => _x( 'Footer', 'Theme customizer section title', 'crux' ),
			'priority' => 900,
			'panel'    => 'crux_options_panel',
		)
	);

}
add_action( 'customize_register', 'stag_customize_register_sections' );

/**
 * Default theme customizations.
 *
 * @return $options an array of default theme options
 */
function stag_get_theme_mods( $args = array() ) {
	$defaults = array(
		'keys_only' => false,
	);

	$args = wp_parse_args( $args, $defaults );

	$fonts = stag_all_font_choices();

	$sidebar_options = array(
		'right-sidebar' => __( 'Right Sidebar', 'crux' ),
		'left-sidebar'  => __( 'Left Sidebar', 'crux' ),
		'no-sidebar'    => __( 'No Sidebar', 'crux' ),
	);

	$mods = array(
		'title_tagline'     => array(
			'general_custom_logo' => array(
				'title'             => __( 'Custom Logo Upload', 'crux' ),
				'type'              => 'Crux_Customize_Image_Control',
				'default'           => '',
				'transport'         => 'refresh',
				'sanitize_callback' => 'esc_url_raw',
			),
		),
		'general_settings'  => array(
			'general_contact_email'      => array(
				'title'             => __( 'Contact Form Email Address', 'crux' ),
				'type'              => 'text',
				'default'           => null,
				'sanitize_callback' => 'esc_url_raw',
			),
			'general_tracking_code'      => array(
				'title'       => __( 'Google Analytics Tracking Code', 'crux' ),
				'type'        => 'Stag_Customize_Textarea_Control',
				'description' => esc_html__( 'Paste the full Google Analytics code snippet here with <script> tags.', 'crux' ),
				'default'     => null,
			),
			'blog_page_background'       => array(
				'title'             => esc_html__( 'Blog Page Background', 'crux' ),
				'description'       => esc_html__( 'Upload the background image for blog page.', 'crux' ),
				'type'              => 'Crux_Customize_Image_Control',
				'transport'         => 'refresh',
				'default'           => '',
				'sanitize_callback' => 'esc_url_raw',
			),
			'blog-line'                  => array(
				'title'             => '',
				'type'              => 'Stag_Customize_Misc_Control',
				'default'           => false,
				'option'            => 'line',
				'sanitize_callback' => 'sanitize_text_field',
			),
			'site_show_searchbar_in_nav' => array(
				'title'             => __( 'Show Search box in Main Menu', 'crux' ),
				'description'       => esc_html__( 'Display a search box in main navigation.', 'crux' ),
				'type'              => 'checkbox',
				'default'           => true,
				'transport'         => 'refresh',
				'sanitize_callback' => 'wp_validate_boolean',
			),
			'site_comments_on_pages'     => array(
				'title'             => __( 'Disable Comments on Pages', 'crux' ),
				'description'       => esc_html__( 'This will globally turn off the comments on pages.', 'crux' ),
				'type'              => 'checkbox',
				'default'           => false,
				'transport'         => 'refresh',
				'sanitize_callback' => 'wp_validate_boolean',
			),
			'footer-line'                => array(
				'title'             => '',
				'type'              => 'Stag_Customize_Misc_Control',
				'default'           => false,
				'option'            => 'line',
				'sanitize_callback' => 'sanitize_text_field',
			),
			'general_footer_text'        => array(
				'title'   => __( 'Copyright Text', 'crux' ),
				'type'    => 'Stag_Customize_Textarea_Control',
				'default' => '&copy; ' . date( 'Y' ) . ' <a href="' . esc_url( home_url( '/' ) ) . '">' . get_bloginfo( 'name' ) . '</a> &mdash; A WordPress Theme by <a href="https://codestag.com">Codestag</a>',
			),
		),
		'styling_options'   => array(
			'style_background_color' => array(
				'title'   => __( 'Background Color', 'crux' ),
				'type'    => 'WP_Customize_Color_Control',
				'default' => '#ffffff',
			),
			'style_accent_color'     => array(
				'title'   => __( 'Accent Color', 'crux' ),
				'type'    => 'WP_Customize_Color_Control',
				'default' => '#71a32f',
			),
		),
		'typography'        => array(
			'body_font'   => array(
				'title'     => __( 'Body Font', 'crux' ),
				'type'      => 'select',
				'default'   => 'Ropa Sans',
				'transport' => 'refresh',
				'choices'   => $fonts,
			),
			'header_font' => array(
				'title'     => __( 'Header Font', 'crux' ),
				'type'      => 'select',
				'default'   => 'BebasNeue',
				'transport' => 'refresh',
				'choices'   => $fonts,
			),
			'subset-line' => array(
				'title'             => '',
				'type'              => 'Stag_Customize_Misc_Control',
				'default'           => false,
				'option'            => 'line',
				'sanitize_callback' => 'sanitize_text_field',
			),
			'subset'      => array(
				'title'             => __( 'Character Subset', 'crux' ),
				'type'              => 'select',
				'default'           => 'latin',
				'choices'           => stag_get_google_font_subsets(),
				'sanitize_callback' => 'sanitize_text_field',
			),
			'subset-info' => array(
				'title'             => '',
				'type'              => 'Stag_Customize_Misc_Control',
				'default'           => false,
				'option'            => 'text',
				'description'       => sprintf(
					esc_html__( 'Not all fonts provide each of these subsets. Please visit the %s to see which subsets are available for each font.', 'crux' ),
					sprintf(
						'<a href="%1$s" target="_blank">%2$s</a>',
						esc_url( 'https://www.google.com/fonts' ),
						esc_html__( 'Google Fonts website', 'crux' )
					)
				),
				'sanitize_callback' => 'sanitize_text_field',
			),
		),
		'sidebar_locations' => array(
			'sidebar_single_page_layout'  => array(
				'title'     => esc_html__( 'Single Post Pages', 'crux' ),
				'type'      => 'select',
				'default'   => 'right-sidebar',
				'transport' => 'refresh',
				'choices'   => $sidebar_options,
			),
			'sidebar_page_layout'         => array(
				'title'     => esc_html__( 'Pages', 'crux' ),
				'type'      => 'select',
				'default'   => 'right-sidebar',
				'transport' => 'refresh',
				'choices'   => $sidebar_options,
			),
			'sidebar_shop_page'           => array(
				'title'     => esc_html__( 'Shop Page', 'crux' ),
				'type'      => 'select',
				'default'   => 'no-sidebar',
				'transport' => 'refresh',
				'choices'   => $sidebar_options,
			),
			'sidebar_archive_page_layout' => array(
				'title'     => esc_html__( 'Archive Pages', 'crux' ),
				'type'      => 'select',
				'default'   => 'right-sidebar',
				'transport' => 'refresh',
				'choices'   => $sidebar_options,
			),
			'sidebar_blog_page_layout'    => array(
				'title'     => esc_html__( 'Blog Page', 'crux' ),
				'type'      => 'select',
				'default'   => 'right-sidebar',
				'transport' => 'refresh',
				'choices'   => $sidebar_options,
			),
		),
		// WooCommerce dependant theme options.
		'shop_settings'     => array(
			'shop_favicon_badge'     => array(
				'title'             => __( 'Favicon Badge', 'crux' ),
				'description'       => esc_html__( 'Show user’s cart item counts as badge in favicon.', 'crux' ),
				'type'              => 'checkbox',
				'default'           => true,
				'transport'         => 'refresh',
				'sanitize_callback' => 'wp_validate_boolean',
			),
			'shop_products_per_row' => array(
				'title'             => __( 'Products Per Row', 'crux' ),
				'description'       => esc_html__( 'Enter how many products should be displayed per row.', 'crux' ),
				'type'              => 'number',
				'default'           => 4,
				'transport'         => 'refresh',
				'sanitize_callback' => 'absint',
			),
			'shop_products_per_page' => array(
				'title'             => __( 'Products Per Page', 'crux' ),
				'description'       => esc_html__( 'Enter how many products should be displayed per page.', 'crux' ),
				'type'              => 'text',
				'default'           => '12',
				'transport'         => 'refresh',
				'sanitize_callback' => 'sanitize_text_field',
			),
			'shop_ratings'           => array(
				'title'             => __( 'Ratings on Catalog Pages', 'crux' ),
				'description'       => esc_html__( 'Show / Hide the ratings meter on the products listed on shop pages.', 'crux' ),
				'type'              => 'checkbox',
				'default'           => true,
				'transport'         => 'refresh',
				'sanitize_callback' => 'wp_validate_boolean',
			),
			'shop_page_sidebar'      => array(
				'title'     => esc_html__( 'Select which sidebar to display on Shop Page', 'crux' ),
				'type'      => 'select',
				'default'   => '',
				'transport' => 'refresh',
				'choices'   => stag_registered_sidebars( array( '' => __( 'Default Sidebar', 'crux' ) ) ),
			),
		),
		'static_content'    => array(
			'static_page_title'              => array(
				'title'             => esc_html__( 'Content Section Title', 'crux' ),
				'description'       => esc_html__( 'Enter the title for the content section.', 'crux' ),
				'type'              => 'text',
				'default'           => '',
				'transport'         => 'refresh',
				'sanitize_callback' => 'sanitize_text_field',
				'priority'          => 10,
			),
			'static_page'                    => array(
				'title'       => esc_html__( 'Select Page.', 'crux' ),
				'description' => esc_html__( 'Select page from which you want to display the content.', 'crux' ),
				'default'     => '',
				'transport'   => 'refresh',
				'type'        => 'dropdown-pages',
				'priority'    => 20,
			),
			'static_page_background'         => array(
				'title'             => esc_html__( 'Background Image', 'crux' ),
				'description'       => esc_html__( 'Select the background image of the content section.', 'crux' ),
				'type'              => 'Crux_Customize_Image_Control',
				'transport'         => 'refresh',
				'default'           => '',
				'sanitize_callback' => 'esc_url_raw',
				'priority'          => 30,
			),
			'static_page_background_color'   => array(
				'title'       => esc_html__( 'Background Color', 'crux' ),
				'description' => esc_html__( 'Select the background color of the content section', 'crux' ),
				'type'        => 'WP_Customize_Color_Control',
				'default'     => '#f7f5f1',
				'priority'    => 40,
				'transport'   => 'postMessage',
			),
			'static_page_text_color'         => array(
				'title'       => esc_html__( 'Text Color', 'crux' ),
				'description' => esc_html__( 'Select the text color of the content section', 'crux' ),
				'type'        => 'WP_Customize_Color_Control',
				'default'     => '#ffffff',
				'priority'    => 50,
				'transport'   => 'postMessage',
			),
			'static_page_link_color'         => array(
				'title'       => esc_html__( 'Link Color', 'crux' ),
				'description' => esc_html__( 'Select the link color of the content section', 'crux' ),
				'type'        => 'WP_Customize_Color_Control',
				'default'     => '#ffffff',
				'priority'    => 60,
				'transport'   => 'postMessage',
			),
			'static_page_background_opacity' => array(
				'title'             => esc_html__( 'Background Opacity', 'crux' ),
				'description'       => esc_html__( 'Enter the background image opacity of the content section', 'crux' ),
				'priority'          => 70,
				'type'              => 'range',
				'default'           => 50,
				'transport'         => 'postMessage',
				'input_attrs'       => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 5,
				),
				'sanitize_callback' => 'absint',
			),
			'static_page_visible_on'         => array(
				'title'             => __( 'Where to show?', 'crux' ),
				'description'       => __( 'Select on which pages static content should be shown.', 'crux' ),
				'type'              => 'Crux_Customize_Control_Checkbox_Multiple',
				'transport'         => 'refresh',
				'default'           => 'single-pages,single-posts,single-product-pages,archive-pages,shop-archive-pages,blog-page,404-page',
				'choices'           => array(
					'single-pages'         => __( 'Single Pages', 'crux' ),
					'single-posts'         => __( 'Single Posts', 'crux' ),
					'single-product-pages' => __( 'Single Product Pages', 'crux' ),
					'archive-pages'        => __( 'Archive Pages', 'crux' ),
					'shop-archive-pages'   => __( 'Shop Archive Pages', 'crux' ),
					'homepage'             => __( 'Homepage', 'crux' ),
					'blog-page'            => __( 'Blog Page', 'crux' ),
					'404-page'             => __( '404 Page', 'crux' ),
				),
				'sanitize_callback' => 'crux_sanitize_multicheck',
				'priority'          => 80,
			),

		),

	);

	$mods = apply_filters( 'stag_theme_mods', $mods );

	/** Return all keys within all sections (for transport, etc) */
	if ( $args['keys_only'] ) {
		$transport = array();

		foreach ( $mods as $section => $settings ) {
			foreach ( $settings as $key => $setting ) {
				if ( isset( $setting['transport'] ) ) {
					$transport[ $key ] = $setting['transport'];
				} else {
					$transport[ $key ] = '';
				}
			}
		}

		return $transport;
	}

	return $mods;
}

/**
 * Output the basic extra CSS for primary and accent colors.
 * Split away from widget colors for brevity.
 *
 * @return void
 */
function stag_header_css() {
	?>
	<style id="stag-custom-css" type="text/css">
		body, .site-content {
			background-color: <?php echo stag_theme_mod( 'styling_options', 'style_background_color' ); ?>;
		}
		a,
		.main-navigation a:hover,
		.mobile-menu a:hover,
		.mobile-menu .current-menu-item > a,
		.main-navigation .current-menu-item > a,
		.archive-lists a:hover {
			color: <?php echo stag_theme_mod( 'styling_options', 'style_accent_color' ); ?>;
		}
		input[type='submit'],
		button,
		.button,
		.cart_dropdown_link .count,
		.tagcloud a:hover {
			background-color: <?php echo stag_theme_mod( 'styling_options', 'style_accent_color' ); ?>;
		}
		.ls-crux .ls-bar-timer, .ls-crux .ls-nav-prev:hover, .ls-crux .ls-nav-next:hover, .ls-crux .ls-nav-stop-active, .ls-crux .ls-nav-start-active {
			background-color: <?php echo stag_theme_mod( 'styling_options', 'style_accent_color' ); ?> !important;
		}
		.cart_dropdown .dropdown_widget:before {
			border-top-color: <?php echo stag_theme_mod( 'styling_options', 'style_accent_color' ); ?> !important;
		}
		.onsale:before {
			border-right-color: <?php echo stag_theme_mod( 'styling_options', 'style_accent_color' ); ?> !important;
		}
		.cart_dropdown_link .count {
			border-color: <?php echo stag_theme_mod( 'styling_options', 'style_accent_color' ); ?> !important;
		}
		body {
			font-family: "<?php echo stag_theme_mod( 'typography', 'body_font' ); ?>";
		}
		h1, h2, h3, h4, h5, h6, .woocommerce-tabs .tabs, .comment-list .fn, .entry-content table th, .comment-content table th, .commentlist .meta {
			font-family: "<?php echo stag_theme_mod( 'typography', 'header_font' ); ?>";
		}

		<?php if ( function_exists( 'stag_is_woocommerce_active' ) && stag_is_woocommerce_active() ) { ?>

			/* WooCommerce CSS */
			.onsale,
			.woocommerce-message,
			.woocommerce-product-gallery__trigger,
			.widget_price_filter .ui-slider-horizontal .ui-slider-range,
			.widget_price_filter .ui-slider-handle {
				background-color: <?php echo stag_theme_mod( 'styling_options', 'style_accent_color' ); ?>;
			}

			.order_details strong,
			.product-title a,
			.product-category h3 mark {
				color: <?php echo stag_theme_mod( 'styling_options', 'style_accent_color' ); ?>;
			}

		<?php } ?>
	</style>
	<?php
}
add_action( 'wp_head', 'stag_header_css' );


/**
 * Add Chosen for easier font selection.
 *
 * @since 2.1.0.
 * @return void
 */
function crux_customize_controls_enqueue_scripts() {
	// Styles
	wp_enqueue_style(
		'chosen',
		get_template_directory_uri() . '/stag-customizer/libs/chosen/chosen' . STAG_SCRIPT_SUFFIX . '.css',
		array(),
		'1.6.2'
	);

	// Scripts
	wp_enqueue_script(
		'chosen',
		get_template_directory_uri() . '/stag-customizer/libs/chosen/chosen.jquery' . STAG_SCRIPT_SUFFIX . '.js',
		array( 'jquery', 'customize-controls' ),
		'1.6.2',
		true
	);

	wp_enqueue_script(
		'customizer-sections',
		get_template_directory_uri() . '/stag-customizer/js/customizer-sections.js',
		array( 'customize-controls', 'chosen' ),
		STAG_THEME_VERSION,
		true
	);

	$localize = array(
		'fontOptions'               => crux_get_font_property_option_keys(),
		'chosen_no_results_default' => esc_html__( 'No results match', 'crux' ),
		'chosen_no_results_fonts'   => esc_html__( 'No matching fonts', 'crux' ),
	);

	// Localize the script
	wp_localize_script(
		'customizer-sections',
		'chosenOptions',
		$localize
	);
}

add_action( 'customize_controls_enqueue_scripts', 'crux_customize_controls_enqueue_scripts' );

if ( ! function_exists( 'crux_get_font_property_option_keys' ) ) :
	/**
	 * Return all the option keys for the specified font property.
	 *
	 * @since  2.1.0.
	 *
	 * @param  string $property    The font property to search for.
	 * @return array                  Array of matching font option keys.
	 */
	function crux_get_font_property_option_keys() {
		$font_keys = array( 'body_font', 'header_font' );

		return $font_keys;
	}
endif;
