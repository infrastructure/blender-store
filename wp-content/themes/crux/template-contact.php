<?php
/**
 * Template Name: Contact Form
 *
 */

get_header() ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main"<?php stag_markup_helper( array( 'context' => 'content' ) ); ?>>

			<?php
			while ( have_posts() ) :
				the_post();
				?>

				<?php get_template_part( 'content', 'page' ); ?>

			<?php endwhile; // end of the loop. ?>
			<?php
			if ( class_exists( 'Crux_Assistant' ) ) {
				echo do_shortcode( '[crux_contact_form]' );
			} else {
				esc_html_e( 'Please install Crux Assistant plugin for Contact Form template to work!', 'crux' );
			}
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
