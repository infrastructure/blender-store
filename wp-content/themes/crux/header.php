<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main id="main">.
 *
 * @package StagFramework
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>
<body
<?php
body_class();
stag_markup_helper( array( 'context' => 'body' ) );
?>
>
<?php wp_body_open(); ?>
<div id="page" class="hfeed site">

	<div id="mobile-wrapper" class="mobile-menu">
		<h3><?php _e( 'Navigation', 'crux' ); ?></h3>
		<a href="#" id="advanced_menu_toggle"><i class="fas fa-align-justify"></i></a>
		<a href="#" id="advanced_menu_hide"><i class="fas fa-times"></i></a>
	</div><!-- #mobile-wrapper -->

	<?php if ( has_nav_menu( 'subheader' ) || ( stag_is_woocommerce_active() && is_store_notice_showing() ) ) : ?>
	<header class="subheader">

		<div class="inside">

			<div class="grids">
				<div class="grid-6">
					<nav class="subheader-menu-wrap"<?php stag_markup_helper( array( 'context' => 'nav' ) ); ?>>
						<?php
						if ( has_nav_menu( 'subheader' ) ) {
							wp_nav_menu(
								array(
									'theme_location' => 'subheader',
									'menu_class'     => 'subheader-menu navigation',
									'menu_id'        => 'subheader-menu',
									'container'      => false,
								)
							);
						}
						?>
					</nav>
				</div>
				<div class="grid-6 subheader-alert">
					<?php
					if ( stag_is_woocommerce_active() ) {
						echo woocommerce_demo_store();
					}
					?>
				</div>
			</div>

		</div>

	</header><!-- .inside  -->
	<?php endif; ?>

	<header id="masthead" class="site-header"<?php stag_markup_helper( array( 'context' => 'header' ) ); ?>>

		<div class="inside site-branding">

			<div class="grids">
				<div class="grid-6">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<?php if ( stag_theme_mod( 'title_tagline', 'general_custom_logo' ) != '' ) : ?>
						<img src="<?php echo stag_theme_mod( 'title_tagline', 'general_custom_logo' ); ?>" alt="<?php bloginfo( 'name' ); ?>">
						<?php else : ?>
						<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
						<?php endif; ?>

						<?php
						$description = get_bloginfo( 'description', 'display' );
						if ( $description || is_customize_preview() ) :
							?>
							<p class="site-description"><?php echo esc_html( $description ); ?></p>
						<?php endif; ?>
					</a>
				</div>

				<div class="grid-6 header--right">

					<div class="user-meta-wrap">
						<?php if ( stag_is_woocommerce_active() ) : ?>
						<nav class="woo-login-navigation navigation">
							<ul>
								<?php if ( ! is_user_logged_in() ) : ?>
								<li><a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>"><?php _e( 'Login', 'crux' ); ?></a></li>
								<?php else : ?>
								<li><a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>"><?php _e( 'My Account', 'crux' ); ?></a></li>
								<li><a href="<?php echo wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>"><?php _e( 'Logout', 'crux' ); ?></a></li>
								<?php endif; ?>
							</ul>
						</nav><!-- .woo-login-navigation -->
						<?php endif; ?>

						<?php
							/**
							 * WPML Language Switcher
							 */
							do_action( 'icl_language_selector' );

							/**
							 * Middle header hooks that can be used for plugin and theme extensions
							 *
							 * Currently: WooCommerce Shopping Cart
							 */
							do_action( 'crux_middle_header' );
						?>
					</div><!-- .user-meta-wrap -->

				</div><!-- .header--right -->
			</div>

		</div><!-- .site-branding -->

		<div id="navbar" class="navbar">

			<nav id="site-navigation" class="main-navigation"<?php stag_markup_helper( array( 'context' => 'nav' ) ); ?>>
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'primary',
						'menu_class'     => 'primary-menu',
						'menu_id'        => 'primary-menu',
						'container'      => false,
					)
				);
				?>
			</nav><!-- #site-navigation -->

		</div><!-- #navbar -->

	</header><!-- #masthead -->

	<div class="content-wrapper">
		<?php get_template_part( '_helper-background' ); ?>

		<div id="content" class="site-content">
