/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );

	// Background color
	wp.customize( 'style_background_color', function( value ) {
		value.bind( function( to ) {
			$( 'body, .site-content' ).css( 'background-color', to );
		} );
	} );

	// Accent color
	wp.customize( 'style_accent_color', function( value ) {
		value.bind( function( to ) {
			$( '.main-navigation a:hover, .mobile-menu a:hover, .mobile-menu .current-menu-item > a, .main-navigation .current-menu-item > a, .archive-lists a:hover, .woo-login-navigation a, .product-title a, .thumbnail-container a, .widget a, .order_details strong, .product-title a, .product-category h3 mark' ).css( 'color', to);
			$( 'input[type="submit"], button, .button, .cart_dropdown_link .count, .tagcloud a:hover, .ls-crux .ls-bar-timer, .ls-crux .ls-nav-prev:hover, .ls-crux .ls-nav-next:hover, .ls-crux .ls-nav-stop-active, .ls-crux .ls-nav-start-active, .woocommerce-message, .widget_price_filter .ui-slider-horizontal .ui-slider-range, .widget_price_filter .ui-slider-handle' ).css( 'background-color', to );
			$( '.cart_dropdown .dropdown_widget:before' ).css( 'border-top-color', to);
			$( '.cart_dropdown_link .count' ).css( 'border-color', to);
			$( '.product-buttons-inner a').css( 'color', '#fff' );
			$( '.woocommerce-breadcrumb a').css( 'color', to );
		} );
	} );

	// Footer Copyright
	wp.customize( 'general_footer_text', function( value ) {
		value.bind( function( to ) {
			$( '.site-info .inside' ).html( to );
		} );
	} );

	// Static Content Section
	wp.customize( 'static_page_background_color', function( value ) {
		value.bind( function( to ) {
			$( '.global-static-content' ).css( 'background-color', to );
		} );
	} );

	wp.customize( 'static_page_text_color', function( value ) {
		value.bind( function( to ) {
			$( '.global-static-content, .global-static-content h1, .global-static-content h2, .global-static-content h3, .global-static-content h4, .global-static-content h5, .global-static-content h6,  .global-static-content p' ).css( 'color', to );
		} );
	} );

	wp.customize( 'static_page_link_color', function( value ) {
		value.bind( function( to ) {
			$( '.global-static-content a' ).css( 'color', to );
		} );
	} );

	wp.customize( 'static_page_background_opacity', function( value ) {
		value.bind( function( to ) {
			$('.static-content-cover').css('opacity', to / 100 );
		} );
	} );

} )( jQuery );
