#!/usr/bin/env python3

# ====================== BEGIN GPL LICENSE BLOCK ======================
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ======================= END GPL LICENSE BLOCK ========================

"""
Find mismatches between auto/manual renewal in variation and subscription metadata.

To run in the Store, prepare a venv by running the following:

python3 -m venv temp-venv
cd temp-venv
. ./bin/activate
pip install mysqlclient

Then update this file with the correct database credentials and run:

python3 find_mismatches.py

If the run is OK, run this to actually commit the changes to the database:

python3 find_mismatches.py --fix
"""

# This script performs the following steps:
#
# - Find subscriptions with ACTIVE status (or status that could lead to it becoming ACTIVE)
# - Check their `_requires_manual_renewal` field. This is what determines
#   whether WooCommerce Subscriptions performs automatic renewals or not, so
#   this is seen as the ground truth.
# - For the subscription itself, and its latest order:
#    - Check the product variation ID. This is the variation that was initially
#      bought by the user. This isn't updated when, for example, the
#      subscription is forced to MANUAL when paying by bank transfer.
#    - Check the `pa_renewal-type` field on the order item. This is what is
#      shown in the admin. It is originally set from the product variation, but
#      can be changed when the renewal method changes between AUTO and MANUAL.
#    - When there is a mismatch between the `_requires_manual_renewal` and the
#      info from the above two points, the order item is updated to reflect the
#      `_requires_manual_renewal` field.
#
# NOTE: this script does NOT influence what subscribers have to pay. Upon
# purchase the price is copied from the product variation into the subscription,
# and this price is left untouched.
#
# NOTE: this script may have to be run a few times to fix everything. I (Sybren)
# am not sure yet as to why, though.

# Relevant fields in the database:
#   Subscription:
#     wp_postmeta._requires_manual_renewal      true/false
#   Subscription and last order:
#     postmeta._wcs_requires_manual_renewal  true/false
#     wp_woocommerce_order_itemmeta.pa_renewal-type   manual/automatic
#     wp_woocommerce_order_itemmeta._product_id       14
#     wp_woocommerce_order_itemmeta._variation_id     19211 or others, see below

import argparse
import dataclasses
import datetime
import json
import textwrap
import re
from pathlib import Path
from typing import Any, Dict, Iterable, List, Optional, Tuple

import MySQLdb
import MySQLdb.connections

# ANSI colour codes
RED = 91
GREEN = 92
YELLOW = 93
MAGENTA = 95
CYAN = 96
WHITE = 97

# Some cleanup regexps for our log file.
ANSI_REMOVE_RE = re.compile("\033.*?m")
SQL_CLEANUP_RE = re.compile("\n\s*")


def success(message: str) -> None:
    print(f"\033[{GREEN}m{message}\033[0m")


def info(message: str) -> None:
    print(message)


def warn(message: str) -> None:
    print(f"\033[{YELLOW}m{message}\033[0m")


def error(message: str) -> None:
    raise SystemExit(f"\033[{RED}m{message}\033[0m")


@dataclasses.dataclass
class Variation:
    class NotLoaded(ValueError):
        pass

    # The order item whose variation information this object represents.
    order_item_id: int = 0

    # The ID of the variation, should be one in VARIATIONS_MANUAL or VARIATIONS_AUTO.
    variation_id: int = -1

    # Renewal type as per the 'pa_renewal-type' metadata field.
    renewal_type: str = ""

    # Whether this variation is found in VARIATIONS_MANUAL or VARIATIONS_AUTO.
    @property
    def is_manual(self) -> bool:
        if self.variation_id == -1:
            raise self.NotLoaded(f"Variation was not loaded: {self}")
        if self.variation_id == 0:
            # These are likely the old "three months free, then automatic monthly" type.
            return False

        if self.variation_id in VARIATIONS_MANUAL:
            return True
        if self.variation_id in VARIATIONS_AUTO:
            return False

        # Unknown variation, just assume it's automatic for now.
        warn(f"Variation {self.variation_id} is unknown")
        return False

    def check_consistency(self) -> str:
        """Return '' for consistent, and description for inconsistent."""

        if self.variation_id == 0:
            # Unknown variation, cannot check consistency. They seem to work, so don't bother complaing.
            return ""

        expect_renewal_type = {
            False: "automatic",
            True: "manual",
        }[self.is_manual]

        # Product code 4164 used title case, product code 14 uses lower case.
        actual_renewal_type = self.renewal_type.lower()

        if expect_renewal_type != actual_renewal_type:
            return f"variation {self.variation_id}: manual={self.is_manual} but renewal type={self.renewal_type!r}"
        return ""

    def correct_variation_id(self, require_manual_renewal: bool) -> int:
        """Return the correct variation ID for the given manual renewal mode.

        This translates between auto & manual renewal while keeping the payment
        frequency the same. The only variation that's missing is
        manual/half-yearly.
        """

        if self.variation_id == 0:
            raise ValueError(f"Variation {self.variation_id} cannot be corrected")

        if self.is_manual:
            auto_var_id = VARIATIONS_MANUAL[self.variation_id]
            return auto_var_id

        manual_var_id = VARIATIONS_AUTO[self.variation_id]
        if manual_var_id == -1:
            raise ValueError(f"Variation {self.variation_id} cannot be made manual")
        return manual_var_id


@dataclasses.dataclass
class Subscription:
    """Data of subscriptions that needs checking."""

    class Incomplete(ValueError):
        """Raised when the subscription data is incomplete and cannot be processed.

        This can happen when the subscription is currently in the process of being created.
        """

    subs_id: int
    parent_order_id: int
    post_date: datetime.datetime

    subs_requires_manual_renewal: Optional[bool] = None
    """From the subscription meta field '_requires_manual_renewal'"""

    subs_variation: Variation = dataclasses.field(default_factory=Variation)
    last_order_id: Optional[int] = None
    last_order_variation: Variation = dataclasses.field(default_factory=Variation)

    def check_consistency(self) -> str:
        """Return '' for consistent, and description for inconsistent."""
        messages = []

        def addmsg(color: int, message: str) -> None:
            if not message:
                return
            messages.append(f"\033[{color}m{message}\033[0m")

        # Check consistency of the subscription with the variations:
        try:
            subs_var_is_manual = self.subs_variation.is_manual
        except Variation.NotLoaded:
            raise self.Incomplete(
                f"subscription {self.subs_id} does not have a subscription-level variation"
            )

        if self.subs_variation.variation_id != self.last_order_variation.variation_id:
            addmsg(
                RED,
                f"Subs variation {self.subs_variation.variation_id} != last order variation {self.last_order_variation.variation_id}",
            )

        if self.subs_requires_manual_renewal != subs_var_is_manual:
            addmsg(
                MAGENTA,
                f"Subs req_manual={self.subs_requires_manual_renewal} but "
                f"subs variation ({self.subs_variation.variation_id}) manual={self.subs_variation.is_manual}",
            )

        try:
            last_order_is_manual = self.last_order_variation.is_manual
        except Variation.NotLoaded:
            raise self.Incomplete(
                f"subscription {self.subs_id} does not have a last-order-level variation (last order={self.last_order_id})"
            )

        if self.subs_requires_manual_renewal != last_order_is_manual:
            addmsg(
                MAGENTA,
                f"Subs req_manual={self.subs_requires_manual_renewal} but "
                f"order variation ({self.last_order_variation.variation_id}) manual={last_order_is_manual}",
            )

        # Check consistency between the variations of the subs & last order:
        if subs_var_is_manual != last_order_is_manual:
            addmsg(
                CYAN,
                f"\033[96mSubs variation manual={subs_var_is_manual} but order variation manual={last_order_is_manual}\033[0m",
            )

        # Check consistency within the variations of the subs & last order:
        addmsg(WHITE, self.subs_variation.check_consistency())
        addmsg(WHITE, self.last_order_variation.check_consistency())

        return "; ".join(messages)

    def __lt__(self, other: "Subscription") -> bool:
        return self.subs_id < other.subs_id


Subscriptions = Dict[int, Subscription]


@dataclasses.dataclass
class LogEntry:
    """For our own JSON log file."""

    subscription_id: int
    inconsistencies: List[str]
    queries: List[Tuple[str, Iterable[Any]]]
    was_skipped: bool


# These map to their counterpart, so 4543 (auto/monthly) maps to 155956
# (manual/monthly) and vice versa. The only variation that's missing is
# manual/half-yearly.
VARIATIONS_MANUAL = {
    155956: 4543,  # monthly
    155962: 19208,  # quarterly
    155963: 19211,  # yearly
}
VARIATIONS_AUTO = {
    4543: 155956,  # monthly
    19208: 155962,  # quarterly
    19210: -1,  # half-yearly
    19211: 155963,  # yearly
}
KNOWN_VARIATION_IDS = set(VARIATIONS_MANUAL) | set(VARIATIONS_AUTO)


def find_subscriptions() -> Tuple[Subscriptions, List[int]]:
    cur = db.cursor()

    # Fetch list of active or activatable subscriptions
    cur.execute(
        """
    SELECT id, post_parent, post_date FROM wp_posts
    WHERE post_type = 'shop_subscription'
    AND post_status in ('wc-active', 'wc-on-hold', 'wc-pending')
    """
    )
    subscriptions = {
        row[0]: Subscription(row[0], row[1], row[2]) for row in cur.fetchall()
    }
    subs_ids = list(subscriptions.keys())
    print(f"Found {len(subscriptions)} active/renewable subscriptions")

    # Collect subscription renewal type
    print("Collect subscription renewal type")
    cur.execute(
        """
        SELECT post_id, meta_value FROM wp_postmeta
        WHERE post_id in %s AND meta_key='_requires_manual_renewal'
        """,
        (subs_ids,),
    )
    for row in cur.fetchall():
        subs = subscriptions[row[0]]
        subs.subs_requires_manual_renewal = row[1].lower() == "true"
    return subscriptions, subs_ids


def find_last_orders(
    subscriptions: Subscriptions, subs_ids: List[int]
) -> Subscriptions:
    print("Find last orders for subscriptions")
    cur = db.cursor()

    # Find the last renewal order for each subscription.
    # This assumes order IDs are incremental (which they are).
    cur.execute(
        """
        SELECT max(post_id), meta_value FROM wp_postmeta
        WHERE meta_key='_subscription_renewal'
        AND meta_value in %s
        GROUP BY meta_value
        """,
        (subs_ids,),
    )
    subs_by_last_order_id = {}
    subs_ids_without_renewal_order = set(subscriptions.keys())
    for row in cur.fetchall():
        order_id, subs_id_string = row
        assert order_id not in subs_by_last_order_id

        subs_id = int(subs_id_string)
        subs = subscriptions[subs_id]
        subs.last_order_id = order_id
        subs_by_last_order_id[order_id] = subs
        subs_ids_without_renewal_order.remove(subs_id)

    # Any missing orders means that these subscriptions haven't been renewed yet.
    # This means the last order is the parent order.
    for subs_id in subs_ids_without_renewal_order:
        subs = subscriptions[subs_id]
        order_id = subs.parent_order_id

        if subs.last_order_id is not None:
            error(
                f"Subscription {subs_id} already has a last order {subs.last_order_id}"
            )
        subs.last_order_id = order_id

        assert order_id not in subs_by_last_order_id
        subs_by_last_order_id[order_id] = subs

    if len(subs_by_last_order_id) != len(subscriptions):
        error(
            f"Found {len(subscriptions)} subscriptions, but {len(subs_by_last_order_id)} last orders"
        )

    return subs_by_last_order_id


def find_renewal_type_for_orders(
    subs_by_order_id: Subscriptions, variation_field_name: str
) -> None:
    order_ids = list(subs_by_order_id.keys())
    cur = db.cursor()
    cur.execute(
        """
        SELECT oi.order_id, oi.order_item_id, oim.meta_key, oim.meta_value
            FROM      wp_woocommerce_order_items oi
            LEFT JOIN wp_woocommerce_order_itemmeta oim
            ON (oi.order_item_id = oim.order_item_id)
        WHERE oi.order_id in %s
        AND oi.order_item_name in ('Blender Cloud Membership', 'Blender Cloud Membership - Renewal')
        AND oi.order_item_type = 'line_item'
        AND oim.meta_key in ('pa_renewal-type', '_product_id', '_variation_id')
        ORDER BY oi.order_id
        """,
        (order_ids,),
    )
    subs_without_variation = []
    subs_unknown_variation = []
    for row in cur.fetchall():
        order_id, order_item_id, meta_key, meta_value = row

        subs: Subscription = subs_by_order_id[order_id]
        var: Variation = getattr(subs, variation_field_name)
        var.order_item_id = order_item_id

        if meta_key == "_product_id":
            # This is the Blender Cloud membership, should always be this value.
            # 14 is the regular Blender Cloud membership product code, but 4164 was used in the past as well.
            assert meta_value in {
                "14",
                "4164",
            }, f"unexpected product code {meta_value!r}"
        elif meta_key == "pa_renewal-type":
            var.renewal_type = meta_value
        elif meta_key == "_variation_id":
            if not meta_value.strip():
                warn(
                    f"Subscription {subs.subs_id} (dated {subs.post_date}) has variation {meta_value!r} in order {order_id} for {variation_field_name}"
                )
                subs_without_variation.append(subs)
                var.variation_id = -1
                continue

            var_id = int(meta_value)
            var.variation_id = var_id

            # Variation ID 0 is an old way of selling the Cloud subscriptions.
            # It seems to work, but is incompatible with the checks in this
            # script. Better to be left alone.
            if var_id != 0 and var_id not in KNOWN_VARIATION_IDS:
                # Unknown variation, just assume it's automatic for now.
                warn(
                    f"Subscription {subs.subs_id} (dated {subs.post_date}) has unknown variation {var_id} in order {order_id}"
                )
                subs_unknown_variation.append(subs)

    if subs_without_variation:
        warn(f"Found {len(subs_without_variation)} subscriptions without variation!")
    if subs_unknown_variation:
        warn(
            f"Found {len(subs_unknown_variation)} subscriptions with unknown variation!"
        )


def find_inconsistent_subscriptions(
    all_subscriptions: Subscriptions,
) -> List[Subscription]:
    inconsistent_subs = []

    for subs in subscriptions.values():
        try:
            msg = subs.check_consistency()
        except Subscription.Incomplete as ex:
            warn(str(ex))
            continue

        if msg:
            inconsistent_subs.append(subs)

    inconsistent_subs.sort()
    return inconsistent_subs


def log_inconsistencies(inconsistent_subs: List[Subscription]) -> None:
    # Black makes these long lines hard to read, but rewrapping also makes it harder.
    # fmt: off
    warn(f"Found {len(inconsistent_subs)} inconsistent subscriptions:")
    print(f"    \033[{MAGENTA}mMAGENTA\033[0m: inconsistency between subscription 'is manual' flag and Product Variation")
    print(f"    \033[{CYAN   }mCYAN\033[0m   : inconsistency between Product Variation of subscription and order")
    print(f"    \033[{WHITE  }mWHITE\033[0m  : inconsistency within Product Variation of subscription or order")
    # fmt: on

    for subs in inconsistent_subs:
        msg = subs.check_consistency()
        print(f"{subs.subs_id:6d}: {msg}")
    warn(f"Found {len(inconsistent_subs)} inconsistent subscriptions")


def fix_inconsistencies(inconsistent_subs: List[Subscription]) -> None:
    cur = db.cursor()

    info(f"Fixing {len(inconsistent_subs)} inconsistencies")
    fixed_sub_ids: List[int] = []
    skipped_sub_ids: List[int] = []

    log: List[LogEntry] = []

    for subs in inconsistent_subs:
        inconsistencies = ANSI_REMOVE_RE.sub("", subs.check_consistency()).split("; ")
        log_entry = LogEntry(subs.subs_id, inconsistencies, [], False)
        log.append(log_entry)

        subs_is_manual = subs.subs_requires_manual_renewal
        assert isinstance(
            subs_is_manual, bool
        ), "by now the renewal type should be known"

        try:
            new_variation_id = subs.subs_variation.correct_variation_id(subs_is_manual)
        except ValueError:
            warn(
                f"Skipping fix of {subs.subs_id:6d} (dated {subs.post_date:%Y-%m-%d}) because "
                f"its variation ({subs.subs_variation.variation_id}) cannot be updated."
            )
            skipped_sub_ids.append(subs.subs_id)
            log_entry.was_skipped = True
            continue

        renewal_type = {
            True: "manual",
            False: "automatic",
        }[subs_is_manual]

        order_item_ids = (
            subs.subs_variation.order_item_id,
            subs.last_order_variation.order_item_id,
        )
        info(
            f"  - subs {subs.subs_id} ({renewal_type}) with order {subs.last_order_id}: "
            f"variation {subs.subs_variation.variation_id} -> {new_variation_id}"
        )

        # Fix variation ID
        sql = """
            UPDATE wp_woocommerce_order_itemmeta
            SET meta_value = %s
            WHERE order_item_id in %s AND meta_key = '_variation_id'
            """.strip()
        args = (str(new_variation_id), order_item_ids)
        log_entry.queries.append((SQL_CLEANUP_RE.sub(" ", sql), args))
        cur.execute(sql, args)

        # Fix renewal type
        sql = """
            UPDATE wp_woocommerce_order_itemmeta
            SET meta_value = %s
            WHERE order_item_id in %s AND meta_key = 'pa_renewal-type'
            """.strip()
        args = (renewal_type, order_item_ids)
        log_entry.queries.append((SQL_CLEANUP_RE.sub(" ", sql), args))
        cur.execute(sql, args)
        fixed_sub_ids.append(subs.subs_id)

    if skipped_sub_ids:
        ids = " ".join(str(subs_id) for subs_id in sorted(skipped_sub_ids))
        lines = "\n".join(f"    {line}" for line in textwrap.wrap(ids))
        warn(f"Skipped {len(skipped_sub_ids)} subscriptions:\n{lines}")
    if fixed_sub_ids:
        ids = " ".join(str(subs_id) for subs_id in sorted(fixed_sub_ids))
        lines = "\n".join(f"    {line}" for line in textwrap.wrap(ids))
        success(f"Fixed {len(fixed_sub_ids)} subscriptions:\n{lines}")
    else:
        error("Unable to fix any subscriptions")

    # Write the queries to a JSON file for prosperity.
    now = datetime.datetime.now()
    json_data = {
        "committed_fixes": cli_args.fix,
        "script_finished_at": now.isoformat(timespec="seconds"),
        "queries": [dataclasses.asdict(log_entry) for log_entry in log],
        "subscription_ids": {
            "fixed": fixed_sub_ids,
            "skipped": skipped_sub_ids,
        },
    }
    json_fname = f"find_mismatches-{now:%Y%m%d-%H%M%S}.json"
    json_path = Path(__file__).resolve().with_name(json_fname)
    with json_path.open("w") as outfile:
        json.dump(json_data, outfile, indent=4)
    success(f"Written log to {json_path}")


def connect_mysql() -> MySQLdb.connections.Connection:
    """Connect to MySQL with credentials from WordPress."""

    php_define_re = re.compile(r"define\(\'(.*?)\', \'(.*?)\'\)")
    wp_config_fields: Dict[str, str] = {}

    def parse_file(php_path: Path) -> None:
        with php_path.open("r") as infile:
            for line in infile:
                match = php_define_re.search(line)
                if not match:
                    continue
                wp_config_fields[match.group(1)] = match.group(2)

    wp_root = Path(__file__).resolve().parent.parent
    parse_file(wp_root / "wp-config.php")
    parse_file(wp_root / "wp-config-local.php")

    db = MySQLdb.connect(
        host=wp_config_fields["DB_HOST"],
        user=wp_config_fields["DB_USER"],
        passwd=wp_config_fields["DB_PASSWORD"],
        db=wp_config_fields["DB_NAME"],
    )
    return db


if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        "-f",
        "--fix",
        action="store_true",
        help="Attempt to fix found issues. Modify the database.",
    )
    cli_args = argparser.parse_args()

    db = connect_mysql()

    subscriptions, subs_ids = find_subscriptions()
    subs_by_last_order_id = find_last_orders(subscriptions, subs_ids)

    print("Finding product variation of subscriptions")
    find_renewal_type_for_orders(subscriptions, "subs_variation")

    print("Finding product variation of last orders")
    find_renewal_type_for_orders(subs_by_last_order_id, "last_order_variation")

    inconsistent_subs = find_inconsistent_subscriptions(subscriptions)
    if not inconsistent_subs:
        success("No inconsistencies found, congratulations!")
        raise SystemExit()

    log_inconsistencies(inconsistent_subs)

    db.begin()
    try:
        fix_inconsistencies(inconsistent_subs)
    except:
        warn("Rolling back database transaction because there was an error")
        db.rollback()
        raise

    if cli_args.fix:
        success("Committing fixes to database")
        db.commit()
    else:
        warn("Rolling back database fixes, run with --fix to actually commit changes")
        db.rollback()
