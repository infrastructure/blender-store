<?php
/** Set up WordPress environment */
$topdir = dirname(__FILE__);
if (!defined('ABSPATH')) {
    require_once("$topdir/wp-load.php");
}

$post_id = $_GET['post'];
$subs = wcs_get_subscription($post_id);
if ($subs === false) wp_die('not a subscription');

$should = bo_is_manual_payment($subs) ? "manual" : "automatic";
$renewal = $subs->get_requires_manual_renewal() ? "manual" : "automatic";
print("Should be: $should<br>\n");
print("According to WOO: $renewal<br>\n");

if ($should != $renewal) {
    print("Fixing...<br>\n");
    bo_subscription_handle_manual_flag($post_id);

    $subs = wcs_get_subscription($post_id);
    $renewal = $subs->get_requires_manual_renewal() ? "manual" : "automatic";
    print("According to WOO: $renewal<br>\n");
} else {
    print("Subscription is OK<br>\n");
}

?>
<a href='/wp-admin/post.php?post=<?php _e($post_id) ?>&action=edit'>Back to the subscription</a>
